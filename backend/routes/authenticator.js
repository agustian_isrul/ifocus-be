module.exports = loginApp => {
    const authenticateService = require('../controller/authenticate');

    var router = require("express").Router();

    router.post("/login", authenticateService.authenticate);

    loginApp.use('/authenticate', router);
}
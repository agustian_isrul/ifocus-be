module.exports = summaryApp => {
    const summaryController = require('../controller/summary');
    const Token = require('../controller/token');

    var router = require("express").Router();
    
    router.post("/save", summaryController.saveSummary);

    summaryApp.use('/summary', Token.checkToken, router);
}
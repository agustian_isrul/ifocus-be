module.exports = customerContributionApp => {
    const customerContributionController = require('../controller/customer-contribution');
    const tokenController = require('../controller/token');

    var router = require("express").Router();
    
    router.post("/search", customerContributionController.searchData);
    router.post("/save", customerContributionController.saveData);
    router.post("/edit", customerContributionController.editData);
    router.post("/delete", customerContributionController.deleteData);

    customerContributionApp.use('/customer-contribution', tokenController.checkToken, router);
}
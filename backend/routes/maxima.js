module.exports = MaximaApp => {
    const maximaController = require('../controller/maxima');
    const Token = require('../controller/token');

    var router = require("express").Router();

    router.post("/maximalist", maximaController.maximalist);
    router.post("/maximalActual", maximaController.actual);
    router.post("/maximaAT", maximaController.at);
    router.post("/tlima", maximaController.tlimas)

    MaximaApp.use('/maximaapi', Token.checkToken, router);
}
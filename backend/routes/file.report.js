module.exports = reportApp => {
    const fileReport = require('../controller/file.report');
    const Token = require('../controller/token');
    const multer = require("multer");
    const storage = multer.memoryStorage();
    const upload = multer({ storage: storage });

    var router = require("express").Router();
    
    // download and upload
    router.post("/downloadtemplate", fileReport.downloadExcel); // category sales or target
    router.post("/sales/template/uploadcontribution", upload.single('files'), fileReport.uploadExcelContribution);
    router.post("/sales/template/uploadregular", upload.single('files'), fileReport.uploadExcelRegular);
    router.post("/sales/template/uploadclinicmap", upload.single('files'), fileReport.uploadClinicMapping);
    reportApp.use('/report', Token.checkToken, router);
}
module.exports = fileUploadApp => {
    const fileUpload = require('../controller/file.upload');
    const Token = require('../controller/token');
    const multer = require("multer");
    const upload = multer();
    // const storage = multer.memoryStorage();
    // const upload = multer({ storage: storage });

    var router = require("express").Router();
    
    router.post("/downloadtemplate", fileUpload.downloadTemplate);
    router.post("/uploaddata", upload.single('files'), fileUpload.uploadDataPendukung);

    fileUploadApp.use('/upload-file', Token.checkToken, router);
}


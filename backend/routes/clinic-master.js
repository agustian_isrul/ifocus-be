module.exports = clinicMasterApp => {
    const clinicMasterController = require('../controller/clinic-master');
    const tokenController = require('../controller/token');

    var router = require("express").Router();
    
    router.post("/search", clinicMasterController.searchData);
    router.post("/save", clinicMasterController.saveData);
    router.post("/approve", clinicMasterController.approveData);
    router.post("/reject", clinicMasterController.rejectData);
    router.post("/cutoff", clinicMasterController.cutOffData);
    router.post("/delete", clinicMasterController.deleteData);

    clinicMasterApp.use('/clinic-master', tokenController.checkToken, router);
}
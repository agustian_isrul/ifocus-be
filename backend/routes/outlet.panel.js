module.exports = outletPanelApp => {
    const outletPanel = require('../controller/outlet.panel');
    const Token = require('../controller/token');

    var router = require("express").Router();
    
    router.post("/search", outletPanel.searchData);
    router.post("/save", outletPanel.saveData);
    router.post("/approve", outletPanel.approveData);
    router.post("/reject", outletPanel.rejectData);
    router.post("/cutoff", outletPanel.cutOffData);

    outletPanelApp.use('/outletpanel', Token.checkToken, router);
}
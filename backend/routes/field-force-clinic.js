module.exports = fieldForceClinicApp => {
    const fieldForceClinicController = require('../controller/field-force-clinic');
    const tokenController = require('../controller/token');

    var router = require("express").Router();
    
    router.post("/search", fieldForceClinicController.searchData);
    router.post("/save", fieldForceClinicController.saveData);
    router.post("/approve", fieldForceClinicController.approveData);
    router.post("/reject", fieldForceClinicController.rejectData);
    router.post("/cutoff", fieldForceClinicController.cutOffData);
    router.post("/delete", fieldForceClinicController.deleteData);

    fieldForceClinicApp.use('/field-force-clinic', tokenController.checkToken, router);
}
module.exports = claimSalesPanelApp => {
    const claimSalesPanelController = require('../controller/claim-sales-panel');
    const tokenController = require('../controller/token');

    var router = require("express").Router();
    
    router.post("/searchsales", claimSalesPanelController.searchSalesData);
    router.post("/save", claimSalesPanelController.saveData);
    router.post("/searchclaim", claimSalesPanelController.searchClaimSales);

    claimSalesPanelApp.use('/claim-sales-panel', tokenController.checkToken, router);
}
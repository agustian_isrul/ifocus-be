module.exports = schemeApp => {
    const schemeController = require('../controller/master.incentive');
    const Token = require('../controller/token');

    var router = require("express").Router();
    
    router.get("/SchemaList", schemeController.searchSchemaList);
    router.get("/FilterList", schemeController.searchFilterList);
    router.post("/OverviewList", schemeController.searchOverviewList);
    router.post("/SubLineList", schemeController.searchSubLineList);
    router.post("/OutletList", schemeController.searchOutletList);
    router.post("/AllTableUpload", schemeController.searchTableUpload);
    router.post("/columninfo", schemeController.searchColumnInfo);

    schemeApp.use('/scheme', Token.checkToken, router);
}
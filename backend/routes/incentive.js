module.exports = schemeConfigApp => {
    const incentiveController = require('../controller/incentive');
    const Token = require('../controller/token');

    var router = require("express").Router();
    
    router.post("/search", incentiveController.searchData);
    router.post("/save", incentiveController.saveData);
    router.post("/edit", incentiveController.editData);
    router.post("/cutoff", incentiveController.cutoffData);
    router.post("/delete", incentiveController.deleteData);
    router.post("/schemaList", incentiveController.schemaListBySchemaConfigId);
    router.post("/detailSchemaList", incentiveController.detailSchemaListByConfigDetailId);
    router.post("/calculate", incentiveController.calculate);

    schemeConfigApp.use('/schemeconfig', Token.checkToken, router);
}
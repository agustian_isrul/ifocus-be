module.exports = customerFieldForceApp => {
    const customerFieldForceController = require('../controller/customer-field-force');
    const tokenController = require('../controller/token');

    var router = require("express").Router();
    
    router.post("/search", customerFieldForceController.searchData);
    router.post("/save", customerFieldForceController.saveData);
    router.post("/edit", customerFieldForceController.editData);
    router.post("/delete", customerFieldForceController.deleteData);

    customerFieldForceApp.use('/customer-field-force', tokenController.checkToken, router);
}
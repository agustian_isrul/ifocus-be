module.exports = dexaApp => {
    const dexaAPI = require('../controller/dexa.api');
    const Token = require('../controller/token');

    var router = require("express").Router();

    router.post("/customeroutlet/list", dexaAPI.findCustomerList);
    router.post("/area/list", dexaAPI.findAreaList);
    router.post("/line/list", dexaAPI.findLiniList);
    router.post("/channel/list", dexaAPI.findChannelList);
    router.post("/fieldforce/list", dexaAPI.findFieldForceList);

    dexaApp.use('/dexaapi', Token.checkToken, router);
}
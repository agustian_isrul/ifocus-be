const Sequelize = require("sequelize"),
      db = require("../../config/databaseSequalize");

const customerRegular = db.define('t10_customer_ff', {
    CustomerFFID: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    CompanyID: {
        type : Sequelize.INTEGER
    },
    PosStructureID: {
        type : Sequelize.INTEGER
    },
    AreaID: {
        type : Sequelize.INTEGER
    },
    AreaCode: {
        type : Sequelize.STRING
    },
    AreaName: {
        type : Sequelize.STRING
    },
    LineID: {
        type : Sequelize.INTEGER
    },
    LineName: {
        type : Sequelize.STRING
    },
    RayonCode: {
        type : Sequelize.STRING
    },
    EmployeeID: {
        type : Sequelize.STRING
    },
    EmployeeName: {
        type : Sequelize.STRING
    },
    EmployeePosition: {
        type : Sequelize.STRING
    },
    EmployeeMRType: {
        type : Sequelize.STRING
    },
    Sitecode: {
        type : Sequelize.INTEGER
    },
    OutletName: {
        type : Sequelize.STRING
    },
    OutletAddress: {
        type : Sequelize.STRING
    },
    StartDate: {
        type : Sequelize.DATE
    },
    EndDate: {
        type : Sequelize.DATE
    },
    UserID: {
        type : Sequelize.INTEGER
    },
    LastUpdate: {
        type : Sequelize.DATE
    },
    ApprovalStatus: {
        type : Sequelize.INTEGER
    },
    isActive: {
        type : Sequelize.INTEGER
    },
    ApprovalDate: {
        type : Sequelize.DATE
    }
},{
	freezeTableName: true,
	timestamps: false
});

module.exports = customerRegular;

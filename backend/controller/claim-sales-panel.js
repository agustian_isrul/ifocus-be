const claimSalesPanelService = require('../services/claim-sales-panel')

exports.searchSalesData = async (request, response) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    bodyData.CompanyID = profileUser.CompanyID;
    bodyData.PosStructureID = profileUser.PosStructureID;
    const resultQuery = await claimSalesPanelService.findSalesBranchList(bodyData);
    response.json(resultQuery);
}

exports.saveData = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    await claimSalesPanelService.saveData(bodyData, profileUser)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}

exports.searchClaimSales = async (request, response) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    const resultQuery = await claimSalesPanelService.findClaimSalesList(bodyData);
    response.json(resultQuery);
}

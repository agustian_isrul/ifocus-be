const clinicMasterService = require('../services/clinic-master')

exports.searchData = async (request, response) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    bodyData.data.CompanyID = profileUser.CompanyID;
    bodyData.data.PosStructureID = profileUser.PosStructureID;
    const resultQuery = await clinicMasterService.findPageData(bodyData.data, bodyData.pageSetting);
    response.json(resultQuery);
}

exports.saveData = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    const tempClinicMasterList = bodyData.customerList.map(tempClinic => {
        return {
            CompanyID: profileUser.CompanyID,
            PosStructureID: profileUser.PosStructureID,
            AreaID: bodyData.area.AreaID,
            AreaCode: bodyData.area.AreaCode,
            AreaName: bodyData.area.AreaName,
            LineID: bodyData.lini.LineID,
            LineName: bodyData.lini.LineName,
            ClinicName: bodyData.ClinicName,
            ClinicAddress: bodyData.ClinicAddress,
            CustomerPanelID: tempClinic.CustomerPanelID,
            StartDate: bodyData.StartDate,
            EndDate: bodyData.EndDate,
            isActive: 0,
            UserID: profileUser.UserID
        };
    });
    await clinicMasterService.saveData(tempClinicMasterList, profileUser)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}

exports.approveData = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    await clinicMasterService.approveData(bodyData, profileUser)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}

exports.rejectData = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    await clinicMasterService.rejectData(bodyData, profileUser)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}

exports.cutOffData = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    await clinicMasterService.cutOffData(bodyData, profileUser)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}

exports.deleteData = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    await clinicMasterService.deleteData(bodyData, profileUser)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}
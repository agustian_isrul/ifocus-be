const summaryService = require('../services/summary')

exports.saveSummary = async (request, response) => {
    const bodyData = request.body;
    const resultQuery = await summaryService.summaryCalculate(bodyData);
    response.send(resultQuery);
}
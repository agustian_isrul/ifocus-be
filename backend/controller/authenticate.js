const httpRequest = require('request');
const jwt = require('jsonwebtoken');
const moment = require("moment-timezone");
const { findSystemParameter } = require('../services/master.incentive');

exports.authenticate = (request, response) => {
    authenticateFromAPI(request.body)
    .then(responseAuthenticate => {
        createResponse(responseAuthenticate, response);
        return;
    })
    .catch(error => {
        response.status(500).json(error);
        return;
    });
}

function authenticateFromAPI(bodyData) {
    return new Promise((resolve, reject) => {
        httpRequest.post({
            url: 'https://ifocusreborn.dexagroup.com/dev/masterdata/api/incentive/auth/login', 
            headers: {
                'Content-Type': 'application/json'
            },
            json: bodyData
        }, function (error, httpResponse, body) {
            if (error) reject(error);
            if (body && body.hasOwnProperty('UserID'))  {
                resolve(body);
            } else {
                reject(body);
            }
        });
    });
}

async function createResponse(responseAuthenticate, response) {
    const token = jwt.sign(responseAuthenticate, 'ifocus-secret-key', {
        // expiresIn: '900s',
        algorithm: 'HS512'
    });
    const tempResponse = {
        profileUser: responseAuthenticate,
        tokenUser: "Bearer " + token
    }
    const tempParameterList = await findSystemParameter();
    for (const tempParameter of tempParameterList) {
        switch (tempParameter.keyname) {
            case 'period_incentive': {
                tempResponse[tempParameter.keyname] = convertPeriod(tempParameter.value);
                break;
            }
            case 'period_sales': {
                tempResponse[tempParameter.keyname] = convertPeriod(tempParameter.value);
                break;
            }
            case 'never_end_date': {
                tempResponse[tempParameter.keyname] = moment(tempParameter.value).format();
                break;
            }
            default: {
                break;
            }
        }
    }
    response.json(tempResponse);
}

function convertPeriod(periodString) {
    const tempPeriod = new Date(periodString.substring(0, 4), new Number(periodString.substring(4, 6)) - 1);
    const tempMonthLong = tempPeriod.toLocaleString('in-ID', { month: 'long' });
    return tempMonthLong + " " + tempPeriod.getFullYear();
}
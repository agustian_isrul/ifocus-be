const jwt = require('jsonwebtoken');

module.exports.checkToken = function (request, response, next) {
  var token = request.headers['authorization'];
  if (token) {
    jwt.verify(token.replace("Bearer ", ""), 'ifocus-secret-key', (error, decode) => {
      if (error) {
        response.status(500).send({
          "status": 500,
          "code": "INVALID TOKEN",
          "message": error.message
        });
      } else {
        decode.exp = Math.floor(Date.now() / 1000) + (900);
        request.profileUser = decode;
        next();
      }
    })
  } else {
    response.status(500).send({
      "status": 500,
      "code": "NO TOKEN PROVIDE",
      "message": "token must be provide in header for endpoint access"
    });
  }
}
const schemeService = require('../services/master.incentive')

exports.searchSchemaList = async (request, response) => {
    const resultQuery = await schemeService.findSchemaList();
    response.send(resultQuery);
}

exports.searchFilterList = async (request, response) => {
    const resultQuery = await schemeService.findFilterList();
    response.send(resultQuery);
}

exports.searchOverviewList = async (request, response) => {
    const profileUser = request.profileUser; 
    const bodyData = request.body;
    const resultQuery = await schemeService.findItemValueList(profileUser.CompanyID, bodyData.LineName);
    response.send(resultQuery);
}

exports.searchSubLineList = async (request, response, next) => {
    const profileUser = request.profileUser;
    const resultQuery = await schemeService.findSubLineList(profileUser.CompanyID)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}

exports.searchOutletList = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    const resultQuery = await schemeService.findOutletList(profileUser.CompanyID, bodyData.area.AreaID, bodyData.lini.LineID)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}

exports.searchTableUpload = async (request, response, next) => {
    await schemeService.findAllTableUpload()
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}

exports.searchColumnInfo = async (request, response, next) => {
    const bodyData = request.body;
    await schemeService.findColumnInfo(bodyData.tableName)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}
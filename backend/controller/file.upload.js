const xlsx = require('xlsx');
const { isDeepStrictEqual } = require("util");
const moment = require("moment-timezone");

const { findColumnInfo, findSystemParameter, insertDataPendukung } = require('../services/master.incentive');

exports.downloadTemplate = async function (request, response, next) {
    try {
        const tempTableUpload = request.body;
        const tempColumnList = await findColumnInfo(tempTableUpload.tableName, 1);
        const columnList = tempColumnList.map(object => {
            let data = {};
            for (const property in object) {
                if (!['LineDesc', 'StartPeriod', 'EndPeriod'].includes(property)) {
                    data[property] = null;
                }
            }
            return data;
        });
        const workbook = xlsx.utils.book_new();
        const worksheet = xlsx.utils.json_to_sheet(columnList);
        xlsx.utils.book_append_sheet(workbook, worksheet, 'Sheet1');
        const buffer = xlsx.write(workbook, { type: 'buffer', bookType: 'xlsx' });
        response.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        response.setHeader('Content-Disposition', 'attachment; filename=' + tempTableUpload.description + '.xlsx');
        response.send(buffer);
    } catch (error) {
        next(error);
    }
}

exports.uploadDataPendukung = async function (request, response, next) {
    try {
        const tempLineDesc = request.body.lini;
        const tempTableName = request.body.tableUpload;

        let tempStartDate = null;
        const tempParameterIncentiveList = await findSystemParameter('period_incentive');
        if (tempParameterIncentiveList && tempParameterIncentiveList.length > 0) {
            tempStartDate = moment.tz(tempParameterIncentiveList[0].value, 'YYYYMM', moment.tz.guess()).subtract(1, 'month');
        }
        const tempEndDateList = await findSystemParameter('never_end_date');
        let tempEndDate = null;
        if (tempEndDateList && tempEndDateList.length > 0) {
            tempEndDate = moment.tz(tempEndDateList[0].value, 'YYYYMMDD', moment.tz.guess());
        }

        const workbooks = xlsx.read(request.file.buffer, {
            type: "buffer"
        });

        const sheetName = workbooks.SheetNames[0];
        const worksheet = workbooks.Sheets[sheetName];
        console.log('worksheet',worksheet);
        let dataExcelList = xlsx.utils.sheet_to_json(worksheet, { raw: false});
        

        const formattedData = dataExcelList.map(row => {
            const formattedDate = moment(row.date_column, 'DD/MM/YYYY').format('YYYY-MM-DD');
            Object.keys(row).forEach(key => {
                if (row[key] === '\N' || row[key] === '\\N' || row[key] === 'NULL') {
                  row[key] = null;
                }
                if (row[key] === null || row[key] === '' ) {
                  row[key] = null;
                }
              });

            return { ...row, date_column: formattedDate };
          });
          
          console.log('formattedData',formattedData);

        // dataExcelList = dataExcelList.map(data => {
        //     const tempDuplicateList = dataExcelList.filter(item => isDeepStrictEqual(item, data));
        //     if (tempDuplicateList && tempDuplicateList.length > 1) {
        //         data.errorMessage = 'terdapat data yang sama, mohon cek kembali dan pastikan data benar';
        //     }
        //     return data;
        // });

        response.status(200).json({
            code: "01",
            message: "Success",
            data: dataExcelList
        });

        const isErrorMessage = dataExcelList.some(item => item.errorMessage);
        if (!isErrorMessage) {
            dataExcelList = dataExcelList.map(item => {
                return {
                    ...item,
                    LineDesc: tempLineDesc,
                    StartPeriod: tempStartDate.format('YYYY-MM-DD'),
                    EndPeriod: tempEndDate.format('YYYY-MM-DD')
                };
            })
            await insertDataPendukung(tempTableName, tempLineDesc, tempStartDate, tempEndDate, dataExcelList);
        }
    } catch (error) {
        next(error);
    }
}
const customerFieldForceService = require('../services/customer-field-force');
const masterIncentiveService = require('../services/master.incentive');

exports.searchData = async (request, response) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    bodyData.data.CompanyID = profileUser.CompanyID;
    bodyData.data.PosStructureID = profileUser.PosStructureID;
    const resultQuery = await customerFieldForceService.findPageData(bodyData.data, bodyData.pageSetting);
    response.json(resultQuery);
}

exports.saveData = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    let tempSubLineList = await masterIncentiveService.findSubLineList(profileUser.CompanyID, bodyData.lini.LineID);
    if (tempSubLineList && tempSubLineList.length > 0) {

    } else {
        tempSubLineList = [bodyData.lini.LineName];
    }
    const bodyDataList = tempSubLineList.map(tempSubLine => {
        return {
            CompanyID: profileUser.CompanyID,
            PosStructureID: profileUser.PosStructureID,
            AreaID: bodyData.area.AreaID,
            AreaCode: bodyData.area.AreaCode,
            AreaName: bodyData.area.AreaName,
            LineID: bodyData.lini.LineID,
            LineName: bodyData.lini.LineName,
            SubLine: tempSubLine.SubLineName,
            RayonCode: bodyData.RayonCode,
            EmployeeID: bodyData.EmployeeID,
            EmployeeName: bodyData.EmployeeName,
            EmployeePosition: bodyData.EmployeePosition,
            EmployeePosition: bodyData.EmployeePosition,
            EmployeeMRType: bodyData.EmployeeMRType,
            Sitecode: bodyData.Sitecode,
            OutletName: bodyData.OutletName,
            OutletAddress: bodyData.OutletAddress,
            StartDate: bodyData.StartDate,
            EndDate: bodyData.EndDate,
            isActive: 0,
            UserID: profileUser.UserID
        };
    });
    await customerFieldForceService.saveData(bodyDataList, profileUser)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}

exports.editData = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    await customerFieldForceService.editData(bodyData, profileUser)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}

exports.deleteData = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    await customerFieldForceService.deleteData(bodyData, profileUser)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}
const moment = require("moment-timezone");
const incentiveSchemeService = require('../services/incentive');
const { calculateDXM } = require('../services/DXM/calculate');
const { calculateFPP } = require('../services/FPP/calculate');

exports.searchData = async (request, response) => {   
    const profileUser = request.profileUser; 
    const bodyData = request.body;
    const resultQuery = await incentiveSchemeService.findPageData(
        bodyData.bodyData, bodyData.pageSetting, profileUser);
    response.send(resultQuery);
}

exports.saveData = async (request, response) => {
    try {
        const profileUser = request.profileUser;
        const bodyData = request.body;
        const today = moment();

        if (bodyData.Status == 3) {
            let tempIncentiveSchema = {
                LineID: bodyData.lini.LineID,
                StartDate: bodyData.StartDate,
                EndDate: bodyData.EndDate,
                Status: bodyData.Status
            }
            let resultQuery = await incentiveSchemeService.findHeaderDataList(tempIncentiveSchema);
            if (resultQuery && resultQuery.length > 0) {
                response.status(500).send({
                    "status": 500,
                    "code": "ERROR",
                    "message": "schema for this period already exist"
                });
                return;
            }
        
            tempIncentiveSchema = {
                LineID: bodyData.lini.LineID,
                StartYear: today.startOf('year').toDate(),
                EndYear: today.endOf('year').toDate(),
                Status: bodyData.Status
            }
            resultQuery = await incentiveSchemeService.findHeaderDataList(tempIncentiveSchema);
            bodyData.Version = bodyData.lini.LineName + " - " + (resultQuery.length + 1) + " - " + today.year();
        }
    
        await incentiveSchemeService.saveData(bodyData, profileUser, today);
        response.send();
    } catch (error) {
        response.status(500).send({
            "status": 500,
            "code": "ERROR",
            "message": error
        });
    }
}

exports.editData = async (request, response, next) => {
    try {
        const profileUser = request.profileUser;
        const bodyData = request.body;
        const today = moment();

        let tempIncentiveSchema = {
            SchemeConfigId: bodyData.SchemeConfigId
        }
        let resultQuery = await incentiveSchemeService.findHeaderDataList(tempIncentiveSchema);
        if (resultQuery && resultQuery.length > 0 && resultQuery[0].Status !== bodyData.Status) {
            if (resultQuery[0].Status == 1) {
                // save from draft
                tempIncentiveSchema = {
                    LineID: bodyData.lini.LineID,
                    StartDate: moment(bodyData.StartDate),
                    EndDate: moment(bodyData.EndDate),
                    Status: '3'
                }
                resultQuery = await incentiveSchemeService.findHeaderDataList(tempIncentiveSchema);
                if (resultQuery && resultQuery.length > 0) {
                    response.status(500).send({
                        "status": 500,
                        "code": "ERROR",
                        "message": "schema for this period already exist"
                    });
                    return;
                }
            
                const tempStartDate = new Date(bodyData.StartDate);
                tempIncentiveSchema = {
                    LineID: bodyData.lini.LineID,
                    StartYear: new Date(tempStartDate.getFullYear(), 0, 1),
                    EndYear: new Date(tempStartDate.getFullYear(), 11, 31),
                    Status: '3'
                }
                resultQuery = await incentiveSchemeService.findHeaderDataList(tempIncentiveSchema);
                bodyData.Version = bodyData.lini.LineName + " - " + (resultQuery.length + 1) + " - " + tempStartDate.getFullYear();
            } else {
                bodyData.Version = null;
                await incentiveSchemeService.saveData(bodyData, profileUser, today);
                response.send();
                return;
            }
        }
        await incentiveSchemeService.editData(bodyData, profileUser, today);
        response.send();
    } catch (error) {
        next(error);
    }
}

exports.cutoffData = async (request, response) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    const today = new Date();
    await incentiveSchemeService.cutoffData(bodyData, profileUser, today)
    .then(resp => {
        response.send();
    })
    .catch(error => {
        next(error);
    });
}

exports.deleteData = async (request, response) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    await incentiveSchemeService.deleteData(bodyData, profileUser)
    .then(resp => {
        response.send();
    })
    .catch(error => {
        next(error);
    });
}

exports.tarikanDataList = async (request, response) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    const resultQuery = await incentiveSchemeService.findTarikanDataList(bodyData.ConfigDetailId);
    response.send(resultQuery);
}

exports.passwordDataList = async (request, response) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    const resultQuery = await incentiveSchemeService.findPasswordDataList(bodyData.ConfigDetailId);
    response.send(resultQuery);
}

exports.parameterDataList = async (request, response) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    const resultQuery = await incentiveSchemeService.findParameterDataList(bodyData.ConfigDetailId);
    response.send(resultQuery);
}

exports.schemaListBySchemaConfigId = async (request, response) => {
    const bodyData = request.body;
    const resultQuery = await incentiveSchemeService.findDataDetailList(bodyData.SchemeConfigId);
    response.send(resultQuery);
}

exports.detailSchemaListByConfigDetailId = async (request, response, next) => {
    try {
        const bodyData = request.body;
        let tarikanList = await incentiveSchemeService.findTarikanDataList(bodyData.ConfigDetailId);
        let parameterList = await incentiveSchemeService.findPasswordDataList(bodyData.ConfigDetailId);
        const incentiveList = await incentiveSchemeService.findParameterDataList(bodyData.ConfigDetailId);
        tarikanList = Object.values(
            tarikanList.reduce((result, currentItem) => {
                const value = currentItem.RecordNo;
                const existing = result[value] || {
                    RecordNo: '',
                    Operator: '',
                    FlagInclude: '',
                    FilterCode: '',
                    FlagGroupHeader: '',
                    FilterValueList: []
                }
                return {
                    ...result,
                    [value] : {
                        ...existing,
                        RecordNo: currentItem.RecordNo,
                        Operator: currentItem.Operator,
                        FlagInclude: currentItem.FlagInclude === 1,
                        FilterCode: currentItem.FilterCode,
                        FlagGroupHeader: currentItem.LineProduct ? true : false,
                        FilterValueList: [...existing['FilterValueList'], {
                            LineProduct: currentItem.LineProduct,
                            ProductID: currentItem.ProductID,
                            FilterValue: currentItem.FilterValue
                        }]
                    }
                }
            }, {})
        );
        tarikanList.filter(tarikanHeader => tarikanHeader.FilterValueList.some(tarikanDetail => tarikanDetail.LineProduct != null))
        .map(tarikan => {
            const tempFilterValue = Object.values(
                tarikan.FilterValueList.reduce((result, currentItem) => {
                    const value = currentItem.LineProduct;
                    const existing = result[value] || {
                        LineProduct: '',
                        itemList: []
                    }
                    return {
                        ...result,
                        [value] : {
                            ...existing,
                            LineProduct: currentItem.LineProduct,
                            itemList: [...existing['itemList'], currentItem]
                        }
                    }
                }, {})
            );
            tarikan.FilterValueList.length = 0;
            tempFilterValue.forEach(tempFilter => {
                tarikan.FilterValueList = tarikan.FilterValueList || [];
                tarikan.FilterValueList.push({
                    LineProduct: tempFilter.LineProduct
                });
                tarikan.FilterValueList = tarikan.FilterValueList.concat(tempFilter.itemList);
            });
        });
        parameterList = parameterList.map(parameter => {
            parameter.itemList = incentiveList.filter(incentive => incentive.RecordNo === parameter.RecordNo) || [];
            return parameter;
        });
        response.json({
            tarikanList,
            parameterList
        });
    } catch (error) {
        next(error);
    }
}

exports.calculate = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    const tempPeriodeFrom = moment(bodyData.PeriodeFrom);
    bodyData.monthFrom = tempPeriodeFrom.month() + 1;
    bodyData.yearFrom = tempPeriodeFrom.year();
    const tempPeriodeTo = moment(bodyData.PeriodeTo);
    bodyData.monthTo = tempPeriodeTo.month() + 1;
    bodyData.yearTo = tempPeriodeTo.year();
    if (bodyData.LineName.includes('ZETA')) {
        bodyData.LineName = 'ZETA';
    }
    const schemaList = await incentiveSchemeService.getHeaderList(bodyData);  
    switch (profileUser.CompanyID) {
        case 2: {
            await calculateFPP(bodyData)
            .then(resultData => {
                response.json({
                    bodyList: resultData,
                    schemaList: schemaList
                });
            })
            .catch(next);
            break;
        }
        default: {
            calculateDXM(bodyData)
            .then(resultData => {
                response.json({
                    bodyList: resultData,
                    schemaList: schemaList
                });
            })
            .catch(next);
            break;
        }
    }
}

const outletPanelService = require('../services/outlet.panel')

exports.searchData = async (request, response) => {   
    const profileUser = request.profileUser; 
    const bodyData = request.body;
    bodyData.outletPanelForm.CompanyID = profileUser.CompanyID;
    bodyData.outletPanelForm.PosStructureID = profileUser.PosStructureID;
    const resultQuery = await outletPanelService.findPageData(bodyData.outletPanelForm, bodyData.pageSetting);
    response.send(resultQuery);
}

exports.saveData = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    const tempNewOutletPanelList = bodyData.customerList.map((item) => {
        return {
            CompanyID: profileUser.CompanyID,
            PosStructureID: profileUser.PosStructureID,
            AreaID: bodyData.area.AreaID,
            AreaCode: bodyData.area.AreaCode,
            AreaName: bodyData.area.AreaName,
            LineID: bodyData.lini.LineID,
            LineName: bodyData.lini.LineName,
            CustomerAreaID: bodyData.customerArea.AreaID,
            CustomerAreaCode: bodyData.customerArea.AreaCode,
            CustomerAreaName: bodyData.customerArea.AreaName,
            ChannelID: bodyData.channel.ChannelID,
            ChannelName: bodyData.channel.ChannelName,
            Sitecode: item.Sitecode,
            OutletName: item.CustName,
            OutletAddress: item.Address,
            StartDate: bodyData.StartDate,
            EndDate: bodyData.EndDate,
            UserID: profileUser.UserID
        }
    });
    await outletPanelService.saveData(tempNewOutletPanelList, profileUser)
    .then(result => {
        response.json(result);
    })
    .catch(next);
}

exports.approveData = async (request, response) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    await outletPanelService.approveData(bodyData, profileUser)
    .then(result => {
        response.json(result);
    })
    .catch(next);
}

exports.rejectData = async (request, response) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    await outletPanelService.rejectData(bodyData, profileUser)
    .then(result => {
        response.json(result);
    })
    .catch(next);
}

exports.cutOffData = async (request, response) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    await outletPanelService.cutOffData(bodyData, profileUser)
    .then(result => {
        response.json(result);
    })
    .catch(next);
}
const fieldForceClinicService = require('../services/field-force-clinic')

exports.searchData = async (request, response) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    bodyData.data.CompanyID = profileUser.CompanyID;
    bodyData.data.PosStructureID = profileUser.PosStructureID;
    const resultQuery = await fieldForceClinicService.findPageData(bodyData.data, bodyData.pageSetting);
    response.json(resultQuery);
}

exports.saveData = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    const tempFFClinicList = bodyData.clinicList.map(tempFFClinic => {
        return {                
            CompanyID: profileUser.CompanyID,
            PosStructureID: profileUser.PosStructureID,
            AreaID: bodyData.area.AreaID,
            AreaCode: bodyData.area.AreaCode,
            AreaName: bodyData.area.AreaName,
            LineID: bodyData.lini.LineID,
            LineName: bodyData.lini.LineName,
            RayonCode: bodyData.RayonCode,
            EmployeeID: bodyData.EmployeeID,
            EmployeeName: bodyData.EmployeeName,
            EmployeePosition: bodyData.EmployeePosition,
            EmployeeMRType: bodyData.EmployeeMRType,
            ClinicID: tempFFClinic.ClinicID,
            StartDate: bodyData.StartDate,
            EndDate: bodyData.EndDate,
            isActive: 0,
            UserID: profileUser.UserID
        };
    });
    await fieldForceClinicService.saveData(tempFFClinicList, profileUser)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}

exports.approveData = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    await fieldForceClinicService.approveData(bodyData, profileUser)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}

exports.rejectData = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    await fieldForceClinicService.rejectData(bodyData, profileUser)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}

exports.cutOffData = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    await fieldForceClinicService.cutOffData(bodyData, profileUser)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}

exports.deleteData = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    await fieldForceClinicService.deleteData(bodyData, profileUser)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}
const Excel = require("exceljs");
const XLSX = require('xlsx');
const path = require('path');
const moment = require("moment-timezone");
const { validationResult } = require('express-validator');
const masterIncentiveService = require('../services/master.incentive');
const customerContributionService = require('../services/customer-contribution');
const customerRegulerService = require('../services/customer-field-force');
// const lodash = require('lodash');
const { isDeepStrictEqual } = require("util");

exports.downloadExcel = async function (req, res) {
    // await check('category').notEmpty().run(req);
    const result = validationResult(req);
    if (!result.isEmpty()) {
        return res.status(400).json({
            code: '02',
            error_validation: result.array(),
            message: 'Validasi Gagal, Parameter anda tidak lengkap',
        });
    }

    try {
        let nameExcel = req.body.Filename;
        let result = await unduhParameterExcel(nameExcel);

        if (result.validasi == true) res.download(`${nameExcel}.xlsx`);
        if (result.validasi == false) res.status(400).json({
            code: '02',
            message: result.notif
        });
    } catch (err) {
        res.status(400).json({
            code: '02',
            message: 'error',
            data: err.message
        });
    }
}

const unduhParameterExcel = async (nameExcel) => {
    let detailExcel = await getFilnameSheetExcel(nameExcel);

    if (Object.keys(detailExcel).length > 0) {
        const newWorkbook = new Excel.Workbook();
        await newWorkbook.xlsx.readFile(detailExcel.filename);
        await newWorkbook.xlsx.writeFile(`${nameExcel}.xlsx`);
        result = {
            validasi: true,
        };
    }
    if (Object.keys(detailExcel).length == 0)
        result = {
            validasi: false,
            notif: "Jenis Excel Tidak Terdaftar",
        };

    return result;
};

const getFilnameSheetExcel = async (nameExcel) => {
    let result = {};
    switch (nameExcel) {
        case "clinic mapping":
            const fileName = "clinic mapping.xlsx";
            result = {
                filename: path.join(__dirname + '../../../excelfile/' + fileName),
                sheetName: "main",
            };
            break;
        case "customer regular":
            const fileName2 = "customer regular.xlsx";
            result = {
                filename: path.join(__dirname + '../../../excelfile/' + fileName2),
                sheetName: "main",
            };
            break;
        case "customer contribution":
            const fileName3 = "customer contribution.xlsx";
            result = {
                filename: path.join(__dirname + '../../../excelfile/' + fileName3),
                sheetName: "main",
            };
            break;
    }

    return result;
}

exports.uploadExcelContribution = async function (request, response, next) {
    try {
        const profileUser = request.profileUser;
        
        const tempPeriodSalesList = await masterIncentiveService.findSystemParameter('period_sales');

        let workbooks = XLSX.read(request.file.buffer, {
            type: "buffer"
        });

        const sheetName = workbooks.SheetNames[0];
        const worksheet = workbooks.Sheets[sheetName];
        const dataExcelList = XLSX.utils.sheet_to_json(worksheet, { raw: false });

        const tempYear = new Number(tempPeriodSalesList[0].value.substring(0, 4)).toString();
        const tempMonth = new Number(tempPeriodSalesList[0].value.substring(4, 6)).toString();
        const tempPeriod = moment(tempPeriodSalesList[0].value, 'YYYYMM');
        const tempStartDate = tempPeriod.date(1).format();
        const tempEndDate = tempPeriod.add(10, 'year').month(11).endOf('month').format();
        const modifiedList = [];

        for (const tempData of dataExcelList) {            
            const tempProperties = {
                CompanyID: profileUser.CompanyID,
                PosStructureID: profileUser.PosStructureID,
                SubLine: tempData['Line Based'],
                RayonCode: tempData.RayonCode,
                Sitecode: tempData.SiteCode,
                Partition: tempData['Portion Sales'],
                StartDate: tempStartDate,
                EndDate: tempEndDate,
                isActive: 0,
                UserID: profileUser.UserID
            }
            const tempDuplicateList = dataExcelList.filter(item => isDeepStrictEqual(item, tempData));
            if (tempDuplicateList && tempDuplicateList.length > 1) {
                tempData.errorMessage = 'terdapat data yang sama, mohon cek kembali dan pastikan data benar';
            }
            if (tempData['Month Target'] !== tempMonth || tempData['Year Target'] !== tempYear) {
                tempData.errorMessage = 'Periode tidak sama periode sales';
            }
            const totalPortion = dataExcelList
            .filter(item => item.Area === tempData.Area && item['Line Based'] === tempData['Line Based'] && 
            item['Line Name'] === tempData['Line Name'] && item.SiteCode === tempData.SiteCode &&
            item['Month Target'] === tempData['Month Target'] && item['Year Target'] === tempData['Year Target'])
            .reduce((accumulator, item) => accumulator + new Number(item['Portion Sales']), 0);
            if (totalPortion !== 100) {
                tempData.errorMessage = 'total portion harus berjumlah 100';
            }
            const tempAreaList = await masterIncentiveService.findArea(profileUser.CompanyID, tempData.Area);
            if (tempAreaList && tempAreaList.length > 0) {
                tempProperties.AreaID = tempAreaList[0].AreaID;
                tempProperties.AreaCode = tempAreaList[0].AreaCode;
                tempProperties.AreaName= tempAreaList[0].AreaName;
            } else {
                tempData.errorMessage = 'Area tidak terdaftar untuk user login ini';
            }
            const tempCustomerAreaList = await masterIncentiveService.findArea(profileUser.CompanyID, tempData.AreaClaim);
            if (tempCustomerAreaList && tempCustomerAreaList.length > 0) {
                tempProperties.CustomerAreaID = tempCustomerAreaList[0].AreaID;
                tempProperties.CustomerAreaCode = tempCustomerAreaList[0].AreaCode;
                tempProperties.CustomerAreaName= tempCustomerAreaList[0].AreaName;
            } else {
                tempData.errorMessage = 'Area Claim tidak terdaftar untuk user login ini';
            }
            const tempLineList = await masterIncentiveService.findLine(profileUser.CompanyID, tempData['Line Name']);
            if (tempLineList && tempLineList.length > 0) {
                tempProperties.LineID = tempLineList[0].LineID;
                tempProperties.LineName = tempLineList[0].LineName;

                const tempSubLineList = await masterIncentiveService.findSubLineList(
                    profileUser.CompanyID, tempProperties.LineID, tempData['Line Based']
                );
                if (tempSubLineList && tempSubLineList.length > 0) {
                    //
                } else {
                    tempData.errorMessage = 'Sub Line tidak terdaftar untuk Line Name ini';
                }
            } else {
                tempData.errorMessage = 'Line Name tidak terdaftar untuk user login ini';
            }
            modifiedList.push(tempProperties);
        };        
        response.status(200).json({
            code: "01",
            message: "Success",
            data: dataExcelList
        });
        const isErrorMessage = dataExcelList.some(item => item.errorMessage);
        if (!isErrorMessage) {
            await customerContributionService.saveData(modifiedList, profileUser);
        }
    } catch (error) {
        next(error);
    }
}

exports.uploadExcelRegular = async function (request, response, next) {
    try {
        const profileUser = request.profileUser;
        
        const tempPeriodSalesList = await masterIncentiveService.findSystemParameter('period_sales');

        let workbooks = XLSX.read(request.file.buffer, {
            type: "buffer"
        });

        const sheetName = workbooks.SheetNames[0];
        const worksheet = workbooks.Sheets[sheetName];
        const dataExcelList = XLSX.utils.sheet_to_json(worksheet, { raw: false });
         
        const tempYear = new Number(tempPeriodSalesList[0].value.substring(0, 4)).toString();
        const tempMonth = new Number(tempPeriodSalesList[0].value.substring(4, 6)).toString();
        const tempPeriod = moment(tempPeriodSalesList[0].value, 'YYYYMM');
        const tempStartDate = tempPeriod.date(1).format();
        const tempEndDate = tempPeriod.add(10, 'year').month(11).endOf('month').format();
        const modifiedList = [];

        for (const tempData of dataExcelList) {            
            const tempProperties = {
                CompanyID: profileUser.CompanyID,
                PosStructureID: profileUser.PosStructureID,
                RayonCode: tempData.RayonCode,
                Sitecode: tempData.SiteCode,
                StartDate: tempStartDate,
                EndDate: tempEndDate,
                isActive: 0,
                UserID: profileUser.UserID
            }
            const tempDuplicateList = dataExcelList.filter(item => isDeepStrictEqual(item, tempData));
            if (tempDuplicateList && tempDuplicateList.length > 1) {
                tempData.errorMessage = 'terdapat data yang sama, mohon cek kembali dan pastikan data benar';
            }
            if (tempData.Month !== tempMonth || tempData.Year !== tempYear) {
                tempData.errorMessage = 'Periode tidak sama periode sales';
            }
            const tempAreaList = await masterIncentiveService.findArea(profileUser.CompanyID, tempData.CustomerArea);
            if (tempAreaList && tempAreaList.length > 0) {
                tempProperties.AreaID = tempAreaList[0].AreaID;
                tempProperties.AreaCode = tempAreaList[0].AreaCode;
                tempProperties.AreaName= tempAreaList[0].AreaName;
            } else {
                tempData.errorMessage = 'Customer Area tidak terdaftar untuk user login ini';
            }
            const tempLineList = await masterIncentiveService.findLine(profileUser.CompanyID, tempData.LineName);
            if (tempLineList && tempLineList.length > 0) {
                tempProperties.LineID = tempLineList[0].LineID;
                tempProperties.LineName = tempLineList[0].LineName;

                const tempSubLineList = await masterIncentiveService.findSubLineList(
                    profileUser.CompanyID, tempProperties.LineID, tempData.Linebased
                );
                if (tempSubLineList && tempSubLineList.length > 0) {
                    tempProperties.SubLine = tempData.Linebased;
                } else {
                    tempData.errorMessage = 'Sub Line tidak terdaftar untuk Line Name ini';
                }
            } else {
                tempData.errorMessage = 'Line Name tidak terdaftar untuk user login ini';
            }
            modifiedList.push(tempProperties);
        };
        response.status(200).json({
            code: "01",
            message: "Success",
            data: dataExcelList
        });
        const isErrorMessage = dataExcelList.some(item => item.errorMessage);
        if (!isErrorMessage) {
            await customerRegulerService.saveData(modifiedList, profileUser);
        }
    } catch (error) {
        next(error);
    }
}

exports.uploadClinicMapping = async function (request, response, next) {
    try {
        const profileUser = request.profileUser;
        
        const tempPeriodSalesList = await masterIncentiveService.findSystemParameter('period_sales');

        let workbooks = XLSX.read(request.file.buffer, {
            type: "buffer"
        });

        const sheetName = workbooks.SheetNames[0];
        const worksheet = workbooks.Sheets[sheetName];
        const dataExcelList = XLSX.utils.sheet_to_json(worksheet, { raw: false });      
        response.status(200).json({
            code: "01",
            message: "Success",
            data: dataExcelList
        });
    } catch (error) {
        next(error);
    }
}
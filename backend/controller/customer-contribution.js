const customerContributionService = require('../services/customer-contribution')

exports.searchData = async (request, response) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    bodyData.data.CompanyID = profileUser.CompanyID;
    bodyData.data.PosStructureID = profileUser.PosStructureID;
    const resultQuery = await customerContributionService.findPageData(bodyData.data, bodyData.pageSetting);
    response.json(resultQuery);
}

exports.saveData = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    const tempEmployeeList = [];
    for (const tempSubLini of bodyData.SubLine) {
        for (const tempEmployee of bodyData.EmployeeList) {
            const tempData = {
                CompanyID: profileUser.CompanyID,
                PosStructureID: profileUser.PosStructureID,
                AreaID: bodyData.area.AreaID,
                AreaCode: bodyData.area.AreaCode,
                AreaName: bodyData.area.AreaName,
                LineID: bodyData.lini.LineID,
                LineName: bodyData.lini.LineName,
                CustomerAreaID: bodyData.customerArea.AreaID,
                CustomerAreaCode: bodyData.customerArea.AreaCode,
                CustomerAreaName: bodyData.customerArea.AreaName,
                SubLine: tempSubLini,
                RayonCode: tempEmployee.RayonCode,
                EmployeeID: tempEmployee.EmployeeID,
                EmployeeName: tempEmployee.EmployeeName,
                EmployeePosition: tempEmployee.EmployeePosition,
                EmployeeMRType: tempEmployee.EmployeeMRType,
                Sitecode: bodyData.Sitecode,
                OutletName: bodyData.OutletName,
                OutletAddress: bodyData.OutletAddress,
                Partition: tempEmployee.Partition,
                StartDate: bodyData.StartDate,
                EndDate: bodyData.EndDate,
                isActive: 0,
                UserID: profileUser.UserID
            };
            tempEmployeeList.push(tempData);
        }
    }
    await customerContributionService.saveData(tempEmployeeList, profileUser)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}

exports.editData = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    await customerContributionService.editData(bodyData, profileUser)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}

exports.deleteData = async (request, response, next) => {
    const profileUser = request.profileUser;
    const bodyData = request.body;
    await customerContributionService.deleteData(bodyData, profileUser)
    .then(resultData => {
        response.json(resultData);
    })
    .catch(next);
}
const httpRequest = require('request');

exports.findCustomerList = async (request, response) => {
    httpRequest.post('https://ifocusreborn.dexagroup.com/dev/masterdata/api/incentive/customeroutlet/list', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        json: request.body
    }, (err, data) => {
        if (err)
            response.status(500).send({
                message:
                    err.message || "Some error occurred while processing data"
            });
        else response.status(data.statusCode).send(data.body);
    });
}

exports.findAreaList = async (request, response) => {
    const profileUser = request.profileUser;
    httpRequest.post('https://ifocusreborn.dexagroup.com/dev/masterdata/api/incentive/area/list', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        json: {
            "CompanyID": profileUser.CompanyID,
            "PosStructureID": profileUser.PosStructureID,
            "EmployeePositionID": profileUser.EmployeePositionID
        }
    }, (err, data) => {
        if (err) {
            response.status(500).send({
                message: err.message || "Some error occurred while processing data"
            });
        } else {
            response.status(data.statusCode).send(data.body);
        }
    });
}

exports.findLiniList = async (request, response) => {
    const profileUser = request.profileUser;
    httpRequest.post('https://ifocusreborn.dexagroup.com/dev/masterdata/api/incentive/line/list', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        json: {
            "CompanyID": profileUser.CompanyID,
            "PosStructureID": profileUser.PosStructureID,
            "EmployeePositionID": profileUser.EmployeePositionID,
            "AreaID": profileUser.AreaID
        }
    }, (err, data) => {
        if (err)
            response.status(500).send({
                message:
                    err.message || "Some error occurred while processing data"
            });
        else response.status(data.statusCode).send(data.body);
    });
}

exports.findChannelList = async (request, response) => {
    httpRequest.post('https://ifocusreborn.dexagroup.com/dev/masterdata/api/incentive/customerchannel/list', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        json: {
            "Type": "panel"
        }
    }, (err, data) => {
        if (err)
            response.status(500).send({
                message:
                    err.message || "Some error occurred while processing data"
            });
        else response.status(data.statusCode).send(data.body);
    });
}

exports.findFieldForceList = async (request, response) => {
    const dataList = [
        { RayonCode: 'BGRVIC001', EmployeeID: 'ORG-10001', EmployeeName: 'Jakarta Barat',   EmployeePosition: 'Staff', EmployeeMRType: 'COMBO' },
        { RayonCode: 'BGRVIC002', EmployeeID: 'ORG-10002', EmployeeName: 'Jakarta Pusat',   EmployeePosition: 'Staff', EmployeeMRType: 'COMBO' },
        { RayonCode: 'BGRVIC003', EmployeeID: 'ORG-10003', EmployeeName: 'Jakarta Selatan', EmployeePosition: 'Staff', EmployeeMRType: 'COMBO' },
        { RayonCode: 'BGRVIC004', EmployeeID: 'ORG-10004', EmployeeName: 'Jakarta Timur',   EmployeePosition: 'Staff', EmployeeMRType: 'COMBO' },
        { RayonCode: 'BGRVIC005', EmployeeID: 'ORG-10005', EmployeeName: 'Jakarta Utara',   EmployeePosition: 'Staff', EmployeeMRType: 'COMBO' }
    ];
    response.json(dataList);
}
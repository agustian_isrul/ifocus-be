const maximaSchemeService = require('../services/maximaService');
const controller = {};

controller.tlimas = async (request, response )=> {
    let monthsales = request.body.FilterCode;
    const resultQuery = await maximaSchemeService.tlima(monthsales);
    response.send(resultQuery)
}

controller.actual = async (request, response) => {
    let monthsales = request.body.MonthSales;
    const resultQuery = await maximaSchemeService.findActual(monthsales);
    response.send(resultQuery)
}
    
controller.at =async (request, response) => {
    console.log('--------debuggg')
    const linedesc = request.body.LineDesc;
    const monthsales = request.body.MonthSales;
    const yearsales = request.body.YearSales;
    const monthfrom = request.body.MonthFrom;
    const monthto = request.body.MonthSalesTo;
    const yearfrom = request.body.YearSalesFrom;
    const yearto = request.body.YearSalesFrom;
    const resultQuery = await maximaSchemeService.actualtarget(linedesc, monthsales, yearsales
       ,monthfrom, monthto, yearfrom, yearto
         );
    response.send(resultQuery)
}

controller.maximalist = async (req, res) => {
    console.log('req',req.body);
    try {
        // let actual = await maximaSchemeService.actualData(req);
        // let target = await maximaSchemeService.targetData(req);
        // let employee = await maximaSchemeService.employeeLini();
        let result = await maximaSchemeService.maximaActualDataList(req);
    
        res.status(200).json({
            code: "01",
            message: "Success",
            data: result,

        });

    } catch (err) {
        res.status(400).json({
            code: "02",
            message: "Error",
            data: err.message,
        });

    }
}

module.exports = controller;
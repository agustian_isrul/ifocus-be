#!/usr/bin/env node

'use strict'

const PORT = process.env.PORT || 3310;

const app = require('../backend')

app.listen(PORT, () => {
  console.log(`Server started on port ${ PORT }`)
}).on('error', err => {
  console.log('ERROR: ', err)
})
'use strict'

const express = require('express')
const cors = require("cors");
const moment = require("moment-timezone");

const app = express();

moment.tz.setDefault('Asia/Jakarta');

app.use(cors());
app.disable('x-powered-by');
app.use(express.json({limit: '50MB'}));
app.use(express.urlencoded({extended: true, limit: '50MB'}));

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

require("../backend/routes/authenticator")(app);
require("../backend/routes/dexa.api")(app);
require("../backend/routes/outlet.panel")(app);
require("../backend/routes/master.incentive")(app);
require("../backend/routes/incentive")(app);
require("../backend/routes/file.report")(app);
require("../backend/routes/summary")(app);
require("../backend/routes/maxima")(app);
require("../backend/routes/clinic-master")(app);
require("../backend/routes/field-force-clinic")(app);
require("../backend/routes/customer-field-force")(app);
require("../backend/routes/customer-contribution")(app);
require("../backend/routes/claim-sales-panel")(app);
require("../backend/routes/file-upload")(app);

// app.use(require('./helpers/errorHandler').all)

module.exports = app
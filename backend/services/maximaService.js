const sequelize = require("sequelize");
const db = require("../../config/databaseSequalize");
const moment = require("moment");
const convertRupiah = require('rupiah-format');
const toPercent = require('decimal-to-percent');
const knex = require('../../config/database');

async function tlima(input) {
    return await knex('t05_filter_tarikan_data')
    .where(builder => {
        if (input === 'Employee') {
            builder.whereLike('FilterCode','Employee%');
        } else {
            builder.where('FilterCode', 'not like', 'Employee%');
        }
});
}

async function findActual(MonthSales) {
    return await knex('s01_actual_data').where(
        "MonthSales", MonthSales
    );
}

async function actualtarget (LineDesc, MonthSales, YearSales, monthPeriodeFrom, monthPeriodeTo, yearPeriodeFrom, yearPeriodeTo) {
    console.log("-----service")
    return await knex
    .with('s03',knex
    .raw('Select RayonCode, EmployeeID, EmployeeName, EmployeeSubLineDesc, LineDesc from s03_structure_employee'))
    .where('s03.LineDesc', LineDesc)
    .groupBy('s03.EmployeeName','s03.RayonCode', 's03.EmployeeID')

    .select('s01.EmployeeID', 's03.EmployeeName', 's03.RayonCode', 's03.EmployeeSubLineDesc', 's03.LineDesc','s01.MonthSales',
    knex.raw('SUM(s01.Actual) as Actual'),
    knex.raw('SUM(s02.Target) as Target'),
    knex.raw('Actual/Target*100 as Persentase'))
    .from('s03').join('s01', 's01.EmployeeID', 's03.EmployeeID')

    .with('s01',knex
    .select('EmployeeID', 'SalesValue', 'LineDesc', 'MonthSales', 'YearSales')        
    .sum('SalesValue as Actual')
    .from('s01_actual_data')
    .whereBetween('s01_actual_data.MonthSales', [monthPeriodeFrom, monthPeriodeTo])
    .whereBetween('s01_actual_data.YearSales', [yearPeriodeFrom, yearPeriodeTo])
    // .where(builder => {
    //     builder.select('FilterCode').from('t05_filter_tarikan_data').where('FilterCode', 'not like', 'Employee%')
    // })
    .groupBy('s01_actual_data.EmployeeID'))
    .from('s03').join('s02', 's02.EmployeeID', 's03.EmployeeID')

    .with('s02',knex
    .select('EmployeeID', 'TargetValue', 'LineDesc', 'MonthSales', 'YearSales')        
    .sum('TargetValue as Target')
    .from('s02_Target_data')
    .whereBetween('MonthSales', [monthPeriodeFrom, monthPeriodeTo])
    .whereBetween('YearSales', [yearPeriodeFrom, yearPeriodeTo])
    .groupBy('s02_target_data.EmployeeID'));
}

async function actualData(request) {
    let bodyData = request.body;
    bodyData.monthFrom = moment(bodyData.PeriodeFrom).format("MM");
    bodyData.monthTo = moment(bodyData.PeriodeTo).format("MM");
    bodyData.yearTo = moment(bodyData.PeriodeTo).format("YYYY");

    return data = await db.query(`select * from s01_actual_data s01 where s01.LineDesc in ('OMNI', 'OMEGA') and s01.MonthSales >= ${bodyData.monthFrom} and s01.MonthSales <= ${bodyData.monthTo} and s01.YearSales = "${bodyData.yearTo}"`,{
        type: sequelize.QueryTypes.SELECT
    })
}

async function targetData(request) {
    let bodyData = request.body;
    bodyData.monthFrom = moment(bodyData.PeriodeFrom).format("MM");
    bodyData.monthTo = moment(bodyData.PeriodeTo).format("MM");
    bodyData.yearTo = moment(bodyData.PeriodeTo).format("YYYY");

    return data = await db.query(`select * from s02_target_data s02 where s02.LineDesc in ('OMNI', 'OMEGA') and s02.MonthSales >= ${bodyData.monthFrom} and s02.MonthSales <= ${bodyData.monthTo} and s02.YearSales = "${bodyData.yearTo}"`,{
        type: sequelize.QueryTypes.SELECT
    })
}

async function employeeLini() {
    return data = db.query(`select * from s03_structure_employee s03 where s03.LineDesc in ('OMNI', 'OMEGA')`,{
        type: sequelize.QueryTypes.SELECT
    })
}

async function maximaActualDataList(request) {
    let bodyData = request.body;

    bodyData.PeriodeFrom = moment(bodyData.PeriodeFrom).format("YYYY-MM-DD");
    bodyData.monthFrom = moment(bodyData.PeriodeFrom).format("MM");
    bodyData.yearFrom = moment(bodyData.PeriodeFrom).format("YYYY");
    bodyData.PeriodeTo = moment(bodyData.PeriodeTo).format("YYYY-MM-DD");
    bodyData.monthTo = moment(bodyData.PeriodeTo).format("MM");
    bodyData.yearTo = moment(bodyData.PeriodeTo).format("YYYY");

    // console.log('bodydata aa', bodyData);

    let data = await db.query(`select s01.EmployeeID  ,s03.EmployeeName, s03.EmployeeSubLineDesc,s01.LineDesc , s01.MonthSales , s01.YearSales 
    from s01_actual_data s01
    inner join(select sse.EmployeeID ,sse.EmployeeName ,sse.EmployeeSubLineDesc
    from s03_structure_employee sse where sse.StartDate >= '${bodyData.PeriodeFrom}' and sse.EndDate > '${bodyData.PeriodeTo}' and sse.EmployeeSublineDesc in ('OMNI','OMEGA')) s03 on s01.EmployeeID = s03.EmployeeID
    where s01.LineDesc in ('OMNI','OMEGA') 
    and s01.MonthSales >= ${bodyData.monthTo} and s01.MonthSales <= ${bodyData.monthFrom} and s01.YearSales = "${bodyData.yearTo}" and s03.EmployeeName is not null
    group by s01.EmployeeID order by s03.EmployeeName asc`, {
        type: sequelize.QueryTypes.SELECT
    });


    let hasilpush = [];
    await Promise.all(
        data.map(async (e) => {
            let actualDD = await getSalesActualValue(e.EmployeeID,bodyData.monthFrom,bodyData.monthTo,bodyData.yearTo);
            let targetDD = await getSalesTargetValue(e.EmployeeID,bodyData.monthFrom,bodyData.monthTo,bodyData.yearTo);
            let AperT = (actualDD/targetDD)*100;
            let datahasil = {
                employeeID: e.EmployeeID,
                employeeName: e.EmployeeName,
                linedesc: e.LineDesc,
                month: e.MonthSales,
                year: e.YearSales,
                esublinedesc: e.EmployeeSubLineDesc,
                actual: convertRupiah.convert(actualDD),
                target: convertRupiah.convert(targetDD),
                AT: Math.round(AperT)+'%'
            }
            hasilpush.push(datahasil)
        })
    )

    return hasilpush
}

const getSalesActualValue = async(id,mfrom,mto,year) =>  {
    let data = await db.query(`select SUM(SalesValue) as jml from s01_actual_data where EmployeeID = '${id}' and MonthSales >= ${mfrom} and MonthSales <= ${mto} and YearSales = "${year}"` , {
        type: sequelize.QueryTypes.SELECT
    })
    let rupiah = data[0].jml
    return (rupiah) ? rupiah : null
}

async function maximaTargetDataList(request){
    let bodyData = request.body;

    bodyData.PeriodeFrom = moment(bodyData.PeriodeFrom).format("YYYY-MM-DD");
    bodyData.monthFrom = moment(bodyData.PeriodeFrom).format("MM");
    bodyData.yearFrom = moment(bodyData.PeriodeFrom).format("YYYY");
    bodyData.PeriodeTo = moment(bodyData.PeriodeTo).format("YYYY-MM-DD");
    bodyData.monthTo = moment(bodyData.PeriodeTo).format("MM");
    bodyData.yearTo = moment(bodyData.PeriodeTo).format("YYYY");

    // console.log('bodydata aa', bodyData);

    let data = await db.query(`select s02.EmployeeID  ,s03.EmployeeName, s03.EmployeeSubLineDesc,s02.LineDesc , s02.MonthSales , s02.YearSales 
    from s02_target_data s02
    inner join(select sse.EmployeeID ,sse.EmployeeName ,sse.EmployeeSubLineDesc
    from s03_structure_employee sse where sse.StartDate >= '${bodyData.PeriodeFrom}' and sse.EndDate > '${bodyData.PeriodeTo}' and sse.EmployeeSublineDesc in ('OMNI','OMEGA')) s03 on s02.EmployeeID = s03.EmployeeID
    where s02.LineDesc in ('OMNI','OMEGA') 
    and s02.MonthSales >= ${bodyData.monthTo} and s02.MonthSales <= ${bodyData.monthFrom} and s02.YearSales = "${bodyData.yearTo}" and s03.EmployeeName is not null
    group by s02.EmployeeID order by s03.EmployeeName asc`, {
        type: sequelize.QueryTypes.SELECT
    });

    let hasilpush = [];
    await Promise.all(
        data.map(async (e) => {
            let datahasil = {
                employeeID: e.EmployeeID,
                employeeName: e.EmployeeName,
                linedesc: e.LineDesc,
                month: e.MonthSales,
                year: e.YearSales,
                esublinedesc: e.EmployeeSubLineDesc,
                target: await getSalesTargetValue(e.EmployeeID,bodyData.monthFrom,bodyData.monthTo,bodyData.yearTo)
            };
            hasilpush.push(datahasil)
        })
    )

    return hasilpush
}

const getSalesTargetValue = async(id,mfrom,mto,year) =>  {
    let data = await db.query(`select SUM(TargetValue) as jml from s02_target_data where EmployeeID = '${id}' and MonthSales >= ${mfrom} and MonthSales <= ${mto} and YearSales = "${year}"` , {
        type: sequelize.QueryTypes.SELECT
    })
    let rupiah = data[0].jml
    return (rupiah) ? rupiah : null
}
module.exports = {
    tlima,
    findActual,
    actualtarget,
    maximaActualDataList,
    maximaTargetDataList,
    actualData,
    targetData,
    employeeLini
}
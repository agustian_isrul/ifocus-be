const moment = require("moment-timezone");

const knex = require('../../config/database');

function findPageData(incentiveScheme, pageSetting, profileUser) {
    const currentDate = moment();
    return knex.with('table_data', (qb) => {
        qb.select(
            "t03.SchemeConfigId",
            "t03.LineID",
            "t03.LineName",
            "t03.StartDate",
            "t03.EndDate",
            "t03.Version",
            "t03.JumlahScheme",
            knex.raw(
                "case when t03.Status = 1 then 1 " +
                "when '"+ currentDate +"' between t03.StartDate and t03.EndDate then 3 " +
                "else 2 " +
                "end as StatusCode"
            ),
            knex.raw(
                "case when t03.Status = 1 then 'Draft' " +
                "when '"+ currentDate +"' between t03.StartDate and t03.EndDate then 'Active' " +
                "else 'Non Active' " +
                "end as StatusName"
            )
        )
        .from('t03_incentive_config as t03')
        .where(function () {
            this.where('CompanyID', profileUser.CompanyID)
            if (incentiveScheme) {
                if (incentiveScheme.SchemeConfigId) {
                    this.where('t03.SchemeConfigId', incentiveScheme.SchemeConfigId);
                }
                if (incentiveScheme.LineID) {
                    this.where('t03.LineID', incentiveScheme.LineID);
                }
                if (incentiveScheme.StatusCode) {
                    this.where('t03.Status', incentiveScheme.StatusCode);
                }
                if (incentiveScheme.StatusName) {
                    if (incentiveScheme.Status === 'Draft') {
                        this.where('t03.Status = 1');
                    }
                    if (incentiveScheme.Status === 'Non Active') {
                        this.where('t03.Status = 2');
                    }
                    if (incentiveScheme.Status === 'Active') {
                        this.where('t03.Status = 3');
                    }
                }
                if (incentiveScheme.StartDate && incentiveScheme.EndDate) {
                    const formatStartDate = moment(incentiveScheme.StartDate).format('YYYY-MM-DD');
                    const formatEndDate = moment(incentiveScheme.EndDate).format('YYYY-MM-DD');
                    this.where('t03.StartDate', '>=', formatStartDate);
                    this.where('t03.EndDate', '<=', formatEndDate);
                }
            }
        })
    })
    .select('*', knex.raw('select count(*) from table_data').wrap('(', ') totalRows'))
    .from('table_data')
    .limit(pageSetting && pageSetting.rows ? pageSetting.rows : 10)
    .offset(pageSetting ? pageSetting.first : 0)
    .orderBy(
        pageSetting && pageSetting.sortField ? pageSetting.sortField : 'SchemeConfigId',
        pageSetting && pageSetting.sortOrder === 1 ? 'asc' : 'desc'
    );
}

async function findHeaderDataList(incentiveScheme) {
    return knex.select(
        "t03.SchemeConfigId",
        "t03.LineID",
        "t03.LineName",
        "t03.StartDate",
        "t03.EndDate",
        "t03.Version",
        "t03.JumlahScheme",
        "t03.Status"
    ).from('t03_incentive_config as t03')
    .where(function () {
        if (incentiveScheme) {
            if (incentiveScheme.SchemeConfigId) {
                this.where('t03.SchemeConfigId', incentiveScheme.SchemeConfigId);
            }
            if (incentiveScheme.NotSchemeConfigId) {
                this.where('t03.SchemeConfigId', '<>', incentiveScheme.NotSchemeConfigId);
            }
            if (incentiveScheme.LineID) {
                this.where('t03.LineID', incentiveScheme.LineID);
            }
            if (incentiveScheme.Status) {
                this.where('t03.Status', incentiveScheme.Status);
            }
            if (incentiveScheme.NotStatus) {
                this.whereNot('t03.Status', incentiveScheme.NotStatus);
            }
            if (incentiveScheme.StartDate && incentiveScheme.EndDate) {
                const formatStartDate = moment(incentiveScheme.StartDate).format('YYYY-MM-DD');
                const formatEndDate = moment(incentiveScheme.EndDate).format('YYYY-MM-DD');
                this.whereRaw('((? between t03.StartDate and t03.EndDate) or (? between t03.StartDate and t03.EndDate))', [formatStartDate, formatEndDate]);
            }
            if (incentiveScheme.StartYear) {
                this.where('t03.StartDate', '>=', incentiveScheme.StartYear);
            }
            if (incentiveScheme.EndYear) {
                this.where('t03.EndDate', '<=', incentiveScheme.EndYear);
            }
        }
    })
}

function saveData(bodyData, profileUser, today) {
    try {
        const SchemeConfig = {
            LineID: bodyData.lini.LineID,
            LineName: bodyData.lini.LineName,
            StartDate: moment(bodyData.StartDate).format('YYYY-MM-DD'),
            EndDate: moment(bodyData.EndDate).format('YYYY-MM-DD'),
            JumlahScheme: bodyData.monthlyList.length + bodyData.quarterList.length,
            Status: bodyData.Status,
            Version: bodyData.Version,
            CompanyID: profileUser.CompanyID
        }
        return knex.transaction(async trx => {
            const SchemeConfigId = await trx('t03_incentive_config').insert(SchemeConfig);
    
            const detailList = bodyData.monthlyList.concat(bodyData.quarterList);
            for (const item of detailList) {
                const tempConfigDetailId = await trx('t04_schema_config').insert({
                    SchemeConfigId: SchemeConfigId,
                    SchemaId: item.SchemaId,
                    SchemaLabel: item.SchemaLabel
                });
                if ('tarikanList' in item) {
                    const tempTarikanList = [];
                    for (const tempTarikan of item.tarikanList) {
                        let tempLineProduct = null;
                        for (const tempFilterValue of tempTarikan.FilterValueList) {
                            if (tempFilterValue && !tempFilterValue.FilterValue) {
                                tempLineProduct = tempFilterValue.LineProduct;
                            } else {
                                const objectTarikan = {
                                    ConfigDetailId: tempConfigDetailId[0],
                                    RecordNo: tempTarikan.RecordNo,
                                    Operator: tempTarikan.Operator,
                                    FlagInclude: tempTarikan.FlagInclude,
                                    FilterCode: tempTarikan.FilterCode,
                                    LineProduct: tempLineProduct,
                                    ProductID: tempTarikan.ProductID,
                                    FilterValue: tempFilterValue.FilterValue
                                }
                                tempTarikanList.push(objectTarikan);
                            }
                        }
                    }
                    await trx('t05_filter_tarikan_data').insert(tempTarikanList);
                }
                if ('parameterList' in item) {
                    const tempParameterList = [];
                    const tempChildParameterList = [];
                    for (const tempParameter of item.parameterList) {
                        const objectParameter = {
                            ConfigDetailId: tempConfigDetailId[0],
                            RecordNo: tempParameter.RecordNo,
                            Operator1: tempParameter.Operator1,
                            TargetType: tempParameter.TargetType,
                            Operator2: tempParameter.Operator2,
                            ValueFrom: tempParameter.ValueFrom,
                            ValueTo: tempParameter.ValueTo
                        }
                        tempParameterList.push(objectParameter);
                        for (const tempChildParameter of tempParameter.itemList) {
                            const objectChildParameter = {
                                ConfigDetailId: tempConfigDetailId[0],
                                RecordNo: tempParameter.RecordNo,
                                TargetType: tempChildParameter.TargetType,
                                Operator1: tempChildParameter.Operator1,
                                Operator2: tempChildParameter.Operator2,
                                ValueFrom: tempChildParameter.ValueFrom,
                                ValueTo: tempChildParameter.ValueTo,
                                Bonus: tempChildParameter.Bonus
                            }
                            tempChildParameterList.push(objectChildParameter);
                        }
                    }
                    await trx('t06_bonus_config').insert(tempParameterList);
                    await trx('t07_bonus_detail').insert(tempChildParameterList);
                }
            }
    
            const tableHistory = {
                table_name: 't03_incentive_config',
                table_action: 1,
                table_data: JSON.stringify(bodyData),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        })
        .catch(function(error) {
            throw error;
        });
    } catch (error) {
        throw error;
    }
}

async function editData(bodyData, profileUser, today) {
    try {
        await knex.transaction(async trx => {
            const detailT04DBList = await trx('t04_schema_config').where('SchemeConfigId', bodyData.SchemeConfigId)
            .orderBy('ConfigDetailId');
            const detailT04List = bodyData.monthlyList.concat(bodyData.quarterList);
            // get T04 record to be deleted
            for (const detailT04DB of detailT04DBList) {
                let isExistingRecord = false;
                for (const detailT04 of detailT04List) {
                    if (detailT04.ConfigDetailId && detailT04DB.ConfigDetailId == detailT04.ConfigDetailId) {
                        isExistingRecord = true;
                        break;
                    }
                }
                if (isExistingRecord == false) {
                    await trx('t04_schema_config').where('ConfigDetailId', detailT04DB.ConfigDetailId).del();
                }
            }
    
            await trx('t03_incentive_config')
            .where('SchemeConfigId', bodyData.SchemeConfigId)
            .update({
                LineID: bodyData.lini.LineID,
                LineName: bodyData.lini.LineName,
                StartDate: new Date(bodyData.StartDate),
                EndDate: new Date(bodyData.EndDate),
                JumlahScheme: bodyData.monthlyList.length + bodyData.quarterList.length,
                Status:bodyData.Status,
                Version: bodyData.Version
            });
    
            for (const item of detailT04List) {
                let tempConfigDetailId = [];
                if ('ConfigDetailId' in item) {
                    await trx('t04_schema_config')
                    .where('ConfigDetailId', item.ConfigDetailId)
                    .update({
                        SchemaId: item.SchemaId,
                        SchemaLabel: item.SchemaLabel
                    });
                    tempConfigDetailId.push(item.ConfigDetailId);
                } else {
                    tempConfigDetailId = await trx('t04_schema_config')
                    .insert({
                        SchemeConfigId: bodyData.SchemeConfigId,
                        SchemaId: item.SchemaId,
                        SchemaLabel: item.SchemaLabel
                    });
                }
                if ('tarikanList' in item) {
                    if (item.ConfigDetailId) {
                        await trx('t05_filter_tarikan_data').where('ConfigDetailId', item.ConfigDetailId).del();
                    }
                    const tempTarikanList = [];
                    for (const tempTarikan of item.tarikanList) {
                        let tempLineProduct = null;
                        for (const tempFilterValue of tempTarikan.FilterValueList) {
                            if (tempFilterValue && !tempFilterValue.FilterValue) {
                                tempLineProduct = tempFilterValue.LineProduct;
                            } else {
                                const objectTarikan = {
                                    ConfigDetailId: tempConfigDetailId[0],
                                    RecordNo: tempTarikan.RecordNo,
                                    Operator: tempTarikan.Operator,
                                    FlagInclude: tempTarikan.FlagInclude,
                                    FilterCode: tempTarikan.FilterCode,
                                    LineProduct: tempLineProduct,
                                    ProductID: tempTarikan.ProductID,
                                    FilterValue: tempFilterValue.FilterValue
                                }
                                tempTarikanList.push(objectTarikan);
                            }
                        }
                    }
                    await trx('t05_filter_tarikan_data').insert(tempTarikanList);
                }
                if ('parameterList' in item) {
                    if (item.ConfigDetailId) {
                        await trx('t06_bonus_config').where('ConfigDetailId', item.ConfigDetailId).del();
                        await trx('t07_bonus_detail').where('ConfigDetailId', item.ConfigDetailId).del();
                    }
                    const tempParameterList = [];
                    const tempChildParameterList = [];
                    for (const tempParameter of item.parameterList) {
                        const objectParameter = {
                            ConfigDetailId: tempConfigDetailId[0],
                            RecordNo: tempParameter.RecordNo,
                            Operator1: tempParameter.Operator1,
                            TargetType: tempParameter.TargetType,
                            Operator2: tempParameter.Operator2,
                            ValueFrom: tempParameter.ValueFrom,
                            ValueTo: tempParameter.ValueTo
                        }
                        tempParameterList.push(objectParameter);
                        for (const tempChildParameter of tempParameter.itemList) {
                            const objectChildParameter = {
                                ConfigDetailId: tempConfigDetailId[0],
                                RecordNo: tempParameter.RecordNo,
                                TargetType: tempChildParameter.TargetType,
                                Operator1: tempChildParameter.Operator1,
                                Operator2: tempChildParameter.Operator2,
                                ValueFrom: tempChildParameter.ValueFrom,
                                ValueTo: tempChildParameter.ValueTo,
                                Bonus: tempChildParameter.Bonus
                            }
                            tempChildParameterList.push(objectChildParameter);
                        }
                    }
                    await trx('t06_bonus_config').insert(tempParameterList);
                    await trx('t07_bonus_detail').insert(tempChildParameterList);
                }
            }
    
            const tableHistory = {
                table_name: 't03_incentive_config',
                table_action: 2,
                table_data: JSON.stringify(bodyData),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        })
        .catch(function(error) {
            throw error;
        });
    } catch (error) {
        throw error;
    }
}

async function cutoffData(bodyData, profileUser, today) {
    try {
        await knex.transaction(async trx => {
            await trx('t03_incentive_config')
            .where('SchemeConfigId', bodyData.SchemeConfigId)
            .update({
                LineID: bodyData.lini.LineID,
                LineName: bodyData.lini.LineName,
                StartDate: new Date(bodyData.StartDate),
                EndDate: new Date(bodyData.EndDate),
                JumlahScheme: bodyData.monthlyList.length + bodyData.quarterList.length,
                Status:bodyData.Status,
                Version: bodyData.Version
            });

            const tableHistory = {
                table_name: 't03_incentive_config',
                table_action: 3,
                table_data: JSON.stringify(bodyData),
                modified_by: profileUser.Username,
                modified_at: today
            }
            await trx('t01_table_history').insert(tableHistory);
        });

    } catch (error) {
        return error;
    }
}

async function deleteData(bodyData, profileUser) {
    const today = new Date();
    try {
        await knex.transaction(async trx => {                        
            await trx('t04_schema_config')
            .where('SchemeConfigId',bodyData.SchemeConfigId)
            .del();
            
            await trx('t03_incentive_config')
                .where('SchemeConfigId', bodyData.SchemeConfigId)
                .del();
            
            const tableHistory = {
                table_name: 't03_incentive_config',
                table_action: 6,
                table_data: JSON.stringify(bodyData),
                modified_by: profileUser.Username,
                modified_at: new Date()
            }
            await trx('t01_table_history').insert(tableHistory);
        });
    } catch (error) {
        return error;
    }
}

async function findDataDetailList(SchemeConfigId) {
    return await knex
    .select(
        't04.ConfigDetailId', 
        't04.SchemaId',
        't04.SchemaLabel',
        'm01.SchemeType'
    )
    .from('t04_schema_config as t04')
    .innerJoin('m01_schema as m01', 'm01.SchemaId', 't04.SchemaId')
    .where('t04.SchemeConfigId', SchemeConfigId)
    .orderBy('t04.ConfigDetailId');
}

async function findTarikanDataList(ConfigDetailId) {
    return await knex
    .select(
        't05.ConfigDetailId',
        't05.RecordNo',
        't05.Operator',
        't05.FlagInclude',
        't05.FilterCode',
        't05.LineProduct',
        't05.ProductID',
        't05.FilterValue'
    )
    .from('t05_filter_tarikan_data as t05')
    .where('t05.ConfigDetailId', ConfigDetailId);
}

async function findTarikanDataListBySchemeConfigId(SchemeConfigId) {
    return await knex.select(
        't05.ConfigDetailId', 't05.ProductID', 't05.ProductName'
    )
    .from('t04_schema_config as t04')
    .innerJoin('t05_filter_tarikan_data as t05', 't05.ConfigDetailId', 't04.ConfigDetailId')
    .where('t04.SchemeConfigId', SchemeConfigId)
}

async function findPasswordDataList(ConfigDetailId) {
    return await knex('t06_bonus_config')
    .where('ConfigDetailId', ConfigDetailId)
    .orderBy('RecordNo');
}

async function findParameterDataList(ConfigDetailId) {
    return await knex('t07_bonus_detail')
    .where('ConfigDetailId', ConfigDetailId)
    .orderBy('RecordNo');
}

async function findParameterDataListBySchemeConfigId(SchemeConfigId) {
    return await knex.select(
        't07.ConfigDetailId', 't07.BonusLabel', 't07.Operator', 't07.ValueFrom', 't07.ValueTo',
        't07.Bonus', 't07.ValueAT'
    )
    .from('t04_schema_config as t04')
    .innerJoin('t07_bonus_detail as t07', 't07.ConfigDetailId', 't04.ConfigDetailId')
    .where('t04.SchemeConfigId', SchemeConfigId)
}

async function getHeaderList(bodyData) {
    return await knex.select(
        't04.ConfigDetailId', 't04.SchemeConfigId', 't04.SchemaId', 't04.SchemaLabel'
    )
    .from('t04_schema_config as t04')
    .leftJoin('m01_schema as m01', 'm01.SchemaId', 't04.SchemaId')
    .where('t04.SchemeConfigId', bodyData.SchemeConfigId)
    .where('m01.SchemeType', bodyData.SchemeType)
    .orderBy('t04.ConfigDetailId');
}

module.exports = {
    findPageData,
    findHeaderDataList,
    saveData,
    editData,
    cutoffData,
    deleteData,
    findTarikanDataList,
    findPasswordDataList,
    findParameterDataList,
    findTarikanDataListBySchemeConfigId,
    findParameterDataListBySchemeConfigId,
    getHeaderList,
    findDataDetailList
}
const moment = require("moment-timezone");

const knex = require('../../config/database')

function findPageData(outletPanel, pageSetting) {
    return knex.with('table_data', (qb) => {
        qb.select(
            "t02.CustomerPanelID",
            "t02.CompanyID",
            "t02.Sitecode",
            "t02.OutletName",
            "t02.OutletAddress",
            "t02.PosStructureID",
            "t02.AreaID",
            "t02.AreaCode",
            "t02.AreaName",
            "t02.CustomerAreaID",
            "t02.CustomerAreaCode",
            "t02.CustomerAreaName",
            "t02.LineID",
            "t02.LineName",
            "t02.ChannelID",
            "t02.ChannelName",
            "t02.StartDate",
            "t02.EndDate",
            "t02.isActive",
            "t02.UserID",
            "t02.LastUpdate",
            "t02.ApprovalStatus",
            "t02.ApprovalDate",
            knex.raw(
                "case when CURDATE() between t02.StartDate and t02.EndDate and t02.ApprovalDate is not null then 'Active' " +
                "else 'Non Active' end as status"
            )
        )
        .from('t02_outlet_panel as t02')
        .where(function() {
            if (outletPanel) {
                if (outletPanel.CompanyID) {
                    this.where('t02.CompanyID', outletPanel.CompanyID);
                }
                if (outletPanel.PosStructureID) {
                    this.where('t02.PosStructureID', outletPanel.PosStructureID);
                }
                if (outletPanel.area) {
                    this.where('t02.AreaID', outletPanel.area.AreaID);
                }
                if (outletPanel.customerArea) {
                    this.where('t02.CustomerAreaID', outletPanel.customerArea.AreaID);
                }
                if (outletPanel.lini) {
                    this.where('t02.LineID', outletPanel.lini.LineID);
                }
                if (outletPanel.channel) {
                    this.where('t02.ChannelID', outletPanel.channel.ChannelID);
                }
                if (outletPanel.Sitecode) {
                    this.where('t02.Sitecode', outletPanel.Sitecode);
                }
                if (outletPanel.OutletName) {
                    this.whereLike('t02.OutletName', '%' + outletPanel.OutletName.replaceAll(' ', '%') + '%');
                }
                if (outletPanel.StartDate) {
                    const formatStartDate = moment(outletPanel.StartDate).format('YYYY-MM-DD');
                    this.where('t02.StartDate', '>=', formatStartDate);
                }
                if (outletPanel.EndDate) {
                    const formatEndDate = moment(outletPanel.EndDate).format('YYYY-MM-DD');                    
                    this.where('t02.EndDate', '<=', formatEndDate);
                }
                if (outletPanel.status) {
                    const today = moment().format('YYYY-MM-DD');
                    this.where('t02.StartDate', '<=', today)
                    .where('t02.EndDate', '>=', today);
                    if (outletPanel.status === 'Non Active') {
                        this.whereNull('t02.ApprovalDate');
                    }
                    if (outletPanel.status === 'Active') {
                        this.whereNotNull('t02.ApprovalDate');
                    }
                }
            }
        })
        .where('t02.isActive', '<>', 3)
    })
    .select('*', knex.raw('select count(*) from table_data').wrap('(', ') totalRows'))
    .from('table_data')
    .limit(pageSetting && pageSetting.rows > 0 ? pageSetting.rows : 10)
    .offset(pageSetting && pageSetting.first > 0 ? pageSetting.first : 0)
    .orderBy(
        pageSetting && pageSetting.sortField ? pageSetting.sortField : 'CustomerPanelID',
        pageSetting && pageSetting.sortOrder === 1 ? 'asc' : 'desc'
    );
}

function saveData(tempNewOutletPanelList, profileUser) {
    try {
        const today = moment();
        return knex.transaction(async trx => {
            for (const tempNewOutletPanel of tempNewOutletPanelList) {
                const tempNewStartDate = moment(tempNewOutletPanel.StartDate);
                const tempNewEndDate = moment(tempNewOutletPanel.EndDate);
                const tempEditEndDate = Object.create(moment(tempNewOutletPanel.StartDate)).subtract(1, 'd');

                const tempOldOutletPanelList = await trx('t02_outlet_panel')
                .where('AreaID', tempNewOutletPanel.AreaID)
                .where('CustomerAreaID', tempNewOutletPanel.CustomerAreaID)
                .where('LineID', tempNewOutletPanel.LineID)
                .where('Sitecode', tempNewOutletPanel.Sitecode)                
                .whereRaw('(? between StartDate and EndDate)', [tempNewStartDate.format('YYYY-MM-DD')]);

                if (tempOldOutletPanelList && tempOldOutletPanelList.length > 0) {
                    for (const tempOldOutletPanel of tempOldOutletPanelList) {
                        const tempOldStartDate = moment(tempOldOutletPanel.StartDate);

                        if (tempOldStartDate.isSameOrAfter(tempNewStartDate, "date")) {
                            await trx('t02_outlet_panel').where({CustomerPanelID: tempOldOutletPanel.CustomerPanelID}).del();
                        }

                        if (tempOldStartDate.isBefore(tempNewStartDate, "date")) {
                            await trx('t02_outlet_panel')
                            .where('CustomerPanelID', tempOldOutletPanel.CustomerPanelID)
                            .update({
                                EndDate: tempEditEndDate.format('YYYY-MM-DD'),
                                LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                            });
                        }
                    }
                }

                const tempRegulerList = await trx('t10_customer_ff')
                // .where('AreaID', tempNewOutletPanel.CustomerAreaID)
                .where('LineID', tempNewOutletPanel.LineID)
                .where('Sitecode', tempNewOutletPanel.Sitecode)                
                .whereRaw('(? between StartDate and EndDate)', [tempNewStartDate.format('YYYY-MM-DD')]);
                if (tempRegulerList && tempRegulerList.length > 0) {
                    for (const tempReguler of tempRegulerList) {
                        const tempOldStartDate = moment(tempReguler.StartDate);

                        if (tempOldStartDate.isSameOrAfter(tempNewStartDate, "date")) {
                            await trx('t10_customer_ff').where({CustomerFFID: tempReguler.CustomerFFID}).del();
                        }

                        if (tempOldStartDate.isBefore(tempNewStartDate, "date")) {
                            await trx('t10_customer_ff')
                            .where('CustomerFFID', tempReguler.CustomerFFID)
                            .update({
                                EndDate: tempEditEndDate.format('YYYY-MM-DD'),
                                LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                            });
                        }
                    }
                }

                const tempContributionList = await trx('t11_customer_contribution')
                // .where('AreaID', tempNewOutletPanel.AreaID)
                .where('LineID', tempNewOutletPanel.LineID)
                // .where('CustomerAreaID', tempNewOutletPanel.CustomerAreaID)
                .where('Sitecode', tempNewOutletPanel.Sitecode)                
                .whereRaw('(? between StartDate and EndDate)', [tempNewStartDate.format('YYYY-MM-DD')]);
                if (tempContributionList && tempContributionList.length > 0) {
                    for (const tempContribution of tempContributionList) {
                        const tempOldStartDate = moment(tempContribution.StartDate);

                        if (tempOldStartDate.isSameOrAfter(tempNewStartDate, "date")) {
                            await trx('t11_customer_contribution').where({CustomerFFID: tempContribution.CustomerFFID}).del();
                        }

                        if (tempOldStartDate.isBefore(tempNewStartDate, "date")) {
                            await trx('t11_customer_contribution')
                            .where('CustomerFFID', tempContribution.CustomerFFID)
                            .update({
                                EndDate: tempEditEndDate.format('YYYY-MM-DD'),
                                LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                            });
                        }
                    }
                }

                tempNewOutletPanel.StartDate = tempNewStartDate.format('YYYY-MM-DD');
                tempNewOutletPanel.EndDate = tempNewEndDate.format('YYYY-MM-DD');
                tempNewOutletPanel.LastUpdate = today.format('YYYY-MM-DD hh:mm:ss');
                await trx('t02_outlet_panel').insert(tempNewOutletPanel); 
            }

            const tableHistory = {
                table_name: 't02_outlet_panel',
                table_action: 1,
                table_data: JSON.stringify(tempNewOutletPanelList),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        });
    } catch (error) {
        throw error;
    }
}

function approveData(bodyDataList, profileUser) {
    const today = moment();
    try {
        return knex.transaction(async trx => {
            for (const data of bodyDataList) {
                await trx('t02_outlet_panel')
                    .where('CustomerPanelID', data.CustomerPanelID)
                    .update({
                        isActive: 1,
                        ApprovalStatus: profileUser.UserID,
                        ApprovalDate: today.format('YYYY-MM-DD hh:mm:ss'),
                        LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                    });
            };
            const tableHistory = {
                table_name: 't02_outlet_panel',
                table_action: 4,
                table_data: JSON.stringify(bodyDataList),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        });
    } catch (error) {
        return error;
    }
}

function rejectData(bodyDataList, profileUser) {
    const today = moment();
    try {
        return knex.transaction(async trx => {
            for (const data of bodyDataList) {
                await trx('t02_outlet_panel')
                .where('CustomerPanelID', data.CustomerPanelID)
                .update({
                    isActive: 3,
                    ApprovalStatus: profileUser.UserID,
                    ApprovalDate: today.format('YYYY-MM-DD hh:mm:ss'),
                    LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                });
            };
            const tableHistory = {
                table_name: 't02_outlet_panel',
                table_action: 5,
                table_data: JSON.stringify(bodyDataList),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        });
    } catch (error) {
        return error;
    }
}

function cutOffData(bodyData, profileUser) {
    try {
        const today = moment();
        return knex.transaction(async trx => {
            for (const tempData of bodyData) {
                await trx('t02_outlet_panel')
                .where('CustomerPanelID', tempData.CustomerPanelID)
                .update({
                    EndDate: moment(tempData.EndDate).format('YYYY-MM-DD'),
                    LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                });
            }
            const tableHistory = {
                table_name: 't02_outlet_panel',
                table_action: 3,
                table_data: JSON.stringify(bodyData),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        });
    } catch (error) {
        return error;
    }
}

module.exports = {
    findPageData,
    saveData,
    approveData,
    rejectData,
    cutOffData
}
const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveTarikanSalesHarian = (
    schema, resultCalculate, actualSalesHarianList, targetSalesHarianList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');    
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    const tempCategoryProductList = filterDataListBySchema.filter(data => data.FilterCode === 'DaySales');
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;
        
        const tempActualSalesList = actualSalesHarianList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode));
        const tempTargetSalesValue = targetSalesHarianList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
        .reduce((accumulator, target) => {return accumulator + target.target}, 0);

        let tempObjectSchemaList = tempCategoryProductList.map(tempObject => {
            const objectSchema = {                    
                schemaId: schema.SchemaId,
                schemaLabel: schema.SchemaLabel,
                rayonCode: employee.RayonCode,
                workDay: tempObject.FilterValue
            }
            objectSchema.actual = (objectSchema.actual || 0) + tempActualSalesList
            .filter(actual => actual.categoryProduct == tempObject.FilterValue)
            .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
            objectSchema.target = tempTargetSalesValue;
            
            objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
        
            const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
            objectSchema.incentive = bonus;

            return objectSchema;
        });
        
        let totalIncentiveAnd = 0;
        let totalIncentiveOr = 0;
        const clasificationAndList = clasificationList.filter(data => data.Operator1H === 'AND');
        const clasificationOrList = clasificationList.filter(data => data.Operator1H === 'OR');
        for (const tempClasification of clasificationAndList) {
            const tempObjectSchema = tempObjectSchemaList.find(tempObject => tempObject.workDay === tempClasification.ValueFromH);
            if (tempObjectSchema && tempObjectSchema.incentive > 0) {
                totalIncentiveAnd = totalIncentiveAnd + tempObjectSchema.incentive;
            }
        }
        for (const tempClasification of clasificationOrList) {
            const tempObjectSchema = tempObjectSchemaList.find(tempObject => tempObject.workDay === tempClasification.ValueFromH);
            if (tempObjectSchema && tempObjectSchema.incentive > 0 && tempObjectSchema.incentive > totalIncentiveOr) {
                totalIncentiveOr = tempObjectSchema.incentive;
            }
        }
        
        tempObjectSchemaList = tempObjectSchemaList.map(tempObject => {
            tempObject.totalIncentive = totalIncentiveAnd + totalIncentiveOr;
            employee.schemaList.push(tempObject);
            return tempObject;
        });

        employee.totalIncentive = (employee.totalIncentive || 0) + totalIncentiveAnd + totalIncentiveOr;

        return employee;
    });
}

module.exports = {
    incentiveTarikanSalesHarian
}
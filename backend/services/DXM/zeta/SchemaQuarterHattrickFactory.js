const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculatePassword,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveQuarterHattrick = (
    schema, resultCalculate, actualDataList, targetDataList, monthFrom, monthTo,
    filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        let countMonthExceedTarget = 0;
        let tempObjectSchemaList = [];
        for (let i = monthFrom; i <= monthTo; i++) {
            const objectSchema = {
                schemaId: schema.SchemaId,
                schemaLabel: schema.SchemaLabel,
                rayonCode: employee.RayonCode,
                monthSales: i
            }
            
            objectSchema.actual = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode) && 
            actual.MonthSales === i)
            .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
            objectSchema.target = targetDataList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode) && 
            target.MonthSales === i)
            .reduce((accumulator, target) => {return accumulator + target.target}, 0);

            if (employee.EmployeePositionCode !== 'MR') {
                const tempSubordinateList = resultCalculate
                .filter(tempSubordinate => tempSubordinate.RayonCodeList
                    .some(tempRayonCode => employee.RayonCodeFilter.includes(tempRayonCode.RayonCodeSup)));
                for (const tempSubordinate of tempSubordinateList) {
                    const tempSchema = tempSubordinate.schemaList
                    .find(item => item.schemaId === schema.SchemaId && item.monthSales === i);
                    if (tempSchema) {
                        objectSchema.actual = objectSchema.actual + tempSchema.actual;
                        objectSchema.target = objectSchema.target + tempSchema.target;
                    }
                }
            }

            if (objectSchema.target > 0) {
                objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
            } else {
                objectSchema.actualTarget = 0;
            }
            
            const isExistPassword = calculatePassword(employee, objectSchema, preeliminationAndList, preeliminationOrList);
            if (isExistPassword === 1) {
                countMonthExceedTarget = countMonthExceedTarget + 1;
            }
            
            tempObjectSchemaList.push(objectSchema);
        }

        const objectSchema = {
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode,
            actual: employee.actualAll,
            target: employee.targetAll,
            actualTarget: employee.actualTargetAll,
            countProduct: countMonthExceedTarget
        }
    
        const bonus = calculateBonus(employee, objectSchema, [], [], clasificationList);
        tempObjectSchemaList = tempObjectSchemaList.map(tempObjectSchema => {
            tempObjectSchema.countProduct = countMonthExceedTarget;
            tempObjectSchema.incentive = bonus;
            tempObjectSchema.totalIncentive = bonus;

            employee.schemaList.push(tempObjectSchema);
        });

        employee.totalIncentive = (employee.totalIncentive || 0) + bonus;

        return employee;
    });
}

module.exports = {
    incentiveQuarterHattrick
}
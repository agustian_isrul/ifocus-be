const {
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculateBonus,
    validateByTargetType
} = require('../../../helpers/utility');

const incentiveGroupProductFokus = (
    schema, resultCalculate, actualValueList, targetValueList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const actualTargetFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'actualTarget' && !data.RecordNoD);
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    const tempCategoryProductList = filterDataListBySchema
    .filter(data => data.FilterCode === 'ProductDesc' || data.FilterCode === 'ProductGroupDesc');
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;
        
        const actualProductList = actualValueList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode));
        const targetProductList = targetValueList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode));

        let productFokusHit = 0;
        let tempActualTotal = 0;
        let tempTargetTotal = 0;
        let tempObjectSchemaList = [];
        for (let category of tempCategoryProductList) {
            const objectSchema = {                    
                schemaId: schema.SchemaId,
                schemaLabel: schema.SchemaLabel,
                rayonCode: employee.RayonCode,
                productID: category.ProductID,
                categoryProduct: category.FilterValue
            }

            objectSchema.actual = actualProductList.filter(actual => actual.categoryProduct === category.FilterValue)
            .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
            tempActualTotal = tempActualTotal + objectSchema.actual;

            objectSchema.target = targetProductList.filter(target => target.categoryProduct === category.FilterValue)
            .reduce((accumulator, target) => {return accumulator + target.target}, 0);
            tempTargetTotal = tempTargetTotal + objectSchema.target;

            if (objectSchema.target > 0) {
                objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
            } else {
                objectSchema.actualTarget = 0;
            }
            const allowToCount = validateByTargetType(actualTargetFilter.TargetTypeH, employee, objectSchema, 
                actualTargetFilter.Operator2H, actualTargetFilter.ValueFromH, actualTargetFilter.ValueToH);
            if (objectSchema.actualTarget > 0 && allowToCount) {
                productFokusHit = productFokusHit + 1;
            }
            tempObjectSchemaList.push(objectSchema);
        }
        
        const tempObjectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode,
            actual: tempActualTotal,
            target: tempTargetTotal
        }
        tempObjectSchema.countProduct = productFokusHit;
        const bonus = calculateBonus(employee, tempObjectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        tempObjectSchemaList.map(objectSchema => {
            objectSchema.countProduct = productFokusHit;
            objectSchema.incentive = bonus;
            objectSchema.totalIncentive = bonus;
            employee.schemaList.push(objectSchema);
            return objectSchema;
        });
    
        employee.totalIncentive = (employee.totalIncentive || 0) + bonus;
        
        return employee;
    });
}

module.exports = {
    incentiveGroupProductFokus
}
const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculatePassword,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveQuarterGrowth = (
    schema, resultCalculate, actualDataList, targetDataList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        const objectSchema = {
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode
        }
        
        objectSchema.actual = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
        .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
        objectSchema.target = targetDataList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
        .reduce((accumulator, target) => {return accumulator + target.actual}, 0);

        if (employee.EmployeePositionCode !== 'MR') {
            const tempSubordinateList = resultCalculate
            .filter(tempSubordinate => tempSubordinate.RayonCodeList
                .some(tempRayonCode => employee.RayonCodeFilter.includes(tempRayonCode.RayonCodeSup)));
            for (const tempSubordinate of tempSubordinateList) {
                const tempSchema = tempSubordinate.schemaList.find(item => item.schemaId === schema.SchemaId);
                if (tempSchema) {
                    objectSchema.actual = objectSchema.actual + tempSchema.actual;
                    objectSchema.target = objectSchema.target + tempSchema.target;
                }
            }
        }

        objectSchema.actualTarget = convertNaNToZero((objectSchema.actual - objectSchema.target) / objectSchema.target * 100);

        const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = bonus;
        
        employee.totalIncentive = (employee.totalIncentive || 0) + bonus;

        employee.schemaList = employee.schemaList || [];
        employee.schemaList.push(objectSchema);

        return employee;
    });
}

module.exports = {
    incentiveQuarterGrowth
}
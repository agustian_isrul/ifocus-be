const knex = require('../../../config/database');

const getEmployeeDataList = (lineDesc) => {
    return knex.select(
        'RayonCode', 'EmployeeID', 'EmployeeName', 'EmployeePositionCode', 'EmployeeAreaCode', 'AreaBased',
        'EmployeeSubLineDesc', 'EmployeeMRType', 'EmployeeVacant', 'RayonCodeSup', 'LineDesc', 'DummyPimda',
        'FlagAddTotal', 'FlagGrowth', 'FlagTarikanSales'
    )
    .from('s03_structure_employee')
    .where('LineDesc', '=', lineDesc)
    .orderBy('RayonCode')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getSchemaList = (schemeConfigId, schemeType) => {
    return knex.select(
        't04.ConfigDetailId', 't04.SchemeConfigId', 't04.SchemaId', 't04.SchemaLabel'
    )
    .from('t04_schema_config as t04')
    .innerJoin('m01_schema as m01', 'm01.SchemaId', 't04.SchemaId')
    .where('t04.SchemeConfigId', schemeConfigId)
    .where('m01.SchemeType', schemeType)
    .orderBy('t04.ConfigDetailId')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getFilterTarikanDataList = (schemeConfigId, schemeType) => {
    return knex.select(
        't05.ConfigDetailId', 't05.RecordNo', 't05.Operator', 't05.FlagInclude',
        't05.FilterCode', 't05.LineProduct', 't05.ProductID', 't05.FilterValue'
    )
    .from('t04_schema_config as t04')
    .innerJoin('m01_schema as m01', 'm01.SchemaId', 't04.SchemaId')
    .innerJoin('t05_filter_tarikan_data as t05', 't05.ConfigDetailId', 't04.ConfigDetailId')
    .where('t04.SchemeConfigId', schemeConfigId)
    .where('m01.SchemeType', schemeType)
    .orderBy('t05.ConfigDetailId')
    .orderBy('t05.RecordNo')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getParameterBonusList = (schemeConfigId, schemeType) => {
    return knex.select(
        't06.ConfigDetailId',
        't06.Operator2 AS Operator2H', 't06.TargetType AS TargetTypeH', 't06.ValueFrom AS ValueFromH',
        't06.ValueTo AS ValueToH', 't06.Operator1 AS Operator1H', 't06.RecordNo AS RecordNoH',
        't07.Operator2 AS Operator2D', 't07.TargetType AS TargetTypeD', 't07.ValueFrom AS ValueFromD', 
        't07.ValueTo as ValueToD', 't07.Operator1 AS Operator1D', 't07.RecordNo AS RecordNoD',
        't07.Bonus'
    )
    .from('t04_schema_config as t04')
    .innerJoin('m01_schema as m01', 'm01.SchemaId', 't04.SchemaId')
    .innerJoin('t06_bonus_config as t06', 't06.ConfigDetailId', 't04.ConfigDetailId')
    .leftJoin('t07_bonus_detail as t07', function() {
        this.on('t07.ConfigDetailId', 't04.ConfigDetailId')
        .andOn('t07.RecordNo', 't06.RecordNo')
    })
    .where('t04.SchemeConfigId', schemeConfigId)
    .where('m01.SchemeType', schemeType)
    .orderBy('t06.ConfigDetailId')
    .orderBy('t06.RecordNo')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getActualDataList = (bodyData, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        's01.RayonCode', 's01.MonthSales', 's01.YearSales',
        knex.raw(
            "CONCAT(UPPER(MONTHNAME(CONCAT(s01.YearSales, '-', s01.MonthSales, '-01'))), ' ', s01.YearSales) AS MonthPeriod"
        ),
        knex.raw('SUM(s01.SalesValue) AS actual'),
        knex.raw("SUM(s01.SalesQuantity) AS unitActual"),
        knex.raw("SUM(s01.PAP) AS pap"),
        knex.raw("SUM(case when s01.StatusFaktur = 'R' then s01.SalesValue ELSE 0 END) AS actualCategoryR"),
        knex.raw("SUM(case when s01.StatusFaktur IN ('A', 'J') then s01.SalesValue ELSE 0 END) AS actualCategoryJ")
    )
    .from('s01_actual_data AS s01')
    .whereBetween('s01.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s01.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                if (data.FilterValueList[0].FlagInclude === 1) {
                    builder.whereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                } else {
                    builder.whereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    if (data.FilterValueList[0].FlagInclude === 1) {
                        this.orWhereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                    } else {
                        this.orWhereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                    }
                });                 
            });
        }
    })
    .groupBy('s01.RayonCode', 's01.MonthSales', 's01.YearSales')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getTargetDataList = (bodyData, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        's02.RayonCode', 's02.MonthSales', 's02.YearSales',
        knex.raw(
            "CONCAT(UPPER(MONTHNAME(CONCAT(s02.YearSales, '-', s02.MonthSales, '-01'))), ' ', s02.YearSales) AS MonthPeriod"
        ),
        knex.raw('SUM(s02.TargetValue) AS target'),
        knex.raw("SUM(case when s02.StatusFaktur = 'R' then s02.TargetValue ELSE 0 END) AS targetCategoryR"),
        knex.raw("SUM(case when s02.StatusFaktur IN ('A', 'J') then s02.TargetValue ELSE 0 END) AS targetCategoryJ")
    )
    .from('s02_target_data as s02')
    .whereBetween('s02.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s02.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterCode) {
                    case 'DaySales': break;
                    default: {
                        switch (data.FilterValueList[0].FlagInclude) {
                            case 1:
                                builder.whereIn('s02.' + data.FilterCode, data.FilterValueFilter);
                                break;
                            default:
                                builder.whereNotIn('s02.' + data.FilterCode, data.FilterValueFilter);
                                break;
                        }
                        break;
                    }
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterCode) {
                        case 'DaySales': break;
                        default: {
                            switch (data.FilterValueList[0].FlagInclude) {
                                case 1:
                                    this.orWhereIn('s02.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                                default:
                                    this.orWhereNotIn('s02.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                            }
                            break;
                        }
                    }
                });                 
            });
        }
    })
    .groupBy('s02.RayonCode', 's02.MonthSales', 's02.YearSales')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getProductivityDataList = (lineDesc) => {
    return knex.select(
        'EmployeeSubLineDesc', 'EmployeePositionCode', 'EmployeeMRType', 'EmployeeAreaCode', 'RayonCode', 'Productivitas'
    )
    .from('m07_productivitas')
    .where('LineDesc', '=', lineDesc)
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getActualProductList = (bodyData, ConfigDetailId, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        's01.RayonCode',
        't05.LineProduct AS categoryProduct',
        knex.raw('SUM(s01.SalesValue) AS actual')
    )
    .from('s01_actual_data AS s01')
    .innerJoin('t05_filter_tarikan_data as t05', function() {
        this.on('t05.ConfigDetailId', '=', ConfigDetailId)
        .andOn(function() {
            this.on('t05.FilterValue', '=', 's01.ProductGroupDesc')
            .orOn('t05.ProductID', '=', 's01.ProductID')
        })
    })
    .whereBetween('s01.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s01.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterCode) {
                    default: {                
                        switch (data.FilterValueList[0].FlagInclude) {
                            case 1:
                                builder.whereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                break;
                            default:
                                builder.whereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                break;
                        }
                        break;
                    }
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterCode) {
                        default: {
                            switch (data.FilterValueList[0].FlagInclude) {
                                case 1:
                                    this.orWhereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                                default:
                                    this.orWhereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                            }
                            break;
                        }
                    }
                });                 
            });
        }
    })
    .groupBy('s01.RayonCode', 't05.LineProduct')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getTargetProductList = (lineDesc) => {
    return knex
    .select(
        'm08.RayonCode', 'm08.EmployeePositionCode', 'm08.LineProduct AS categoryProduct', 'm08.EmployeeSubLineDesc', 
        'm08.EmployeeMRType', 'm08.ProductID', 'm08.ProductDesc', 'm08.Operator', 'm08.ValueMr', 'm08.TUMValue AS target'
    )
    .from('m08_tum_incentive AS m08')
    .where('m08.LineDesc', '=', lineDesc)
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getActualChannelList = (bodyData, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        's01.RayonCode',
        'm09.ChannelGroup AS channelGroup',
        knex.raw('SUM(s01.SalesValue) AS actual')
    )
    .from('s01_actual_data AS s01')
    .innerJoin('m09_outlet_project AS m09', function() {
        this.on('m09.RayonCode', '=', 's01.RayonCode').on('m09.Site_Code', '=', 's01.SiteCode')
    })
    .whereBetween('s01.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s01.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterValueList[0].FlagInclude) {
                    case 1:
                        builder.whereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                        break;
                    default:
                        builder.whereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                        break;
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterValueList[0].FlagInclude) {
                        case 1:
                            this.orWhereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                            break;
                        default:
                            this.orWhereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                            break;
                    }
                });                 
            });
        }
    })
    .groupBy('s01.RayonCode', 'm09.ChannelGroup')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getTargetChannelList = (lineDesc) => {
    return knex.select(
        'm10.EmployeeAreaCode', 'm10.RayonCode', 'm10.EmployeeSubLineDesc', 
        'm10.EmployeeMRType as channelGroup', 'm10.LineProduct',
        'm10.Sales AS actual', 'm10.MaxTarget AS target'
    )
    .from('m10_target_outlet AS m10')
    .where('m10.LineDesc', '=', lineDesc)
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getPreviousQuarterActualList = (lineDesc, previousQuarterMonth, previousQuarterYear) => {
    return knex.select(
        'LineDesc', 'RayonCode', 'GrowthType', 'MaxSales as target'
    )
    .from('m13_max_sales as m13')
    .where(builder => {
        if (previousQuarterMonth) {
            builder.where('QuarterN', '=', previousQuarterMonth);
        }
        if (previousQuarterYear) {
            builder.where('YearSales', '=', previousQuarterYear);
        }
        if (lineDesc === 'ONCOLOGY') {
            builder.whereIn('LineDesc', ['OMNI', 'OMEGA']);
        } else {
            builder.where('LineDesc', '=', lineDesc);
        }
    })
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getNavyCabangList = (lineDesc, monthPeriodFrom, monthPeriodTo, yearPeriodFrom, yearPeriodTo) => {
    return knex.select(
        'EmployeeAreaCode', 'AreaGabungan', 'SalesValue', 'TargetValue'
    )
    .from('m11_navy_cabang as m11')
    .whereBetween('MonthSales', [monthPeriodFrom, monthPeriodTo])
    .whereBetween('YearSales', [yearPeriodFrom, yearPeriodTo])
    .where('LineDesc', '=', lineDesc)
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getCriteriaCabangList = (lineDesc) => {
    return knex.select(
        'AreaGabungan', 'Kriteria'
    )
    .from('m06_kriteria_cabang as m06')
    .where('m06.LineDesc', '=', lineDesc)
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getActualDataCabangList = (bodyData, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        'm06.AreaGabungan',
        knex.raw("SUM(case when s01.DaySales <= 15 then s01.SalesValue ELSE 0 END) AS actualMiddle"),
        knex.raw("SUM(s01.SalesValue) AS actual")
    )
    .from('m06_kriteria_cabang as m06')
    .innerJoin('m15_area_group AS m15', function() {
        this.on('m15.LineDesc', '=', 'm06.LineDesc')
        .on('m15.AreaGabungan', '=', 'm06.AreaGabungan')
    })
    .innerJoin('s01_actual_data as s01', function() {
        this.on('s01.LineDesc', '=', 'm15.LineDesc')
        .on(function() {
            this.on('s01.RayonCode', '=', 'm15.RayonCode')
            .orOn('s01.SupRayonCode', '=', 'm15.RayonCode')
        })
    })
    .whereBetween('s01.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s01.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterValueList[0].FlagInclude) {
                    case 1:
                        builder.whereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                        break;
                    default:
                        builder.whereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                        break;
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterValueList[0].FlagInclude) {
                        case 1:
                            this.orWhereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                            break;
                        default:
                            this.orWhereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                            break;
                    }
                });                 
            });
        }
    })
    .groupBy('m06.AreaGabungan')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getTargetDataCabangList = (bodyData, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        'm06.AreaGabungan',
        knex.raw("SUM(s02.TargetValue) AS target")
    )
    .from('m06_kriteria_cabang as m06')
    .innerJoin('m15_area_group AS m15', function() {
        this.on('m15.LineDesc', '=', 'm06.LineDesc')
        .on('m15.AreaGabungan', '=', 'm06.AreaGabungan')
    })
    .innerJoin('s02_target_data as s02', function() {
        this.on('s02.LineDesc', '=', 'm15.LineDesc')
        .on(function() {
            this.on('s02.RayonCode', '=', 'm15.RayonCode')
            .orOn('s02.SupRayonCode', '=', 'm15.RayonCode')
        })
    })
    .whereBetween('s02.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s02.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterValueList[0].FlagInclude) {
                    case 1 : builder.whereIn('s02.' + data.FilterCode, data.FilterValueFilter); break;
                    case 0 : builder.whereNotIn('s02.' + data.FilterCode, data.FilterValueFilter); break;
                    default: break;
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterValueList[0].FlagInclude) {
                        case 1 : this.orWhereIn('s02.' + data.FilterCode, data.FilterValueFilter); break;
                        case 0 : this.orWhereNotIn('s02.' + data.FilterCode, data.FilterValueFilter); break;
                        default: break;
                    }
                });                 
            });
        }
    })
    .groupBy('m06.AreaGabungan')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

/*
**  ZETA SPECIFIC QUERY
*/
const getActualProductZetaList = (bodyData, ConfigDetailId, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        's01.RayonCode',
        's01.ProductID',
        knex.raw(
            "case when t05.LineProduct IS NOT NULL then t05.LineProduct " +
            "when t05.LineProduct IS NULL AND t05.FilterCode = 'ProductGroupDesc' then s01.ProductGroupDesc " +
            "when t05.LineProduct IS NULL AND t05.FilterCode = 'ProductDesc' then s01.ProductDesc " +
            "END as categoryProduct"
        ),
        knex.raw('SUM(s01.SalesQuantity) AS actual')
    )
    .from('s01_actual_data AS s01')
    .innerJoin('t05_filter_tarikan_data as t05', function() {
        this.on('t05.ConfigDetailId', '=', ConfigDetailId)
        .andOn(function() {
            this.on('t05.FilterValue', '=', 's01.ProductGroupDesc')
            .orOn('t05.FilterValue', '=', 's01.ProductDesc')
            .orOn('t05.ProductID', '=', 's01.ProductID')
        })
    })
    .whereBetween('s01.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s01.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterValueList[0].FlagInclude) {
                    case 1:
                        builder.whereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                        break;
                    default:
                        builder.whereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                        break;
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterValueList[0].FlagInclude) {
                        case 1:
                            this.orWhereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                            break;
                        default:
                            this.orWhereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                            break;
                    }
                });                 
            });
        }
    })
    .groupBy('s01.RayonCode', 's01.ProductID', knex.raw(
        "case when t05.LineProduct IS NOT NULL then t05.LineProduct " +
        "when t05.LineProduct IS NULL AND t05.FilterCode = 'ProductGroupDesc' then s01.ProductGroupDesc " +
        "when t05.LineProduct IS NULL AND t05.FilterCode = 'ProductDesc' then s01.ProductDesc END "
    ))
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getTargetProductZetaList = (bodyData, ConfigDetailId, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        's02.RayonCode',
        's02.ProductID',
        knex.raw(
            "case when t05.LineProduct IS NOT NULL then t05.LineProduct " +
            "when t05.LineProduct IS NULL AND t05.FilterCode = 'ProductGroupDesc' then s02.ProductGroupDesc " +
            "when t05.LineProduct IS NULL AND t05.FilterCode = 'ProductDesc' then s02.ProductDesc " +
            "END as categoryProduct"
        ),
        knex.raw('SUM(s02.TargetQuantity) AS target')
    )
    .from('s02_target_data AS s02')
    .innerJoin('t05_filter_tarikan_data as t05', function() {
        this.on('t05.ConfigDetailId', '=', ConfigDetailId)
        .andOn(function() {
            this.on('t05.FilterValue', '=', 's02.ProductGroupDesc')
            .orOn('t05.FilterValue', '=', 's02.ProductDesc')
            .orOn('t05.ProductID', '=', 's02.ProductID')
        })
    })
    .whereBetween('s02.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s02.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterValueList[0].FlagInclude) {
                    case 1:
                        builder.whereIn('s02.' + data.FilterCode, data.FilterValueFilter);
                        break;
                    default:
                        builder.whereNotIn('s02.' + data.FilterCode, data.FilterValueFilter);
                        break;
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterValueList[0].FlagInclude) {
                        case 1:
                            this.orWhereIn('s02.' + data.FilterCode, data.FilterValueFilter);
                            break;
                        default:
                            this.orWhereNotIn('s02.' + data.FilterCode, data.FilterValueFilter);
                            break;
                    }
                });                 
            });
        }
    })
    .groupBy('s02.RayonCode', 's02.ProductID', knex.raw(
        "case when t05.LineProduct IS NOT NULL then t05.LineProduct " +
        "when t05.LineProduct IS NULL AND t05.FilterCode = 'ProductGroupDesc' then s02.ProductGroupDesc " +
        "when t05.LineProduct IS NULL AND t05.FilterCode = 'ProductDesc' then s02.ProductDesc END "
    ))
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getActualSalesHarianList = (bodyData, ConfigDetailId, filterAndTarikanList, filterOrTarikanList) => {
    return knex
    .with('table_work_days', (queryBuilder) => {
        queryBuilder.select(
            knex.raw("ROW_NUMBER() OVER(PARTITION BY m02.MonthID, m02.YearID) AS row_num"),
            'm02.MonthID', 
            'm02.YearID', 
            knex.raw("COUNT(*) OVER(PARTITION BY m02.MonthID, m02.YearID) AS total_work_day"),
            'm02.SalesDate'
        )
        .from('m02_work_days as m02')
        .whereBetween('m02.MonthID', [bodyData.monthFrom, bodyData.monthTo])
        .whereBetween('m02.YearID', [bodyData.yearFrom, bodyData.yearTo])
    })
    .with('table_group_actual', (queryBuilder) => {
        queryBuilder.select(
            't05.ConfigDetailId', 't05.RecordNo', 't05.Operator', 't05.FlagInclude', 't05.FilterCode', 't05.FilterValue',
            knex.raw(
                "(SELECT DAY(twd.SalesDate) " +
                "FROM table_work_days as twd " +
                "WHERE twd.row_num = case when t05.FilterValue > 0 then t05.FilterValue " +
                "ELSE twd.total_work_day + t05.FilterValue END " +
                ") AS SalesDate "
            )
        )
        .from('t05_filter_tarikan_data as t05')
        .where('t05.ConfigDetailId', '=', ConfigDetailId)
        .where(knex.raw("t05.FilterCode = 'DaySales'"))
        // .where('t05.FilterCode', '=', ['DaySales'])
    })
    .select(
        's01.RayonCode',
        'tga.FilterValue AS categoryProduct',
        knex.raw('SUM(s01.SalesValue) AS actual')
    )
    .from('s04_sales_invoice AS s01')
    .leftJoin('table_group_actual as tga', function() {
        this.on('s01.DaySales', '<=', 'tga.SalesDate')
    })
    .whereBetween('s01.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s01.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterCode) {
                    case 'DaySales': break;
                    default: {
                        switch (data.FilterValueList[0].FlagInclude) {
                            case 1:
                                builder.whereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                break;
                            default:
                                builder.whereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                break;
                        }
                        break;
                    }
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterCode) {
                        case 'DaySales': break;
                        default: {
                            switch (data.FilterValueList[0].FlagInclude) {
                                case 1:
                                    this.orWhereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                                default:
                                    this.orWhereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                            }
                            break;
                        }
                    }
                });                 
            });
        }
    })
    .groupBy('s01.RayonCode', 'tga.FilterValue')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

/*
**  PROSPERITY SPECIFIC QUERY
*/

const getActualProsperityList = (bodyData, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        's01.RayonCode', 's01.MonthSales', 's01.YearSales',
        knex.raw(
            "CONCAT(UPPER(MONTHNAME(CONCAT(s01.YearSales, '-', s01.MonthSales, '-01'))), ' ', s01.YearSales) AS MonthPeriod"
        ),
        knex.raw('SUM(s01.SalesValue) AS actual'),
        knex.raw(
            "SUM(case when s01.LineProduct = 'DXM PROGRESS' OR s01.LineProduct = 'DXM PROFIT' then s01.SalesValue ELSE 0 END) " +
            "AS actualDXM"
        ),
        knex.raw(
            "SUM(case when s01.LineProduct = 'DXM PROGRESS' OR s01.LineProduct = 'DXM PROFIT' then s01.PAP + s01.PLP ELSE 0 END) " +
            "AS discountDXM"
        ),
        knex.raw(
            "SUM(case when s01.LineProduct = 'CHD HERBA' OR s01.LineProduct = 'CHD HERBAWELL' then s01.SalesValue ELSE 0 END) " +
            "AS actualCHD"
        )
    )
    .from('s01_actual_data AS s01')
    .whereBetween('s01.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s01.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                if (data.FilterValueList[0].FlagInclude === 1) {
                    builder.whereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                } else {
                    builder.whereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    if (data.FilterValueList[0].FlagInclude === 1) {
                        this.orWhereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                    } else {
                        this.orWhereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                    }
                });                 
            });
        }
    })
    .groupBy('s01.RayonCode', 's01.MonthSales', 's01.YearSales')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getTargetProsperityList = (bodyData, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        's02.RayonCode', 's02.MonthSales', 's02.YearSales',
        knex.raw(
            "CONCAT(UPPER(MONTHNAME(CONCAT(s02.YearSales, '-', s02.MonthSales, '-01'))), ' ', s02.YearSales) AS MonthPeriod"
        ),
        knex.raw('SUM(s02.TargetValue) AS target'),
        knex.raw(
            "SUM(case when s02.LineProduct = 'DXM PROGRESS' OR s02.LineProduct = 'DXM PROFIT' then s02.TargetValue ELSE 0 END) " +
            "AS targetDXM"
        ),
        knex.raw(
            "SUM(case when s02.LineProduct = 'CHD HERBA' OR s02.LineProduct = 'CHD HERBAWELL' then s02.TargetValue ELSE 0 END) " +
            "AS targetCHD"
        )
    )
    .from('s02_target_data as s02')
    .whereBetween('s02.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s02.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterCode) {
                    case 'DaySales': break;
                    default: {
                        switch (data.FilterValueList[0].FlagInclude) {
                            case 1:
                                builder.whereIn('s02.' + data.FilterCode, data.FilterValueFilter);
                                break;
                            default:
                                builder.whereNotIn('s02.' + data.FilterCode, data.FilterValueFilter);
                                break;
                        }
                        break;
                    }
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterCode) {
                        case 'DaySales': break;
                        default: {
                            switch (data.FilterValueList[0].FlagInclude) {
                                case 1:
                                    this.orWhereIn('s02.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                                default:
                                    this.orWhereNotIn('s02.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                            }
                            break;
                        }
                    }
                });                 
            });
        }
    })
    .groupBy('s02.RayonCode', 's02.MonthSales', 's02.YearSales')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getActualChannelProsperityList = (bodyData, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        's01.RayonCode',
        knex.raw("SUBSTRING(s01.LineProduct, 1, 3) AS categoryProduct"),
        knex.raw("SUM(s01.SalesValue) AS actual")
    )
    .from('s01_actual_data AS s01')
    .innerJoin('m09_outlet_project AS m09', function() {
        this.on('m09.RayonCode', '=', 's01.RayonCode').on('m09.Site_Code', '=', 's01.SiteCode')
    })
    .whereBetween('s01.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s01.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterValueList[0].FlagInclude) {
                    case 1:
                        builder.whereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                        break;
                    default:
                        builder.whereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                        break;
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterValueList[0].FlagInclude) {
                        case 1:
                            this.orWhereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                            break;
                        default:
                            this.orWhereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                            break;
                    }
                });                 
            });
        }
    })
    .groupBy('s01.RayonCode', knex.raw("SUBSTRING(s01.LineProduct, 1, 3)"))
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getActualProductProsperityList = (bodyData, ConfigDetailId, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        's01.RayonCode',
        't05.LineProduct AS categoryProduct',
        't05.FilterValue as productDesc',
        knex.raw('SUM(s01.SalesValue) AS actual'),
        knex.raw('SUM(s01.SalesQuantity) AS actualUnit')
    )
    .from('s01_actual_data AS s01')
    .innerJoin('t05_filter_tarikan_data as t05', function() {
        this.on('t05.ConfigDetailId', '=', ConfigDetailId)
        .andOn(function() {
            this.on('t05.FilterValue', '=', 's01.ProductGroupDesc')
            .orOn('t05.FilterValue', '=', 's01.ProductDesc')
        })
    })
    .whereBetween('s01.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s01.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterValueList[0].FlagInclude) {
                    case 1:
                        builder.whereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                        break;
                    default:
                        builder.whereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                        break;
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterValueList[0].FlagInclude) {
                        case 1:
                            this.orWhereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                            break;
                        default:
                            this.orWhereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                            break;
                    }
                });
            });
        }
    })
    .groupBy('s01.RayonCode', 't05.LineProduct', 't05.FilterValue')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

/*
**  HARMONY SPECIFIC QUERY
*/

const getActualHarmonyList = (bodyData, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        's01.RayonCode', 's01.MonthSales', 's01.YearSales',
        knex.raw(
            "CONCAT(UPPER(MONTHNAME(CONCAT(s01.YearSales, '-', s01.MonthSales, '-01'))), ' ', s01.YearSales) AS MonthPeriod"
        ),
        knex.raw(
            "SUM(case when s01.ProductID = 'TETA002' AND s01.StatusFaktur = 'J' then 0 ELSE s01.SalesValue END) AS actual "
        ),
        knex.raw(
            "SUM(case when s01.StatusFaktur IN ('R', 'I') then s01.SalesValue ELSE 0 END) AS actualReguler"
        ),
        knex.raw(
            "SUM(case when s01.ProductID IN ('AVIG001', 'JUBI001', 'REGK001') then s01.SalesValue ELSE 0 END) AS actualCovid"
        )
    )
    .from('s01_actual_data AS s01')
    .whereBetween('s01.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s01.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                if (data.FilterValueList[0].FlagInclude === 1) {
                    builder.whereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                } else {
                    builder.whereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    if (data.FilterValueList[0].FlagInclude === 1) {
                        this.orWhereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                    } else {
                        this.orWhereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                    }
                });                 
            });
        }
    })
    .groupBy('s01.RayonCode', 's01.MonthSales', 's01.YearSales')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getTargetHarmonyList = (bodyData, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        's02.RayonCode', 's02.MonthSales', 's02.YearSales',
        knex.raw(
            "CONCAT(UPPER(MONTHNAME(CONCAT(s02.YearSales, '-', s02.MonthSales, '-01'))), ' ', s02.YearSales) AS MonthPeriod"
        ),
        knex.raw(
            "SUM(case when (s02.ProductID = 'TETA002' AND s02.StatusFaktur = 'A') " +
            "then 0 ELSE s02.TargetValue END) AS target"
        ),
        knex.raw(
            "SUM(case when s02.StatusFaktur != 'R' " + 
            "AND s02.ProductID NOT IN ('INBU002', 'A-INBUMINFORTE', 'BRIX001', 'BRIX002', 'A-BRIXA25', 'A-BRIXA5') " +
            "then 0 ELSE s02.TargetValue END) AS targetReguler"
        ),
        knex.raw(
            "SUM(case when s02.ProductID IN ('AVIG001', 'JUBI001', 'REGK001') then s02.TargetValue ELSE 0 END) AS targetCovid"
        )
    )
    .from('s02_target_data as s02')
    .whereBetween('s02.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s02.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterCode) {
                    case 'DaySales': break;
                    default: {
                        switch (data.FilterValueList[0].FlagInclude) {
                            case 1:
                                builder.whereIn('s02.' + data.FilterCode, data.FilterValueFilter);
                                break;
                            default:
                                builder.whereNotIn('s02.' + data.FilterCode, data.FilterValueFilter);
                                break;
                        }
                        break;
                    }
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterCode) {
                        case 'DaySales': break;
                        default: {
                            switch (data.FilterValueList[0].FlagInclude) {
                                case 1:
                                    this.orWhereIn('s02.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                                default:
                                    this.orWhereNotIn('s02.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                            }
                            break;
                        }
                    }
                });                 
            });
        }
    })
    .groupBy('s02.RayonCode', 's02.MonthSales', 's02.YearSales')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getActualProductHarmonyList = (bodyData, ConfigDetailId, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        's01.RayonCode',
        't05.LineProduct AS categoryProduct',
        knex.raw('SUM(s01.SalesValue) AS actual')
    )
    .from('s01_actual_data AS s01')
    .innerJoin('t05_filter_tarikan_data as t05', function() {
        this.on('t05.ConfigDetailId', '=', ConfigDetailId)
        .andOn(function() {
            this.on('t05.FilterValue', '=', 's01.ProductGroupDesc')
            .orOn('t05.ProductID', '=', 's01.ProductID')
        })
    })
    .whereBetween('s01.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s01.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterCode) {
                    default: {                
                        switch (data.FilterValueList[0].FlagInclude) {
                            case 1:
                                builder.whereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                break;
                            default:
                                builder.whereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                break;
                        }
                        break;
                    }
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterCode) {
                        default: {
                            switch (data.FilterValueList[0].FlagInclude) {
                                case 1:
                                    this.orWhereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                                default:
                                    this.orWhereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                            }
                            break;
                        }
                    }
                });                 
            });
        }
    })
    .groupBy('s01.RayonCode', 't05.LineProduct')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getTargetProductHarmonyList = (bodyData, ConfigDetailId, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        's02.RayonCode',
        't05.LineProduct AS categoryProduct',
        knex.raw('SUM(s02.TargetValue) AS target')
    )
    .from('s02_target_data AS s02')
    .innerJoin('t05_filter_tarikan_data as t05', function() {
        this.on('t05.ConfigDetailId', '=', ConfigDetailId)
        .andOn(function() {
            this.on('t05.FilterValue', '=', 's02.ProductGroupDesc')
            .orOn('t05.ProductID', '=', 's02.ProductID')
        })
    })
    .whereBetween('s02.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s02.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterCode) {
                    default: {
                        switch (data.FilterValueList[0].FlagInclude) {
                            case 1:
                                builder.whereIn('s02.' + data.FilterCode, data.FilterValueFilter);
                                break;
                            default:
                                builder.whereNotIn('s02.' + data.FilterCode, data.FilterValueFilter);
                                break;
                        }
                        break;
                    }
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterCode) {
                        default: {
                            switch (data.FilterValueList[0].FlagInclude) {
                                case 1:
                                    this.orWhereIn('s02.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                                default:
                                    this.orWhereNotIn('s02.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                            }
                            break;
                        }
                    }
                });                 
            });
        }
    })
    .groupBy('s02.RayonCode', 't05.LineProduct')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getActualCabangHarmonyList = (bodyData, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        'm06.AreaGabungan',
        knex.raw("SUM(case when s01.DaySales <= 15 then s01.SalesValue ELSE 0 END) AS actualMiddle"),
        knex.raw("SUM(s01.SalesValue) AS actual")
    )
    .from('m06_kriteria_cabang as m06')
    .innerJoin('m15_area_group AS m15', function() {
        this.on('m15.LineDesc', '=', 'm06.LineDesc')
        .on('m15.AreaGabungan', '=', 'm06.AreaGabungan')
    })
    .innerJoin('s01_actual_data as s01', function() {
        this.on('s01.LineDesc', '=', 'm15.LineDesc')
        .on('s01.CustomerArea', '=', 'm15.EmployeeAreaCode')
    })
    .whereBetween('s01.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s01.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(function() {
        this.where('s01.ProductID', '<>', 'TETA002').orWhere('s01.StatusFaktur', '<>', 'J')
    })
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterValueList[0].FlagInclude) {
                    case 1:
                        builder.whereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                        break;
                    default:
                        builder.whereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                        break;
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterValueList[0].FlagInclude) {
                        case 1:
                            this.orWhereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                            break;
                        default:
                            this.orWhereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                            break;
                    }
                });                 
            });
        }
    })
    .groupBy('m06.AreaGabungan')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getTargetCabangHarmonyList = (bodyData, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        'm06.AreaGabungan',
        knex.raw("SUM(s02.TargetValue) AS target")
    )
    .from('m06_kriteria_cabang as m06')
    .innerJoin('m15_area_group AS m15', function() {
        this.on('m15.LineDesc', '=', 'm06.LineDesc')
        .on('m15.AreaGabungan', '=', 'm06.AreaGabungan')
    })
    .innerJoin('s02_target_data as s02', function() {
        this.on('s02.LineDesc', '=', 'm15.LineDesc')
        .on('s02.CustomerArea', '=', 'm15.EmployeeAreaCode')
    })
    .whereBetween('s02.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s02.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(function() {
        this.where('s02.ProductID', '<>', 'TETA002').orWhere('s02.StatusFaktur', '<>', 'A')
    })
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterValueList[0].FlagInclude) {
                    case 1 : builder.whereIn('s02.' + data.FilterCode, data.FilterValueFilter); break;
                    case 0 : builder.whereNotIn('s02.' + data.FilterCode, data.FilterValueFilter); break;
                    default: break;
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterValueList[0].FlagInclude) {
                        case 1 : this.orWhereIn('s02.' + data.FilterCode, data.FilterValueFilter); break;
                        case 0 : this.orWhereNotIn('s02.' + data.FilterCode, data.FilterValueFilter); break;
                        default: break;
                    }
                });                 
            });
        }
    })
    .groupBy('m06.AreaGabungan')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

/*
**  OGB Specific Query
*/

const getIFocusMobileList = (lineDesc) => {
    return knex.select(
        'RayonCode', 'TargetOutlet', 'TotalSales'
    )
    .from('m16_ifocus_mobile as m16')
    .where('m16.LineDesc', '=', lineDesc)
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getAreaGroupList = (lineDesc) => {
    return knex.select(
        'RayonCode', 'EmployeeAreaCode'
    )
    .from('m15_area_group as m15')
    .where('m15.LineDesc', '=', lineDesc)
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getActualProductOGBList = (bodyData, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        's01.RayonCode',
        's01.CustomerDesc',
        's01.ProductID',
        knex.raw('SUM(s01.SalesValue) AS salesHNA'),
        knex.raw('SUM(s01.SalesQuantity) AS salesUnit')
    )
    .from('s01_actual_data AS s01')
    .whereBetween('s01.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s01.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterCode) {
                    case 'ProductID': {
                        switch (data.FilterValueList[0].FlagInclude) {
                            case 1:
                                builder.whereIn('s01.' + data.FilterCode, data.ProductIDList);
                                break;
                            default:
                                builder.whereNotIn('s01.' + data.FilterCode, data.ProductIDList);
                                break;
                        }
                        break;
                    }
                    default: {
                        switch (data.FilterValueList[0].FlagInclude) {
                            case 1:
                                builder.whereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                break;
                            default:
                                builder.whereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                break;
                        }
                        break;
                    }
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterCode) {
                        case 'ProductID' : {
                            switch (data.FilterValueList[0].FlagInclude) {
                                case 1:
                                    this.orWhereIn('s01.' + data.FilterCode, data.ProductIDList);
                                    break;
                                default:
                                    this.orWhereNotIn('s01.' + data.FilterCode, data.ProductIDList);
                                    break;
                            }
                            break;
                        }
                        default: {
                            switch (data.FilterValueList[0].FlagInclude) {
                                case 1:
                                    this.orWhereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                                default:
                                    this.orWhereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                            }
                            break;
                        }
                    }
                });                 
            });
        }
    })
    .groupBy('s01.RayonCode', 's01.CustomerDesc', 's01.ProductID')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

module.exports = {
    getEmployeeDataList,
    getProductivityDataList,
    getSchemaList,
    getFilterTarikanDataList,
    getParameterBonusList,
    getActualDataList,
    getTargetDataList,
    getActualProductList,
    getTargetProductList,
    getActualProductZetaList,
    getTargetProductZetaList,
    getActualChannelList,
    getTargetChannelList,
    getPreviousQuarterActualList,
    getNavyCabangList,
    getCriteriaCabangList,
    getActualDataCabangList,
    getTargetDataCabangList,
    getActualSalesHarianList,
    getActualProsperityList,
    getTargetProsperityList,
    getActualChannelProsperityList,
    getActualProductProsperityList,
    getActualHarmonyList,
    getTargetHarmonyList,
    getActualProductHarmonyList,
    getTargetProductHarmonyList,
    getActualCabangHarmonyList,
    getTargetCabangHarmonyList,
    getIFocusMobileList,
    getAreaGroupList,
    getActualProductOGBList
}
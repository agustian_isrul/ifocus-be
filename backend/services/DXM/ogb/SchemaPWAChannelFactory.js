const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculateBonus
} = require('../../../helpers/utility');

const incentivePWAChannel = (
    schema, resultCalculate, actualProductList, targetProductList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');

    const productList = filterDataListBySchema.filter(data => data.FilterCode === 'ProductID');
    const actualTargetFilter = filterParameterListBySchema.find(data => !data.RecordNoD && data.FilterCode === 'actualTarget');
    if (actualTargetFilter) {
        const indexFilter = filterParameterListBySchema.indexOf(actualTargetFilter);
        filterParameterListBySchema.splice(indexFilter, 1);
    }
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode,
            actual: employee.actualAll,
            target: employee.targetAll,
            actualTarget: employee.actualTargetAll,
            targetOutlet: 0
        }

        switch (employee.EmployeeMRType) {
            case 'HOSPITAL': {
                for (const tempProduct of productList) {
                    const tempObjectSchema = {                    
                        schemaId: schema.SchemaId,
                        schemaLabel: schema.SchemaLabel,
                        rayonCode: employee.RayonCode
                    }
                    tempObjectSchema.actual = actualProductList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode) &&
                    actual.ProductID === tempProduct.ProductID)
                    .reduce((accumulator, actual) => {return accumulator + actual.salesUnit}, 0);
                    tempObjectSchema.target = targetProductList.filter(target => target.EmployeeMRType === employee.EmployeeMRType && 
                    target.ProductID === tempProduct.ProductID)
                    .reduce((accumulator, target) => {return accumulator + target.target}, 0);
                    tempObjectSchema.actualTarget = convertNaNToZero(tempObjectSchema.actual / tempObjectSchema.target * 100);

                    if (actualTargetFilter) {                        
                        const isExceedAT = validateByTargetType(actualTargetFilter.TargetTypeH, employee, tempObjectSchema, 
                            actualTargetFilter.Operator2H, actualTargetFilter.ValueFromH, actualTargetFilter.ValueToH);
                        if (isExceedAT) {
                            objectSchema.targetOutlet = objectSchema.targetOutlet + 1;
                        }
                    } else {
                        if (tempObjectSchema.actualTarget >= 100) {
                            objectSchema.targetOutlet = objectSchema.targetOutlet + 1;
                        }
                    }
                }
                break;
            }
            default: {
                const tempChannelList = Object.values(actualProductList
                .filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
                .sort((a, b) => {
                    const nameA = a.CustomerDesc.toUpperCase();
                    const nameB = b.CustomerDesc.toUpperCase();
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    return 0;
                })
                .reduce((result, item) => {
                    const value = item.CustomerDesc;
                    const existing = result[value] || {
                        channel: value,
                        countProduct: 0
                    };
                    return {
                        ...result,
                        [value] : {
                            ...existing,
                            countProduct: existing.countProduct + 1
                        }
                    }
                }, {}));
                
                objectSchema.targetOutlet = tempChannelList.filter(tempChannel => tempChannel.countProduct >= 10)
                .reduce((accumulator, target) => {return accumulator + 1}, 0);
                break;
            }
        }
        
        const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = bonus;
        
        employee.totalIncentive = (employee.totalIncentive || 0) + objectSchema.totalIncentive;

        employee.schemaList.push(objectSchema);

        return employee;
    });
}

module.exports = {
    incentivePWAChannel
}
const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveProductFocus = (
    schema, resultCalculate, actualProductList, targetProductList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');    
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    const tempCategoryProductList = filterDataListBySchema
    .filter(data => data.FilterCode === 'ProductDesc' || data.FilterCode === 'ProductGroupDesc');
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;
        
        const tempActualProductList = actualProductList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode));

        let totalIncentive = 0;
        let tempObjectSchemaList = tempCategoryProductList.map(tempObject => {
            const objectSchema = {                    
                schemaId: schema.SchemaId,
                schemaLabel: schema.SchemaLabel,
                rayonCode: employee.RayonCode,
                ProductID: tempObject.ProductID,
                categoryProduct: tempObject.FilterValue
            }
            objectSchema.actual = (objectSchema.actual || 0) + tempActualProductList
            .filter(actual => actual.ProductID == tempObject.ProductID)
            .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
            objectSchema.target = (objectSchema.target || 0) + targetProductList
            .filter(target => target.ProductID === tempObject.ProductID && target.EmployeeMRType === employee.EmployeeMRType)
            .reduce((accumulator, target) => {return accumulator + target.target}, 0);
            objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
        
            const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
            objectSchema.incentive = bonus;
            totalIncentive = totalIncentive + bonus;

            return objectSchema;
        });

        tempObjectSchemaList = tempObjectSchemaList.map(tempObject => {
            tempObject.totalIncentive = totalIncentive;
            employee.schemaList.push(tempObject);
            return tempObject;
        });

        employee.totalIncentive = (employee.totalIncentive || 0) + totalIncentive;

        return employee;
    });
}

module.exports = {
    incentiveProductFocus
}
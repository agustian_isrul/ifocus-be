const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    validateByTargetType,
    calculatePassword
} = require('../../../helpers/utility');

const incentiveRerata = (schema, resultCalculate, filterDataListBySchema, filterParameterListBySchema) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');

    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode
        }
        
        objectSchema.actual = employee.actualAll;
        objectSchema.target = employee.targetAll;
        objectSchema.actualTarget = employee.actualTargetAll;
        objectSchema.totalIncentiveSubordinate = resultCalculate
        .filter(tempSubordinate => tempSubordinate.RayonCodeList
            .some(tempRayonCode => employee.RayonCodeFilter.includes(tempRayonCode.RayonCodeSup)))
        .reduce((accumulator, tempSubordinate) => {return accumulator + tempSubordinate.totalIncentive}, 0);
    
        const bonus = calculateStaticBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = bonus;
        
        employee.totalIncentive = (employee.totalIncentive || 0) + objectSchema.totalIncentive;

        employee.schemaList.push(objectSchema);

        return employee;
    });
}

const calculateStaticBonus = (employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList) => {
    if (calculatePassword(employee, objectSchema, preeliminationAndList, preeliminationOrList) === 0) {
        return 0;
    }
    if (employee.countSubordinate > 0) {
        let bonus = Math.round(objectSchema.totalIncentiveSubordinate / employee.countSubordinate);
        if (clasificationList && clasificationList.length > 0) {
            const objectClassification = clasificationList.find(criteria => validateByTargetType(
                criteria.TargetTypeH, employee, objectSchema, criteria.Operator2H, criteria.ValueFromH, criteria.ValueToH
            ));
            if (objectClassification && objectClassification.detail && objectClassification.detail.length > 0) {
                const objectIncentive = objectClassification.detail.find(criteriaDetail => validateByTargetType(
                    criteriaDetail.TargetTypeD, employee, objectSchema, criteriaDetail.Operator2D, 
                    criteriaDetail.ValueFromD, criteriaDetail.ValueToD
                ) && criteriaDetail.Bonus);
                if (objectIncentive) {
                    return (bonus >= objectIncentive.Bonus) ? objectIncentive.Bonus : bonus;
                }
            }
        }
        return bonus;
    }
    return 0;
}

module.exports = {
    incentiveRerata
}
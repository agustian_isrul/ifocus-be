const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveTarikanSales = (
    schema, resultCalculate, actualSalesHarianList, targetSalesHarianList, areaGroupList,
    filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');    
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    const tempCategoryProductList = filterDataListBySchema.filter(data => data.FilterCode === 'DaySales');
    
    let areaList = [];
    if (areaGroupList && areaGroupList.length > 0) {
        const tempAreaList = Object.values(areaGroupList.reduce((result, item) => {
            const value = item.EmployeeAreaCode;
            const existing = result[value] || {
                AreaCode: value,
                RayonCodeList: []
            }
            return {
                ...result,
                [value] : {
                    ...existing,
                    RayonCodeList: [...existing.RayonCodeList, item.RayonCode]
                }
            } 
        }, {}));
        for (const tempArea of tempAreaList) {
            for (const tempCategory of tempCategoryProductList) {
                const tempAreaData = {
                    AreaCode: tempArea.AreaCode,
                    RayonCodeList: tempArea.RayonCodeList,
                    categoryProduct: tempCategory.FilterValue
                }

                tempAreaData.actual = actualSalesHarianList.filter(actual => tempArea.RayonCodeList.includes(actual.RayonCode) && 
                actual.categoryProduct == tempCategory.FilterValue)
                .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
                tempAreaData.target = targetSalesHarianList.filter(target => tempArea.RayonCodeList.includes(target.RayonCode))
                .reduce((accumulator, target) => {return accumulator + target.target}, 0);                
                tempAreaData.actualTarget = convertNaNToZero(tempAreaData.actual / tempAreaData.target * 100);

                tempAreaData.actualArea = actualSalesHarianList.filter(actual => tempArea.RayonCodeList.includes(actual.RayonCode))
                .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);

                areaList.push(tempAreaData);
            }
        }
    }
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        if (areaGroupList && areaGroupList.length > 0 && employee.RayonCodeList[0].FlagTarikanSales) {
            const tempRayonCode = areaGroupList.find(data => employee.RayonCodeFilter.includes(data.RayonCode));
            if (!tempRayonCode) {
                return employee;
            }
        }

        let tempObjectSchemaList = tempCategoryProductList.map(tempObject => {
            const objectSchema = {                    
                schemaId: schema.SchemaId,
                schemaLabel: schema.SchemaLabel,
                rayonCode: employee.RayonCode,
                workDay: tempObject.FilterValue
            }
            objectSchema.actual = actualSalesHarianList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode) && 
            actual.categoryProduct == tempObject.FilterValue)
            .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
            objectSchema.target = targetSalesHarianList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
            .reduce((accumulator, target) => {return accumulator + target.target}, 0);
            objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);

            if (areaList && areaList.length > 0 && employee.RayonCodeList[0].FlagTarikanSales) {
                const tempAreaList = areaList
                .filter(actual => actual.RayonCodeList.some(tempRayon => employee.RayonCodeFilter.includes(tempRayon)) && 
                actual.categoryProduct == tempObject.FilterValue);
                objectSchema.actualArea = tempAreaList
                .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
                objectSchema.targetArea = tempAreaList
                .reduce((accumulator, actual) => {return accumulator + actual.target}, 0);
                objectSchema.actualTargetArea = convertNaNToZero(objectSchema.actualArea / objectSchema.targetArea * 100);
                objectSchema.actualAreaAll = tempAreaList
                .reduce((accumulator, actual) => {return accumulator + actual.actualArea}, 0);
                objectSchema.actualTargetAreaAll = convertNaNToZero(objectSchema.actualAreaAll / objectSchema.targetArea * 100);
            }
        
            const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
            objectSchema.incentive = bonus;

            return objectSchema;
        });
        
        let totalIncentiveAnd = 0;
        let totalIncentiveOr = 0;
        const clasificationAndList = clasificationList.filter(data => data.Operator1H === 'AND');
        const clasificationOrList = clasificationList.filter(data => data.Operator1H === 'OR');
        if (clasificationAndList && clasificationAndList.length > 0) {
            for (const tempClasification of clasificationAndList) {
                const tempObjectSchema = tempObjectSchemaList.find(tempObject => tempObject.workDay === tempClasification.ValueFromH);
                if (tempObjectSchema && tempObjectSchema.incentive > 0) {
                    totalIncentiveAnd = totalIncentiveAnd + tempObjectSchema.incentive;
                }
            }
        }
        if (clasificationOrList && clasificationOrList.length > 0) {
            for (const tempClasification of clasificationOrList) {
                const tempObjectSchema = tempObjectSchemaList.find(tempObject => tempObject.workDay === tempClasification.ValueFromH);
                if (tempObjectSchema && tempObjectSchema.incentive > 0 && tempObjectSchema.incentive > totalIncentiveOr) {
                    totalIncentiveOr = tempObjectSchema.incentive;
                }
            }
        }
        
        tempObjectSchemaList = tempObjectSchemaList.map(tempObject => {
            tempObject.totalIncentive = totalIncentiveAnd + totalIncentiveOr;
            employee.schemaList.push(tempObject);
            return tempObject;
        });

        employee.totalIncentive = (employee.totalIncentive || 0) + totalIncentiveAnd + totalIncentiveOr;

        return employee;
    });
}

module.exports = {
    incentiveTarikanSales
}
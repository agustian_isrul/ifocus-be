const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    isExistEmployee,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveATQuarter = (
    schema, resultCalculate, employeeDataList, actualDataList, targetDataList, productivityDataList, 
    filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const filterEmployeeList = convertArrayToNestedArray(employeeDataList
    .filter(currentEmployee => filterEmployeeDataList
        .every(element => element.FilterValueList[0].FlagInclude === 1 ? 
            element.FilterValueFilter.includes(currentEmployee[element.FilterCode]) : 
            !element.FilterValueFilter.includes(currentEmployee[element.FilterCode])
        )
    ),
    'EmployeeID', 'RayonCode');

    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);

    for (let employee of filterEmployeeList) {
        let currentEmployee = isExistEmployee(employee, resultCalculate);
        if (!currentEmployee) {
            currentEmployee = employee;
            currentEmployee.EmployeeName = employee.RayonCodeList[0].EmployeeName;
            currentEmployee.EmployeePositionCode = employee.RayonCodeList[0].EmployeePositionCode;
            currentEmployee.EmployeeName = employee.RayonCodeList[0].EmployeeName;
            currentEmployee.EmployeeAreaCode = employee.RayonCodeList[0].EmployeeAreaCode;
            currentEmployee.EmployeeSubLineDesc = employee.RayonCodeList[0].EmployeeSubLineDesc;
            currentEmployee.EmployeeMRType = employee.RayonCodeList[0].EmployeeMRType;
            currentEmployee.EmployeeVacant = employee.RayonCodeList[0].EmployeeVacant;

            let tempCurrentEmployee = null;
            switch (currentEmployee.EmployeeMRType) {
                case 'BETA': {
                    tempCurrentEmployee = setupEmployeeBetaProperties(
                        currentEmployee, schema, actualDataList, targetDataList, 
                        preeliminationAndList, preeliminationOrList, clasificationList
                    );
                    break;
                }
                case 'JKN': {
                    tempCurrentEmployee = setupEmployeeJKNProperties(
                        currentEmployee, schema, resultCalculate, actualDataList, targetDataList, productivityDataList,
                        preeliminationAndList, preeliminationOrList, clasificationList
                    );
                    break;
                }
                default: {
                    tempCurrentEmployee = setupEmployeeRegulerProperties(
                        currentEmployee, schema, resultCalculate, actualDataList, targetDataList, productivityDataList,
                        preeliminationAndList, preeliminationOrList, clasificationList
                    );
                    break;                    
                }
            }
            if (tempCurrentEmployee) {
                resultCalculate = resultCalculate || [];
                resultCalculate.push(tempCurrentEmployee);
            }
        }
    }

    return resultCalculate;
}

const setupEmployeeBetaProperties = (
    employee, schema, actualDataList, targetDataList, preeliminationAndList, preeliminationOrList, clasificationList
) => {
    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode
    }

    objectSchema.actual = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
    .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
    objectSchema.target = targetDataList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
    .reduce((accumulator, target) => {return accumulator + target.target}, 0);

    if (objectSchema.actual <= 0 && objectSchema.target <= 0) {
        return null;
    }
    
    objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);

    employee.actualAll = objectSchema.actual;
    employee.targetAll = objectSchema.target;
    employee.actualTargetAll = objectSchema.actualTarget;
    employee.productivity = productivityDataList
    .filter(data => data.EmployeePositionCode === employee.EmployeePositionCode && data.EmployeeMRType === employee.EmployeeMRType)
    .reduce((accumulator, item) => {return accumulator + item.Productivitas}, 0);

    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
    objectSchema.incentive = bonus;
    objectSchema.totalIncentive = bonus;
    
    employee.totalIncentive = (employee.totalIncentive || 0) + bonus;

    employee.schemaList = employee.schemaList || [];
    employee.schemaList.push(objectSchema);

    return employee;
}

const setupEmployeeJKNProperties = (
    employee, schema, resultCalculate, actualDataList, targetDataList, productivityDataList, 
    preeliminationAndList, preeliminationOrList, clasificationList
) => {
    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode
    }

    let tempActual = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
    .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
    let tempTarget = targetDataList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
    .reduce((accumulator, target) => {return accumulator + target.target}, 0);
    let tempPAP = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
    .reduce((accumulator, actual) => {return accumulator + actual.pap}, 0);
    // let tempProductivity = 0;
    let tempProductivity = productivityDataList
    .filter(data => data.EmployeePositionCode === employee.EmployeePositionCode && data.EmployeeMRType === employee.EmployeeMRType)
    .reduce((accumulator, item) => {return accumulator + item.Productivitas}, 0);
    // if (['SUP', 'KOORD'].includes(employee.EmployeePositionCode)) {
    //     tempProductivity = productivityDataList
    //     .filter(data => data.EmployeePositionCode === 'SUP' && data.EmployeeMRType === employee.EmployeeMRType)
    //     .reduce((accumulator, item) => {return accumulator + item.Productivitas}, 0);
    // } else {
    //     tempProductivity = productivityDataList
    //     .filter(data => data.EmployeePositionCode === employee.EmployeePositionCode && data.EmployeeMRType === employee.EmployeeMRType)
    //     .reduce((accumulator, item) => {return accumulator + item.Productivitas}, 0);
    // }
    let countSubordinate = 0;

    if (employee.EmployeePositionCode !== 'MR') {
        const tempSubordinateList = Object.values(resultCalculate
        .filter(tempSubordinate => tempSubordinate.RayonCodeList
            .some(tempRayonCode => employee.RayonCodeFilter.includes(tempRayonCode.RayonCodeSup)))
        .reduce((result, item) => {
            const value = employee.RayonCode;
            const existing = result[value] || {
                rayonCode: value,
                actualAll: 0,
                targetAll: 0,
                pap: 0,
                countSubordinate: 0
            };
            return {
                ...result,
                [value] : {
                    ...existing,
                    actualAll: existing.actualAll + item.actualAll,
                    targetAll: existing.targetAll + item.targetAll,
                    pap: existing.pap + item.pap,
                    countSubordinate: existing.countSubordinate + 1
                }
            }
        }, {}));

        if (tempSubordinateList && tempSubordinateList.length > 0) {
            tempActual = tempActual + tempSubordinateList[0].actualAll;
            tempTarget = tempTarget + tempSubordinateList[0].targetAll;
            tempPAP = tempPAP + tempSubordinateList[0].pap;
            countSubordinate = tempSubordinateList[0].countSubordinate;
        }
    }

    objectSchema.actual = tempActual;
    objectSchema.target = tempTarget;

    if (objectSchema.actual <= 0 && objectSchema.target <= 0) {
        return null;
    }
    
    objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);

    employee.actualAll = objectSchema.actual;
    employee.targetAll = objectSchema.target;
    employee.actualTargetAll = objectSchema.actualTarget;
    employee.productivity = tempProductivity * 3;
    employee.maxTargetProductivity = Math.max(employee.targetAll, employee.productivity);
    employee.actualTargetMax = convertNaNToZero(employee.actualAll / employee.maxTargetProductivity * 100);
    employee.actualProductivity = convertNaNToZero(employee.actualAll / employee.productivity * 100);
    employee.pap = tempPAP;
    employee.discountPAP = convertNaNToZero(employee.pap / employee.actualAll * 100);
    employee.netSales = employee.actualAll - employee.pap;
    employee.nettoAP = convertNaNToZero(employee.netSales / employee.productivity * 100);
    if (employee.EmployeePositionCode !== 'MR') {
        employee.countSubordinate = countSubordinate;
    }

    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
    objectSchema.incentive = bonus;
    objectSchema.totalIncentive = bonus;
    
    employee.totalIncentive = (employee.totalIncentive || 0) + bonus;

    employee.schemaList = employee.schemaList || [];
    employee.schemaList.push(objectSchema);

    return employee;
}

const setupEmployeeRegulerProperties = (
    employee, schema, resultCalculate, actualDataList, targetDataList, productivityDataList, 
    preeliminationAndList, preeliminationOrList, clasificationList
) => {
    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode
    }

    let tempActual = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
    .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
    let tempTarget = targetDataList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
    .reduce((accumulator, target) => {return accumulator + target.target}, 0);
    let tempPAP = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
    .reduce((accumulator, actual) => {return accumulator + actual.pap}, 0);
    let tempActualCategoryR = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
    .reduce((accumulator, actual) => {return accumulator + actual.actualCategoryR}, 0);
    let tempTargetCategoryR = targetDataList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
    .reduce((accumulator, target) => {return accumulator + target.targetCategoryR}, 0);
    let tempActualCategoryJ = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
    .reduce((accumulator, actual) => {return accumulator + actual.actualCategoryJ}, 0);
    let tempTargetCategoryJ = targetDataList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
    .reduce((accumulator, target) => {return accumulator + target.targetCategoryJ}, 0);
    let tempProductivity = 0;
    const tempProductivityList = productivityDataList
    .filter(data => data.EmployeePositionCode === employee.EmployeePositionCode && data.EmployeeMRType === employee.EmployeeMRType);
    if (tempProductivityList && tempProductivityList.length > 0) {
        if (tempProductivityList.length === 1) {
            tempProductivity = tempProductivityList[0].Productivitas;
        } else {
            const tempSupervisorProductivity = productivityDataList.find(data => data.EmployeeAreaCode === employee.EmployeeAreaCode);
            if (tempSupervisorProductivity) {
                tempProductivity = tempSupervisorProductivity.Productivitas;
            } else {
                const tempDMProductivity = productivityDataList.find(data => employee.RayonCodeFilter.includes(data.RayonCode));
                if (tempDMProductivity) {
                    tempProductivity = tempDMProductivity.Productivitas;
                } else {
                    const tempEmptyProductivity = productivityDataList.find(data => !data.EmployeeAreaCode && !data.RayonCode);
                    if (tempEmptyProductivity) {
                        tempProductivity = tempEmptyProductivity.Productivitas;
                    }
                }
            }
        }
    }
    // if (['SUP', 'KOORD'].includes(employee.EmployeePositionCode) && employee.RayonCodeList
    // .some(data => ['ACEH', 'JAYAPURA', 'KUPANG', 'BATAM', 'JAMBI', 'MAGELANG'].includes(data.AreaBased))) {
    //     tempProductivity = new Number(450000000);
    // } else if ('DM' === employee.EmployeePositionCode && employee.RayonCodeFilter.includes('PLUOGBD01')) {
    //     tempProductivity = new Number(1300000000);
    // } else {
    //     if (['SUP', 'KOORD'].includes(employee.EmployeePositionCode)) {
    //         tempProductivity = productivityDataList
    //         .filter(data => data.EmployeePositionCode === 'SUP' && data.EmployeeMRType === employee.EmployeeMRType)
    //         .reduce((accumulator, item) => {return accumulator + item.Productivitas}, 0);
    //     } else {
    //         tempProductivity = productivityDataList
    //         .filter(data => data.EmployeePositionCode === employee.EmployeePositionCode && data.EmployeeMRType === employee.EmployeeMRType)
    //         .reduce((accumulator, item) => {return accumulator + item.Productivitas}, 0);
    //     }
    // }
    let countSubordinate = 0;

    if (employee.EmployeePositionCode !== 'MR') {
        const tempSubordinateList = Object.values(resultCalculate
        .filter(tempSubordinate => tempSubordinate.RayonCodeList
            .some(tempRayonCode => employee.RayonCodeFilter.includes(tempRayonCode.RayonCodeSup)))
        .reduce((result, item) => {
            const value = employee.RayonCode;
            const existing = result[value] || {
                rayonCode: value,
                actualAll: 0,
                targetAll: 0,
                pap: 0,
                actualJKN: 0,
                targetJKN: 0,
                actualReguler: 0,
                targetReguler: 0,
                countSubordinate: 0
            };
            return {
                ...result,
                [value] : {
                    ...existing,
                    actualAll: existing.actualAll + item.actualAll,
                    targetAll: existing.targetAll + item.targetAll,
                    pap: existing.pap + item.pap,
                    actualJKN: existing.actualJKN + item.actualJKN,
                    targetJKN: existing.targetJKN + item.targetJKN,
                    actualReguler: existing.actualReguler + item.actualReguler,
                    targetReguler: existing.targetReguler + item.targetReguler,
                    countSubordinate: existing.countSubordinate + 1
                }
            }
        }, {}));

        if (tempSubordinateList && tempSubordinateList.length > 0) {
            tempActual = tempActual + tempSubordinateList[0].actualAll;
            tempTarget = tempTarget + tempSubordinateList[0].targetAll;
            tempPAP = tempPAP + tempSubordinateList[0].pap;
            tempActualCategoryJ = tempActualCategoryJ + tempSubordinateList[0].actualJKN;
            tempTargetCategoryJ = tempTargetCategoryJ + tempSubordinateList[0].targetJKN;
            tempActualCategoryR = tempActualCategoryR + tempSubordinateList[0].actualReguler;
            tempTargetCategoryR = tempTargetCategoryR + tempSubordinateList[0].targetReguler;
            countSubordinate = tempSubordinateList[0].countSubordinate;
        }
    }

    objectSchema.actual = tempActual;
    objectSchema.target = tempTarget;

    if (objectSchema.actual <= 0 && objectSchema.target <= 0) {
        return null;
    }
    
    objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);

    employee.actualAll = objectSchema.actual;
    employee.targetAll = objectSchema.target;
    employee.actualTargetAll = objectSchema.actualTarget;
    employee.productivity = tempProductivity * 3;
    employee.maxTargetProductivity = Math.max(employee.targetAll, employee.productivity);
    employee.actualTargetMax = convertNaNToZero(employee.actualAll / employee.maxTargetProductivity * 100);
    employee.actualProductivity = convertNaNToZero(employee.actualAll / employee.productivity * 100);
    employee.pap = tempPAP;
    employee.discountPAP = convertNaNToZero(employee.pap / employee.actualAll * 100);
    employee.netSales = employee.actualAll - employee.pap;
    employee.nettoAP = convertNaNToZero(employee.netSales / employee.productivity * 100);
    employee.actualJKN = tempActualCategoryJ;
    employee.targetJKN = tempTargetCategoryJ;
    employee.actualTargetJKN = convertNaNToZero(employee.actualJKN / employee.targetJKN * 100);
    employee.actualReguler = tempActualCategoryR;
    employee.targetReguler = tempTargetCategoryR;
    employee.actualTargetReguler = convertNaNToZero(employee.actualReguler / employee.targetReguler * 100);
    if (employee.EmployeePositionCode !== 'MR') {
        employee.countSubordinate = countSubordinate;
    }

    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
    objectSchema.incentive = bonus;
    objectSchema.totalIncentive = bonus;
    
    employee.totalIncentive = (employee.totalIncentive || 0) + bonus;

    employee.schemaList = employee.schemaList || [];
    employee.schemaList.push(objectSchema);

    return employee;
}

module.exports = {
    incentiveATQuarter
}
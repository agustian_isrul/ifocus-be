const queryOperation = require('./query-operations');
const { reduceFilterTarikanList } = require('../../helpers/utility');

const calculateDXM = async (bodyData) => {
    try {
        const schemaList = await queryOperation.getSchemaList(bodyData.SchemeConfigId, bodyData.SchemeType);
        const filterTarikanDataList = await queryOperation.getFilterTarikanDataList(bodyData.SchemeConfigId, bodyData.SchemeType);
        const parameterBonusList = await queryOperation.getParameterBonusList(bodyData.SchemeConfigId, bodyData.SchemeType);
        const employeeDataList = await queryOperation.getEmployeeDataList(bodyData.LineName);
        const productivityDataList = await queryOperation.getProductivityDataList(bodyData.LineName);
        const targetProductList = await queryOperation.getTargetProductList(bodyData.LineName);
        const targetChannelList = await queryOperation.getTargetChannelList(bodyData.LineName);
        let resultCalculate = [];

        for (const schema of schemaList) {
            const filterDataListBySchema = filterTarikanDataList
            .filter(data => data.ConfigDetailId === schema.ConfigDetailId);            
            const filterParameterListBySchema = parameterBonusList
            .filter(data => data.ConfigDetailId === schema.ConfigDetailId);
            const filterAndTarikanList = reduceFilterTarikanList(filterDataListBySchema, 'AND');
            const filterOrTarikanList = reduceFilterTarikanList(filterDataListBySchema, 'OR');

            switch (schema.SchemaId) {
                case 1: {
                    switch (bodyData.LineName) {
                        case 'PROSPERITY': {
                            const { incentiveAT } = require('./prosperity/SchemaATFactory');
                            const actualDataList = await queryOperation.getActualProsperityList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            const targetDataList = await queryOperation.getTargetProsperityList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = await incentiveAT(
                                schema, resultCalculate, employeeDataList, actualDataList, targetDataList, productivityDataList,
                                filterDataListBySchema, filterParameterListBySchema);
                            actualDataList.length = 0;
                            targetDataList.length = 0;
                            break;
                        }
                        case 'HARMONY': {
                            const { incentiveAT } = require('./harmony/SchemaATFactory');
                            const actualDataList = await queryOperation.getActualHarmonyList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            const targetDataList = await queryOperation.getTargetHarmonyList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = await incentiveAT(
                                schema, resultCalculate, employeeDataList, actualDataList, targetDataList, productivityDataList,
                                filterDataListBySchema, filterParameterListBySchema);
                            actualDataList.length = 0;
                            targetDataList.length = 0;
                            break;
                        }
                        case 'OGB': {
                            const { incentiveAT } = require('./ogb/SchemaATFactory');
                            const actualDataList = await queryOperation.getActualDataList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            const targetDataList = await queryOperation.getTargetDataList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = await incentiveAT(
                                schema, resultCalculate, employeeDataList, actualDataList, targetDataList, productivityDataList,
                                filterDataListBySchema, filterParameterListBySchema);
                            actualDataList.length = 0;
                            targetDataList.length = 0;
                            break;
                        }
                        default: {
                            const { incentiveAT } = require('./victory/SchemaFactory');
                            const actualDataList = await queryOperation.getActualDataList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            const targetDataList = await queryOperation.getTargetDataList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = await incentiveAT(
                                schema, resultCalculate, employeeDataList, actualDataList, targetDataList, productivityDataList,
                                filterDataListBySchema, filterParameterListBySchema);
                            actualDataList.length = 0;
                            targetDataList.length = 0;
                            break;
                        }
                    }
                    break;
                }
                case 2: {
                    switch (bodyData.LineName) {
                        case 'ZETA': {
                            const { incentiveProductFocus } = require('./zeta/SchemaProductFocusFactory');
                            const actualProductList = await queryOperation.getActualProductZetaList(
                                bodyData, schema.ConfigDetailId, filterAndTarikanList, filterOrTarikanList);
                            const tempTargetProductList = await queryOperation.getTargetProductZetaList(
                                bodyData, schema.ConfigDetailId, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = await incentiveProductFocus(
                                schema, resultCalculate, actualProductList, tempTargetProductList,
                                filterDataListBySchema, filterParameterListBySchema);
                            actualProductList.length = 0;
                            tempTargetProductList.length = 0;
                            break;
                        }
                        case 'OGB': {
                            const { incentiveProductFocus } = require('./ogb/SchemaProductFocusFactory');
                            const actualProductList = await queryOperation.getActualProductZetaList(
                                bodyData, schema.ConfigDetailId, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = await incentiveProductFocus(
                                schema, resultCalculate, actualProductList, targetProductList,
                                filterDataListBySchema, filterParameterListBySchema);
                            actualProductList.length = 0;
                            break;
                        }
                        case 'VICTORY': {
                            const { incentiveProductFocus } = require('./victory/SchemaProductFocusFactory');
                            const actualProductList = await queryOperation.getActualProductList(
                                bodyData, schema.ConfigDetailId, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = await incentiveProductFocus(
                                schema, resultCalculate, actualProductList, targetProductList,
                                filterDataListBySchema, filterParameterListBySchema);
                            actualProductList.length = 0;
                            break;
                        }
                        case 'PROSPERITY': {
                            const { incentiveProductFocus } = require('./prosperity/SchemaProductFocusFactory');
                            const actualProductList = await queryOperation.getActualProductProsperityList(
                                bodyData, schema.ConfigDetailId, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = await incentiveProductFocus(
                                schema, resultCalculate, actualProductList, targetProductList,
                                filterDataListBySchema, filterParameterListBySchema);
                            actualProductList.length = 0;
                            break;
                        }
                        case 'HARMONY': {
                            const { incentiveProductFocus } = require('./harmony/SchemaProductFocusFactory');
                            const actualProductList = await queryOperation.getActualProductHarmonyList(
                                bodyData, schema.ConfigDetailId, filterAndTarikanList, filterOrTarikanList);
                            const tempTargetProductList = await queryOperation.getTargetProductHarmonyList(
                                bodyData, schema.ConfigDetailId, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = await incentiveProductFocus(
                                schema, resultCalculate, actualProductList, tempTargetProductList, targetProductList,
                                filterDataListBySchema, filterParameterListBySchema);
                            actualProductList.length = 0;
                            break;
                        }
                        default: break;
                    }
                    break;
                }
                case 3: {
                    switch (bodyData.LineName) {
                        case 'OGB' : {
                            const { incentiveGroupProductFokus } = require('./ogb/SchemaGroupProductFactory');
                            const actualProductList = await queryOperation.getActualProductZetaList(
                                bodyData, schema.ConfigDetailId, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = await incentiveGroupProductFokus(
                                schema, resultCalculate, actualProductList, targetProductList,
                                filterDataListBySchema, filterParameterListBySchema);
                            actualProductList.length = 0;
                            break;
                        }
                        default: {
                            const { incentiveGroupProductFokus } = require('./zeta/SchemaGroupProductFactory');
                            const actualProductList = await queryOperation.getActualProductZetaList(
                                bodyData, schema.ConfigDetailId, filterAndTarikanList, filterOrTarikanList);
                            const targetProductList = await queryOperation.getTargetProductZetaList(
                                bodyData, schema.ConfigDetailId, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = await incentiveGroupProductFokus(
                                schema, resultCalculate, actualProductList, targetProductList,
                                filterDataListBySchema, filterParameterListBySchema);
                            actualProductList.length = 0;
                            targetProductList.length = 0;
                            break;
                        }
                    }
                    break;
                }
                case 4: {
                    const { incentivePWAChannel } = require('./ogb/SchemaPWAChannelFactory');
                    const actualProductList = await queryOperation.getActualProductOGBList(
                        bodyData, filterAndTarikanList, filterOrTarikanList);
                    resultCalculate = incentivePWAChannel(
                        schema, resultCalculate, actualProductList, targetProductList, 
                        filterDataListBySchema, filterParameterListBySchema);
                    actualProductList.length = 0;
                    break;
                }
                case 5: {
                    switch (bodyData.LineName) {
                        case 'VICTORY': {
                            const { incentiveOutletProject } = require('./victory/SchemaOutletProjectFactory');
                            const actualChannelList = await queryOperation.getActualChannelList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = await incentiveOutletProject(
                                schema, resultCalculate, actualChannelList, targetChannelList,
                                filterDataListBySchema, filterParameterListBySchema);
                            actualChannelList.length = 0;
                            break;
                        }
                        case 'PROSPERITY': {
                            const { incentiveOutletProject } = require('./prosperity/SchemaOutletProjectFactory');
                            const actualChannelList = await queryOperation.getActualChannelProsperityList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = await incentiveOutletProject(
                                schema, resultCalculate, actualChannelList, targetChannelList,
                                filterDataListBySchema, filterParameterListBySchema);
                            actualChannelList.length = 0;
                            break;
                        }
                        case 'HARMONY': {
                            const { incentiveOutletProject } = require('./harmony/SchemaOutletProjectFactory');
                            resultCalculate = await incentiveOutletProject(
                                schema, resultCalculate, targetChannelList, filterDataListBySchema, filterParameterListBySchema);
                            break;
                        }
                        default: break;
                    }
                    break;
                }
                case 6: {
                    switch (bodyData.LineName) {
                        case 'VICTORY': {
                            const { incentiveAddTotal } = require('./victory/SchemaAddTotalFactory');
                            resultCalculate = await incentiveAddTotal(
                                schema, resultCalculate, filterDataListBySchema, filterParameterListBySchema);
                            break;
                        }
                        case 'PROSPERITY': {
                            const { incentiveAddTotal } = require('./prosperity/SchemaAddTotalFactory');
                            resultCalculate = await incentiveAddTotal(
                                schema, resultCalculate, filterDataListBySchema, filterParameterListBySchema);
                            break;
                        }
                        case 'HARMONY': {
                            const { incentiveAddTotal } = require('./harmony/SchemaAddTotalFactory');
                            resultCalculate = await incentiveAddTotal(
                                schema, resultCalculate, filterDataListBySchema, filterParameterListBySchema);
                            break;
                        }
                        case 'OGB': {
                            const { incentiveAddTotal } = require('./ogb/SchemaAddTotalFactory');
                            const targetDataList = await queryOperation.getPreviousQuarterActualList(bodyData.LineName);
                            resultCalculate = await incentiveAddTotal(
                                schema, resultCalculate, targetDataList, filterDataListBySchema, filterParameterListBySchema);
                            targetDataList.length = 0;
                            break;
                        }
                        default: break;
                    }
                    break;
                }
                case 8: {
                    switch (bodyData.LineName) {
                        case 'VICTORY': {
                            const { incentiveGrowth } = require('./victory/SchemaGrowthFactory');
                            const actualDataList = await queryOperation.getActualDataList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            const previousQuarterActualList = await queryOperation.getPreviousQuarterActualList(bodyData.LineName);
                            resultCalculate = incentiveGrowth(
                                schema, resultCalculate, employeeDataList, actualDataList, previousQuarterActualList,
                                filterDataListBySchema, filterParameterListBySchema);
                            break;
                        }
                        case 'PROSPERITY': {
                            const { incentiveGrowth } = require('./prosperity/SchemaGrowthFactory');
                            const previousQuarterActualList = await queryOperation.getPreviousQuarterActualList(bodyData.LineName);
                            resultCalculate = incentiveGrowth(
                                schema, resultCalculate, previousQuarterActualList, 
                                filterDataListBySchema, filterParameterListBySchema);
                            break;
                        }
                        case 'OGB': {
                            const { incentiveGrowth } = require('./ogb/SchemaGrowthFactory');
                            const actualDataList = await queryOperation.getActualDataList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            const targetDataList = await queryOperation.getPreviousQuarterActualList(bodyData.LineName);
                            resultCalculate = incentiveGrowth(
                                schema, resultCalculate, actualDataList, targetDataList, 
                                filterDataListBySchema, filterParameterListBySchema);
                            targetDataList.length = 0;
                            actualDataList.length = 0;
                            break;
                        }
                        default: break;
                    }
                    break;
                }
                case 9: {
                    switch (bodyData.LineName) {
                        case 'ZETA': {
                            const { incentiveTarikanSalesHarian } = require('./zeta/SchemaTarikanSalesHarianFactory');
                            const actualSalesHarianList = await queryOperation.getActualSalesHarianList(
                                bodyData, schema.ConfigDetailId, filterAndTarikanList, filterOrTarikanList);
                            const targetSalesHarianList = await queryOperation.getTargetDataList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = await incentiveTarikanSalesHarian(
                                schema, resultCalculate, actualSalesHarianList, targetSalesHarianList,
                                filterDataListBySchema, filterParameterListBySchema);
                            actualSalesHarianList.length = 0;
                            targetSalesHarianList.length = 0;
                            break;
                        }
                        case 'OGB': {
                            const { incentiveTarikanSales } = require('./ogb/SchemaTarikanSalesFactory');
                            const actualSalesHarianList = await queryOperation.getActualSalesHarianList(
                                bodyData, schema.ConfigDetailId, filterAndTarikanList, filterOrTarikanList);
                            const targetSalesHarianList = await queryOperation.getTargetDataList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            let areaGroupList = [];
                            const employeeMRTypeFilter = filterDataListBySchema.find(data => data.FilterCode === 'EmployeeMRType');
                            if (employeeMRTypeFilter && ['APOTIK', 'HOSPITAL', 'COMBO'].includes(employeeMRTypeFilter.FilterValue)) {
                                areaGroupList = await queryOperation.getAreaGroupList(bodyData.LineName);
                            }
                            resultCalculate = await incentiveTarikanSales(
                                schema, resultCalculate, actualSalesHarianList, targetSalesHarianList, areaGroupList,
                                filterDataListBySchema, filterParameterListBySchema);
                            actualSalesHarianList.length = 0;
                            targetSalesHarianList.length = 0;
                            break;
                        }
                        default: break;
                    }
                    break;
                }
                case 10: {
                    const { incentivePenalty } = require('./zeta/SchemaPenaltyFactory');
                    const actualProductList = await queryOperation.getActualProductZetaList(
                        bodyData, schema.ConfigDetailId, filterAndTarikanList, filterOrTarikanList);
                    const targetProductList = await queryOperation.getTargetProductZetaList(
                        bodyData, schema.ConfigDetailId, filterAndTarikanList, filterOrTarikanList);
                    resultCalculate = await incentivePenalty(
                        schema, resultCalculate, actualProductList, targetProductList,
                        filterDataListBySchema, filterParameterListBySchema);
                    actualProductList.length = 0;
                    targetProductList.length = 0;
                    break;
                }
                case 11: {
                    switch (bodyData.LineName) {
                        case 'VICTORY': {
                            const { incentiveRerataSubordinate } = require('./victory/SchemaRerataSubordinateFactory');
                            resultCalculate = incentiveRerataSubordinate(
                                schema, resultCalculate, employeeDataList, filterDataListBySchema, filterParameterListBySchema
                            );
                            break;
                        }
                        case 'PROSPERITY': {
                            const { incentiveRerata } = require('./prosperity/SchemaRerataFactory');
                            resultCalculate = incentiveRerata(
                                schema, resultCalculate, filterDataListBySchema, filterParameterListBySchema
                            );
                            break;
                        }
                        case 'HARMONY': {
                            const { incentiveRerataSubordinate } = require('./harmony/SchemaRerataSubordinateFactory');
                            resultCalculate = incentiveRerataSubordinate(
                                schema, resultCalculate, filterDataListBySchema, filterParameterListBySchema
                            );
                            break;
                        }
                        case 'OGB': {
                            const { incentiveRerata } = require('./ogb/SchemaRerataFactory');
                            resultCalculate = incentiveRerata(
                                schema, resultCalculate, filterDataListBySchema, filterParameterListBySchema
                            );
                            break;
                        }
                        default: break;
                    }
                    break;
                }
                case 12: {
                    switch (bodyData.LineName) {
                        case 'VICTORY': {
                            const { incentiveTarikanSalesTim } = require('./victory/SchemaTarikanSalesTimFactory');
                            const criteriaCabangList = await queryOperation.getCriteriaCabangList(bodyData.LineName);
                            const actualDataCabangList = await queryOperation.getActualDataCabangList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            const targetDataCabangList = await queryOperation.getTargetDataCabangList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = incentiveTarikanSalesTim(
                                schema, resultCalculate, criteriaCabangList, actualDataCabangList, 
                                targetDataCabangList, filterParameterListBySchema);
                            break;
                        }
                        case 'HARMONY': {
                            const { incentiveTarikanSalesTim } = require('./harmony/SchemaTarikanSalesTimFactory');
                            const criteriaCabangList = await queryOperation.getCriteriaCabangList(bodyData.LineName);
                            const actualDataCabangList = await queryOperation.getActualCabangHarmonyList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            const targetDataCabangList = await queryOperation.getTargetCabangHarmonyList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = incentiveTarikanSalesTim(
                                schema, resultCalculate, criteriaCabangList, actualDataCabangList, 
                                targetDataCabangList, filterParameterListBySchema);
                            break;
                        }
                        default: break;
                    }
                    break;
                }
                case 13: {
                    switch (bodyData.LineName) {
                        case 'OGB': {
                            const { incentiveATQuarter } = require('./ogb/SchemaATQuarterFactory');
                            const actualDataList = await queryOperation.getActualDataList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            const targetDataList = await queryOperation.getTargetDataList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = await incentiveATQuarter(
                                schema, resultCalculate, employeeDataList, actualDataList, targetDataList, productivityDataList,
                                filterDataListBySchema, filterParameterListBySchema);
                            actualDataList.length = 0;
                            targetDataList.length = 0;
                            break;
                        }
                        default: {
                            const { incentiveAT } = require('./victory/SchemaFactory');
                            const actualDataList = await queryOperation.getActualDataList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            const targetDataList = await queryOperation.getTargetDataList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = await incentiveAT(
                                schema, resultCalculate, employeeDataList, actualDataList, targetDataList, productivityDataList,
                                filterDataListBySchema, filterParameterListBySchema);
                            actualDataList.length = 0;
                            targetDataList.length = 0;
                            break;
                        }
                    }
                    break;
                }
                case 14: {
                    const { incentiveQuarterHattrick } = require('./zeta/SchemaQuarterHattrickFactory');
                    const actualDataList = await queryOperation.getActualDataList(
                        bodyData, filterAndTarikanList, filterOrTarikanList);
                    const targetDataList = await queryOperation.getTargetDataList(
                        bodyData, filterAndTarikanList, filterOrTarikanList);
                    resultCalculate = await incentiveQuarterHattrick(
                        schema, resultCalculate, actualDataList, targetDataList, bodyData.monthFrom, bodyData.monthTo, 
                        filterDataListBySchema, filterParameterListBySchema);
                    actualDataList.length = 0;
                    targetDataList.length = 0;
                    break;
                }
                case 15: {
                    switch (bodyData.LineName) {
                        case 'OGB': {
                            const { incentiveQuarterGrowth } = require('./ogb/SchemaQuarterGrowthFactory');
                            const actualDataList = await queryOperation.getActualDataList(
                                bodyData, filterAndTarikanList, filterOrTarikanList
                            );
                            const tempBody = Object.create(bodyData);
                            const tempPeriodFrom = new Date(tempBody.PeriodeFrom.getFullYear(), tempBody.PeriodeFrom.getMonth() - 3, 1);
                            tempBody.monthFrom = tempPeriodFrom.getMonth() + 1;
                            tempBody.yearFrom = tempPeriodFrom.getFullYear();
                            const tempPeriodTo = new Date(tempBody.PeriodeTo.getFullYear(), tempBody.PeriodeTo.getMonth() - 2, 0);
                            tempBody.monthTo = tempPeriodTo.getMonth() + 1;
                            tempBody.yearTo = tempPeriodTo.getFullYear();
                            const targetDataList = await queryOperation.getActualDataList(
                                tempBody, filterAndTarikanList, filterOrTarikanList
                            );
                            resultCalculate = await incentiveQuarterGrowth(
                                schema, resultCalculate, actualDataList, targetDataList, 
                                filterDataListBySchema, filterParameterListBySchema
                            );
                            actualDataList.length = 0;
                            targetDataList.length = 0;
                            break;
                        }
                        default: {
                            const { incentiveQuarterGrowth } = require('./zeta/SchemaQuarterGrowthFactory');
                            const actualDataList = await queryOperation.getActualDataList(
                                bodyData, filterAndTarikanList, filterOrTarikanList
                            );
                            const tempBody = Object.create(bodyData);
                            const tempPeriodFrom = new Date(tempBody.PeriodeFrom.getFullYear(), tempBody.PeriodeFrom.getMonth() - 3, 1);
                            tempBody.monthFrom = tempPeriodFrom.getMonth() + 1;
                            tempBody.yearFrom = tempPeriodFrom.getFullYear();
                            const tempPeriodTo = new Date(tempBody.PeriodeTo.getFullYear(), tempBody.PeriodeTo.getMonth() - 2, 0);
                            tempBody.monthTo = tempPeriodTo.getMonth() + 1;
                            tempBody.yearTo = tempPeriodTo.getFullYear();
                            const targetDataList = await queryOperation.getActualDataList(
                                tempBody, filterAndTarikanList, filterOrTarikanList
                            );
                            resultCalculate = await incentiveQuarterGrowth(
                                schema, resultCalculate, actualDataList, targetDataList, 
                                filterDataListBySchema, filterParameterListBySchema
                            );
                            actualDataList.length = 0;
                            targetDataList.length = 0;
                            break;
                        }
                    }
                    break;
                }
                case 16: {
                    const { incentiveQuartalTim } = require('./victory/SchemaQuartalTimFactory');
                    const criteriaCabangList = await queryOperation.getCriteriaCabangList(bodyData.LineName);
                    const actualDataCabangList = await queryOperation.getActualDataCabangList(
                        bodyData, filterAndTarikanList, filterOrTarikanList);
                    const targetDataCabangList = await queryOperation.getTargetDataCabangList(
                        bodyData, filterAndTarikanList, filterOrTarikanList);
                    resultCalculate = incentiveQuartalTim(
                        schema, resultCalculate, criteriaCabangList, actualDataCabangList, 
                        targetDataCabangList, filterParameterListBySchema);
                    break;
                }
                case 17: {
                    switch (bodyData.LineName) {
                        case 'OGB': {
                            const { incentivePBFChannel } = require('./ogb/SchemaPBFChannelFactory');
                            const actualDataList = await queryOperation.getActualDataList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = incentivePBFChannel(
                                schema, resultCalculate, actualDataList, filterDataListBySchema, filterParameterListBySchema
                            );
                            break;
                        }
                        default: {
                            const { incentiveExtra } = require('./prosperity/SchemaExtraFactory');
                            resultCalculate = incentiveExtra(schema, resultCalculate, 
                                filterDataListBySchema, filterParameterListBySchema);
                            break;
                        }
                    }
                    break;
                }
                case 18: {                    
                    const { incentiveTrading } = require('./prosperity/SchemaTradingFactory');
                    const actualDataList = await queryOperation.getActualProsperityList(
                        bodyData, filterAndTarikanList, filterOrTarikanList);
                    const targetDataList = await queryOperation.getTargetProsperityList(
                        bodyData, filterAndTarikanList, filterOrTarikanList);
                    resultCalculate = await incentiveTrading(
                        schema, resultCalculate, actualDataList, targetDataList, 
                        filterDataListBySchema, filterParameterListBySchema);
                    actualDataList.length = 0;
                    targetDataList.length = 0;
                    break;
                }
                case 19: {                    
                    const { incentiveATReguler } = require('./harmony/SchemaATRegulerFactory');
                    resultCalculate = await incentiveATReguler(
                        schema, resultCalculate, filterDataListBySchema, filterParameterListBySchema);
                    break;
                }
                case 22: {                    
                    const { incentiveOwnProduct } = require('./harmony/SchemaOwnProductFactory');
                    const actualProductList = await queryOperation.getActualProductHarmonyList(
                        bodyData, schema.ConfigDetailId, filterAndTarikanList, filterOrTarikanList);
                    const tempTargetProductList = await queryOperation.getTargetProductHarmonyList(
                        bodyData, schema.ConfigDetailId, filterAndTarikanList, filterOrTarikanList);
                    resultCalculate = await incentiveOwnProduct(
                        schema, resultCalculate, actualProductList, tempTargetProductList, 
                        filterDataListBySchema, filterParameterListBySchema);
                    break;
                }
                case 23: {                    
                    const { incentiveMRSpecialist } = require('./harmony/SchemaATMRSpecialistFactory');
                    resultCalculate = await incentiveMRSpecialist(
                        schema, resultCalculate, filterDataListBySchema, filterParameterListBySchema);
                    break;
                }
                case 24: {                    
                    const { incentiveFocusMobile } = require('./ogb/SchemaFocusMobileFactory');
                    const targetDataList = await queryOperation.getIFocusMobileList(bodyData.LineName);
                    resultCalculate = await incentiveFocusMobile(
                        schema, resultCalculate, targetDataList, filterDataListBySchema, filterParameterListBySchema);
                    targetDataList.length = 0;
                    break;
                }
                case 25: {
                    const { incentiveATNavyCabang } = require('./victory/SchemaATNavyCabangFactory');
                    const actualNavyCabangList = await queryOperation.getNavyCabangList(
                        bodyData.LineName, bodyData.monthFrom, bodyData.monthTo, bodyData.yearFrom, bodyData.yearTo);
                    resultCalculate = await incentiveATNavyCabang(
                        schema, resultCalculate, actualNavyCabangList, filterDataListBySchema, filterParameterListBySchema
                    );
                    actualNavyCabangList.length = 0;
                    break;
                }
                case 27: {
                    switch (bodyData.LineName) {
                        case 'VICTORY': {
                            const { incentiveAreaBinaan } = require('./victory/SchemaAreaBinaanFactory');
                            resultCalculate = incentiveAreaBinaan(
                                schema, resultCalculate, employeeDataList, filterDataListBySchema, filterParameterListBySchema
                            );
                            break;
                        }
                        case 'HARMONY': {
                            const { incentiveBinaanDSM } = require('./harmony/SchemaBinaanDSMFactory');
                            resultCalculate = incentiveBinaanDSM(
                                schema, resultCalculate, employeeDataList, filterDataListBySchema, filterParameterListBySchema
                            );
                            break;
                        }
                        default: break;
                    }
                    break;
                }
                case 28: {
                    const { incentiveSuprasma } = require('./victory/SchemaSuprasmaFactory');
                    const actualDataList = await queryOperation.getActualDataList(bodyData, filterAndTarikanList, filterOrTarikanList);
                    resultCalculate = incentiveSuprasma(
                        schema, resultCalculate, employeeDataList, actualDataList, targetProductList,
                        filterDataListBySchema, filterParameterListBySchema);
                    break;
                }
                default:
                    break;
            }
        }
        
        return resultCalculate;
    } catch (error) {
        throw error;
    }
}

module.exports = {
    calculateDXM
}
const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveATNavyCabang = (
    schema, resultCalculate, navyCabangList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;
        
        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode
        }

        let accumulatedNavy = navyCabangList.find(navy => navy.EmployeeAreaCode === employee.EmployeeAreaCode);
        if (!accumulatedNavy) return employee;
        const areaGabunganSplit = accumulatedNavy.AreaGabungan.split('+').map(kata => kata.trim());
        navyCabangList.filter(navy => areaGabunganSplit.includes(navy.EmployeeAreaCode))
        .forEach(navy => {
            objectSchema.actual = (objectSchema.actual || 0) + navy.SalesValue;
            objectSchema.target = (objectSchema.target || 0) + navy.TargetValue;
        });
        
        objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
    
        const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = bonus;

        employee.schemaList.push(objectSchema);

        employee.totalIncentive = (employee.totalIncentive || 0) + bonus;

        return employee;
    });
}

module.exports = {
    incentiveATNavyCabang
}
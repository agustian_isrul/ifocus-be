const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    validateByTargetType,
    calculatePassword
} = require('../../../helpers/utility');

const incentiveSuprasma = (
    schema, resultCalculate, employeeDataList, actualDataList, suprasmaTargetUnitList, 
    filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        const objectSchema = {
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode
        }
        
        objectSchema.actual = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
        .reduce((accumulator, actual) => {return accumulator + actual.unitActual}, 0);

        if (employee.EmployeePositionCode === 'SUP' || employee.EmployeePositionCode === 'KOORD') {
            const subordinateEmployeeList = employeeDataList
            .filter(subordinate => employee.RayonCodeFilter.includes(subordinate.RayonCodeSup));
            objectSchema.actual = objectSchema.actual + actualDataList
            .filter(actual => subordinateEmployeeList.some(subordinate => subordinate.RayonCode === actual.RayonCode))
            .reduce((accumulator, actual) => {return accumulator + actual.unitActual}, 0);
            objectSchema.target = suprasmaTargetUnitList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
            .reduce((accumulator, target) => {return accumulator + target.target}, 0);
        } else if (employee.EmployeePositionCode === 'DM') {
            const subordinateEmployee = employeeDataList.find(subordinate => 
                employee.RayonCodeFilter.includes(subordinate.RayonCodeSup) && subordinate.DummyPimda === 1);
            if (!subordinateEmployee) {
                return employee;
            }
            const dummyPimdaEmployee = resultCalculate.find(currentEmployee => 
                currentEmployee.RayonCodeFilter.includes(subordinateEmployee.RayonCode));
            const tempSchemaSuprasma = dummyPimdaEmployee.schemaList.find(tempSchema => tempSchema.schemaId === schema.SchemaId);
            if (tempSchemaSuprasma && tempSchemaSuprasma.actual > 0) {
                objectSchema.actual = (objectSchema.actual || 0) + tempSchemaSuprasma.actual;
                objectSchema.target = (objectSchema.target || 0) + tempSchemaSuprasma.target;
            }
        }
        
        objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
    
        const bonus = calculateStaticBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = bonus;

        employee.schemaList.push(objectSchema);

        employee.totalIncentive = (employee.totalIncentive || 0) + bonus;

        return employee;
    });
}

const calculateStaticBonus = (employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList) => {
    const isExistPassword = calculatePassword(employee, objectSchema, preeliminationAndList, preeliminationOrList);
    if (isExistPassword === 0) {
        return 0;
    }

    if (clasificationList && clasificationList.length > 0) {
        const objectClassification = clasificationList.find(criteria => validateByTargetType(
            criteria.TargetTypeH, employee, objectSchema, criteria.Operator2H, criteria.ValueFromH, criteria.ValueToH
        ));
        if (objectClassification && objectClassification.detail && objectClassification.detail.length > 0) {
            const objectIncentive = objectClassification.detail.find(criteriaDetail => validateByTargetType(
                criteriaDetail.TargetTypeD, employee, objectSchema, criteriaDetail.Operator2D, 
                criteriaDetail.ValueFromD, criteriaDetail.ValueToD
            ) && criteriaDetail.Bonus);
            if (objectIncentive) {
                return objectSchema.actual * objectIncentive.Bonus;
            }
        }
    }
    return 0;
}

module.exports = {
    incentiveSuprasma
}
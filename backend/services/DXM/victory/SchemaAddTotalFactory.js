const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    validateByTargetType,
    calculatePassword
} = require('../../../helpers/utility');

const incentiveAddTotal = (schema, resultCalculate, filterDataListBySchema, filterParameterListBySchema) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');

    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode
        }
        
        objectSchema.actual = employee.actualAll;
        objectSchema.target = employee.targetAll;
        objectSchema.actualTarget = employee.actualTargetAll;
    
        const bonus = calculateStaticBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = bonus;

        employee.schemaList.push(objectSchema);

        employee.totalIncentive = (employee.totalIncentive || 0) + bonus;

        return employee;
    });
}

const calculateStaticBonus = (employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList) => {
    if (calculatePassword(employee, objectSchema, preeliminationAndList, preeliminationOrList) === 0) {
        return 0;
    }

    let bonus = 0;
    if (clasificationList && clasificationList.length > 0) {
        const objectClassification = clasificationList.find(criteria => validateByTargetType(
            criteria.TargetTypeH, employee, objectSchema, criteria.Operator2H, criteria.ValueFromH, criteria.ValueToH
        ));
        if (objectClassification && objectClassification.detail && objectClassification.detail.length > 0) {
            const objectIncentive = objectClassification.detail.find(criteriaDetail => validateByTargetType(
                criteriaDetail.TargetTypeD, employee, objectSchema, criteriaDetail.Operator2D, 
                criteriaDetail.ValueFromD, criteriaDetail.ValueToD
            ) && criteriaDetail.Bonus);
            if (objectIncentive) {
                let tempValueFrom = objectIncentive.ValueFromD;
                if (!objectIncentive.TargetTypeH && objectIncentive.ValueFromH) {
                    tempValueFrom = objectIncentive.ValueFromH;
                }
                if ((employee.EmployeePositionCode === 'SUP' || employee.EmployeePositionCode === 'KOORD') && 
                employee.EmployeeSubLineDesc === 'VICTORY') {
                    bonus = Math.round(tempValueFrom / 100 * (objectSchema.actual - objectSchema.target));
                } else {
                    bonus = Math.round(tempValueFrom / 100 * (objectSchema.actual - employee.maxTargetProductivity));
                }
                bonus = (bonus >= objectIncentive.Bonus) ? objectIncentive.Bonus : bonus;
            }
        }
    }
    return bonus > 0 ? bonus : 0;
}

module.exports = {
    incentiveAddTotal
}
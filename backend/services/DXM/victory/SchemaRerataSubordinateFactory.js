const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    calculatePassword,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveRerataSubordinate = (
    schema, resultCalculate, employeeDataList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode
        }
        
        objectSchema.actual = employee.actualAll;
        objectSchema.target = employee.targetAll;
        objectSchema.actualTarget = employee.actualTargetAll;
        
        const employeeSubordinateList = employeeDataList
        .filter(subordinate => employee.RayonCodeFilter.includes(subordinate.RayonCodeSup) && subordinate.DummyPimda === 0);
        const existingEmployeeList = resultCalculate.filter(currentEmployee => 
            employeeSubordinateList.some(subordinate => currentEmployee.RayonCodeFilter.includes(subordinate.RayonCode)));
        if (employee.EmployeeSubLineDesc === 'VICTORY') {
            objectSchema.countSubordinate = existingEmployeeList.length;
        }
        existingEmployeeList.forEach(currentEmployee => {
            if (employee.EmployeeSubLineDesc === 'NAVY' && currentEmployee.actualTargetAll >= 100) {
                objectSchema.countSubordinate = (objectSchema.countSubordinate || 0) + 1;
            }
            const objectBonus23 = currentEmployee.schemaList
            .find(tempSchema => tempSchema.schemaId === 1);
            objectSchema.totalIncentiveAT = (objectSchema.totalIncentiveAT || 0) + 
            (objectBonus23.totalIncentive || 0);
            if (employee.EmployeePositionCode === 'DM') {
                const objectBonus6 = currentEmployee.schemaList
                .find(tempSchema => tempSchema.schemaId === 6);
                objectSchema.totalIncentiveAddTotal = (objectSchema.totalIncentiveAddTotal || 0) + 
                (objectBonus6.totalIncentive || 0);
            }
        });
    
        let bonus = 0;
        if (employee.EmployeeSubLineDesc === 'VICTORY') {
            bonus = calculateStaticBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList);
        } else {
            bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        }
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = bonus;

        employee.schemaList.push(objectSchema);

        employee.totalIncentive = (employee.totalIncentive || 0) + bonus;

        return employee;
    });
}

const calculateStaticBonus = (employee, objectSchema, preeliminationAndList, preeliminationOrList) => {
    const isExistPassword = calculatePassword(employee, objectSchema, preeliminationAndList, preeliminationOrList);
    if (isExistPassword === 0) {
        return 0;
    }
    if (objectSchema.countSubordinate && objectSchema.countSubordinate > 0) {
        if (employee.EmployeePositionCode === 'SUP' || employee.EmployeePositionCode === 'KOORD') {
            return Math.round(objectSchema.totalIncentiveAT / objectSchema.countSubordinate);
        }
        if (employee.EmployeePositionCode === 'DM') {
            return Math.round((objectSchema.totalIncentiveAddTotal + objectSchema.totalIncentiveAT) / objectSchema.countSubordinate);
        }
    }
    return 0;
}

module.exports = {
    incentiveRerataSubordinate
}
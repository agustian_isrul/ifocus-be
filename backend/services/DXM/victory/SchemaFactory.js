const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    isExistEmployee,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveAT = (
    schema, resultCalculate, employeeDataList, actualDataList, targetDataList, productivityDataList, 
    filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const filterEmployeeList = convertArrayToNestedArray(employeeDataList
    .filter(currentEmployee => filterEmployeeDataList
        .every(element => element.FilterValueList[0].FlagInclude === 1 ? 
            element.FilterValueFilter.includes(currentEmployee[element.FilterCode]) : 
            !element.FilterValueFilter.includes(currentEmployee[element.FilterCode])
        )
    ),
    'EmployeeID', 'RayonCode', 'EmployeeSubLineDesc');

    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);

    for (let employee of filterEmployeeList) {
        let currentEmployee = isExistEmployee(employee, resultCalculate);
        if (!currentEmployee) {
            currentEmployee = employee;
            currentEmployee.EmployeeName = employee.RayonCodeList[0].EmployeeName;
            currentEmployee.EmployeePositionCode = employee.RayonCodeList[0].EmployeePositionCode;
            currentEmployee.EmployeeName = employee.RayonCodeList[0].EmployeeName;
            currentEmployee.EmployeeAreaCode = employee.RayonCodeList[0].EmployeeAreaCode;
            currentEmployee.EmployeeSubLineDesc = employee.RayonCodeList[0].EmployeeSubLineDesc;
            currentEmployee.EmployeeMRType = employee.RayonCodeList[0].EmployeeMRType;
            currentEmployee.EmployeeVacant = employee.RayonCodeList[0].EmployeeVacant;
            const tempCurrentEmployee = setupEmployeeProperties(
                currentEmployee, schema, resultCalculate, actualDataList, targetDataList, productivityDataList, 
                preeliminationAndList, preeliminationOrList, clasificationList
            )
            if (tempCurrentEmployee) {
                resultCalculate = resultCalculate || [];
                resultCalculate.push(tempCurrentEmployee);
            }
        } else {
            currentEmployee = calculateExistingEmployee(
                currentEmployee, schema, preeliminationAndList, preeliminationOrList, clasificationList
            );
            let dataEmployee = resultCalculate
            .find(data => data.RayonCodeFilter.includes(currentEmployee.RayonCodeFilter[0]));
            dataEmployee = currentEmployee;
        }
    }

    return resultCalculate;
}

const setupEmployeeProperties = (
    employee, schema, resultCalculate, actualDataList, targetDataList, productivityDataList, 
    preeliminationAndList, preeliminationOrList, clasificationList
) => {
    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode
    }

    objectSchema.actual = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
    .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
    objectSchema.target = targetDataList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
    .reduce((accumulator, target) => {return accumulator + target.target}, 0);
    employee.productivity = findProductivityValue(employee, productivityDataList);

    if (employee.EmployeePositionCode !== 'MR') {
        resultCalculate.filter(existingEmployee => existingEmployee.RayonCodeList.some(filterRayonCode => 
            employee.RayonCodeFilter.includes(filterRayonCode.RayonCodeSup)))
        .forEach(existingEmployee => {
            objectSchema.actual = objectSchema.actual + existingEmployee.actualAll;
            objectSchema.target = objectSchema.target + existingEmployee.targetAll;
            employee.productivity = employee.productivity + existingEmployee.productivity
        });
    }

    if (objectSchema.actual <= 0 && objectSchema.target <= 0) {
        return null;
    }
    
    objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);

    employee.actualAll = objectSchema.actual;
    employee.targetAll = objectSchema.target;
    employee.actualTargetAll = objectSchema.actualTarget;
    employee.maxTargetProductivity = Math.max(objectSchema.target, employee.productivity);
    employee.actualTargetMax = convertNaNToZero(objectSchema.actual / employee.maxTargetProductivity * 100);
    employee.actualProductivity = convertNaNToZero(objectSchema.actual / employee.productivity * 100);

    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
    objectSchema.incentive = bonus;
    objectSchema.totalIncentive = bonus;
    
    employee.totalIncentive = (employee.totalIncentive || 0) + bonus;

    employee.schemaList = employee.schemaList || [];
    employee.schemaList.push(objectSchema);

    return employee;
}

const calculateExistingEmployee = (employee, schema, preeliminationAndList, preeliminationOrList, clasificationList) => {
    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode,
        actual : employee.actualAll,
        target : employee.targetAll,
        actualTarget: employee.actualTargetAll
    }
    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
    objectSchema.incentive = bonus;
    objectSchema.totalIncentive = bonus;
    
    employee.totalIncentive = (employee.totalIncentive || 0) + bonus;

    employee.schemaList = employee.schemaList || [];
    employee.schemaList.push(objectSchema);

    return employee;
}

const findProductivityValue = (employee, productivityList) => {
    return (productivityList
        .filter(productivity => productivity.EmployeeMRType === employee.EmployeeMRType &&
            productivity.EmployeePositionCode === employee.EmployeePositionCode &&
            productivity.EmployeeSubLineDesc === employee.EmployeeSubLineDesc) || [])
        .reduce((accumulator, productivity) => {return accumulator + productivity.Productivitas}, 0);
    // if (employee.EmployeeMRType === 'APOTIK' && employee.EmployeePositionCode === 'MR' && 
    // employee.EmployeeSubLineDesc !== 'N') {
    //     return (productivityList
    //     .filter(productivity => productivity.EmployeeMRType === employee.EmployeeMRType 
    //         && productivity.EmployeePositionCode === employee.EmployeePositionCode) || [])
    //     .reduce((accumulator, productivity) => {return accumulator + productivity.Productivitas}, 0);
    // } 
    
    // if (employee.EmployeeMRType !== 'APOTIK' && employee.EmployeePositionCode === 'MR' && 
    // employee.EmployeeSubLineDesc !== 'N') {
    //     return (productivityList
    //     .filter(productivity => productivity.EmployeeMRType === employee.EmployeeMRType 
    //         && productivity.EmployeeSubLineDesc === employee.EmployeeSubLineDesc
    //         && productivity.EmployeePositionCode === employee.EmployeePositionCode) || [])
    //     .reduce((accumulator, productivity) => {return accumulator + productivity.Productivitas}, 0);
    // } 
    
    // if (employee.EmployeeSubLineDesc === 'VICTORY' &&
    // (employee.EmployeePositionCode === 'SUP' || employee.EmployeePositionCode === 'KOORD')) {
    //     return (productivityList
    //     .filter(productivity => productivity.EmployeeMRType === 'COMBO'
    //         && productivity.EmployeePositionCode === 'SUP') || [])
    //     .reduce((accumulator, productivity) => {return accumulator + productivity.Productivitas}, 0);
    // } 
    
    // if (employee.EmployeeSubLineDesc === 'N' && employee.EmployeePositionCode === 'MR') {
    //     return (productivityList
    //     .filter(productivity => productivity.EmployeeSubLineDesc === employee.EmployeeSubLineDesc
    //         && productivity.EmployeePositionCode === 'MR') || [])
    //     .reduce((accumulator, productivity) => {return accumulator + productivity.Productivitas}, 0);
    // } 
    
    // if (employee.EmployeeSubLineDesc === 'NAVY' && 
    // (employee.EmployeePositionCode === 'SUP' || employee.EmployeePositionCode === 'KOORD')) {
    //     return (productivityList
    //     .filter(productivity => productivity.EmployeeSubLineDesc === 'N'
    //         && productivity.EmployeePositionCode === 'SUP') || [])
    //     .reduce((accumulator, productivity) => {return accumulator + productivity.Productivitas}, 0);
    // }

    // return 0;
}

module.exports = {
    incentiveAT
}
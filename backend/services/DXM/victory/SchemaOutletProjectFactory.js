const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveOutletProject = (
    schema, resultCalculate, actualChannelList, targetChannelList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;
        
        const tempActualChannelList = actualChannelList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode));

        let tempObjectSchemaList = [];
        if (employee.EmployeePositionCode !== 'MR') {
            resultCalculate.filter(existingEmployee => existingEmployee.RayonCodeList.some(filterRayonCode => 
                employee.RayonCodeFilter.includes(filterRayonCode.RayonCodeSup)))
            .forEach(existingEmployee => {
                const tempSchemaList = existingEmployee.schemaList
                .filter(tempSchema => tempSchema.schemaId === schema.SchemaId) || [];
                tempObjectSchemaList = tempObjectSchemaList.concat(tempSchemaList);
            });
            if (employee.EmployeePositionCode === 'DM') {
                tempObjectSchemaList = Object.values(tempObjectSchemaList.reduce((res, item) => {
                    const value = item['schemaId']
                    const existing = res[value] || {['schemaId']: value, ['actual']: 0, ['target']: 0}
                    return {
                        ...res,
                        [value] : {
                            ...existing,
                            ['actual']: existing['actual'] + item['actual'],
                            ['target']: existing['target'] + item['target']
                        }
                    } 
                }, {}));
            } else {
                tempObjectSchemaList = Object.values(tempObjectSchemaList.reduce((res, item) => {
                    const value = item['channelGroup']
                    const existing = res[value] || {['channelGroup']: value, ['actual']: 0, ['target']: 0}
                    return {
                        ...res,
                        [value] : {
                            ...existing,
                            ['actual']: existing['actual'] + item['actual'],
                            ['target']: existing['target'] + item['target']
                        }
                    } 
                }, {}));
            }
        } else {
            tempObjectSchemaList = targetChannelList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode));
        }

        let totalIncentive = 0;
        tempObjectSchemaList = tempObjectSchemaList.map(tempObject => {
            const objectSchema = {                    
                schemaId: schema.SchemaId,
                schemaLabel: schema.SchemaLabel,
                rayonCode: employee.RayonCode
            }
            if (tempObject.channelGroup) {
                objectSchema.channelGroup = tempObject.channelGroup;
            }
            const tempActualProduct = tempActualChannelList.find(actual => actual.channelGroup === tempObject.channelGroup);
            if (tempActualProduct && tempActualProduct.actual > 0) {
                objectSchema.actual = (objectSchema.actual || 0) + (tempObject.actual || 0) + tempActualProduct.actual;
            } else {
                objectSchema.actual = (objectSchema.actual || 0) + (tempObject.actual || 0);
            }
            objectSchema.target = tempObject.target;
            objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
        
            const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
            objectSchema.incentive = bonus;
            totalIncentive = totalIncentive + bonus;

            return objectSchema;
        });

        tempObjectSchemaList = tempObjectSchemaList.map(tempObject => {
            tempObject.totalIncentive = totalIncentive;
            employee.schemaList.push(tempObject);
            return tempObject;
        });

        employee.totalIncentive = (employee.totalIncentive || 0) + totalIncentive;

        return employee;
    });
}

module.exports = {
    incentiveOutletProject
}
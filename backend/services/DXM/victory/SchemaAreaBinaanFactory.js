const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveAreaBinaan = (
    schema, resultCalculate, employeeDataList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode
        }
        
        const employeeSubordinate = employeeDataList.find(subordinate => 
            employee.RayonCodeFilter.includes(subordinate.RayonCodeSup) && subordinate.DummyPimda === 1);
        if (!employeeSubordinate) {
            return employee;
        }

        const existingEmployee = resultCalculate.find(currentEmployee => 
            currentEmployee.RayonCodeFilter.includes(employeeSubordinate.RayonCode));        
        if (existingEmployee) {
            objectSchema.actual = existingEmployee.actualAll || 0;
            objectSchema.target = existingEmployee.targetAll || 0;
            objectSchema.actualTarget = existingEmployee.actualTargetAll || 0;
        
            const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
            objectSchema.incentive = bonus;
            objectSchema.totalIncentive = bonus;
    
            employee.schemaList.push(objectSchema);
    
            employee.totalIncentive = (employee.totalIncentive || 0) + bonus;
        }

        return employee;
    });
}

module.exports = {
    incentiveAreaBinaan
}
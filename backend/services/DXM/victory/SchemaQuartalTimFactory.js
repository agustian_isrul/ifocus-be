const { 
    convertNaNToZero, 
    clasificationGroupList, 
    validateByTargetType, 
    calculatePassword 
} = require('../../../helpers/utility');

const incentiveQuartalTim = (
    schema, resultCalculate, criteriaCabangList, actualDataCabangList, targetDataCabangList, filterParameterListBySchema
) => {    
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);

    criteriaCabangList.map(cabang => {
        const newEmployee = {
            EmployeeID: '',
            RayonCode: '',
            EmployeeName: '',
            EmployeePositionCode: '',
            EmployeeAreaCode: cabang.EmployeeAreaCode,
            EmployeeSubLineDesc: '',
            EmployeeMRType: '',
            EmployeeVacant: '',
            actualAll: 0,
            targetAll: 0,
            productivity: 0,
            actualProductivity: 0
        }

        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            criteria: cabang.Kriteria,
            actual: 0,
            target: 0
        }
        
        const actualData = actualDataCabangList.find(actual => actual.EmployeeAreaCode === cabang.EmployeeAreaCode);
        if (actualData) {
            objectSchema.actual = actualData.sales_total;
            newEmployee.actualAll = actualData.sales_total;
        }

        const targetData = targetDataCabangList.find(target => target.EmployeeAreaCode === cabang.EmployeeAreaCode);
        if (targetData && targetData.target_total > 0) {
            objectSchema.target = targetData.target_total;
            newEmployee.targetAll = targetData.target_total;
        }

        if (objectSchema.actual <= 0 && objectSchema.target <= 0) {
            return cabang;
        }

        objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
        
        newEmployee.maxTargetProductivity = newEmployee.targetAll;
        newEmployee.actualTargetAll = convertNaNToZero(newEmployee.actualAll / newEmployee.targetAll * 100);
        newEmployee.actualTargetMax = newEmployee.actualTargetAll;

        const bonus = calculateStaticBonus(
            newEmployee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList
        );
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = bonus;
        
        newEmployee.totalIncentive = (newEmployee.totalIncentive || 0) + bonus;
        newEmployee.schemaList = newEmployee.schemaList || [];
        newEmployee.schemaList.push(objectSchema);

        resultCalculate.push(newEmployee);
        
        return cabang;
    })
    return resultCalculate;
}

const calculateStaticBonus = (employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList) => {
    if (calculatePassword(employee, objectSchema, preeliminationAndList, preeliminationOrList) === 0) {
        return 0;
    }

    let bonus = 0;
    if (clasificationList && clasificationList.length > 0) {
        const objectClassification = clasificationList.find(criteria => validateByTargetType(
            criteria.TargetTypeH, employee, objectSchema, criteria.Operator2H, criteria.ValueFromH, criteria.ValueToH
        ));
        if (objectClassification && objectClassification.detail && objectClassification.detail.length > 0) {
            const objectIncentive = objectClassification.detail.find(criteriaDetail => validateByTargetType(
                criteriaDetail.TargetTypeD, employee, objectSchema, criteriaDetail.Operator2D, 
                criteriaDetail.ValueFromD, criteriaDetail.ValueToD
            ) && criteriaDetail.Bonus);
            if (objectIncentive) {
                let tempValueFrom = objectIncentive.ValueFromD;
                if (!objectIncentive.TargetTypeH && objectIncentive.ValueFromH) {
                    tempValueFrom = objectIncentive.ValueFromH;
                }
                bonus = Math.round(tempValueFrom / 100 * (objectSchema.actual - employee.maxTargetProductivity));
                bonus = (bonus >= objectIncentive.Bonus) ? objectIncentive.Bonus : bonus;
            }
        }
    }
    return bonus > 0 ? bonus : 0;
}

module.exports = {
    incentiveQuartalTim
}
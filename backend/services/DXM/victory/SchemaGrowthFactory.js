const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveGrowth = (
    schema, resultCalculate, employeeDataList, actualDataList, previousQuarterActualList, 
    filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');

    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);

    const filterChannelGroup = filterDataListBySchema.find(data => data.FilterCode === 'ChannelGroup');
    let tempChannelGroup = 'TOTAL';
    if (filterChannelGroup && filterChannelGroup.FilterValue === 'HOSPITAL' && filterChannelGroup.FlagInclude === 1) {
        tempChannelGroup = 'HOSPITAL';
    } else if (filterChannelGroup && filterChannelGroup.FilterValue === 'HOSPITAL' && filterChannelGroup.FlagInclude === 0) {
        tempChannelGroup = 'APOTIK';
    }
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;
        
        let actualDataValue = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
        .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);

        if (employee.EmployeePositionCode !== 'MR') {
            const subordinateEmployeeList = employeeDataList
            .filter(subordinate => employee.RayonCodeFilter.includes(subordinate.RayonCodeSup));
            actualDataValue = actualDataValue + actualDataList
            .filter(actual => subordinateEmployeeList.some(subordinate => subordinate.RayonCode === actual.RayonCode))
            .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
        }

        const previousActualValue = previousQuarterActualList
        .filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode) && actual.GrowthType === tempChannelGroup)
        .reduce((accumulator, actual) => {return accumulator + actual.target}, 0);

        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode,
            actual: actualDataValue,
            target: previousActualValue
        }
        objectSchema.actualTarget = convertNaNToZero((objectSchema.actual - objectSchema.target) / objectSchema.target * 100);
        
        const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = bonus;

        employee.schemaList.push(objectSchema);
        employee.totalIncentive = (employee.totalIncentive || 0) + bonus;

        return employee;
    });
}

module.exports = {
    incentiveGrowth
}
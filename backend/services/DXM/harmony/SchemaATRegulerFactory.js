const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    validateByTargetType,
    calculatePassword
} = require('../../../helpers/utility');

const incentiveATReguler = (schema, resultCalculate, filterDataListBySchema, filterParameterListBySchema) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');

    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode
        }
        
        objectSchema.actual = employee.actualReguler;
        objectSchema.target = employee.targetReguler;
        objectSchema.actualTarget = employee.actualTargetReguler;
    
        const bonus = calculateStaticBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = bonus;

        employee.schemaList.push(objectSchema);

        employee.totalIncentive = (employee.totalIncentive || 0) + bonus;

        return employee;
    });
}

const calculateStaticBonus = (employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList) => {
    if (calculatePassword(employee, objectSchema, preeliminationAndList, preeliminationOrList) === 0) {
        return 0;
    }

    let bonus = 0;
    if (clasificationList && clasificationList.length > 0) {
        for (criteria of clasificationList) {
            objectSchema.totalIncentive = Math.round(objectSchema.actual * criteria.ValueFromH / 100);
            if (criteria.detail && criteria.detail.length > 0) {
                criteriaDetailList = criteria.detail.filter(criteriaDetail => criteriaDetail.Bonus);
                const objectIncentive = criteriaDetailList.sort((a, b) => b.ValueFromD - a.ValueFromD)
                .find(criteriaDetail => criteriaDetail.Bonus && validateByTargetType(
                    criteriaDetail.TargetTypeD, employee, objectSchema, criteriaDetail.Operator2D, 
                    criteriaDetail.ValueFromD, criteriaDetail.ValueToD
                ));
                if (objectIncentive) {
                    return objectIncentive.Bonus;
                } else {
                    return objectSchema.totalIncentive;
                }
            }

            // const isClassification = validateByTargetType(
            //     criteria.TargetTypeH, employee, objectSchema, criteria.Operator2H, criteria.ValueFromH, criteria.ValueToH
            // );
            // if (isClassification && criteria.detail && criteria.detail.length > 0) {
            //     for (const criteriaDetail of criteria.detail) {
            //         objectSchema.incentive = Math.round(objectSchema.actual * criteriaDetail.ValueFromH / 100);                  
            //         const isIncentive = validateByTargetType('incentive', employee, objectSchema, 
            //             criteriaDetail.Operator2D, criteriaDetail.ValueFromD, criteriaDetail.ValueToD
            //         );
            //         if (isIncentive) {
            //             bonus = new Number(criteriaDetail.ValueFromD);
            //         } else {
            //             bonus = objectSchema.incentive;
            //         }
            //     }
            // }
        }
    }
    return bonus > 0 ? bonus : 0;
}

module.exports = {
    incentiveATReguler
}
const { 
    convertNaNToZero,
    clasificationGroupList,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveTarikanSalesTim = (
    schema, resultCalculate, criteriaCabangList, actualDataCabangList, targetDataCabangList, filterParameterListBySchema
) => {    
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);

    criteriaCabangList.map(cabang => {
        const newEmployee = {
            EmployeeID: '',
            RayonCode: '',
            EmployeeName: '',
            EmployeePositionCode: '',
            EmployeeAreaCode: cabang.AreaGabungan,
            EmployeeSubLineDesc: '',
            EmployeeMRType: '',
            EmployeeVacant: '',
            actualAll: 0,
            targetAll: 0,
            productivity: 0,
            actualProductivity: 0,
            maxTargetProductivity: 0,
            actualTargetAll: 0,
            actualTargetMax: 0,
            totalIncentive: 0,
            schemaList: []
        }
        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            categoryProduct: cabang.Kriteria,
            actual: 0,
            target: 0
        }
        
        const actualData = actualDataCabangList.find(actual => actual.AreaGabungan === cabang.AreaGabungan);
        if (actualData) {
            objectSchema.actual = actualData.actualMiddle;
            newEmployee.actualAll = actualData.actual;
        }

        const targetData = targetDataCabangList.find(target => target.AreaGabungan === cabang.AreaGabungan);
        if (targetData) {
            objectSchema.target = targetData.target;
            newEmployee.targetAll = targetData.target;
        }

        if (objectSchema.actual <= 0 && objectSchema.target <= 0) {
            return cabang;
        }

        objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
        
        newEmployee.maxTargetProductivity = newEmployee.targetAll;
        newEmployee.actualTargetAll = convertNaNToZero(newEmployee.actualAll / newEmployee.targetAll * 100);
        newEmployee.actualTargetMax = newEmployee.actualTargetAll;

        const bonus = calculateBonus(newEmployee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = bonus;

        newEmployee.schemaList.push(objectSchema);
        newEmployee.totalIncentive = (newEmployee.totalIncentive || 0) + bonus;
        
        resultCalculate.push(newEmployee);
        
        return cabang;
    })    
    return resultCalculate;
}

module.exports = {
    incentiveTarikanSalesTim
}
const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    validateByTargetType,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveMRSpecialist = (schema, resultCalculate, filterDataListBySchema, filterParameterListBySchema) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');

    const employeeMRTypeFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'EmployeeMRType');
    if (employeeMRTypeFilter) {
        const indexFilter = filterParameterListBySchema.indexOf(employeeMRTypeFilter);
        filterParameterListBySchema.splice(indexFilter, 1);
    }

    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        let tempPIMDAList = [];
        resultCalculate.filter(tempResult => tempResult.RayonCodeList
            .some(tempRayonCode => employee.RayonCodeFilter.includes(tempRayonCode.RayonCodeSup)))
        .forEach(tempResult => {
            tempPIMDAList = tempPIMDAList.concat(tempResult.RayonCodeFilter);
        });
        
        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode,
            actual: 0,
            target: 0
        }
        const tempMRSpecialistList = resultCalculate.filter(tempSubordinate => tempSubordinate.RayonCodeList
            .some(tempRayonCode => tempPIMDAList.includes(tempRayonCode.RayonCodeSup)));
        if (tempMRSpecialistList && tempMRSpecialistList.length > 0) {
            for (const tempMR of tempMRSpecialistList) {
                if (employeeMRTypeFilter && validateByTargetType(employeeMRTypeFilter.TargetTypeH, tempMR, objectSchema, 
                    employeeMRTypeFilter.Operator2H, employeeMRTypeFilter.ValueFromH, employeeMRTypeFilter.ValueToH)) {
                    objectSchema.actual = objectSchema.actual + tempMR.actualAll;
                    objectSchema.target = objectSchema.target + tempMR.targetAll;
                }
            }
            objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
        
            const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
            objectSchema.incentive = bonus;
            objectSchema.totalIncentive = bonus;
    
            employee.schemaList.push(objectSchema);
    
            employee.totalIncentive = (employee.totalIncentive || 0) + bonus;
        }
        // const tempMRSpecialistList = Object.values(resultCalculate
        // .filter(tempSubordinate => tempSubordinate.RayonCodeList
        //     .some(tempRayonCode => tempPIMDAList.includes(tempRayonCode.RayonCodeSup) && 
        //     tempRayonCode.EmployeeMRType === tempSubordinateEmployeeMRType))
        // .reduce((result, item) => {
        //     const value = employee.RayonCode;
        //     const existing = result[value] || {
        //         rayonCode: value,
        //         actualAll: 0,
        //         targetAll: 0
        //     };
        //     return {
        //         ...result,
        //         [value] : {
        //             ...existing,
        //             actualAll: existing.actualAll + item.actualAll,
        //             targetAll: existing.targetAll + item.targetAll
        //         }
        //     }
        // }, {}));

        // if (tempMRSpecialistList && tempMRSpecialistList.length > 0) {
        //     const objectSchema = {                    
        //         schemaId: schema.SchemaId,
        //         schemaLabel: schema.SchemaLabel,
        //         rayonCode: employee.RayonCode
        //     }
        //     objectSchema.actual = tempMRSpecialistList[0].actualAll;
        //     objectSchema.target = tempMRSpecialistList[0].targetAll;
        //     objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
        
        //     const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        //     objectSchema.incentive = bonus;
        //     objectSchema.totalIncentive = bonus;
    
        //     employee.schemaList.push(objectSchema);
    
        //     employee.totalIncentive = (employee.totalIncentive || 0) + bonus;
        // }

        return employee;
    });
}

module.exports = {
    incentiveMRSpecialist
}
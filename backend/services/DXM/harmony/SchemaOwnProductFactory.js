const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculateBonus,
    calculatePassword
} = require('../../../helpers/utility');

const incentiveOwnProduct = (
    schema, resultCalculate, actualProductList, targetProductList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const filterLineProductList = Object.values(filterDataListBySchema
    .filter(data => data.LineProduct)
    .reduce((result, item) => {
        const value = item.LineProduct;
        const existing = result[value] || {categoryProduct: value}
        return {
            ...result,
            [value] : {
                ...existing
            }
        }
    }, {}));
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        let tempCountSubordinate = 0;
        let tempProductFokusSchemaList = [];
        if (employee.EmployeePositionCode !== 'MR') {
            const tempSubordinateList = resultCalculate.filter(tempSubordinate => tempSubordinate.RayonCodeList
                .some(tempRayonCode => employee.RayonCodeFilter.includes(tempRayonCode.RayonCodeSup) && 
                ((employee.EmployeePositionCode === 'DM' && tempRayonCode.EmployeePositionCode !== 'MR' && 
                tempRayonCode.DummyPimda === 0) || 
                (employee.EmployeePositionCode !== 'DM' && tempRayonCode.EmployeePositionCode === 'MR'))));
            
            tempCountSubordinate = tempSubordinateList.length;

            tempSubordinateList.forEach(tempSubordinate => {
                const tempSchemaList = tempSubordinate.schemaList
                .filter(tempSchema => tempSchema.schemaId === schema.SchemaId) || [];
                tempProductFokusSchemaList = tempProductFokusSchemaList.concat(tempSchemaList);
            });
        }

        let tempTotalIncentive = 0;
        switch (employee.EmployeePositionCode) {
            case 'MR': {
                let tempObjectSchemaList = [];
                for (const tempCategory of filterLineProductList) {
                    const objectSchema = {                    
                        schemaId: schema.SchemaId,
                        schemaLabel: schema.SchemaLabel,
                        rayonCode: employee.RayonCode,
                        categoryProduct: tempCategory.categoryProduct
                    }
                    objectSchema.actual = actualProductList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode) && 
                    actual.categoryProduct === tempCategory.categoryProduct)
                    .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
                    objectSchema.target = targetProductList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode) && 
                    target.categoryProduct === tempCategory.categoryProduct)
                    .reduce((accumulator, target) => {return accumulator + target.target}, 0);
                    objectSchema.maxSalesTarget = Math.max(objectSchema.actual, objectSchema.target);
                    objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
                
                    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
                    objectSchema.incentive = bonus;
                    tempTotalIncentive = tempTotalIncentive + bonus;
        
                    tempObjectSchemaList.push(objectSchema);
                }
        
                tempObjectSchemaList = tempObjectSchemaList.map(tempObject => {
                    tempObject.totalIncentive = tempTotalIncentive;
                    employee.schemaList.push(tempObject);
                    return tempObject;
                });
                break;
            }
            default: {
                tempProductFokusSchemaList = Object.values(tempProductFokusSchemaList.reduce((result, item) => {
                    const value = employee.RayonCode;
                    const existing = result[value] || {
                        rayonCode: value, 
                        actual: 0,
                        target: 0, 
                        incentive: 0
                    }
                    return {
                        ...result,
                        [value] : {
                            ...existing,
                            actual: existing.actual + item.actual,
                            target: existing.target + item.target,
                            incentive: existing.incentive + item.incentive
                        }
                    }
                }, {}));

                const objectSchema = {                    
                    schemaId: schema.SchemaId,
                    schemaLabel: schema.SchemaLabel,
                    rayonCode: employee.RayonCode
                }
                objectSchema.actual = tempProductFokusSchemaList[0].actual;
                objectSchema.target = tempProductFokusSchemaList[0].target;
                objectSchema.maxSalesTarget = Math.max(objectSchema.target, objectSchema.actual);
                objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
                objectSchema.subordinateIncentive = tempProductFokusSchemaList[0].incentive;
                objectSchema.countSubordinate = tempCountSubordinate;
            
                const bonus = calculateStaticBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList);
                objectSchema.incentive = bonus;
                objectSchema.totalIncentive = bonus;
                tempTotalIncentive = bonus;
                employee.schemaList.push(objectSchema);
                break;
            }
        }

        employee.totalIncentive = (employee.totalIncentive || 0) + tempTotalIncentive;

        return employee;
    });
}

const calculateStaticBonus = (employee, objectSchema, preeliminationAndList, preeliminationOrList) => {
    if (calculatePassword(employee, objectSchema, preeliminationAndList, preeliminationOrList) === 0) {
        return 0;
    }
    if (objectSchema.countSubordinate > 0) {
        return Math.round(objectSchema.subordinateIncentive / objectSchema.countSubordinate * 1.2);
    }
    return 0;
}

module.exports = {
    incentiveOwnProduct
}
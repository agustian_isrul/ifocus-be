const { 
    convertArrayToNestedArray,
    validateByTargetType,
    calculatePassword
} = require('../../../helpers/utility');

const incentiveRerataSubordinate = (
    schema, resultCalculate, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');

    let tempIndexIncentive = 1.2;
    const indexIncentiveFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'indexIncentive');
    if (indexIncentiveFilter) {
        const indexFilter = filterParameterListBySchema.indexOf(indexIncentiveFilter);
        filterParameterListBySchema.splice(indexFilter, 1);
        tempIndexIncentive = indexIncentiveFilter.ValueFromH;
    }

    const employeeMRTypeFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'EmployeeMRType');
    if (employeeMRTypeFilter) {
        const indexFilter = filterParameterListBySchema.indexOf(employeeMRTypeFilter);
        filterParameterListBySchema.splice(indexFilter, 1);
    }
    
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        const tempSubordinateList = resultCalculate.filter(tempSubordinate => tempSubordinate.RayonCodeList
            .some(tempRayonCode => employee.RayonCodeFilter.includes(tempRayonCode.RayonCodeSup)));

        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode,
            actual: employee.actualAll,
            target: employee.targetAll,
            actualTarget: employee.actualTargetAll,
            countSubordinate: 0,
            subordinateIncentive: 0
        }
        
        if (tempSubordinateList && tempSubordinateList.length > 0) {
            for (const tempSubordinate of tempSubordinateList) {
                if (employeeMRTypeFilter && validateByTargetType(employeeMRTypeFilter.TargetTypeH, tempSubordinate, objectSchema, 
                    employeeMRTypeFilter.Operator2H, employeeMRTypeFilter.ValueFromH, employeeMRTypeFilter.ValueToH)) {
                    objectSchema.countSubordinate = objectSchema.countSubordinate + 1;

                    const tempSchemaList = tempSubordinate.schemaList
                    .filter(tempSchema => tempSchema.schemaId === 1);
                    if (tempSchemaList && tempSchemaList.length > 0) {
                        for (const tempSchema of tempSchemaList) {
                            objectSchema.subordinateIncentive = objectSchema.subordinateIncentive + tempSchema.incentive;
                        }
                    }
                }
            }
        }
            
        let bonus = calculateStaticBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList);        
        if (employee.EmployeePositionCode === 'DM') {
            bonus = bonus * tempIndexIncentive;
        }
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = bonus;

        employee.schemaList.push(objectSchema);

        employee.totalIncentive = (employee.totalIncentive || 0) + bonus;

        return employee;
    });
}

const calculateStaticBonus = (employee, objectSchema, preeliminationAndList, preeliminationOrList) => {
    if (calculatePassword(employee, objectSchema, preeliminationAndList, preeliminationOrList) === 0) {
        return 0;
    }
    if (objectSchema.countSubordinate > 0) {
        return Math.round(objectSchema.subordinateIncentive / objectSchema.countSubordinate);
    }
    return 0;
}

module.exports = {
    incentiveRerataSubordinate
}
const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    validateByTargetType,
    calculateBonus,
    calculatePassword
} = require('../../../helpers/utility');

const incentiveProductFocus = (
    schema, resultCalculate, actualProductList, tempTargetProductList, targetProductList, 
    filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');

    const filterLineProductList = Object.values(filterDataListBySchema
    .filter(data => data.LineProduct)
    .reduce((result, item) => {
        const value = item.LineProduct;
        const existing = result[value] || {categoryProduct: value}
        return {
            ...result,
            [value] : {
                ...existing
            }
        }
    }, {}));

    const employeeMRTypeFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'EmployeeMRType');
    if (employeeMRTypeFilter) {
        const indexFilter = filterParameterListBySchema.indexOf(employeeMRTypeFilter);
        filterParameterListBySchema.splice(indexFilter, 1);
    }

    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        let tempTotalIncentive = 0;
        switch (employee.EmployeePositionCode) {
            case 'MR': {
                let tempObjectSchemaList = [];
                for (const tempCategory of filterLineProductList) {
                    const objectSchema = {                    
                        schemaId: schema.SchemaId,
                        schemaLabel: schema.SchemaLabel,
                        rayonCode: employee.RayonCode,
                        categoryProduct: tempCategory.categoryProduct
                    }
                    objectSchema.actual = actualProductList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode) && 
                    actual.categoryProduct === tempCategory.categoryProduct)
                    .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
                    
                    objectSchema.target = tempTargetProductList.filter(target => 
                        employee.RayonCodeFilter.includes(target.RayonCode) && 
                        target.categoryProduct === tempCategory.categoryProduct)
                    .reduce((accumulator, target) => {return accumulator + target.target}, 0);

                    objectSchema.tum = targetProductList.filter(target => target.categoryProduct === tempCategory.categoryProduct &&
                    target.EmployeePositionCode === 'MR')
                    .reduce((accumulator, target) => {return accumulator + target.target}, 0);
                    objectSchema.maxTargetTum = Math.max(objectSchema.target, objectSchema.tum);
                    objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.maxTargetTum * 100);
                
                    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
                    objectSchema.incentive = bonus;
                    tempTotalIncentive = tempTotalIncentive + bonus;
        
                    tempObjectSchemaList.push(objectSchema);
                }
        
                tempObjectSchemaList = tempObjectSchemaList.map(tempObject => {
                    tempObject.totalIncentive = tempTotalIncentive;
                    employee.schemaList.push(tempObject);
                    return tempObject;
                });
                break;
            }
            case 'DM': {
                const objectSchema = {                    
                    schemaId: schema.SchemaId,
                    schemaLabel: schema.SchemaLabel,
                    rayonCode: employee.RayonCode,
                    actual: 0,
                    target: 0,
                    tum: 0,
                    maxTargetTum: 0,
                    actualTarget: 0,
                    subordinateIncentive: 0,
                    countSubordinate: 0
                }
                const tempSubordinateList = resultCalculate.filter(tempSubordinate => tempSubordinate.RayonCodeList
                    .some(tempRayonCode => employee.RayonCodeFilter.includes(tempRayonCode.RayonCodeSup) && 
                    tempRayonCode.EmployeePositionCode !== 'MR'));
                if (tempSubordinateList && tempSubordinateList.length > 0) {
                    for (const tempSubordinate of tempSubordinateList) {
                        if (employeeMRTypeFilter && validateByTargetType(employeeMRTypeFilter.TargetTypeH, tempSubordinate, objectSchema, 
                            employeeMRTypeFilter.Operator2H, employeeMRTypeFilter.ValueFromH, employeeMRTypeFilter.ValueToH)) {
                            objectSchema.countSubordinate = objectSchema.countSubordinate + 1;

                            const tempSchemaList = tempSubordinate.schemaList
                            .filter(tempSchema => tempSchema.schemaId === schema.SchemaId);
                            if (tempSchemaList && tempSchemaList.length > 0) {
                                for (const tempSchema of tempSchemaList) {
                                    objectSchema.actual = objectSchema.actual + tempSchema.actual;
                                    objectSchema.target = objectSchema.target + tempSchema.target;
                                    objectSchema.tum = objectSchema.tum + tempSchema.tum;
                                    objectSchema.subordinateIncentive = objectSchema.subordinateIncentive + tempSchema.incentive;
                                }
                            }
                        }
                    }
                    objectSchema.maxTargetTum = Math.max(objectSchema.target, objectSchema.tum);
                    objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.maxTargetTum * 100);
                }
            
                const bonus = calculateStaticBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList);
                objectSchema.incentive = bonus;
                objectSchema.totalIncentive = bonus;
                tempTotalIncentive = bonus;
                employee.schemaList.push(objectSchema);
                break;
            }
            default: {
                let tempProductFokusSchemaList = [];
                let tempObjectSchemaList = [];

                const tempSubordinateList = resultCalculate.filter(tempSubordinate => tempSubordinate.RayonCodeList
                    .some(tempRayonCode => employee.RayonCodeFilter.includes(tempRayonCode.RayonCodeSup) && 
                    tempRayonCode.EmployeePositionCode === 'MR'));
                
                const tempCountSubordinate = tempSubordinateList.length;
    
                tempSubordinateList.forEach(tempSubordinate => {
                    const tempSchemaList = tempSubordinate.schemaList
                    .filter(tempSchema => tempSchema.schemaId === schema.SchemaId) || [];
                    tempProductFokusSchemaList = tempProductFokusSchemaList.concat(tempSchemaList);
                });
                
                tempProductFokusSchemaList = Object.values(tempProductFokusSchemaList.reduce((result, item) => {
                    const value = item.categoryProduct;
                    const existing = result[value] || {
                        categoryProduct: value, 
                        actual: 0,
                        target: 0, 
                        incentive: 0
                    }
                    return {
                        ...result,
                        [value] : {
                            ...existing,
                            actual: existing.actual + item.actual,
                            target: existing.target + item.target,
                            incentive: existing.incentive + item.incentive
                        }
                    }
                }, {}));

                for (const tempCategory of filterLineProductList) {
                    const objectSchema = {                    
                        schemaId: schema.SchemaId,
                        schemaLabel: schema.SchemaLabel,
                        rayonCode: employee.RayonCode,
                        categoryProduct: tempCategory.categoryProduct
                    }
                    objectSchema.actual = tempProductFokusSchemaList
                    .filter(actual => actual.categoryProduct === tempCategory.categoryProduct)
                    .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
                    objectSchema.target = tempProductFokusSchemaList
                    .filter(actual => actual.categoryProduct === tempCategory.categoryProduct)
                    .reduce((accumulator, actual) => {return accumulator + actual.target}, 0);
                    objectSchema.tum = searchPimdaTarget(targetProductList, tempCategory.categoryProduct, tempCountSubordinate);
                    objectSchema.maxTargetTum = Math.max(objectSchema.target, objectSchema.tum);
                    objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.maxTargetTum * 100);
                    objectSchema.countSubordinate = tempCountSubordinate;
                
                    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
                    objectSchema.incentive = bonus;
                    tempTotalIncentive = tempTotalIncentive + bonus;
        
                    tempObjectSchemaList.push(objectSchema);
                }
        
                tempObjectSchemaList = tempObjectSchemaList.map(tempObject => {
                    tempObject.totalIncentive = tempTotalIncentive;
                    employee.schemaList.push(tempObject);
                    return tempObject;
                });
                break;
            }
        }

        employee.totalIncentive = (employee.totalIncentive || 0) + tempTotalIncentive;

        return employee;
    });
}

const searchPimdaTarget = (targetPimdaProductList, categoryProduct, countSubordinate) => {
    let tempTarget = 0;
    const tempTargetPimdaList = targetPimdaProductList.filter(data => data.categoryProduct === categoryProduct && 
        data.EmployeePositionCode === 'SUP');
    for (const target of tempTargetPimdaList) {
        const isCorrect = validateCountSubordinate(countSubordinate, target.Operator, target.ValueMr);
        if (isCorrect && tempTarget <= target.target) {
            tempTarget = target.target;
        }
    }
    return tempTarget;
}

const validateCountSubordinate = (operand, operator, valueFrom) => {
    switch (operator) {
        case ">=" : {
            return operand >= valueFrom;
        }
        case "<=" : {
            return operand <= valueFrom;
        }
        case ">" : {
            return operand > valueFrom;
        }
        case "<" : {
            return operand < valueFrom;
        }
        case "=" : {
            return operand == valueFrom;
        }
        default:
            break;
    }
    return true;
}

const calculateStaticBonus = (employee, objectSchema, preeliminationAndList, preeliminationOrList) => {
    if (calculatePassword(employee, objectSchema, preeliminationAndList, preeliminationOrList) === 0) {
        return 0;
    }
    if (objectSchema.countSubordinate > 0) {
        return Math.round(objectSchema.subordinateIncentive / objectSchema.countSubordinate);
    }
    return 0;
}

module.exports = {
    incentiveProductFocus
}
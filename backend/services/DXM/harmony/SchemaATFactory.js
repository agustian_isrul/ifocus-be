const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    isExistEmployee,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveAT = (
    schema, resultCalculate, employeeDataList, actualDataList, targetDataList, productivityDataList, 
    filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const filterEmployeeList = convertArrayToNestedArray(employeeDataList
    .filter(currentEmployee => filterEmployeeDataList
        .every(element => element.FilterValueList[0].FlagInclude === 1 ? 
            element.FilterValueFilter.includes(currentEmployee[element.FilterCode]) : 
            !element.FilterValueFilter.includes(currentEmployee[element.FilterCode])
        )
    ),
    'EmployeeID', 'RayonCode');

    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);

    for (let employee of filterEmployeeList) {
        let currentEmployee = isExistEmployee(employee, resultCalculate);
        if (!currentEmployee) {
            currentEmployee = employee;
            currentEmployee.EmployeeName = employee.RayonCodeList[0].EmployeeName;
            currentEmployee.EmployeePositionCode = employee.RayonCodeList[0].EmployeePositionCode;
            currentEmployee.EmployeeName = employee.RayonCodeList[0].EmployeeName;
            currentEmployee.EmployeeAreaCode = employee.RayonCodeList[0].EmployeeAreaCode;
            currentEmployee.EmployeeSubLineDesc = employee.RayonCodeList[0].EmployeeSubLineDesc;
            currentEmployee.EmployeeMRType = employee.RayonCodeList[0].EmployeeMRType;
            currentEmployee.EmployeeVacant = employee.RayonCodeList[0].EmployeeVacant;
            const tempCurrentEmployee = setupEmployeeProperties(
                currentEmployee, schema, resultCalculate, actualDataList, targetDataList, productivityDataList, 
                preeliminationAndList, preeliminationOrList, clasificationList
            )
            if (tempCurrentEmployee) {
                resultCalculate = resultCalculate || [];
                resultCalculate.push(tempCurrentEmployee);
            }
        } else {
            currentEmployee = calculateExistingEmployee(
                currentEmployee, schema, preeliminationAndList, preeliminationOrList, clasificationList
            );
            let dataEmployee = resultCalculate
            .find(data => data.RayonCodeFilter.includes(currentEmployee.RayonCodeFilter[0]));
            dataEmployee = currentEmployee;
        }
    }

    return resultCalculate;
}

const setupEmployeeProperties = (
    employee, schema, resultCalculate, actualDataList, targetDataList, productivityDataList, 
    preeliminationAndList, preeliminationOrList, clasificationList
) => {
    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode
    }

    let tempProductivity = 0;
    let tempActualReguler = 0;
    let tempTargetReguler = 0;
    let tempActualCovid = 0;
    let tempTargetCovid = 0;
    let tempCountSubordinate = 0;
    if (employee.EmployeePositionCode === 'MR') {
        objectSchema.actual = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
        .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
        objectSchema.target = targetDataList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
        .reduce((accumulator, target) => {return accumulator + target.target}, 0);
        tempProductivity = productivityDataList.filter(productivity => 
            productivity.EmployeePositionCode === employee.EmployeePositionCode && 
            productivity.EmployeeMRType === employee.EmployeeMRType)
        .reduce((accumulator, productivity) => {return accumulator + productivity.Productivitas}, 0);
        // tempProductivity = findProductivityValue(employee, productivityDataList);
        tempActualReguler = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
        .reduce((accumulator, actual) => {return accumulator + actual.actualReguler}, 0);
        tempTargetReguler = targetDataList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
        .reduce((accumulator, target) => {return accumulator + target.targetReguler}, 0);
        tempActualCovid = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
        .reduce((accumulator, actual) => {return accumulator + actual.actualCovid}, 0);
        tempTargetCovid = targetDataList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
        .reduce((accumulator, target) => {return accumulator + target.targetCovid}, 0);
    } else {
        objectSchema.actual = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
        .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
        objectSchema.target = targetDataList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
        .reduce((accumulator, target) => {return accumulator + target.target}, 0);
        
        const tempSubordinateList = Object.values(resultCalculate
        .filter(tempSubordinate => tempSubordinate.RayonCodeList
            .some(tempRayonCode => employee.RayonCodeFilter.includes(tempRayonCode.RayonCodeSup)))
        .reduce((result, item) => {
            const value = employee.RayonCode;
            const existing = result[value] || {
                rayonCode: value,
                actualAll: 0,
                targetAll: 0,
                productivity: 0,
                actualReguler: 0,
                targetReguler: 0,
                actualCovid: 0,
                targetCovid: 0
            };
            return {
                ...result,
                [value] : {
                    ...existing,
                    actualAll: existing.actualAll + item.actualAll,
                    targetAll: existing.targetAll + item.targetAll,
                    productivity: existing.productivity + item.productivity,
                    actualReguler: existing.actualReguler + item.actualReguler,
                    targetReguler: existing.targetReguler + item.targetReguler,
                    actualCovid: existing.actualCovid + item.actualCovid,
                    targetCovid: existing.targetCovid + item.targetCovid
                }
            }
        }, {}));

        if (tempSubordinateList && tempSubordinateList.length > 0) {
            objectSchema.actual = objectSchema.actual + tempSubordinateList[0].actualAll;
            objectSchema.target = objectSchema.target + tempSubordinateList[0].targetAll;
        
            tempProductivity = tempSubordinateList[0].productivity;
            tempActualReguler = tempSubordinateList[0].actualReguler;
            tempTargetReguler = tempSubordinateList[0].targetReguler;
            tempActualCovid = tempSubordinateList[0].actualCovid;
            tempTargetCovid = tempSubordinateList[0].targetCovid;
            tempCountSubordinate = tempSubordinateList[0].countSubordinate;
        }
    }

    if (objectSchema.actual <= 0 && objectSchema.target <= 0) {
        return null;
    }
    
    objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);

    employee.actualAll = objectSchema.actual;
    employee.targetAll = objectSchema.target;
    employee.actualTargetAll = objectSchema.actualTarget;
    employee.productivity = tempProductivity;    
    employee.actualProductivity = convertNaNToZero(employee.actualAll / employee.productivity * 100);
    employee.maxTargetProductivity = Math.max(employee.targetAll, employee.productivity);
    employee.actualTargetMax = convertNaNToZero(employee.actualAll / employee.maxTargetProductivity * 100);
    employee.actualReguler = tempActualReguler;
    employee.targetReguler = tempTargetReguler;
    employee.actualTargetReguler = convertNaNToZero(employee.actualReguler / employee.targetReguler * 100);
    employee.actualCovid = tempActualCovid;
    employee.targetCovid = tempTargetCovid;
    if (employee.EmployeePositionCode !== 'MR') {
        employee.countSubordinate = tempCountSubordinate;
    }

    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
    objectSchema.incentive = bonus;
    objectSchema.totalIncentive = bonus;
    
    employee.totalIncentive = (employee.totalIncentive || 0) + bonus;

    employee.schemaList = employee.schemaList || [];
    employee.schemaList.push(objectSchema);

    return employee;
}

const calculateExistingEmployee = (employee, schema, preeliminationAndList, preeliminationOrList, clasificationList) => {
    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode,
        actual : employee.actualAll,
        target : employee.targetAll,
        actualTarget: employee.actualTargetAll
    }
    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
    objectSchema.incentive = bonus;
    objectSchema.totalIncentive = bonus;
    
    employee.totalIncentive = (employee.totalIncentive || 0) + bonus;

    employee.schemaList = employee.schemaList || [];
    employee.schemaList.push(objectSchema);

    return employee;
}

// const findProductivityValue = (employee, productivityList) => {
//     if (employee.EmployeePositionCode === 'MR' && employee.EmployeeMRType === 'GOLD') {
//         return 0;
//     }
//     if (employee.EmployeePositionCode === 'MR' && productivityList && productivityList.length > 0) {
//         return productivityList[0].Productivitas;
//     }
//     return 0;
// }

module.exports = {
    incentiveAT
}
const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveOutletProject = (
    schema, resultCalculate, actualTargetChannelList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');

    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        const tempActualTargetList = Object.values(actualTargetChannelList
        .filter(item => employee.RayonCodeFilter.includes(item.RayonCode))
        .reduce((result, item) => {
            const value = employee.RayonCode;
            const existing = result[value] || {
                rayonCode: value,
                actual: 0,
                target: 0
            }
            return {
                ...result,
                [value] : {
                    ...existing,
                    actual: existing.actual + item.actual,
                    target: existing.target + item.target
                }
            } 
        }, {}));

        if (tempActualTargetList && tempActualTargetList.length > 0) {
            const objectSchema = {                    
                schemaId: schema.SchemaId,
                schemaLabel: schema.SchemaLabel,
                rayonCode: employee.RayonCode
            }
            if (tempActualTargetList && tempActualTargetList.length > 0) {
                objectSchema.actual = tempActualTargetList[0].actual;
                objectSchema.target = tempActualTargetList[0].target;
                objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
            } else {
                objectSchema.actual = 0;
                objectSchema.target = 0;
                objectSchema.actualTarget = 0;
            }
            
            const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
            objectSchema.incentive = bonus;
            objectSchema.totalIncentive = bonus;
            
            employee.totalIncentive = (employee.totalIncentive || 0) + bonus;
        
            employee.schemaList = employee.schemaList || [];
            employee.schemaList.push(objectSchema);
        }
    
        return employee;
    });
}

module.exports = {
    incentiveOutletProject
}
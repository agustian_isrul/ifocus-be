const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculateBonus,
    validateByTargetType
} = require('../../../helpers/utility');

const incentiveOutletProject = (
    schema, resultCalculate, actualChannelList, targetChannelList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');

    const discountDXMProsFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'discountDXMPros');
    const actualCHDHerbaFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'actualCHDHerba');
    const indexIncentiveFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'indexIncentive');
    const ratioMRFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'ratio');
    if (discountDXMProsFilter) {
        const indexFilter = filterParameterListBySchema.indexOf(discountDXMProsFilter);
        filterParameterListBySchema.splice(indexFilter, 1);        
        if (actualCHDHerbaFilter) {
            const indexFilter = filterParameterListBySchema.indexOf(actualCHDHerbaFilter);
            filterParameterListBySchema.splice(indexFilter, 1);
        }
        if (indexIncentiveFilter) {
            const indexFilter = filterParameterListBySchema.indexOf(indexIncentiveFilter);
            filterParameterListBySchema.splice(indexFilter, 1);
        }
        if (ratioMRFilter) {
            const indexFilter = filterParameterListBySchema.indexOf(ratioMRFilter);
            filterParameterListBySchema.splice(indexFilter, 1);
        }
    }

    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    const tempLineProductList = ['DXM', 'CHD'];
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        let tempTotalIncentive = 0;
        let tempObjectSchemaList = [];
        for (const tempLineProduct of tempLineProductList) {
            const objectSchema = {                    
                schemaId: schema.SchemaId,
                schemaLabel: schema.SchemaLabel,
                rayonCode: employee.RayonCode,
                categoryProduct: tempLineProduct
            }
            objectSchema.actual = actualChannelList
            .filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode) && actual.categoryProduct === tempLineProduct)
            .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
            objectSchema.target = targetChannelList
            .filter(target => employee.RayonCodeFilter.includes(target.RayonCode) && target.LineProduct === tempLineProduct)
            .reduce((accumulator, target) => {return accumulator + target.target}, 0);

            if (employee.EmployeePositionCode !== 'MR') {
                const tempSubordinateList = resultCalculate
                .filter(tempSubordinate => tempSubordinate.RayonCodeList
                    .some(tempRayonCode => employee.RayonCodeFilter.includes(tempRayonCode.RayonCodeSup)));
                let tempSchemaList = [];
                if (tempSubordinateList && tempSubordinateList.length > 0) {
                    for (const tempSubordinate of tempSubordinateList) {
                        tempSchemaList = tempSchemaList.concat(tempSubordinate.schemaList
                        .filter(tempSchema => tempSchema.schemaId === schema.SchemaId && 
                            tempSchema.categoryProduct === tempLineProduct));
                    }
                }
                if (tempSchemaList && tempSchemaList.length > 0) {
                    tempSchemaList = Object.values(tempSchemaList.reduce((result, item) => {
                        const value = item.categoryProduct;
                        const existing = result[value] || {
                            categoryProduct: value,
                            actual: 0,
                            target: 0
                        };
                        return {
                            ...result,
                            [value] : {
                                ...existing,
                                actual: existing.actual + item.actual,
                                target: existing.target + item.target
                            }
                        }
                    }, {}));

                    objectSchema.actual = objectSchema.actual + tempSchemaList[0].actual;
                    objectSchema.target = objectSchema.target + tempSchemaList[0].target;
                }
            }

            objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
            objectSchema['actualTarget' + tempLineProduct] = objectSchema.actualTarget;
        
            const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
            objectSchema.incentive = bonus;
            tempTotalIncentive = tempTotalIncentive + bonus;

            tempObjectSchemaList.push(objectSchema);
        }
    
        if (tempTotalIncentive > 0 && indexIncentiveFilter) {    
            let isValidDiscountDXMPros = false;
            if (tempTotalIncentive > 0 && discountDXMProsFilter) {
                isValidDiscountDXMPros = validateByTargetType(discountDXMProsFilter.TargetTypeH, employee, null, 
                    discountDXMProsFilter.Operator2H, discountDXMProsFilter.ValueFromH, discountDXMProsFilter.ValueToH);
            }
            let isValidActualCHDHerba = false;
            if (tempTotalIncentive > 0 && actualCHDHerbaFilter) {
                let tempValueFromH = actualCHDHerbaFilter.ValueFromH;
                if (ratioMRFilter) {
                    tempValueFromH = employee[actualCHDHerbaFilter.ValueFromH] * ratioMRFilter.ValueFromH;
                }
                isValidActualCHDHerba = validateByTargetType(actualCHDHerbaFilter.TargetTypeH, employee, null, 
                    actualCHDHerbaFilter.Operator2H, tempValueFromH, actualCHDHerbaFilter.ValueToH);
            }
            if (tempTotalIncentive > 0 && (isValidDiscountDXMPros || isValidActualCHDHerba)) {
                let indexIncentive = 0;
                if (indexIncentiveFilter) {
                    indexIncentive = indexIncentiveFilter.ValueFromH;
                }    
                tempTotalIncentive = Math.round(tempTotalIncentive * indexIncentive / 100);
            }
        }
        
        employee.totalIncentive = (employee.totalIncentive || 0) + tempTotalIncentive;
        
        tempObjectSchemaList = tempObjectSchemaList.map(tempObjectSchema => {
            tempObjectSchema.totalIncentive = tempTotalIncentive;
            employee.schemaList.push(tempObjectSchema);
            return tempObjectSchema;
        });

        return employee;
    });
}

module.exports = {
    incentiveOutletProject
}
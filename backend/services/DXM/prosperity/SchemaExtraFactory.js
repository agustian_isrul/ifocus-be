const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveExtra = (schema, resultCalculate, filterDataListBySchema, filterParameterListBySchema) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');

    const ratioMRFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'ratio');
    if (ratioMRFilter) {
        const indexFilter = filterParameterListBySchema.indexOf(ratioMRFilter);
        filterParameterListBySchema.splice(indexFilter, 1);
    }

    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode
        }
        
        objectSchema.actual = employee.actualAll;
        objectSchema.target = employee.targetAll;
        objectSchema.actualTarget = employee.actualTargetAll;
    
        const tempEmployee = Object.create(employee);
        if (ratioMRFilter) {
            tempEmployee.countSubordinate = tempEmployee.countSubordinate * ratioMRFilter.ValueFromH;
        }
        const bonus = calculateBonus(tempEmployee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = bonus;
        
        employee.totalIncentive = (employee.totalIncentive || 0) + objectSchema.totalIncentive;

        employee.schemaList.push(objectSchema);

        return employee;
    });
}

module.exports = {
    incentiveExtra
}
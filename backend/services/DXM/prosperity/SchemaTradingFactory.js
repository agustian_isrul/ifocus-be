const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveTrading = (
    schema, resultCalculate, actualDataList, targetDataList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');

    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);

    const tempActualDXMPros = actualDataList.filter(actual => actual.RayonCode.includes('PROS'))
    .reduce((accumulator, actual) => {return accumulator + actual.actualDXM}, 0);
    const tempTargetDXMPros = targetDataList.filter(target => target.RayonCode.includes('PROS'))
    .reduce((accumulator, target) => {return accumulator + target.targetDXM}, 0);
    const tempActualTargetDXMPros = convertNaNToZero(tempActualDXMPros / tempTargetDXMPros * 100);
    const tempDiscountValueDXMPros = actualDataList.filter(actual => actual.RayonCode.includes('PROS'))
    .reduce((accumulator, actual) => {return accumulator + actual.discountDXM}, 0);
    const tempDiscountDXMPros = convertNaNToZero(tempDiscountValueDXMPros / tempActualDXMPros * 100);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode
        }

        objectSchema.actual = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
        .reduce((accumulator, actual) => {return accumulator + actual.actualDXM}, 0);
        objectSchema.target = targetDataList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
        .reduce((accumulator, target) => {return accumulator + target.targetDXM}, 0);
        objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
        objectSchema.actualTotalDXMPros = tempActualDXMPros;
        objectSchema.targetTotalDXMPros = tempTargetDXMPros;
        objectSchema.actualTargetTotalDXMPros = tempActualTargetDXMPros;
        objectSchema.discountTotalValueDXMPros = tempDiscountValueDXMPros;
        objectSchema.discountTotalDXMPros = tempDiscountDXMPros;
    
        const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = bonus;
        
        employee.totalIncentive = (employee.totalIncentive || 0) + objectSchema.totalIncentive;

        employee.schemaList.push(objectSchema);

        return employee;
    });
}

module.exports = {
    incentiveTrading
}
const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculateBonus,
    validateByTargetType
} = require('../../../helpers/utility');

const incentiveGrowth = (schema, resultCalculate, previousQuarterActualList, filterDataListBySchema, filterParameterListBySchema) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');

    const discountDXMProsFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'discountDXMPros');
    const actualCHDHerbaFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'actualCHDHerba');
    const indexIncentiveFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'indexIncentive');
    if (discountDXMProsFilter) {
        const indexFilter = filterParameterListBySchema.indexOf(discountDXMProsFilter);
        filterParameterListBySchema.splice(indexFilter, 1);        
        if (actualCHDHerbaFilter) {
            const indexFilter = filterParameterListBySchema.indexOf(actualCHDHerbaFilter);
            filterParameterListBySchema.splice(indexFilter, 1);
        }
        if (indexIncentiveFilter) {
            const indexFilter = filterParameterListBySchema.indexOf(indexIncentiveFilter);
            filterParameterListBySchema.splice(indexFilter, 1);
        }
    }

    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode,
            actual: employee.actualAll
        }
        objectSchema.target = previousQuarterActualList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
        .reduce((accumulator, actual) => {return accumulator + actual.target}, 0);
        objectSchema.actualTarget = convertNaNToZero((objectSchema.actual - objectSchema.target) / objectSchema.actual * 100);
        objectSchema.totalSchemaId1 = employee.schemaList.filter(data => data.schemaId === 1)
        .reduce((accumulator, data) => {return accumulator + data.totalIncentive}, 0);
        
        const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        objectSchema.incentive = bonus;
    
        let isValidDiscountDXMPros = false;
        if (bonus > 0 && discountDXMProsFilter) {
            isValidDiscountDXMPros = validateByTargetType(discountDXMProsFilter.TargetTypeH, employee, objectSchema, 
                discountDXMProsFilter.Operator2H, discountDXMProsFilter.ValueFromH, discountDXMProsFilter.ValueToH);
        }
        let isValidActualCHDHerba = false;
        if (bonus > 0 && actualCHDHerbaFilter) {
            isValidActualCHDHerba = validateByTargetType(actualCHDHerbaFilter.TargetTypeH, employee, objectSchema, 
                actualCHDHerbaFilter.Operator2H, actualCHDHerbaFilter.ValueFromH, actualCHDHerbaFilter.ValueToH);
        }
        if (bonus > 0 && (isValidDiscountDXMPros || isValidActualCHDHerba)) {
            let indexIncentive = 0;
            if (indexIncentiveFilter) {
                indexIncentive = indexIncentiveFilter.ValueFromH;
            }    
            objectSchema.totalIncentive = Math.round(bonus * indexIncentive / 100);
        } else {    
            objectSchema.totalIncentive = bonus;
        }
        
        employee.totalIncentive = (employee.totalIncentive || 0) + objectSchema.totalIncentive;

        employee.schemaList.push(objectSchema);

        return employee;
    });
}

module.exports = {
    incentiveGrowth
}
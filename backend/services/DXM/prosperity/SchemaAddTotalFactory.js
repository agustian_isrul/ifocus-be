const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    validateByTargetType,
    calculatePassword
} = require('../../../helpers/utility');

const incentiveAddTotal = (schema, resultCalculate, filterDataListBySchema, filterParameterListBySchema) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');

    const discountDXMProsFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'discountDXMPros');
    const indexIncentiveFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'indexIncentive');
    if (discountDXMProsFilter) {
        const indexFilter = filterParameterListBySchema.indexOf(discountDXMProsFilter);
        filterParameterListBySchema.splice(indexFilter, 1);
        if (indexIncentiveFilter) {
            const indexFilter = filterParameterListBySchema.indexOf(indexIncentiveFilter);
            filterParameterListBySchema.splice(indexFilter, 1);
        }
    }

    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode
        }
        
        objectSchema.actual = employee.actualAll;
        objectSchema.target = employee.targetAll;
        objectSchema.actualTarget = employee.actualTargetAll;
    
        const bonus = calculateStaticBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        objectSchema.incentive = bonus;
    
        let isValidDiscountDXMPros = false;
        if (bonus > 0 && discountDXMProsFilter) {
            isValidDiscountDXMPros = validateByTargetType(discountDXMProsFilter.TargetTypeH, employee, objectSchema, 
                discountDXMProsFilter.Operator2H, discountDXMProsFilter.ValueFromH, discountDXMProsFilter.ValueToH);
        }
        if (bonus > 0 && isValidDiscountDXMPros) {
            let indexIncentive = 0;
            if (indexIncentiveFilter) {
                indexIncentive = indexIncentiveFilter.ValueFromH;
            }    
            objectSchema.totalIncentive = Math.round(bonus * indexIncentive / 100);
        } else {    
            objectSchema.totalIncentive = bonus;
        }
        
        employee.totalIncentive = (employee.totalIncentive || 0) + objectSchema.totalIncentive;

        employee.schemaList.push(objectSchema);

        return employee;
    });
}

const calculateStaticBonus = (employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList) => {
    if (calculatePassword(employee, objectSchema, preeliminationAndList, preeliminationOrList) === 0) {
        return 0;
    }

    let bonus = 0;
    if (clasificationList && clasificationList.length > 0) {
        const objectClassification = clasificationList.find(criteria => validateByTargetType(
            criteria.TargetTypeH, employee, objectSchema, criteria.Operator2H, criteria.ValueFromH, criteria.ValueToH
        ));
        if (objectClassification && objectClassification.detail && objectClassification.detail.length > 0) {
            const objectIncentive = objectClassification.detail.find(criteriaDetail => validateByTargetType(
                criteriaDetail.TargetTypeD, employee, objectSchema, criteriaDetail.Operator2D, 
                criteriaDetail.ValueFromD, criteriaDetail.ValueToD
            ) && criteriaDetail.Bonus);
            if (objectIncentive) {
                let tempValueFrom = objectIncentive.ValueFromD;
                if (!objectIncentive.TargetTypeH && objectIncentive.ValueFromH) {
                    tempValueFrom = objectIncentive.ValueFromH;
                }
                bonus = Math.round(tempValueFrom / 100 * (objectSchema.actual - objectSchema.target));
                bonus = (bonus >= objectIncentive.Bonus) ? objectIncentive.Bonus : bonus;
            }
        }
    }
    return bonus > 0 ? bonus : 0;
}

module.exports = {
    incentiveAddTotal
}
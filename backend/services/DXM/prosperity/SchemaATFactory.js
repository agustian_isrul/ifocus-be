const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    isExistEmployee,
    calculateBonus,
    validateByTargetType
} = require('../../../helpers/utility');

const incentiveAT = (
    schema, resultCalculate, employeeDataList, actualDataList, targetDataList, 
    productivityDataList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const filterEmployeeList = convertArrayToNestedArray(employeeDataList
    .filter(currentEmployee => filterEmployeeDataList
        .every(element => element.FilterValueList[0].FlagInclude === 1 ? 
            element.FilterValueFilter.includes(currentEmployee[element.FilterCode]) : 
            !element.FilterValueFilter.includes(currentEmployee[element.FilterCode])
        )
    ),
    'EmployeeID', 'RayonCode');

    const discountDXMProsFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'discountDXMPros');
    const actualCHDHerbaFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'actualCHDHerba');
    const indexIncentiveFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'indexIncentive');
    const ratioMRFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'ratio');
    if (indexIncentiveFilter) {
        const indexFilter = filterParameterListBySchema.indexOf(indexIncentiveFilter);
        filterParameterListBySchema.splice(indexFilter, 1);        
        if (actualCHDHerbaFilter) {
            const indexFilter = filterParameterListBySchema.indexOf(actualCHDHerbaFilter);
            filterParameterListBySchema.splice(indexFilter, 1);
        }
        if (discountDXMProsFilter) {
            const indexFilter = filterParameterListBySchema.indexOf(discountDXMProsFilter);
            filterParameterListBySchema.splice(indexFilter, 1);
        }
        if (ratioMRFilter) {
            const indexFilter = filterParameterListBySchema.indexOf(ratioMRFilter);
            filterParameterListBySchema.splice(indexFilter, 1);
        }
    }

    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);

    for (let employee of filterEmployeeList) {
        let currentEmployee = isExistEmployee(employee, resultCalculate);
        if (!currentEmployee) {
            currentEmployee = employee;
            currentEmployee.EmployeeName = employee.RayonCodeList[0].EmployeeName;
            currentEmployee.EmployeePositionCode = employee.RayonCodeList[0].EmployeePositionCode;
            currentEmployee.EmployeeName = employee.RayonCodeList[0].EmployeeName;
            currentEmployee.EmployeeAreaCode = employee.RayonCodeList[0].EmployeeAreaCode;
            currentEmployee.EmployeeSubLineDesc = employee.RayonCodeList[0].EmployeeSubLineDesc;
            currentEmployee.EmployeeMRType = employee.RayonCodeList[0].EmployeeMRType;
            currentEmployee.EmployeeVacant = employee.RayonCodeList[0].EmployeeVacant;
            const tempCurrentEmployee = setupEmployeeProperties(
                currentEmployee, schema, resultCalculate, actualDataList, targetDataList, productivityDataList,
                discountDXMProsFilter, actualCHDHerbaFilter, indexIncentiveFilter, ratioMRFilter,
                preeliminationAndList, preeliminationOrList, clasificationList
            )
            if (tempCurrentEmployee) {
                resultCalculate = resultCalculate || [];
                resultCalculate.push(tempCurrentEmployee);
            }
        } else {
            currentEmployee = calculateExistingEmployee(
                currentEmployee, schema, discountDXMProsFilter, actualCHDHerbaFilter, indexIncentiveFilter, ratioMRFilter,
                preeliminationAndList, preeliminationOrList, clasificationList);
            let dataEmployee = resultCalculate
            .find(data => data.RayonCodeFilter.includes(currentEmployee.RayonCodeFilter[0]));
            dataEmployee = currentEmployee;
        }
    }

    return resultCalculate;
}

const setupEmployeeProperties = (
    employee, schema, resultCalculate, actualDataList, targetDataList, productivityDataList,
    discountDXMProsFilter, actualCHDHerbaFilter, indexIncentiveFilter, ratioMRFilter,
    preeliminationAndList, preeliminationOrList, clasificationList
) => {
    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode
    }

    if (employee.EmployeePositionCode === 'MR') {
        objectSchema.actual = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
        .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
        objectSchema.target = targetDataList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
        .reduce((accumulator, target) => {return accumulator + target.target}, 0);
        objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
    
        employee.actualAll = objectSchema.actual;
        employee.targetAll = objectSchema.target;
        employee.actualTargetAll = objectSchema.actualTarget;
        const tempProductivity = productivityDataList.find(data => data.EmployeePositionCode === employee.EmployeePositionCode && 
            !data.EmployeeMRType);
        if (tempProductivity) {
            employee.productivity = tempProductivity.Productivitas;
        }
        employee.maxTargetProductivity = Math.max(objectSchema.target, employee.productivity);
        employee.actualTargetMax = convertNaNToZero(objectSchema.actual / employee.maxTargetProductivity * 100);
        employee.actualProductivity = convertNaNToZero(objectSchema.actual / employee.productivity * 100);
    
        /* 
        **  DXM Pros
        */
        employee.actualDXMPros = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
        .reduce((accumulator, actual) => {return accumulator + actual.actualDXM}, 0);
        employee.targetDXMPros = targetDataList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
        .reduce((accumulator, target) => {return accumulator + target.targetDXM}, 0);
        employee.actualTargetDXMPros = convertNaNToZero(employee.actualDXMPros / employee.targetDXMPros * 100);
        employee.discountValueDXMPros = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
        .reduce((accumulator, actual) => {return accumulator + actual.discountDXM}, 0);
        employee.discountDXMPros = convertNaNToZero(employee.discountValueDXMPros / employee.actualDXMPros * 100);
        const tempProductivityDXM = productivityDataList.find(data => data.EmployeePositionCode === employee.EmployeePositionCode && 
            data.EmployeeMRType === 'DXM');
        if (tempProductivityDXM) {
            employee.productivityDXMPros = tempProductivityDXM.Productivitas;
        }
        // employee.productivityDXMPros = 55000000;
        employee.actualProductivityDXMPros = convertNaNToZero(employee.actualDXMPros / employee.productivityDXMPros * 100);
        employee.maxTargetProductivityDXMPros = Math.max(employee.targetDXMPros, employee.productivityDXMPros);
        employee.actualTargetMaxDXMPros = convertNaNToZero(employee.actualDXMPros / employee.maxTargetProductivityDXMPros * 100);
    
        /* 
        **  CHD Pros
        */
        employee.actualCHDPros = employee.actualAll - employee.actualDXMPros;
        employee.targetCHDPros = employee.targetAll - employee.targetDXMPros;
        employee.actualTargetCHDPros = convertNaNToZero(employee.actualCHDPros / employee.targetCHDPros * 100);
        const tempProductivityCHD = productivityDataList.find(data => data.EmployeePositionCode === employee.EmployeePositionCode && 
            data.EmployeeMRType === 'CHD');
        if (tempProductivityCHD) {
            employee.productivityCHDPros = tempProductivityCHD.Productivitas;
        }
        // employee.productivityCHDPros = 25000000;
        employee.actualProductivityCHDPros = convertNaNToZero(employee.actualCHDPros / employee.productivityCHDPros * 100);
        employee.maxTargetProductivityCHDPros = Math.max(employee.targetCHDPros, employee.productivityCHDPros);
        employee.actualTargetMaxCHDPros = convertNaNToZero(employee.actualCHDPros / employee.maxTargetProductivityCHDPros * 100);
    
        /* 
        **  CHD HERBA FAMILY
        */
        employee.actualCHDHerba = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
        .reduce((accumulator, actual) => {return accumulator + actual.actualCHD}, 0);
    } else {
        const tempSubordinateList = Object.values(resultCalculate
        .filter(tempSubordinate => tempSubordinate.RayonCodeList
            .some(tempRayonCode => employee.RayonCodeFilter.includes(tempRayonCode.RayonCodeSup)))
        .reduce((result, item) => {
            const value = employee.RayonCode;
            const existing = result[value] || {
                rayonCode: value,
                actualAll: 0,
                targetAll: 0,
                productivity: 0,
                actualDXMPros: 0,
                targetDXMPros: 0,
                discountValueDXMPros: 0,
                productivityDXMPros: 0,
                actualCHDHerba: 0,
                countSubordinate: 0
            };
            return {
                ...result,
                [value] : {
                    ...existing,
                    actualAll: existing.actualAll + item.actualAll,
                    targetAll: existing.targetAll + item.targetAll,
                    productivity: existing.productivity + item.productivity,
                    actualDXMPros: existing.actualDXMPros + item.actualDXMPros,
                    targetDXMPros: existing.targetDXMPros + item.targetDXMPros,
                    discountValueDXMPros: existing.discountValueDXMPros + item.discountValueDXMPros,
                    productivityDXMPros: existing.productivityDXMPros + item.productivityDXMPros,
                    actualCHDHerba: existing.actualCHDHerba + item.actualCHDHerba,
                    countSubordinate: existing.countSubordinate + 1
                }
            }
        }, {}));

        if (tempSubordinateList && tempSubordinateList.length > 0) {
            objectSchema.actual = tempSubordinateList[0].actualAll;
            objectSchema.target = tempSubordinateList[0].targetAll;
            objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
        
            employee.actualAll = objectSchema.actual;
            employee.targetAll = objectSchema.target;
            employee.actualTargetAll = objectSchema.actualTarget;
            employee.productivity = tempSubordinateList[0].productivity;
            employee.maxTargetProductivity = Math.max(objectSchema.target, employee.productivity);
            employee.actualTargetMax = convertNaNToZero(objectSchema.actual / employee.maxTargetProductivity * 100);
            employee.actualProductivity = convertNaNToZero(objectSchema.actual / employee.productivity * 100);
            employee.countSubordinate = tempSubordinateList[0].countSubordinate;
        
            /* 
            **  DXM Pros
            */
            employee.actualDXMPros = tempSubordinateList[0].actualDXMPros;
            employee.targetDXMPros = tempSubordinateList[0].targetDXMPros;
            employee.actualTargetDXMPros = convertNaNToZero(employee.actualDXMPros / employee.targetDXMPros * 100);
            employee.discountValueDXMPros = tempSubordinateList[0].discountValueDXMPros;
            employee.discountDXMPros = convertNaNToZero(employee.discountValueDXMPros / employee.actualDXMPros * 100);
            employee.productivityDXMPros = tempSubordinateList[0].productivityDXMPros;
            employee.actualProductivityDXMPros = convertNaNToZero(employee.actualDXMPros / employee.productivityDXMPros * 100);
            employee.maxTargetProductivityDXMPros = Math.max(employee.targetDXMPros, employee.productivityDXMPros);
            employee.actualTargetMaxDXMPros = convertNaNToZero(employee.actualDXMPros / employee.maxTargetProductivityDXMPros * 100);
        
            /* 
            **  CHD Pros
            */
            employee.actualCHDPros = employee.actualAll - employee.actualDXMPros;
            employee.targetCHDPros = employee.targetAll - employee.targetDXMPros;
            employee.actualTargetCHDPros = convertNaNToZero(employee.actualCHDPros / employee.targetCHDPros * 100);
            employee.productivityCHDPros = tempSubordinateList[0].productivityCHDPros;
            employee.actualProductivityCHDPros = convertNaNToZero(employee.actualCHDPros / employee.productivityCHDPros * 100);
            employee.maxTargetProductivityCHDPros = Math.max(employee.targetCHDPros, employee.productivityCHDPros);
            employee.actualTargetMaxCHDPros = convertNaNToZero(employee.actualCHDPros / employee.maxTargetProductivityCHDPros * 100);
        
            /* 
            **  CHD HERBA FAMILY
            */
            employee.actualCHDHerba = tempSubordinateList[0].actualCHDHerba;
        }
    }

    if (objectSchema.actual <= 0 && objectSchema.target <= 0) {
        return null;
    }

    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
    objectSchema.incentive = bonus;
    
    if (bonus > 0 && indexIncentiveFilter) {
        let isValidDiscountDXMPros = false;
        if (bonus > 0 && discountDXMProsFilter) {
            isValidDiscountDXMPros = validateByTargetType(discountDXMProsFilter.TargetTypeH, employee, objectSchema, 
                discountDXMProsFilter.Operator2H, discountDXMProsFilter.ValueFromH, discountDXMProsFilter.ValueToH);
        }
        let isValidActualCHDHerba = false;
        if (bonus > 0 && actualCHDHerbaFilter) {
            let tempValueFromH = actualCHDHerbaFilter.ValueFromH;
            if (ratioMRFilter) {
                tempValueFromH = employee[actualCHDHerbaFilter.ValueFromH] * ratioMRFilter.ValueFromH;
            }
            isValidActualCHDHerba = validateByTargetType(actualCHDHerbaFilter.TargetTypeH, employee, objectSchema, 
                actualCHDHerbaFilter.Operator2H, tempValueFromH, actualCHDHerbaFilter.ValueToH);
        }
        if (bonus > 0 && (isValidDiscountDXMPros || isValidActualCHDHerba)) {
            let indexIncentive = 0;
            if (indexIncentiveFilter) {
                indexIncentive = indexIncentiveFilter.ValueFromH;
            }    
            objectSchema.totalIncentive = Math.round(bonus * indexIncentive / 100);
        } else {    
            objectSchema.totalIncentive = bonus;
        }
    } else {    
        objectSchema.totalIncentive = bonus;
    }
    
    employee.totalIncentive = (employee.totalIncentive || 0) + objectSchema.totalIncentive;

    employee.schemaList = employee.schemaList || [];
    employee.schemaList.push(objectSchema);

    return employee;
}

const calculateExistingEmployee = (
    employee, schema, discountDXMProsFilter, actualCHDHerbaFilter, indexIncentiveFilter, ratioMRFilter,
    preeliminationAndList, preeliminationOrList, clasificationList
) => {
    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode,
        actual : employee.actualAll,
        target : employee.targetAll,
        actualTarget: employee.actualTargetAll
    }
    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
    objectSchema.incentive = bonus;
    
    if (bonus > 0 && indexIncentiveFilter) {
        let isValidDiscountDXMPros = false;
        if (bonus > 0 && discountDXMProsFilter) {
            isValidDiscountDXMPros = validateByTargetType(discountDXMProsFilter.TargetTypeH, employee, objectSchema, 
                discountDXMProsFilter.Operator2H, discountDXMProsFilter.ValueFromH, discountDXMProsFilter.ValueToH);
        }
        let isValidActualCHDHerba = false;
        if (bonus > 0 && actualCHDHerbaFilter) {
            let tempValueFromH = actualCHDHerbaFilter.ValueFromH;
            if (ratioMRFilter) {
                tempValueFromH = employee[actualCHDHerbaFilter.ValueFromH] * ratioMRFilter.ValueFromH;
            }
            isValidActualCHDHerba = validateByTargetType(actualCHDHerbaFilter.TargetTypeH, employee, objectSchema, 
                actualCHDHerbaFilter.Operator2H, tempValueFromH, actualCHDHerbaFilter.ValueToH);
        }
        if (bonus > 0 && (isValidDiscountDXMPros || isValidActualCHDHerba)) {
            let indexIncentive = 0;
            if (indexIncentiveFilter) {
                indexIncentive = indexIncentiveFilter.ValueFromH;
            }    
            objectSchema.totalIncentive = Math.round(bonus * indexIncentive / 100);
        } else {    
            objectSchema.totalIncentive = bonus;
        }
    } else {    
        objectSchema.totalIncentive = bonus;
    }
    
    employee.totalIncentive = (employee.totalIncentive || 0) + objectSchema.totalIncentive;

    employee.schemaList = employee.schemaList || [];
    employee.schemaList.push(objectSchema);

    return employee;
}

module.exports = {
    incentiveAT
}
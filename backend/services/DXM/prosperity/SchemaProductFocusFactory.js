const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculateBonus,
    validateByTargetType
} = require('../../../helpers/utility');

const incentiveProductFocus = (
    schema, resultCalculate, actualProductList, targetProductList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');

    const discountDXMProsFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'discountDXMPros');
    const actualTargetUnitFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'actualTarget');
    const indexIncentiveFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'indexIncentive');
    if (discountDXMProsFilter) {
        const indexFilter = filterParameterListBySchema.indexOf(discountDXMProsFilter);
        filterParameterListBySchema.splice(indexFilter, 1);        
        if (actualTargetUnitFilter) {
            const indexFilter = filterParameterListBySchema.indexOf(actualTargetUnitFilter);
            filterParameterListBySchema.splice(indexFilter, 1);
        }
        if (indexIncentiveFilter) {
            const indexFilter = filterParameterListBySchema.indexOf(indexIncentiveFilter);
            filterParameterListBySchema.splice(indexFilter, 1);
        }
    }
    
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        let tempObjectSchemaList = [];
        let tempTotalIncentive = 0;
        if (employee.EmployeePositionCode === 'MR') {
            const tempTargetList = Object.values(targetProductList.reduce((result, item) => {
                const value = item.categoryProduct;
                const existing = result[value] || {
                    categoryProduct: value,
                    target: 0,
                    categoryProductList: []
                };
                return {
                    ...result,
                    [value] : {
                        ...existing,
                        target: !item.ProductDesc ? item.target : existing.target,
                        categoryProductList: item.ProductDesc ? [...existing.categoryProductList, item] : existing.categoryProductList
                    }
                }
            }, {}));
    
            const tempActualList = actualProductList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode));
            for (const tempTarget of tempTargetList) {
                let tempCountExceedTarget = 0;
                let tempTotalActual = 0;
                const tempProductSchemaList = [];
                for (const tempProduct of tempTarget.categoryProductList) {
                    const tempSchema = {                    
                        schemaId: schema.SchemaId,
                        schemaLabel: schema.SchemaLabel,
                        rayonCode: employee.RayonCode,
                        categoryProduct: tempProduct.ProductDesc
                    }
                    const tempActualPerProduct = tempActualList.find(actual => actual.productDesc === tempProduct.ProductDesc);
                    if (tempActualPerProduct && tempActualPerProduct.actualUnit > 0) {
                        tempSchema.actual = tempActualPerProduct.actualUnit;
                        tempTotalActual = tempTotalActual + tempActualPerProduct.actual;
                    } else {
                        tempSchema.actual = 0;
                    }
                    tempSchema.target = tempProduct.target;
                    tempSchema.actualTarget = convertNaNToZero(tempSchema.actual / tempSchema.target * 100);
    
                    tempProductSchemaList.push(tempSchema);
    
                    let isValidActualTargetUnit = false;
                    if (actualTargetUnitFilter) {
                        isValidActualTargetUnit = validateByTargetType(actualTargetUnitFilter.TargetTypeH, employee, tempSchema, 
                            actualTargetUnitFilter.Operator2H, actualTargetUnitFilter.ValueFromH, actualTargetUnitFilter.ValueToH);
                    }
                    if (isValidActualTargetUnit) {
                        tempCountExceedTarget = tempCountExceedTarget + 1;
                    }
                }
                const objectSchema = {                    
                    schemaId: schema.SchemaId,
                    schemaLabel: schema.SchemaLabel,
                    rayonCode: employee.RayonCode,
                    categoryProduct: tempTarget.categoryProduct
                }
                objectSchema.actual = tempTotalActual;
                objectSchema.target = tempTarget.target;
                objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
                objectSchema.countProduct = tempCountExceedTarget;
                objectSchema.unitProductList = tempProductSchemaList;
    
                const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
                objectSchema.incentive = bonus;
                tempTotalIncentive = tempTotalIncentive + bonus;
    
                tempObjectSchemaList.push(objectSchema);
            }
        } else {            
            const tempSubordinateList = resultCalculate
            .filter(tempSubordinate => tempSubordinate.RayonCodeList
                .some(tempRayonCode => employee.RayonCodeFilter.includes(tempRayonCode.RayonCodeSup)));
            let tempSchemaList = [];
            if (tempSubordinateList && tempSubordinateList.length > 0) {
                for (const tempSubordinate of tempSubordinateList) {
                    tempSchemaList = tempSchemaList.concat(tempSubordinate.schemaList
                        .filter(tempSchema => tempSchema.schemaId === schema.SchemaId));
                }
            }
            if (tempSchemaList && tempSchemaList.length > 0) {
                tempSchemaList = Object.values(tempSchemaList.reduce((result, item) => {
                    const value = item.categoryProduct;
                    const existing = result[value] || {
                        categoryProduct: value,
                        actual: 0,
                        target: 0,
                        unitProductList: []
                    };
                    return {
                        ...result,
                        [value] : {
                            ...existing,
                            actual: existing.actual + item.actual,
                            target: existing.target + item.target,
                            unitProductList: existing.unitProductList.concat(item.unitProductList)
                        }
                    }
                }, {}));
                for (const tempSchema of tempSchemaList) {
                    let tempCountExceedTarget = 0;
                    const tempProductSchemaList = [];
                    tempSchema.unitProductList = Object.values(tempSchema.unitProductList.reduce((result, item) => {
                        const value = item.categoryProduct;
                        const existing = result[value] || {
                            categoryProduct: value,
                            actualUnit: 0,
                            targetUnit: 0
                        };
                        return {
                            ...result,
                            [value] : {
                                ...existing,
                                actualUnit: existing.actualUnit + item.actualUnit,
                                targetUnit: existing.targetUnit + item.targetUnit
                            }
                        }
                    }, {}));
                    for (const tempUnit of tempSchema.unitProductList) {
                        const tempObjectSchema = {                    
                            schemaId: schema.SchemaId,
                            schemaLabel: schema.SchemaLabel,
                            rayonCode: employee.RayonCode,
                            categoryProduct: tempUnit.categoryProduct,
                            actual: tempUnit.actualUnit,
                            target: tempUnit.targetUnit
                        }
                        tempObjectSchema.actualTarget = convertNaNToZero(tempObjectSchema.actual / tempObjectSchema.target * 100);
        
                        tempProductSchemaList.push(tempObjectSchema);
        
                        let isValidActualTargetUnit = false;
                        if (actualTargetUnitFilter) {
                            isValidActualTargetUnit = validateByTargetType(actualTargetUnitFilter.TargetTypeH, employee, tempObjectSchema, 
                                actualTargetUnitFilter.Operator2H, actualTargetUnitFilter.ValueFromH, actualTargetUnitFilter.ValueToH);
                        }
                        if (isValidActualTargetUnit) {
                            tempCountExceedTarget = tempCountExceedTarget + 1;
                        }
                    }

                    const objectSchema = {                    
                        schemaId: schema.SchemaId,
                        schemaLabel: schema.SchemaLabel,
                        rayonCode: employee.RayonCode,
                        categoryProduct: tempSchema.categoryProduct
                    }
                    objectSchema.actual = tempSchema.actual;
                    objectSchema.target = tempSchema.target;
                    objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
                    objectSchema.countProduct = tempCountExceedTarget;
                    objectSchema.unitProductList = tempProductSchemaList;
        
                    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
                    objectSchema.incentive = bonus;
                    tempTotalIncentive = tempTotalIncentive + bonus;
        
                    tempObjectSchemaList.push(objectSchema);
                }
            }
        }
    
        let isValidDiscountDXMPros = false;
        if (tempTotalIncentive > 0 && discountDXMProsFilter) {
            isValidDiscountDXMPros = validateByTargetType(discountDXMProsFilter.TargetTypeH, employee, null, 
                discountDXMProsFilter.Operator2H, discountDXMProsFilter.ValueFromH, discountDXMProsFilter.ValueToH);
        }
        if (tempTotalIncentive > 0 && isValidDiscountDXMPros) {
            let indexIncentive = 0;
            if (indexIncentiveFilter && indexIncentiveFilter.ValueFromH > 0) {
                indexIncentive = indexIncentiveFilter.ValueFromH;
            }
            tempTotalIncentive = Math.round(tempTotalIncentive * indexIncentive / 100);
        }
        
        employee.totalIncentive = (employee.totalIncentive || 0) + tempTotalIncentive;

        tempObjectSchemaList = tempObjectSchemaList.map(tempObject => {
            tempObject.totalIncentive = tempTotalIncentive;
            employee.schemaList.push(tempObject);
            return tempObject;
        });

        return employee;
    });
}

module.exports = {
    incentiveProductFocus
}
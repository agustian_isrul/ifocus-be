const { 
    convertArrayToNestedArray,
    validateByTargetType,
    calculatePassword
} = require('../../../helpers/utility');

const incentiveRerata = (schema, resultCalculate, filterDataListBySchema, filterParameterListBySchema) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');

    const discountDXMProsFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'discountDXMPros');
    const actualCHDHerbaFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'actualCHDHerba');
    const indexIncentiveFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'indexIncentive');
    const ratioMRFilter = filterParameterListBySchema.find(data => data.TargetTypeH === 'ratio');
    if (indexIncentiveFilter) {
        const indexFilter = filterParameterListBySchema.indexOf(indexIncentiveFilter);
        filterParameterListBySchema.splice(indexFilter, 1);        
        if (actualCHDHerbaFilter) {
            const indexFilter = filterParameterListBySchema.indexOf(actualCHDHerbaFilter);
            filterParameterListBySchema.splice(indexFilter, 1);
        }
        if (discountDXMProsFilter) {
            const indexFilter = filterParameterListBySchema.indexOf(discountDXMProsFilter);
            filterParameterListBySchema.splice(indexFilter, 1);
        }
        if (ratioMRFilter) {
            const indexFilter = filterParameterListBySchema.indexOf(ratioMRFilter);
            filterParameterListBySchema.splice(indexFilter, 1);
        }
    }

    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode
        }
        
        objectSchema.actual = employee.actualAll;
        objectSchema.target = employee.targetAll;
        objectSchema.actualTarget = employee.actualTargetAll;
        objectSchema.totalIncentiveSubordinate = 0;

        if (employee.EmployeePositionCode !== 'MR') {
            const tempSubordinateList = resultCalculate
            .filter(tempSubordinate => tempSubordinate.RayonCodeList
                .some(tempRayonCode => employee.RayonCodeFilter.includes(tempRayonCode.RayonCodeSup)));
            
            if (tempSubordinateList && tempSubordinateList.length > 0) {
                for (const tempSubordinate of tempSubordinateList) {
                    objectSchema.totalIncentiveSubordinate = objectSchema.totalIncentiveSubordinate + 
                    tempSubordinate.schemaList.filter(tempSchema => tempSchema.schemaId === 1)
                    .reduce((accumulator, tempSchema) => {return accumulator + tempSchema.totalIncentive}, 0);
                    
                    const tempSchema2 = tempSubordinate.schemaList.find(tempSchema => tempSchema.schemaId === 2);
                    if (tempSchema2 && tempSchema2.totalIncentive > 0) {
                        objectSchema.totalIncentiveSubordinate = objectSchema.totalIncentiveSubordinate + tempSchema2.totalIncentive;
                    }
                    
                    const tempSchema5 = tempSubordinate.schemaList.find(tempSchema => tempSchema.schemaId === 5);
                    if (tempSchema5 && tempSchema5.totalIncentive > 0) {
                        objectSchema.totalIncentiveSubordinate = objectSchema.totalIncentiveSubordinate + tempSchema5.totalIncentive;
                    }
                }
            }
        }
    
        const bonus = calculateStaticBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList);
        objectSchema.incentive = bonus;
    
        if (bonus > 0 && indexIncentiveFilter) {
            let isValidDiscountDXMPros = false;
            if (bonus > 0 && discountDXMProsFilter) {
                isValidDiscountDXMPros = validateByTargetType(discountDXMProsFilter.TargetTypeH, employee, objectSchema, 
                    discountDXMProsFilter.Operator2H, discountDXMProsFilter.ValueFromH, discountDXMProsFilter.ValueToH);
            }
            let isValidActualCHDHerba = false;
            if (bonus > 0 && actualCHDHerbaFilter) {
                let tempValueFromH = actualCHDHerbaFilter.ValueFromH;
                if (ratioMRFilter) {
                    tempValueFromH = employee[actualCHDHerbaFilter.ValueFromH] * ratioMRFilter.ValueFromH;
                }
                isValidActualCHDHerba = validateByTargetType(actualCHDHerbaFilter.TargetTypeH, employee, objectSchema, 
                    actualCHDHerbaFilter.Operator2H, tempValueFromH, actualCHDHerbaFilter.ValueToH);
            }
            if (bonus > 0 && (isValidDiscountDXMPros || isValidActualCHDHerba)) {
                let indexIncentive = 0;
                if (indexIncentiveFilter) {
                    indexIncentive = indexIncentiveFilter.ValueFromH;
                }    
                objectSchema.totalIncentive = Math.round(bonus * indexIncentive / 100);
            } else {    
                objectSchema.totalIncentive = bonus;
            }
        } else {    
            objectSchema.totalIncentive = bonus;
        }
        
        employee.totalIncentive = (employee.totalIncentive || 0) + objectSchema.totalIncentive;

        employee.schemaList.push(objectSchema);

        return employee;
    });
}

const calculateStaticBonus = (employee, objectSchema, preeliminationAndList, preeliminationOrList) => {
    if (calculatePassword(employee, objectSchema, preeliminationAndList, preeliminationOrList) === 0) {
        return 0;
    }
    if (employee.countSubordinate > 0) {
        return Math.round(objectSchema.totalIncentiveSubordinate / employee.countSubordinate);
    }
    return 0;
}

module.exports = {
    incentiveRerata
}
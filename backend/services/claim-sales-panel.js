const moment = require("moment-timezone");

const knex = require('../../config/database');

function findSalesBranchList(outletPanel) {
    return knex.with('table_claim_sales', (qb) => {
        qb.select(
            "m23.Sitecode", "m23.CustomerAreaID", "m23.ProductID", "m23.LineID", 
            knex.raw("SUM(m23.UnitSales) AS UnitSales")
        )
        .from("m23_claim_sales_panel AS m23")
        .leftJoin('m17_system_parameter AS m17', function() {
            this.on(knex.raw("m17.keyname = 'period_incentive'"));
        })
        .where(knex.raw("EXTRACT(YEAR_MONTH FROM m23.InvoiceDate) = m17.value"))
        .groupBy("m23.CustomerAreaID", "m23.LineID", "m23.SiteCode", "m23.ProductID")
    })
    .select(
        "m22.Invoice", "m22.InvoiceID", "m22.AreaID", "m22.AreaName AS AreaCode", "m22.ProductOra AS ProductID", "m22.ProductName",
        "m22.StatusFaktur", "m22.ListPrice AS PriceSales", "m22.Unit AS UnitSales", "m22.SalesHNA AS ValueSales",
        knex.raw("case when m23.UnitSales IS NULL then 0 ELSE m23.UnitSales END AS ClaimedUnit"), 
        knex.raw("m22.Unit - case when m23.UnitSales IS NULL then 0 ELSE m23.UnitSales END AS AvailableUnit"),
        knex.raw("m22.DiscAFCab + m22.DiscAFPrin AS PAP"),
        knex.raw("m22.DiscLFCab + m22.DiscLFPrin AS PLP"),
        knex.raw("0 as EditUnit"),
        knex.raw("NULL AS Remark"),
        "m22.InvoiceDate", "m22.OrderDate", "m22.LineProduct"
    )
    .from('m22_sales_branch AS m22')
    .leftJoin('m17_system_parameter AS m17', function() {
        this.on(knex.raw("m17.keyname = 'period_incentive'"));
    })
    .leftJoin('table_claim_sales AS m23', function() {
        this.on("m23.Sitecode", "=", "m22.SiteCode")
        .on("m23.ProductID", "=", "m22.ProductOra")
        .on("m23.CustomerAreaID", "=", "m22.AreaID")
        .on("m23.LineID", "=", "m22.LineID")
    })
    .where(knex.raw("EXTRACT(YEAR_MONTH FROM m22.InvoiceDate) = m17.value"))
    .where(function() {
        this.where('m22.Unit', '<>', 'm23.UnitSales')
        .orWhereNull("m23.UnitSales")
    })
    .where(function() {
        if (outletPanel) {
            if (outletPanel.CompanyID) {
                this.where('m22.CompanyID', outletPanel.CompanyID);
            }
            if (outletPanel.area) {
                this.where('m22.AreaID', outletPanel.area.AreaID);
            }
            if (outletPanel.lini) {
                this.where('m22.LineID', outletPanel.lini.LineID);
            }
            if (outletPanel.Sitecode) {
                this.where('m22.SiteCode', outletPanel.Sitecode);
            }
            if (outletPanel.ProductID) {
                this.where('m22.ProductOra', outletPanel.ProductID);
            }
        }
    })
}

function saveData(bodyData, profileUser) {
    try {
        const today = moment();
        return knex.transaction(async trx => {
            for (const tempClaimSales of bodyData.claimSalesList) {                
                const tempClaimSalesTable = await trx.select(knex.raw("SUM(m23.UnitSales) AS UnitSales"))
                .from("m23_claim_sales_panel AS m23")
                .leftJoin('m17_system_parameter AS m17', function() {
                    this.on(knex.raw("m17.keyname = 'period_incentive'"));
                })
                .where(knex.raw("EXTRACT(YEAR_MONTH FROM m23.InvoiceDate) = m17.value"))
                .where({
                    CustomerAreaID: bodyData.area.AreaID,
                    LineID: bodyData.lini.LineID,
                    Sitecode: bodyData.Sitecode,
                    ProductID: tempClaimSales.ProductID
                })
                .groupBy("m23.CustomerAreaID", "m23.LineID", "m23.Sitecode", "m23.ProductID")
                .first();
                if (tempClaimSalesTable && tempClaimSales.UnitSales < 0 && tempClaimSalesTable.UnitSales < tempClaimSales.UnitSales) {
                    //throw new Error('Insufficient stock');
                    continue;
                }
                if (tempClaimSalesTable && tempClaimSales.UnitSales > 0 && tempClaimSalesTable.UnitSales > tempClaimSales.UnitSales) {
                    // throw new Error('Insufficient stock');
                    continue;
                }
                const tempOrderDate = moment(tempClaimSales.OrderDate);
                await trx('m23_claim_sales_panel').insert({
                    EmployeeID: bodyData.EmployeeID,
                    PosStructureID: profileUser.PosStructureID,
                    RayonCode: bodyData.RayonCode,
                    EmployeeName: bodyData.EmployeeName,
                    Sitecode: bodyData.Sitecode,
                    OutletName: bodyData.OutletName,
                    CustomerAreaID: bodyData.area.AreaID,
                    CustomerAreaCode: bodyData.area.AreaCode,
                    AreaID: bodyData.area.AreaID,
                    AreaCode: bodyData.area.AreaCode,
                    ClinicCode: bodyData.ClinicCode,
                    ClinicName: bodyData.ClinicName,
                    ProductID: tempClaimSales.ProductID,
                    ProductName: tempClaimSales.ProductName,
                    LineProduct: tempClaimSales.LineProduct,
                    LineID: bodyData.lini.LineID,
                    LineName: bodyData.lini.LineName,
                    Invoice: tempClaimSales.Invoice,
                    InvoiceDate: moment(tempClaimSales.InvoiceDate).format('YYYY-MM-DD'),
                    DateSales: tempOrderDate.date(),
                    MonthSales: tempOrderDate.month() + 1,
                    YearSales: tempOrderDate.year(),
                    ListPrice: tempClaimSales.PriceSales,
                    UnitSales: tempClaimSales.EditUnit,
                    ValueSales: tempClaimSales.PriceSales * tempClaimSales.EditUnit,
                    PAP: tempClaimSales.PAP,
                    PLP: tempClaimSales.PLP,
                    InvoiceID: tempClaimSales.InvoiceID,
                    ChannelCustID: bodyData.channel.ChannelID,
                    Remark: tempClaimSales.Remark
                });
            }
            
            const tableHistory = {
                table_name: 'm23_claim_sales_panel',
                table_action: 2,
                table_data: JSON.stringify(bodyData),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        })
    } catch (error) {
        throw error;
    }
}

function findClaimSalesList(criteriaData) {
    return knex.select(
        'm23.AreaID', 'm23.AreaCode', 'm23.LineID', 'm23.LineName', 'm23.Sitecode', 'm23.OutletName',
        'm23.EmployeeID', 'm23.RayonCode', 'm23.EmployeeName', 'm23.ProductID', 'm23.ProductName', 'm23.LineProduct',
        'm23.ClinicCode', 'm23.ClinicName', 'm23.InvoiceID', 'm23.Invoice', 'm23.InvoiceDate', 
        'm23.ListPrice', 'm23.UnitSales', 'm23.ValueSales', 'm23.PAP', 'm23.PLP', 'm23.Remark'
    )
    .from("m23_claim_sales_panel AS m23")
    .leftJoin('m17_system_parameter AS m17', function() {
        this.on(knex.raw("m17.keyname = 'period_incentive'"));
    })
    .where(knex.raw("EXTRACT(YEAR_MONTH FROM m23.InvoiceDate) = m17.value"))
    .where(function() {
        if (criteriaData) {
            if (criteriaData.area) {
                this.where('m23.AreaID', criteriaData.area.AreaID);
            }
            if (criteriaData.lini) {
                this.where('m23.LineID', criteriaData.lini.LineID);
            }
            if (criteriaData.channel) {
                this.where('m23.ChannelCustID', criteriaData.channel.ChannelID);
            }
            if (criteriaData.Sitecode) {
                this.where('m23.SiteCode', criteriaData.Sitecode);
            }
            if (criteriaData.ProductID) {
                this.where('m23.ProductID', criteriaData.ProductID);
            }
            if (criteriaData.RayonCode) {
                this.where('m23.RayonCode', criteriaData.RayonCode);
            }
        }
    })
}

module.exports = {
    findSalesBranchList,
    saveData,
    findClaimSalesList
}
const moment = require("moment-timezone");

const knex = require('../../config/database')

function findPageData(outletPanel, pageSetting) {
    return knex.with('table_data', (qb) => {
        qb.select(
            "t08.ClinicID",
            "t08.CompanyID",
            "t08.PosStructureID",
            "t08.AreaID",
            "t08.AreaCode",
            "t08.AreaName",
            "t08.LineID",
            "t08.LineName",
            "t08.ClinicCode",
            "t08.ClinicName",
            "t08.ClinicAddress",
            "t08.CustomerPanelID",
            "t08.StartDate",
            "t08.EndDate",
            "t08.isActive",
            "t08.UserID",
            "t08.LastUpdate",
            "t08.ApprovalStatus",
            "t08.ApprovalDate",
            knex.raw(
                "case when CURDATE() between t08.StartDate and t08.EndDate and t08.ApprovalDate is not null then 'Active' " +
                "else 'Non Active' end as status"
            ),
            "t02.Sitecode",
            "t02.OutletName",
            "t02.OutletAddress"
        )
        .from('t08_clinic_panel as t08')
        .leftJoin('t02_outlet_panel as t02', 't02.CustomerPanelID', 't08.CustomerPanelID')
        .where(function() {
            if (outletPanel) {
                if (outletPanel.CompanyID) {
                    this.where('t08.CompanyID', outletPanel.CompanyID);
                }
                if (outletPanel.PosStructureID) {
                    this.where('t08.PosStructureID', outletPanel.PosStructureID);
                }
                if (outletPanel.area) {
                    this.where('t08.AreaID', outletPanel.area.AreaID);
                }
                if (outletPanel.lini) {
                    this.where('t08.LineID', outletPanel.lini.LineID);
                }
                if (outletPanel.ClinicName) {
                    this.whereLike('t08.ClinicName', '%' + outletPanel.ClinicName.replaceAll(' ', '%') + '%');
                }
                if (outletPanel.Sitecode) {
                    this.where('t02.Sitecode', outletPanel.Sitecode);
                }
                if (outletPanel.channel) {
                    this.where('t02.ChannelID', outletPanel.channel.ChannelID);
                }
                if (outletPanel.customerList && outletPanel.customerList.length > 0) {
                    const tempCustomerList = outletPanel.customerList.map(data => {
                        return data.CustomerPanelID;
                    });
                    this.whereIn('t08.CustomerPanelID', tempCustomerList);
                }
                if (outletPanel.StartDate) {
                    const formatStartDate = moment(outletPanel.StartDate).format('YYYY-MM-DD');
                    this.where('t08.StartDate', '>=', formatStartDate);
                }
                if (outletPanel.EndDate) {
                    const formatEndDate = moment(outletPanel.EndDate).format('YYYY-MM-DD');                    
                    this.where('t08.EndDate', '<=', formatEndDate);
                }
                if (outletPanel.status) {
                    const today = moment().format('YYYY-MM-DD');
                    this.where('t08.StartDate', '<=', today)
                    .where('t08.EndDate', '>=', today);
                    if (outletPanel.status === 'Non Active') {
                        this.whereNull('t08.ApprovalDate');
                    }
                    if (outletPanel.status === 'Active') {
                        this.whereNotNull('t08.ApprovalDate');
                    }
                }
            }
        })        
        .where('t08.isActive', '<>', 3)
    })
    .select('*', knex.raw('select count(*) from table_data').wrap('(', ') totalRows'))
    .from('table_data')
    .limit(pageSetting && pageSetting.rows > 0 ? pageSetting.rows : 10)
    .offset(pageSetting && pageSetting.first > 0 ? pageSetting.first : 0)
    .orderBy(
        pageSetting && pageSetting.sortField ? pageSetting.sortField : 'ClinicID',
        pageSetting && pageSetting.sortOrder === 1 ? 'asc' : 'desc'
    );
}

function saveData(tempClinicMasterList, profileUser) {
    try {
        const today = moment();        
        return knex.transaction(async trx => {
            for (const tempNewClinic of tempClinicMasterList) {
                const tempNewStartDate = moment(tempNewClinic.StartDate);
                const tempNewEndDate = moment(tempNewClinic.EndDate);
                const tempEditEndDate = Object.create(moment(tempNewClinic.StartDate)).subtract(1, 'd');

                const currentDataList = await trx('t08_clinic_panel as t08')
                .where('t08.AreaID', tempNewClinic.AreaID)
                .where('t08.LineID', tempNewClinic.LineID)
                .where('t08.CustomerPanelID', tempNewClinic.CustomerPanelID)
                .where('t08.CompanyID', tempNewClinic.CompanyID)
                .whereRaw('(? between t08.StartDate and t08.EndDate)', [tempNewStartDate.format('YYYY-MM-DD')]);

                if (currentDataList && currentDataList.length > 0) {
                    for (const tempOldClinic of currentDataList) {
                        const tempOldStartDate = moment(tempOldClinic.StartDate);
                        tempNewClinic.ClinicCode = tempOldClinic.ClinicCode;

                        // old period = new period => delete
                        if (tempOldStartDate.isSameOrAfter(tempNewStartDate, "date")) {
                            await trx('t08_clinic_panel')
                            .where({ClinicID: tempOldClinic.ClinicID})
                            .del();
                        }
    
                        // old period < new period => cut off
                        if (tempOldStartDate.isBefore(tempNewStartDate, "date")) {
                            await trx('t08_clinic_panel')
                            .where('ClinicID', tempOldClinic.ClinicID)
                            .update({
                                EndDate: tempEditEndDate.format('YYYY-MM-DD'),
                                LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                            });
                        }
                    }
                }

                tempNewClinic.StartDate = tempNewStartDate.format('YYYY-MM-DD');
                tempNewClinic.EndDate = tempNewEndDate.format('YYYY-MM-DD');
                tempNewClinic.LastUpdate = today.format('YYYY-MM-DD hh:mm:ss');
                if (!tempNewClinic.ClinicCode) {
                    await trx('m19_running_number').where({keyname: 'ClinicCode'}).increment('increment_value', 1);
                    const tempClinicIDList = await trx('m19_running_number').where({keyname: 'ClinicCode'});
                    tempNewClinic.ClinicCode = tempNewClinic.AreaCode + "-" + tempNewStartDate.format('MM') +
                    tempNewStartDate.format('YYYY').toString().substring(2, 4) +
					('0000' + tempClinicIDList[0].increment_value).slice(-6);
                }
                await trx('t08_clinic_panel').insert(tempNewClinic);
                //tempClinicIDList[0].increment_value.padStart(6, '0')
                // ('0000' + tempClinicIDList[0].increment_value).slice(-6);
            }
            
            const tableHistory = {
                table_name: 't08_clinic_panel',
                table_action: 2,
                table_data: JSON.stringify(tempClinicMasterList),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        })
    } catch (error) {
        throw error;
    }
}

function approveData(bodyDataList, profileUser) {
    const today = moment();
    try {
        return knex.transaction(async trx => {
            for (const data of bodyDataList) {
                await trx('t08_clinic_panel')
                    .where('ClinicID', data.ClinicID)
                    .update({
                        isActive: 1,
                        ApprovalStatus: profileUser.UserID,
                        ApprovalDate: today.format('YYYY-MM-DD hh:mm:ss'),
                        LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                    });
            }
            const tableHistory = {
                table_name: 't08_clinic_panel',
                table_action: 4,
                table_data: JSON.stringify(bodyDataList),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        });
    } catch (error) {
        return error;
    }
}

function rejectData(bodyDataList, profileUser) {
    const today = moment();
    try {
        return knex.transaction(async trx => {
            for (const data of bodyDataList) {
                await trx('t08_clinic_panel')
                    .where('ClinicID', data.ClinicID)
                    .update({
                        isActive: 3,
                        ApprovalStatus: profileUser.UserID,
                        ApprovalDate: today.format('YYYY-MM-DD hh:mm:ss'),
                        LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                    });
            }
            const tableHistory = {
                table_name: 't08_clinic_panel',
                table_action: 5,
                table_data: JSON.stringify(bodyDataList),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        });
    } catch (error) {
        return error;
    }
}

function cutOffData(bodyData, profileUser) {
    const today = moment();
    try {
        return knex.transaction(async trx => {
            for (const tempData of bodyData) {
                await trx('t08_clinic_panel')
                .where('ClinicID', tempData.ClinicID)
                .update({
                    ClinicAddress: tempData.ClinicAddress,
                    EndDate: moment(tempData.EndDate).format('YYYY-MM-DD'),
                    LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                });
            }
            const tableHistory = {
                table_name: 't08_clinic_panel',
                table_action: 3,
                table_data: JSON.stringify(bodyData),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        });
    } catch (error) {
        return error;
    }
}

function deleteData(bodyDataList, profileUser) {
    try {
        const today = moment();
        return knex.transaction(async trx => {
            for (const tempBodyData of bodyDataList) {
                await trx('t08_clinic_panel').where({ClinicID: tempBodyData.ClinicID}).del();
            }
            const tableHistory = {
                table_name: 't08_clinic_panel',
                table_action: 6,
                table_data: JSON.stringify(bodyData),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        });
    } catch (error) {
        return error;
    }
}

module.exports = {
    findPageData,
    saveData,
    approveData,
    rejectData,
    cutOffData,
    deleteData
}
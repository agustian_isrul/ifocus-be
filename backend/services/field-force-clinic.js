const moment = require("moment-timezone");

const knex = require('../../config/database')

function findPageData(outletPanel, pageSetting) {
    return knex.with('table_data', (qb) => {
        qb.select(
            "t09.FFClinicID",
            "t09.CompanyID",
            "t09.PosStructureID",
            "t09.AreaID",
            "t09.AreaCode",
            "t09.AreaName",
            "t09.LineID",
            "t09.LineName",
            "t09.RayonCode",
            "t09.EmployeeID",
            "t09.EmployeeName",
            "t09.EmployeePosition",
            "t09.EmployeeMRType",
            "t09.ClinicID",
            "t09.StartDate",
            "t09.EndDate",
            "t09.isActive",
            "t09.UserID",
            "t09.LastUpdate",
            "t09.ApprovalStatus",
            "t09.ApprovalDate",
            knex.raw(
                "case when CURDATE() between t09.StartDate and t09.EndDate and t09.ApprovalDate is not null then 'Active' " +
                "else 'Non Active' end as status"
            ),
            "t08.ClinicCode",
            "t08.ClinicName",
            "t08.ClinicAddress"
        )
        .from('t09_ff_clinic as t09')
        .leftJoin('t08_clinic_panel as t08', 't08.ClinicID', 't09.ClinicID')
        .where(function() {
            if (outletPanel) {
                if (outletPanel.CompanyID) {
                    this.where('t09.CompanyID', outletPanel.CompanyID);
                }
                if (outletPanel.PosStructureID) {
                    this.where('t09.PosStructureID', outletPanel.PosStructureID);
                }
                if (outletPanel.area) {
                    this.where('t09.AreaID', outletPanel.area.AreaID);
                }
                if (outletPanel.lini) {
                    this.where('t09.LineID', outletPanel.lini.LineID);
                }
                if (outletPanel.RayonCode) {
                    this.where('t09.RayonCode', outletPanel.RayonCode);
                }
                if (outletPanel.EmployeeID) {
                    this.where('t09.EmployeeID', outletPanel.EmployeeID);
                }
                if (outletPanel.EmployeeName) {
                    this.where('t09.EmployeeName', outletPanel.EmployeeName);
                }
                if (outletPanel.EmployeePosition) {
                    this.where('t09.EmployeePosition', outletPanel.EmployeePosition);
                }
                if (outletPanel.ClinicCode) {
                    this.where('t08.ClinicCode', outletPanel.ClinicCode);
                }
                if (outletPanel.ClinicName) {
                    this.whereLike('t08.ClinicName', '%' + outletPanel.ClinicName.replaceAll(' ', '%') + '%');
                }
                if (outletPanel.StartDate) {
                    const formatStartDate = moment(outletPanel.StartDate).format('YYYY-MM-DD');
                    this.where('t09.StartDate', '>=', formatStartDate);
                }
                if (outletPanel.EndDate) {
                    const formatEndDate = moment(outletPanel.EndDate).format('YYYY-MM-DD');                    
                    this.where('t09.EndDate', '<=', formatEndDate);
                }
                if (outletPanel.status) {
                    const today = moment().format('YYYY-MM-DD');
                    this.where('t09.StartDate', '<=', today)
                    .where('t09.EndDate', '>=', today);
                    if (outletPanel.status === 'Non Active') {
                        this.whereNull('t09.ApprovalDate');
                    }
                    if (outletPanel.status === 'Active') {
                        this.whereNotNull('t09.ApprovalDate');
                    }
                }
            }
        })
        .where('t09.isActive', '<>', 3)
    })
    .select('*', knex.raw('select count(*) from table_data').wrap('(', ') totalRows'))
    .from('table_data')
    .limit(pageSetting && pageSetting.rows > 0 ? pageSetting.rows : 10)
    .offset(pageSetting && pageSetting.first > 0 ? pageSetting.first : 0)
    .orderBy(
        pageSetting && pageSetting.sortField ? pageSetting.sortField : 'FFClinicID',
        pageSetting && pageSetting.sortOrder === 1 ? 'asc' : 'desc'
    );
}

function saveData(tempFFClinicList, profileUser) {
    try {
        const today = moment();        
        return knex.transaction(async trx => {
            for (const tempNewEmployee of tempFFClinicList) {
                const tempNewStartDate = moment(tempNewEmployee.StartDate);
                const tempNewEndDate = moment(tempNewEmployee.EndDate);
                const tempEditEndDate = Object.create(moment(tempNewEmployee.StartDate)).subtract(1, 'd');

                const currentDataList = await trx.select(
                    "t09.FFClinicID",
                    "t09.StartDate",
                    "t09.EndDate",
                )
                .from('t09_ff_clinic as t09')
                .where('t09.ClinicID', tempNewEmployee.ClinicID)
                .where('t09.RayonCode', tempNewEmployee.RayonCode)
                .where('t09.CompanyID', tempNewEmployee.CompanyID)
                .whereRaw('(? between t09.StartDate and t09.EndDate)', [tempNewStartDate.format('YYYY-MM-DD')]);

                if (currentDataList && currentDataList.length > 0) {
                    for (const tempOldEmployee of currentDataList) {
                        const tempOldStartDate = moment(tempOldEmployee.StartDate);
                        const tempOldEndDate = moment(tempOldEmployee.EndDate);
    
                        // old period = new period => delete
                        if (tempOldStartDate.isSameOrAfter(tempNewStartDate, "date")) {
                            await trx('t09_ff_clinic')
                            .where({FFClinicID: tempOldEmployee.FFClinicID})
                            .del();
                        }
    
                        // old period < new period => cut off
                        if (tempOldStartDate.isBefore(tempNewStartDate, "date")) {
                            await trx('t09_ff_clinic')
                            .where('FFClinicID', tempOldEmployee.FFClinicID)
                            .update({
                                EndDate: tempEditEndDate.format('YYYY-MM-DD'),
                                LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                            });
                        }
                    }
                }

                tempNewEmployee.StartDate = tempNewStartDate.format('YYYY-MM-DD');
                tempNewEmployee.EndDate = tempNewEndDate.format('YYYY-MM-DD');
                tempNewEmployee.LastUpdate = today.format('YYYY-MM-DD hh:mm:ss');
                await trx('t09_ff_clinic').insert(tempNewEmployee);    
            }
            
            const tableHistory = {
                table_name: 't09_ff_clinic',
                table_action: 2,
                table_data: JSON.stringify(tempFFClinicList),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        })
        .catch(function(error) {
            throw error;
        });
    } catch (error) {
        throw error;
    }
}

async function approveData(bodyDataList, profileUser) {
    const today = moment();
    try {
        await knex.transaction(async trx => {
            for (const data of bodyDataList) {
                await trx('t09_ff_clinic')
                    .where('FFClinicID', data.FFClinicID)
                    .update({
                        isActive: 1,
                        ApprovalStatus: profileUser.UserID,
                        ApprovalDate: today.format('YYYY-MM-DD hh:mm:ss'),
                        LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                    });
            }
            const tableHistory = {
                table_name: 't09_ff_clinic',
                table_action: 4,
                table_data: JSON.stringify(bodyDataList),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        });
    } catch (error) {
        return error;
    }
}

async function rejectData(bodyDataList, profileUser) {
    const today = moment();
    try {
        await knex.transaction(async trx => {
            for (const data of bodyDataList) {
                await trx('t09_ff_clinic')
                    .where('FFClinicID', data.FFClinicID)
                    .update({
                        isActive: 3,
                        ApprovalStatus: profileUser.UserID,
                        ApprovalDate: today.format('YYYY-MM-DD hh:mm:ss'),
                        LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                    });
            }
            const tableHistory = {
                table_name: 't09_ff_clinic',
                table_action: 5,
                table_data: JSON.stringify(bodyDataList),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        });
    } catch (error) {
        return error;
    }
}

async function cutOffData(bodyData, profileUser) {
    try {
        const today = moment();
        await knex.transaction(async trx => {
            for (const tempData of bodyData) {
                await trx('t09_ff_clinic')
                .where('FFClinicID', tempData.FFClinicID)
                .update({
                    EndDate: moment(tempData.EndDate).format('YYYY-MM-DD'),
                    LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                });
            }
            const tableHistory = {
                table_name: 't09_ff_clinic',
                table_action: 3,
                table_data: JSON.stringify(bodyData),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        });
    } catch (error) {
        return error;
    }
}

async function deleteData(bodyDataList, profileUser) {
    try {
        const today = moment();
        await knex.transaction(async trx => {
            for (const tempBodyData of bodyDataList) {
                await trx('t09_ff_clinic').where({FFClinicID: tempBodyData.FFClinicID}).del();
            }
            const tableHistory = {
                table_name: 't09_ff_clinic',
                table_action: 6,
                table_data: JSON.stringify(bodyDataList),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        });
    } catch (error) {
        return error;
    }
}

module.exports = {
    findPageData,
    saveData,
    approveData,
    rejectData,
    cutOffData,
    deleteData
}
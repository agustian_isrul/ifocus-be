const {
    convertArrayToNestedArray,
    createEmployeeFPPList,
    clasificationGroupList,
    convertNaNToZero,
    isExistEmployee,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveRKTK = (
    schema, resultCalculate, employeeDataList, actualValueList, targetValueList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const filterEmployeeList = createEmployeeFPPList(employeeDataList, filterEmployeeDataList);
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);

    for (let employee of filterEmployeeList) {
        let isEmployeeExist = true;
        let currentEmployee = isExistEmployee(employee, resultCalculate);
        if (!currentEmployee) {
            isEmployeeExist = false;
            currentEmployee = employee;
        }
        currentEmployee = setupEmployeeProperties(
            currentEmployee, schema, resultCalculate, actualValueList, targetValueList,  
            preeliminationAndList, preeliminationOrList, clasificationList
        )
        if (currentEmployee && (currentEmployee.actualAll > 0 || currentEmployee.targetAll > 0)) {
            if (isEmployeeExist) {
                let dataEmployee = resultCalculate.find(data => data.RayonCodeFilter.includes(currentEmployee.RayonCodeFilter[0]));
                dataEmployee = currentEmployee;
            } else {
                resultCalculate = resultCalculate || [];
                resultCalculate.push(currentEmployee);
            }
        }
    }

    return resultCalculate;
}

const setupEmployeeProperties = (
    employee, schema, resultCalculate, actualValueList, targetValueList, 
    preeliminationAndList, preeliminationOrList, clasificationList
) => {
    let tempActual = actualValueList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
    .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
    let tempTarget = targetValueList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
    .reduce((accumulator, target) => {return accumulator + target.target}, 0);
    let countSubordinate = 0;
    let tempTotalIncentive = 0;

    const tempSubordinateList = Object.values(resultCalculate
    .filter(tempSubordinate => tempSubordinate.RayonCodeList
        .some(tempRayonCode => employee.RayonCodeFilter.includes(tempRayonCode.RayonCodeSup)))
    .reduce((result, item) => {
        const value = employee.RayonCode;
        const existing = result[value] || {
            rayonCode: value,
            actualAll: 0,
            targetAll: 0,
            countSubordinate: 0,
            totalIncentive: 0
        };
        return {
            ...result,
            [value] : {
                ...existing,
                actualAll: existing.actualAll + item.actualAll,
                targetAll: existing.targetAll + item.targetAll,
                countSubordinate: existing.countSubordinate + 1,
                totalIncentive: existing.totalIncentive + item.totalIncentive
            }
        }
    }, {}));

    if (tempSubordinateList && tempSubordinateList.length > 0) {
        tempActual = tempActual + tempSubordinateList[0].actualAll;
        tempTarget = tempTarget + tempSubordinateList[0].targetAll;
        countSubordinate = tempSubordinateList[0].countSubordinate;
        tempTotalIncentive = tempSubordinateList[0].totalIncentive;
    }

    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode
    }

    objectSchema.actual = tempActual;
    objectSchema.target = tempTarget;
    
    if (objectSchema.actual <= 0 && objectSchema.target <= 0) {
        return null;
    }
    
    objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
    objectSchema.countSubordinate = countSubordinate;
    objectSchema.totalIncentiveSubordinate = tempTotalIncentive;

    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
    objectSchema.incentive = bonus;
    objectSchema.totalIncentive = new Number(bonus) * objectSchema.totalIncentiveSubordinate;
    
    employee.actualAll = employee.actualAll || objectSchema.actual;
    employee.targetAll = employee.targetAll || objectSchema.target;
    employee.actualTargetAll = employee.actualTargetAll || objectSchema.actualTarget;
    employee.totalIncentive = (employee.totalIncentive || 0) + objectSchema.totalIncentive;

    employee.schemaList = employee.schemaList || [];
    employee.schemaList.push(objectSchema);

    return employee;
}

module.exports = {
    incentiveRKTK
}
const {
    convertArrayToNestedArray,
    clasificationGroupList,
    calculateBonus,
    validateByTargetType
} = require('../../../helpers/utility');

const incentiveRKTK = (
    schema, resultCalculate, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const actualTargetFilter = filterParameterListBySchema.find(data => !data.RecordNoD && data.TargetTypeH === 'actualTargetAll');
    const subordinateEmployeeFilter = filterDataListBySchema.find(data => data.FilterCode === 'SubPositionCode');
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        const employeeSubordinateList = resultCalculate
        .filter(result => result.RayonCodeList.some(subordinate => employee.RayonCodeFilter.includes(subordinate.RayonCodeSup)));
        if (employee.EmployeePositionCode === 'DM') {
            return incentiveRKTKDM(
                schema, employeeSubordinateList, employee, subordinateEmployeeFilter, actualTargetFilter, 
                preeliminationAndList, preeliminationOrList, clasificationList);
        } else {
            return incentiveRKTKPimda(
                schema, employeeSubordinateList, employee, actualTargetFilter, 
                preeliminationAndList, preeliminationOrList, clasificationList);
        }
    })
}

const incentiveRKTKPimda = (
    schema, employeeSubordinateList, employee, actualTargetFilter, 
    preeliminationAndList, preeliminationOrList, clasificationList
) => {
    if (employeeSubordinateList && employeeSubordinateList.length > 0) {
        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode,
            actual: employee.actualAll,
            target: employee.targetAll,
            actualTarget: employee.actualTargetAll
        }

        let countSubordinate = employeeSubordinateList.length;
        let countExceedTarget = 0;
        let totalIncentiveSubordinate = 0;
        for (subordinate of employeeSubordinateList) {
            const allowToCount = validateByTargetType(actualTargetFilter.TargetTypeH, subordinate, objectSchema, 
                actualTargetFilter.Operator2H, actualTargetFilter.ValueFromH, actualTargetFilter.ValueToH);
            if (allowToCount) {
                countExceedTarget = countExceedTarget + 1;
            }
            totalIncentiveSubordinate = totalIncentiveSubordinate + subordinate.totalIncentive;
        }
        objectSchema.countProduct = countExceedTarget;
        objectSchema.countSubordinate = countSubordinate;
        objectSchema.totalIncentiveSubordinate = totalIncentiveSubordinate;
    
        const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = bonus * totalIncentiveSubordinate;
    
        employee.schemaList.push(objectSchema);
        employee.totalIncentive = (employee.totalIncentive || 0) + objectSchema.totalIncentive;
    }
    
    return employee;
}

const incentiveRKTKDM = (
    schema, employeeSubordinateList, employee, subordinateEmployeeFilter, actualTargetFilter, 
    preeliminationAndList, preeliminationOrList, clasificationList
) => {
    const tempEmployeeSubordinateList = employeeSubordinateList.filter(subordinate => 
        subordinateEmployeeFilter.FlagInclude === 1 ? 
        subordinate.EmployeePositionCode === subordinateEmployeeFilter.FilterValue : 
        subordinate.EmployeePositionCode !== subordinateEmployeeFilter.FilterValue);
    
    if (tempEmployeeSubordinateList && tempEmployeeSubordinateList.length > 0) {
        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode,
            actual: employee.actualAll,
            target: employee.targetAll,
            actualTarget: employee.actualTargetAll
        }

        let countSubordinate = 0;
        let countExceedTarget = 0;
        let totalIncentiveSubordinate = 0;
        for (subordinate of tempEmployeeSubordinateList) {
            countSubordinate = countSubordinate + 1;
            const allowToCount = validateByTargetType(actualTargetFilter.TargetTypeH, subordinate, objectSchema, 
                actualTargetFilter.Operator2H, actualTargetFilter.ValueFromH, actualTargetFilter.ValueToH);
            if (allowToCount) {
                countExceedTarget = countExceedTarget + 1;
            }
            totalIncentiveSubordinate = totalIncentiveSubordinate + subordinate.totalIncentive;
        }
        objectSchema.countProduct = countExceedTarget;
        objectSchema.countSubordinate = countSubordinate;
        objectSchema.totalIncentiveSubordinate = totalIncentiveSubordinate;
    
        const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = bonus * totalIncentiveSubordinate;
    
        employee.schemaList.push(objectSchema);
        employee.totalIncentive = (employee.totalIncentive || 0) + objectSchema.totalIncentive;
    }
    
    return employee;
}

module.exports = {
    incentiveRKTK
}
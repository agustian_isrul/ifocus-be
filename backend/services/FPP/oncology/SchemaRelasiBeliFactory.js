const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveRelasiBeli = (
    schema, resultCalculate, relasiBeliList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode
        }

        objectSchema.actual = relasiBeliList
        .filter(relasiBeli => employee.RayonCodeFilter.includes(relasiBeli.RayonCode) && 
        relasiBeli.categoryMonth === 'CURRENT')
        .reduce((accumulator, relasiBeli) => {return accumulator + relasiBeli.RelasiBeli}, 0);
        
        let tempRelasiBeliList = relasiBeliList
        .filter(relasiBeli => employee.RayonCodeFilter.includes(relasiBeli.RayonCode) && 
        relasiBeli.categoryMonth === 'PREVIOUS');
        let countPreviousMonth = 1;
        if (tempRelasiBeliList && tempRelasiBeliList.length > 0) {
            countPreviousMonth = tempRelasiBeliList[0].countPreviousMonth;
        }
        objectSchema.target = tempRelasiBeliList
        .reduce((accumulator, relasiBeli) => {return accumulator + relasiBeli.RelasiBeli}, 0) / countPreviousMonth;
    
        objectSchema.actualTarget = objectSchema.actual - objectSchema.target;
    
        const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = bonus;
    
        employee.totalIncentive = (employee.totalIncentive || 0) + bonus;
        employee.schemaList.push(objectSchema);
    
        return employee;
    });
}

module.exports = {
    incentiveRelasiBeli
}
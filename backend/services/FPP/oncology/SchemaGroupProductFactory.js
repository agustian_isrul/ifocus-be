const {
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculateBonus,
    validateByTargetType
} = require('../../../helpers/utility');

const incentiveGroupProductFokus = (
    schema, resultCalculate, actualValueList, targetValueList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const totalIncentiveFilter = filterParameterListBySchema
    .find(data => data.TargetTypeH === 'totalIncentive' && !data.RecordNoD);
    if (totalIncentiveFilter) {
        const indexTotalIncentive = filterParameterListBySchema.indexOf(totalIncentiveFilter);
        filterParameterListBySchema.splice(indexTotalIncentive, 1);
    }
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    const filterCategoryProductList = filterDataListBySchema
    .filter(data => data.FilterCode === 'ProductDesc' || data.FilterCode === 'ProductGroupDesc');
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        if ((employee.EmployeePositionCode === 'SUP' || employee.EmployeePositionCode === 'KOORD') && 
        employee.EmployeeSubLineDesc !== 'OMEGA') {
            return incentiveGroupProductFokusAM(
                schema, resultCalculate, employee, preeliminationAndList, preeliminationOrList, clasificationList
            );
        } else {
            return incentiveGroupProductFokusMRPS(
                schema, employee, filterCategoryProductList, actualValueList, targetValueList,
                preeliminationAndList, preeliminationOrList, clasificationList, totalIncentiveFilter
            );
        }
    });
}

const incentiveGroupProductFokusMRPS = (
    schema, employee, filterCategoryProductList, actualValueList, targetValueList, 
    preeliminationAndList, preeliminationOrList, clasificationList, totalIncentiveFilter
) => {
    let productFokusHit = 0;
    let tempActualTotal = 0;
    let tempActualHJTotal = 0;
    let tempTargetTotal = 0;
    let tempObjectSchemaList = [];
    for (let category of filterCategoryProductList) {
        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode,
            categoryProduct: category.FilterValue
        }

        let tempActualList = actualValueList
        .filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode) && 
        actual.categoryProduct === category.FilterValue);
        tempActualList = Object.values(tempActualList.reduce((res, item) => {
            const value = item['categoryProduct']
            const existing = res[value] || {['categoryProduct']: value, ['actualHJ']: 0, ['actualUnit']: 0}
            return {
                ...res,
                [value] : {
                    ...existing,
                    ['actualHJ']: existing['actualHJ'] + item['actual'],
                    ['actualUnit']: existing['actualUnit'] + item['unitActual']
                }
            } 
        }, {}));
        objectSchema.actual = 0;
        objectSchema.actualHJ = 0;
        if (tempActualList && tempActualList.length > 0) {
            objectSchema.actual = tempActualList[0].actualUnit;
            objectSchema.actualHJ = tempActualList[0].actualHJ;
        }

        objectSchema.target = targetValueList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode) &&
        target.categoryProduct === category.FilterValue)
        .reduce((accumulator, target) => {return accumulator + target.target}, 0);

        if (objectSchema.target > 0) {
            objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
        } else {
            objectSchema.actualTarget = 0;
        }

        if (objectSchema.actual >= objectSchema.target) {
            tempActualTotal = tempActualTotal + objectSchema.actual;
            tempActualHJTotal = tempActualHJTotal + objectSchema.actualHJ;
            tempTargetTotal = tempTargetTotal + objectSchema.target;
            productFokusHit = productFokusHit + 1;
        }

        tempObjectSchemaList.push(objectSchema);
    }
    
    const tempObjectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode,
        actual: tempActualTotal,
        actualHJ: tempActualHJTotal,
        target: tempTargetTotal
    }
    tempObjectSchema.actualTarget = convertNaNToZero(tempObjectSchema.actual / tempObjectSchema.target * 100);
    tempObjectSchema.countProduct = productFokusHit;
    const bonus = calculateBonus(employee, tempObjectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
    tempObjectSchema.incentive = bonus;
    tempObjectSchema.totalIncentive = bonus;
    if (bonus > 0 && bonus <= 100) {
        tempObjectSchema.totalIncentive = Math.round(bonus / 100 * tempActualHJTotal);
    }
    const allowToCount = validateByTargetType(totalIncentiveFilter.TargetTypeH, employee, tempObjectSchema, 
        totalIncentiveFilter.Operator2H, totalIncentiveFilter.ValueFromH, totalIncentiveFilter.ValueToH);
    if (allowToCount) {
        tempObjectSchema.totalIncentive = new Number(totalIncentiveFilter.ValueFromH);
    }
    tempObjectSchemaList.forEach(objectSchema => {
        objectSchema.countProduct = productFokusHit;
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = tempObjectSchema.totalIncentive;
        employee.schemaList.push(objectSchema);
    });

    employee.totalIncentive = (employee.totalIncentive || 0) + tempObjectSchema.totalIncentive;
    
    return employee;
}

const incentiveGroupProductFokusAM = (
    schema, resultCalculate, employee, preeliminationAndList, preeliminationOrList, clasificationList
) => {
    const employeeSubordinateList = resultCalculate
    .filter(result => result.EmployeePositionCode === 'MR' && 
    result.RayonCodeList.some(subordinate => employee.RayonCodeFilter.includes(subordinate.RayonCodeSup)));
    if (employeeSubordinateList && employeeSubordinateList.length > 0) {
        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode,
            actual: employee.actualAll,
            target: employee.targetAll,
            actualTarget: employee.actualTargetAll
        }

        let countSubordinate = 0;
        let tempSchemaGroupProductList = [];
        for (const subordinate of employeeSubordinateList) {
            const tempSchemaList = subordinate.schemaList.filter(tempSchema => tempSchema.schemaId === schema.SchemaId && 
                tempSchema.actualTarget >= 100 && tempSchema.totalIncentive > 0);
            if (tempSchemaList && tempSchemaList.length > 0) {
                countSubordinate = countSubordinate + 1;
                tempSchemaGroupProductList = tempSchemaGroupProductList.concat(tempSchemaList);
            }
        }

        if (tempSchemaGroupProductList && tempSchemaGroupProductList.length > 0) {
            tempSchemaGroupProductList = Object.values(tempSchemaGroupProductList.reduce((result, item) => {
                const value = item.categoryProduct;
                const existing = result[value] || {
                    categoryProduct: value,
                    totalProduct: 0
                }
                return {
                    ...result,
                    [value] : {
                        ...existing,
                        totalProduct: existing.totalProduct + 1
                    }
                } 
            }, {}));
        }
        
        objectSchema.countProduct = tempSchemaGroupProductList.length;
        objectSchema.countSubordinate = countSubordinate;
    
        const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = bonus;
    
        employee.schemaList.push(objectSchema);
        employee.totalIncentive = (employee.totalIncentive || 0) + bonus;
    }
    
    return employee;
}

module.exports = {
    incentiveGroupProductFokus
}
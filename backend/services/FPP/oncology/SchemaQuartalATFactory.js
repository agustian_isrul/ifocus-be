const {
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    isExistEmployee,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveQuarterAT = (
    schema, resultCalculate, employeeDataList, actualDataList, targetDataList, 
    filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const filterEmployeeList = createEmployeeFPPList(employeeDataList, filterEmployeeDataList);
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);

    for (let employee of filterEmployeeList) {
        let currentEmployee = isExistEmployee(employee, resultCalculate);
        if (!currentEmployee) {
            currentEmployee = employee;
            const tempCurrentEmployee = setupEmployeeProperties(currentEmployee, schema, actualDataList, targetDataList);
            if (tempCurrentEmployee) {
                resultCalculate = resultCalculate || [];
                resultCalculate.push(tempCurrentEmployee);
            }
        }
    }

    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;

        if (employee.RayonCodeList[0].RayonCodeSup) {
            objectSchema.incentive = new Number();
            objectSchema.totalIncentive = objectSchema.incentive;
    
            employee.totalIncentive = (employee.totalIncentive || 0) + objectSchema.incentive;
        }
    
        if (employee.RayonCodeList[0].RayonCodeSup) {
            const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
            objectSchema.incentive = bonus;
            objectSchema.totalIncentive = bonus;
            
            employee.totalIncentive = (employee.totalIncentive || 0) + bonus;
        
            employee.schemaList = employee.schemaList || [];
            employee.schemaList.push(objectSchema);
        
            return employee;
        }
    });
}

const setupEmployeeProperties = (employee, schema, actualDataList, targetDataList) => {
    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode
    }

    objectSchema.actual = actualDataList
    .filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
    .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
    objectSchema.target = targetDataList
    .filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
    .reduce((accumulator, target) => {return accumulator + target.target}, 0);

    if (employee.EmployeePositionCode !== 'MR') {
    }

    if (objectSchema.actual <= 0 && objectSchema.target <= 0) {
        return null;
    }

    objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);

    employee.actualAll = objectSchema.actual;
    employee.targetAll = objectSchema.target;
    employee.actualTargetAll = objectSchema.actualTarget;
    employee.schemaList = employee.schemaList || [];
    employee.schemaList.push(objectSchema);

    return employee;
}

module.exports = {
    incentiveQuarterAT
}
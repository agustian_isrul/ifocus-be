const {
    convertArrayToNestedArray,
    createEmployeeFPPList,
    clasificationGroupList,
    convertNaNToZero,
    isExistEmployee,
    calculateBonus,
    toCamelCase,
    capitalizeFirstChar
} = require('../../../helpers/utility');

const incentiveAT = (
    schema, resultCalculate, employeeDataList, actualDataList, targetDataList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const filterSubLiniList = filterDataListBySchema.filter(data => data.FilterCode === 'SubLineDesc');
    const filterEmployeeList = createEmployeeFPPList(employeeDataList, filterEmployeeDataList);
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);

    for (let employee of filterEmployeeList) {
        let currentEmployee = isExistEmployee(employee, resultCalculate);
        if (!currentEmployee) {
            currentEmployee = employee;
            const tempCurrentEmployee = setupEmployeeProperties(
                currentEmployee, schema, actualDataList, targetDataList, filterSubLiniList, 
                preeliminationAndList, preeliminationOrList, clasificationList
            );
            if (tempCurrentEmployee) {
                resultCalculate = resultCalculate || [];
                resultCalculate.push(tempCurrentEmployee);
            }
        } else {
            currentEmployee = calculateExistingEmployee(
                currentEmployee, schema, preeliminationAndList, preeliminationOrList, clasificationList
            );
            let dataEmployee = resultCalculate
            .find(data => data.RayonCodeFilter.includes(currentEmployee.RayonCodeFilter[0]));
            dataEmployee = currentEmployee;
        }
    }

    return resultCalculate;
}

const setupEmployeeProperties = (
    employee, schema, actualDataList, targetDataList, filterSubLiniList, 
    preeliminationAndList, preeliminationOrList, clasificationList
) => {
    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode
    }

    if (employee.EmployeePositionCode === 'SUP' || employee.EmployeePositionCode === 'KOORD') {
        objectSchema.actual = actualDataList
        .filter(actual => employee.RayonCodeFilter.includes(actual.SupRayonCode))
        .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
        objectSchema.target = targetDataList
        .filter(target => employee.RayonCodeFilter.includes(target.SupRayonCode))
        .reduce((accumulator, target) => {return accumulator + target.target}, 0);
    } else if (employee.EmployeePositionCode === 'DM') {
        objectSchema.actual = actualDataList
        .filter(actual => employee.RayonCodeFilter.includes(actual.DMRayonCode))
        .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
        objectSchema.target = targetDataList
        .filter(target => employee.RayonCodeFilter.includes(target.DMRayonCode))
        .reduce((accumulator, target) => {return accumulator + target.target}, 0);
    } else {
        objectSchema.actual = actualDataList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
        .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
        objectSchema.target = targetDataList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
        .reduce((accumulator, target) => {return accumulator + target.target}, 0);
    }

    if (objectSchema.actual <= 0 && objectSchema.target <= 0) {
        return null;
    }
    
    objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);

    employee.actualAll = objectSchema.actual;
    employee.targetAll = objectSchema.target;
    employee.actualTargetAll = objectSchema.actualTarget;

    if (filterSubLiniList && filterSubLiniList.length > 0) {
        for (const filterSubLini of filterSubLiniList) {
            const tempSubLiniName = capitalizeFirstChar(toCamelCase(filterSubLini.FilterValue));
            employee['actual' + tempSubLiniName] = actualDataList
            .filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode) && actual.SubLineDesc === filterSubLini.FilterValue)
            .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
            employee['target' + tempSubLiniName] = targetDataList
            .filter(target => employee.RayonCodeFilter.includes(target.RayonCode) && target.SubLineDesc === filterSubLini.FilterValue)
            .reduce((accumulator, target) => {return accumulator + target.target}, 0);
            employee['actualTarget' + tempSubLiniName] = convertNaNToZero(
                employee['actual' + tempSubLiniName] / employee['target' + tempSubLiniName] * 100);
        }
    }

    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
    objectSchema.incentive = bonus;
    objectSchema.totalIncentive = bonus;
    
    employee.totalIncentive = (employee.totalIncentive || 0) + bonus;

    employee.schemaList = employee.schemaList || [];
    employee.schemaList.push(objectSchema);

    return employee;
}

const calculateExistingEmployee = (employee, schema, preeliminationAndList, preeliminationOrList, clasificationList) => {
    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode,
        actual : employee.actualAll,
        target : employee.targetAll,
        actualTarget: employee.actualTargetAll
    }
    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
    objectSchema.incentive = bonus;
    objectSchema.totalIncentive = bonus;
    
    employee.totalIncentive = (employee.totalIncentive || 0) + bonus;

    employee.schemaList = employee.schemaList || [];
    employee.schemaList.push(objectSchema);

    return employee;
}

module.exports = {
    incentiveAT
}
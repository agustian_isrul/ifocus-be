const knex = require('../../../config/database');

const getEmployeeDataList = (lineDesc) => {
    return knex.select(
        'RayonCode', 'EmployeeID', 'EmployeeName', 'EmployeePositionCode', 'EmployeeAreaCode',
        'EmployeeSubLineDesc', 'EmployeeMRType', 'EmployeeVacant', 'RayonCodeSup', 'LineDesc', 'IsDefault'
    )
    .from('s03_structure_employee')
    .where('LineDesc', '=', lineDesc)
    .orderBy('PositionID')
    .orderBy('RayonCode')
    .orderBy('RayonCodeSup')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getSchemaList = (schemeConfigId, schemeType) => {
    return knex.select(
        't04.ConfigDetailId', 't04.SchemeConfigId', 't04.SchemaId', 't04.SchemaLabel'
    )
    .from('t04_schema_config as t04')
    .innerJoin('m01_schema as m01', 'm01.SchemaId', 't04.SchemaId')
    .where('t04.SchemeConfigId', schemeConfigId)
    .where('m01.SchemeType', schemeType)
    .orderBy('t04.ConfigDetailId')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getFilterTarikanDataList = (schemeConfigId, schemeType) => {
    return knex.select(
        't05.ConfigDetailId', 't05.RecordNo', 't05.Operator', 't05.FlagInclude',
        't05.FilterCode', 't05.LineProduct', 't05.ProductID', 't05.FilterValue'
    )
    .from('t04_schema_config as t04')
    .innerJoin('m01_schema as m01', 'm01.SchemaId', 't04.SchemaId')
    .innerJoin('t05_filter_tarikan_data as t05', 't05.ConfigDetailId', 't04.ConfigDetailId')
    .where('t04.SchemeConfigId', schemeConfigId)
    .where('m01.SchemeType', schemeType)
    .orderBy('t05.ConfigDetailId')
    .orderBy('t05.RecordNo')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getParameterBonusList = (schemeConfigId, schemeType) => {
    return knex.select(
        't06.ConfigDetailId',
        't06.Operator2 AS Operator2H', 't06.TargetType AS TargetTypeH', 't06.ValueFrom AS ValueFromH',
        't06.ValueTo AS ValueToH', 't06.Operator1 AS Operator1H', 't06.RecordNo AS RecordNoH',
        't07.Operator2 AS Operator2D', 't07.TargetType AS TargetTypeD', 't07.ValueFrom AS ValueFromD', 
        't07.ValueTo as ValueToD', 't07.Operator1 AS Operator1D', 't07.RecordNo AS RecordNoD',
        't07.Bonus'
    )
    .from('t04_schema_config as t04')
    .innerJoin('m01_schema as m01', 'm01.SchemaId', 't04.SchemaId')
    .innerJoin('t06_bonus_config as t06', 't06.ConfigDetailId', 't04.ConfigDetailId')
    .leftJoin('t07_bonus_detail as t07', function() {
        this.on('t07.ConfigDetailId', 't04.ConfigDetailId')
        .andOn('t07.RecordNo', 't06.RecordNo')
    })
    .where('t04.SchemeConfigId', schemeConfigId)
    .where('m01.SchemeType', schemeType)
    .orderBy('t06.ConfigDetailId')
    .orderBy('t06.RecordNo')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getActualDataList = (bodyData, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        's01.RayonCode', 's01.MonthSales', 's01.YearSales', 's01.SubLineDesc', 's01.SupRayonCode', 's01.DMRayonCode',
        knex.raw(
            "CONCAT(UPPER(MONTHNAME(CONCAT(s01.YearSales, '-', s01.MonthSales, '-01'))), ' ', s01.YearSales) AS MonthPeriod"
        ),
        knex.raw('SUM(s01.SalesValue) AS actual')
    )
    .from('s01_actual_data AS s01')
    .whereBetween('s01.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s01.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterCode) {
                    case 'SubPositionCode': break;
                    case 'TargetHJSistem': break;
                    case 'DiscountValue' : {
                        builder.where(knex.raw('(s01.SalesNonDiscount - s01.SalesValue) / s01.SalesNonDiscount * 100 < ?', 
                        data.FilterValue));
                        break;
                    }
                    default: {
                        switch (data.FilterValueList[0].FlagInclude) {
                            case 1:
                                builder.whereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                break;
                            default:
                                builder.whereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                break;
                        }
                        break;
                    }
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterCode) {
                        case 'SubPositionCode': break;
                        case 'TargetHJSistem': break;
                        case 'DiscountValue': {
                            this.orWhere(knex.raw('(s01.SalesNonDiscount - s01.SalesValue) / s01.SalesNonDiscount * 100 < ?', 
                            data.FilterValue));
                            break;
                        }
                        default: {
                            switch (data.FilterValueList[0].FlagInclude) {
                                case 1:
                                    this.orWhereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                                default:
                                    this.orWhereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                            }
                            break;
                        }
                    }
                });                 
            });
        }
    })
    .groupBy('s01.RayonCode', 's01.MonthSales', 's01.YearSales', 's01.SubLineDesc', 's01.SupRayonCode', 's01.DMRayonCode')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getTargetDataList = (bodyData, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        's02.RayonCode', 's02.MonthSales', 's02.YearSales', 's02.SubLineDesc', 's02.SupRayonCode', 's02.DMRayonCode',
        knex.raw(
            "CONCAT(UPPER(MONTHNAME(CONCAT(s02.YearSales, '-', s02.MonthSales, '-01'))), ' ', s02.YearSales) AS MonthPeriod"
        ),
        knex.raw('SUM(s02.TargetValue) AS target')
    )
    .from('s02_target_data as s02')
    .whereBetween('s02.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s02.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterCode) {
                    case 'DiscountValue' : break;
                    case 'ProductDesc': break;
                    case 'TargetHJSistem': break;
                    case 'SubPositionCode': break;
                    default: {
                        switch (data.FilterValueList[0].FlagInclude) {
                            case 1 : builder.whereIn('s02.' + data.FilterCode, data.FilterValueFilter); break;
                            case 0 : builder.whereNotIn('s02.' + data.FilterCode, data.FilterValueFilter); break;
                            default: break;
                        }
                        break;
                    }
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterCode) {
                        case 'DiscountValue' : break;
                        case 'ProductDesc': break;
                        case 'TargetHJSistem': break;
                        case 'SubPositionCode': break;
                        default: {
                            switch (data.FilterValueList[0].FlagInclude) {
                                case 1 : this.orWhereIn('s02.' + data.FilterCode, data.FilterValueFilter); break;
                                case 0 : this.orWhereNotIn('s02.' + data.FilterCode, data.FilterValueFilter); break;
                                default: break;
                            }
                            break;
                        }
                    }
                });                 
            });
        }
    })
    .groupBy('s02.RayonCode', 's02.MonthSales', 's02.YearSales', 's02.SubLineDesc', 's02.SupRayonCode', 's02.DMRayonCode')
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getActualProductList = (bodyData, ConfigDetailId, filterAndTarikanList, filterOrTarikanList) => {
    return knex.select(
        's01.RayonCode',
        knex.raw(
            "case when t05.LineProduct IS NULL then t05.FilterValue ELSE t05.LineProduct END AS categoryProduct "
        ),
        knex.raw('SUM(s01.SalesValue) AS actual'),
        knex.raw("SUM(s01.SalesQuantity) AS unitActual")
    )
    .from('s01_actual_data AS s01')
    .innerJoin('t05_filter_tarikan_data as t05', function() {
        this.on('t05.ConfigDetailId', '=', ConfigDetailId)
        .andOn(function() {
            this.on('t05.FilterValue', '=', 's01.ProductGroupDesc')
            .orOn('t05.FilterValue', '=', 's01.ProductDesc')
        })
    })
    .whereBetween('s01.MonthSales', [bodyData.monthFrom, bodyData.monthTo])
    .whereBetween('s01.YearSales', [bodyData.yearFrom, bodyData.yearTo])
    .where(builder => {
        if (filterAndTarikanList && filterAndTarikanList.length > 0) {
            filterAndTarikanList.forEach(data => {
                switch (data.FilterCode) {
                    case 'DiscountValue' : {
                        builder.where(knex.raw('(s01.SalesNonDiscount - s01.SalesValue) / s01.SalesNonDiscount * 100 < ?', 
                        data.FilterValue));
                        break;
                    }
                    default: {
                        switch (data.FilterValueList[0].FlagInclude) {
                            case 1:
                                builder.whereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                break;
                            default:
                                builder.whereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                break;
                        }
                        break;
                    }
                }
            });
        }
        if (filterOrTarikanList && filterOrTarikanList.length > 0) {
            builder.andWhere(function() {
                filterOrTarikanList.forEach(data => {
                    switch (data.FilterCode) {
                        case 'DiscountValue': {
                            this.orWhere(knex.raw('(s01.SalesNonDiscount - s01.SalesValue) / s01.SalesNonDiscount * 100 < ?', 
                            data.FilterValue));
                            break;
                        }
                        default: {
                            switch (data.FilterValueList[0].FlagInclude) {
                                case 1:
                                    this.orWhereIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                                default:
                                    this.orWhereNotIn('s01.' + data.FilterCode, data.FilterValueFilter);
                                    break;
                            }
                            break;
                        }
                    }
                });                 
            });
        }
    })
    .groupBy('s01.RayonCode', knex.raw(
        "case when t05.LineProduct IS NULL then t05.FilterValue ELSE t05.LineProduct END"
    ))
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getTargetProductList = (lineDesc) => {
    return knex
    .select(
        'm08.RayonCode', 'm08.LineProduct AS categoryProduct', 'm08.TUMValue AS target'
    )
    .from('m08_tum_incentive AS m08')
    .where('m08.LineDesc', '=', lineDesc)
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getAveragePreviousYearList = (lineDesc) => {
    return knex.select(
        'm13.RayonCode',
        'm13.MaxSales AS average'
    )
    .from('m13_max_sales as m13')
    .where('m13.LineDesc', '=', lineDesc)
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getRelasiBeliList = (LineDesc, currentMonth, currentYear, previousMonth, previousYear) => {
    return knex.select(
        'm14.RayonCode',
        knex.raw(
            "case when m14.MonthSales = " + currentMonth + " AND m14.YearSales = " + currentYear + 
            " then 'CURRENT' ELSE 'PREVIOUS' END as categoryMonth "
        ),
        'm14.RelasiBeli', 
        'm14.MonthSales',
        'm14.YearSales',
        knex.raw("? as countPreviousMonth", previousMonth.length)
    )
    .from('m14_relasi_beli as m14')
    .where('m14.LineDesc', '=', LineDesc)
    .where(function() {
        this.orWhere(function() {
            this.where('m14.MonthSales', '=', currentMonth)
            .where('m14.YearSales', '=', currentYear)
        })
        .orWhere(function() {
            this.whereIn('m14.MonthSales', previousMonth)
            .where('m14.YearSales', '=', previousYear)
        })
    })
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

const getTargetOutletList = (LineDesc) => {
    return knex.select(
        'm10.RayonCode',
        'm10.MaxTarget AS TotalOutlet'
    )
    .from('m10_target_outlet AS m10')
    .where('m10.LineDesc', '=', LineDesc)
    .then(function(rows) {
        return rows;
    })
    .catch(function(error) {
        throw error;
    });
}

module.exports = {
    getEmployeeDataList,
    getSchemaList,
    getFilterTarikanDataList,
    getParameterBonusList,
    getActualDataList,
    getTargetDataList,
    getActualProductList,
    getTargetProductList,
    getAveragePreviousYearList,
    getRelasiBeliList,
    getTargetOutletList
}
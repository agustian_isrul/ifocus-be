const {
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculateBonus,
    validateByTargetType
} = require('../../../helpers/utility');

const incentiveGroupProductFokus = (
    schema, resultCalculate, actualValueList, targetValueList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const actualTargetFilter = filterParameterListBySchema
    .find(data => data.TargetTypeH === 'actualTarget' && !data.RecordNoD);
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    const filterProductList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode === 'ProductDesc' || data.FilterCode === 'ProductGroupDesc'), 'LineProduct', 'FilterValue');
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;
        
        const actualProductList = actualValueList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode));
        const targetProductList = targetValueList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode));

        let productFokusHit = 0;
        let tempActualTotal = 0;
        let tempTargetTotal = 0;
        let tempObjectSchemaList = [];
        for (let category of filterProductList) {
            const objectSchema = {                    
                schemaId: schema.SchemaId,
                schemaLabel: schema.SchemaLabel,
                rayonCode: employee.RayonCode,
                categoryProduct: category.LineProduct
            }
            const actualProduct = actualProductList.find(actual => actual.categoryProduct === category.LineProduct);
            if (actualProduct && actualProduct.actual > 0) {
                objectSchema.actual = actualProduct.actual;
                tempActualTotal = tempActualTotal + actualProduct.actual;
            } else {
                objectSchema.actual = 0;
            }
            const targetProduct = targetProductList.find(target => target.categoryProduct === category.LineProduct);
            if (targetProduct && targetProduct.target > 0) {
                objectSchema.target = targetProduct.target;
                tempTargetTotal = tempTargetTotal + targetProduct.target;
            } else {
                objectSchema.target = 0;
            }
            if (objectSchema.target > 0) {
                objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
            } else {
                objectSchema.actualTarget = 0;
            }
            const allowToCount = validateByTargetType(actualTargetFilter.TargetTypeH, employee, objectSchema, 
                actualTargetFilter.Operator2H, actualTargetFilter.ValueFromH, actualTargetFilter.ValueToH);
            if (objectSchema.actualTarget > 0 && allowToCount) {
                productFokusHit = productFokusHit + 1;
            }
            tempObjectSchemaList.push(objectSchema);
        }
        
        const tempObjectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode,
            actual: tempActualTotal,
            target: tempTargetTotal
        }
        tempObjectSchema.actualTarget = convertNaNToZero(tempObjectSchema.actual / tempObjectSchema.target * 100);
        tempObjectSchema.countProduct = productFokusHit;
        const bonus = calculateBonus(employee, tempObjectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        tempObjectSchemaList.forEach(objectSchema => {
            objectSchema.countProduct = tempObjectSchema.countProduct;
            objectSchema.incentive = bonus;
            objectSchema.totalIncentive = bonus * 500000;
            employee.schemaList.push(objectSchema);
        });
    
        employee.totalIncentive = (employee.totalIncentive || 0) + (bonus * 500000);
        
        return employee;
    });
}

module.exports = {
    incentiveGroupProductFokus
}
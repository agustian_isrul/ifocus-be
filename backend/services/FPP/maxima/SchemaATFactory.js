const {
    convertArrayToNestedArray,
    createEmployeeFPPList,
    clasificationGroupList,
    convertNaNToZero,
    isExistEmployee,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveAT = (
    schema, resultCalculate, employeeDataList, actualValueList, targetValueList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const filterEmployeeList = createEmployeeFPPList(employeeDataList, filterEmployeeDataList);
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);

    for (let employee of filterEmployeeList) {
        let isEmployeeExist = true;
        let currentEmployee = isExistEmployee(employee, resultCalculate);
        if (!currentEmployee) {
            isEmployeeExist = false;
            currentEmployee = employee;
        }
        currentEmployee = setupEmployeeProperties(
            currentEmployee, schema, actualValueList, targetValueList, 
            preeliminationAndList, preeliminationOrList, clasificationList
        )
        if (currentEmployee.actualAll > 0 || currentEmployee.targetAll > 0) {
            if (isEmployeeExist) {
                let dataEmployee = resultCalculate.find(data => data.RayonCodeFilter.includes(currentEmployee.RayonCodeFilter[0]));
                dataEmployee = currentEmployee;
            } else {
                resultCalculate = resultCalculate || [];
                resultCalculate.push(currentEmployee);
            }
        }
    }

    return resultCalculate;
}

const setupEmployeeProperties = (
    employee, schema, actualValueList, targetValueList, preeliminationAndList, preeliminationOrList, clasificationList
) => {
    let actualValue = actualValueList.filter(actual => employee.RayonCodeFilter.includes(actual.RayonCode))
    .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
    let targetValue = targetValueList.filter(target => employee.RayonCodeFilter.includes(target.RayonCode))
    .reduce((accumulator, target) => {return accumulator + target.target}, 0);

    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode
    }
    objectSchema.actual = actualValue;
    objectSchema.target = targetValue;
    objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);

    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
    objectSchema.incentive = bonus;
    objectSchema.totalIncentive = bonus * 500000;
    
    employee.totalIncentive = (employee.totalIncentive || 0) + (bonus * 500000);
    employee.actualAll = employee.actualAll || objectSchema.actual;
    employee.targetAll = employee.targetAll || objectSchema.target;
    employee.actualTargetAll = employee.actualTargetAll || objectSchema.actualTarget;

    employee.schemaList = employee.schemaList || [];
    employee.schemaList.push(objectSchema);

    return employee;
}

module.exports = {
    incentiveAT
}
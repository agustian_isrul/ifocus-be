const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveRelasiBeli = (
    schema, resultCalculate, relasiBeliList, outletList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');

    const filterTargetOutlet = filterDataListBySchema.find(data => data.FilterCode === 'Target_OP');
    
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;
        
        const targetOutletObject = outletList.find(outlet => employee.RayonCodeFilter.includes(outlet.RayonCode));
        switch (filterTargetOutlet.FilterValue) {
            case '1': return incentivePoint1(
                schema, employee, relasiBeliList, targetOutletObject, preeliminationAndList, preeliminationOrList, clasificationList
            );
            case '2': return incentivePoint2(
                schema, employee, targetOutletObject, preeliminationAndList, preeliminationOrList, clasificationList
            );
            case '3': return incentivePoint3(
                schema, employee, targetOutletObject, preeliminationAndList, preeliminationOrList, clasificationList
            );
            default : break;
        }
    });
}

const incentivePoint1 = (
    schema, employee, relasiBeliList, targetOutletObject, preeliminationAndList, preeliminationOrList, clasificationList
) => {
    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode
    }

    objectSchema.actual = relasiBeliList
    .filter(relasiBeli => employee.RayonCodeFilter.includes(relasiBeli.RayonCode) && 
    relasiBeli.categoryMonth === 'CURRENT')
    .reduce((accumulator, relasiBeli) => {return accumulator + relasiBeli.RelasiBeli}, 0);
    
    let tempRelasiBeliList = relasiBeliList
    .filter(relasiBeli => employee.RayonCodeFilter.includes(relasiBeli.RayonCode) && 
    relasiBeli.categoryMonth === 'PREVIOUS');
    let countPreviousMonth = 1;
    if (tempRelasiBeliList && tempRelasiBeliList.length > 0) {
        countPreviousMonth = tempRelasiBeliList[0].countPreviousMonth;
    }
    objectSchema.target = tempRelasiBeliList
    .reduce((accumulator, relasiBeli) => {return accumulator + relasiBeli.RelasiBeli}, 0) / countPreviousMonth;    
    objectSchema.actualTarget = employee.actualTargetAll;
    objectSchema.delta = objectSchema.actual - objectSchema.target;
    if (targetOutletObject && targetOutletObject.TotalOutlet > 0) {
        objectSchema.targetOutlet = targetOutletObject.TotalOutlet;
    } else {
        objectSchema.targetOutlet = 0;
    }

    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
    objectSchema.incentive = bonus;
    objectSchema.totalIncentive = bonus * 500000;

    employee.totalIncentive = (employee.totalIncentive || 0) + (bonus * 500000);
    employee.schemaList.push(objectSchema);

    return employee;
}

const incentivePoint2 = (
    schema, employee, targetOutletObject, preeliminationAndList, preeliminationOrList, clasificationList
) => {
    if (targetOutletObject && targetOutletObject.TotalOutlet > 15) {
        return employee;
    }
    const schemaGroupProductFokusList = employee.schemaList.filter(schema => schema.schemaId === 3);

    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode,
        actual: 0,
        target: 0
    }
    for (let schemaGroupProductFokus of schemaGroupProductFokusList) {
        objectSchema.actual = objectSchema.actual + schemaGroupProductFokus.actual;
        objectSchema.target = objectSchema.target + schemaGroupProductFokus.target;
    }
    objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
            
    if (targetOutletObject && targetOutletObject.TotalOutlet > 0) {
        objectSchema.targetOutlet = targetOutletObject.TotalOutlet;
    } else {
        objectSchema.targetOutlet = 0;
    }

    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
    objectSchema.incentive = bonus;
    objectSchema.totalIncentive = bonus * 500000;

    employee.totalIncentive = (employee.totalIncentive || 0) + (bonus * 500000);
    employee.schemaList.push(objectSchema);

    return employee;
}

const incentivePoint3 = (
    schema, employee, targetOutletObject, preeliminationAndList, preeliminationOrList, clasificationList
) => {
    const rbFilter = preeliminationAndList.find(data => data.TargetTypeH === 'delta' && !data.RecordNoD);
    if (rbFilter) {
        const indexFilter = preeliminationAndList.indexOf(rbFilter);
        preeliminationAndList.splice(indexFilter, 1);
    }
    const ratioFilter = preeliminationAndList.find(data => data.TargetTypeH === 'ratio' && !data.RecordNoD);
    if (ratioFilter) {
        const indexFilter = preeliminationAndList.indexOf(ratioFilter);
        preeliminationAndList.splice(indexFilter, 1);
    }

    if (targetOutletObject && targetOutletObject.TotalOutlet > 15) {
        const employeeDeltaRb = employee.schemaList.find(tempSchema => tempSchema.schemaId === schema.SchemaId)

        if(ratioFilter && employeeDeltaRb && employeeDeltaRb.delta < rbFilter.FilterValue && targetOutletObject.TotalOutlet >= 15) {
            const tempTotalIncentive = (employee.totalIncentive || 0);
            employee.totalIncentiveOriginal = tempTotalIncentive
            employee.totalIncentive = tempTotalIncentive * ratioFilter.FilterValue;
        } else {
            employee.totalIncentive = (employee.totalIncentive || 0);
            employee.totalIncentiveOriginal = (employee.totalIncentive || 0);
        }

        return employee;
    }

    const schemaGrowthChannel = employee.schemaList.find(schema => schema.schemaId === 8);

    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode,
        actual: schemaGrowthChannel.actual,
        target: schemaGrowthChannel.target,
        actualTarget: schemaGrowthChannel.actualTarget
    }
            
    if (targetOutletObject && targetOutletObject.TotalOutlet > 0) {
        objectSchema.targetOutlet = targetOutletObject.TotalOutlet;
    } else {
        objectSchema.targetOutlet = 0;
    }

    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
    objectSchema.incentive = bonus;
    objectSchema.totalIncentive = bonus * 500000;

    const tempTotalIncentive = (employee.totalIncentive || 0)  + (bonus * 500000);
    employee.totalIncentive = tempTotalIncentive;
    employee.totalIncentiveOriginal = tempTotalIncentive;

    employee.schemaList.push(objectSchema);

    return employee;
}

module.exports = {
    incentiveRelasiBeli
}
const {
    convertArrayToNestedArray,
    convertNaNToZero,
    isExistEmployee,
    validateByTargetType
} = require('../../../helpers/utility');

const incentiveRKTK = (
    schema, resultCalculate, employeeDataList, actualDataList, targetDataList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');

    const tempEmployeeList = employeeDataList
    .filter(currentEmployee => filterEmployeeDataList
        .every(element => element.FilterValueList[0].FlagInclude === 1 ? 
            element.FilterValueFilter.includes(currentEmployee[element.FilterCode]) : 
            !element.FilterValueFilter.includes(currentEmployee[element.FilterCode])
        )
    );
    const filterEmployeeList = Object.values(tempEmployeeList.reduce((res, item) => {
        const keyGroup = 'RayonCode';
        const value = item.RayonCodeSup ? item.EmployeeID + item.RayonCodeSup : item.EmployeeID;
        const existing = res[value] || {
            EmployeeID: item.EmployeeID, 
            [keyGroup]:'' , 
            [keyGroup + "Filter"]: [],
            [keyGroup + "List"]: [], 
            EmployeeName: '',
            EmployeePositionCode: '',
            EmployeeAreaCode: '',
            EmployeeSubLineDesc: '',
            EmployeeVacant: ''
        }
        return {
            ...res,
            [value] : {
                ...existing,
                [keyGroup]: item["IsDefault"] === 1 ? item[keyGroup] : existing[keyGroup],
                [keyGroup + "Filter"]: [...existing[keyGroup + "Filter"], item[keyGroup]],
                [keyGroup + "List"]: [...existing[keyGroup + "List"], item],
                EmployeeName: item.EmployeeName,
                EmployeePositionCode: item["IsDefault"] === 1 ? item.EmployeePositionCode : existing["EmployeePositionCode"],
                EmployeeAreaCode: item["IsDefault"] === 1 ? item["EmployeeAreaCode"] : existing["EmployeeAreaCode"],
                EmployeeSubLineDesc: item.IsDefault === 1 ? item.EmployeeSubLineDesc : existing.EmployeeSubLineDesc,
                EmployeeVacant: item.IsDefault === 1 ? item.EmployeeVacant : existing.EmployeeVacant
            }
        } 
    }, {}));

    const filterActualTargetAll = filterParameterListBySchema.find(data => !data.RecordNoD && data.TargetTypeH === 'actualTargetAll');
    const filterActualTarget = filterParameterListBySchema.find(data => !data.RecordNoD && data.TargetTypeH === 'actualTarget');

    for (let employee of filterEmployeeList) {
        let isEmployeeExist = true;
        let currentEmployee = isExistEmployee(employee, resultCalculate);
        if (!currentEmployee) {
            isEmployeeExist = false;
            currentEmployee = employee;
        }
        currentEmployee = setupEmployeeProperties(
            currentEmployee, schema, resultCalculate, actualDataList, targetDataList, filterActualTargetAll, filterActualTarget
        )
        if (currentEmployee.actualAll > 0 || currentEmployee.targetAll > 0) {
            if (isEmployeeExist) {
                let dataEmployee = resultCalculate.find(data => data.RayonCodeFilter.includes(currentEmployee.RayonCodeFilter[0]));
                dataEmployee = currentEmployee;
            } else {
                resultCalculate = resultCalculate || [];
                resultCalculate.push(currentEmployee);
            }
        }
    }

    return resultCalculate;
}

const setupEmployeeProperties = (
    employee, schema, resultCalculate, actualDataList, targetDataList, filterActualTargetAll, filterActualTarget
) => {
    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode,
        actual: 0,
        target: 0
    }

    if (employee.EmployeePositionCode === 'SUP' || employee.EmployeePositionCode === 'KOORD') {
        objectSchema.actual = objectSchema.actual + actualDataList
        .filter(actual => employee.RayonCodeFilter.includes(actual.SupRayonCode))
        .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
        objectSchema.target = objectSchema.target + targetDataList
        .filter(target => employee.RayonCodeFilter.includes(target.SupRayonCode))
        .reduce((accumulator, target) => {return accumulator + target.target}, 0);
    } else {
        objectSchema.actual = objectSchema.actual + actualDataList
        .filter(actual => employee.RayonCodeFilter.includes(actual.DMRayonCode))
        .reduce((accumulator, actual) => {return accumulator + actual.actual}, 0);
        objectSchema.target = objectSchema.target + targetDataList
        .filter(target => employee.RayonCodeFilter.includes(target.DMRayonCode))
        .reduce((accumulator, target) => {return accumulator + target.target}, 0);
    }

    const employeeSubordinateList = resultCalculate.filter(existingEmployee => existingEmployee.RayonCodeList.some(filterRayonCode => 
        employee.RayonCodeFilter.includes(filterRayonCode.RayonCodeSup)));
    let tempSubordinate = 0;
    let tempSubordinateExceedTarget = 0;
    let tempIncentiveSubordinate = 0;
    for (let employeeSubordinate of employeeSubordinateList) {
        if ((employee.EmployeePositionCode === 'DM' && employeeSubordinate.EmployeePositionCode !== 'MR') || 
        (employee.EmployeePositionCode !== 'DM' && employeeSubordinate.EmployeePositionCode === 'MR')) {
            tempSubordinate = tempSubordinate + 1;
            const allowToCount = validateByTargetType(filterActualTargetAll.TargetTypeH, employeeSubordinate, objectSchema, 
                filterActualTargetAll.Operator2H, filterActualTargetAll.ValueFromH, filterActualTargetAll.ValueToH);
            if (allowToCount) {
                tempSubordinateExceedTarget = tempSubordinateExceedTarget + 1;
            }
            tempIncentiveSubordinate = tempIncentiveSubordinate + 
            (employeeSubordinate.totalIncentiveOriginal || employeeSubordinate.totalIncentive);
        }
    }

    objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
    objectSchema.CountSubordinate = tempSubordinate;
    objectSchema.CountSubordinateTarget = tempSubordinateExceedTarget;
    objectSchema.IncentiveRKTK = tempIncentiveSubordinate;

    const isReadyToContinue = validateByTargetType(filterActualTarget.TargetTypeH, employee, objectSchema, 
        filterActualTarget.Operator2H, filterActualTarget.ValueFromH, filterActualTarget.ValueToH);
    if (isReadyToContinue && 
        (employee.EmployeePositionCode === 'SUP' || employee.EmployeePositionCode === 'KOORD')) {
        if (objectSchema.CountSubordinate > 4 && objectSchema.CountSubordinateTarget < objectSchema.CountSubordinate) {
            objectSchema.incentive = Math.round(objectSchema.IncentiveRKTK / 4 * 1.6);
        } else {
            objectSchema.incentive = Math.round(objectSchema.IncentiveRKTK / objectSchema.CountSubordinate * 1.6);
        }
    } else if (isReadyToContinue && employee.EmployeePositionCode === 'DM') {
        if (objectSchema.CountSubordinate > 4) {
            objectSchema.incentive = Math.round(objectSchema.IncentiveRKTK / objectSchema.CountSubordinateTarget * 1.6);
        } else {
            objectSchema.incentive = Math.round(objectSchema.IncentiveRKTK / objectSchema.CountSubordinate * 1.6);
        }
    } else {
        objectSchema.incentive = 0;
    }
    objectSchema.totalIncentive = objectSchema.incentive;

    employee.totalIncentive = (employee.totalIncentive || 0) + objectSchema.incentive;
    employee.actualAll = employee.actualAll || objectSchema.actual;
    employee.targetAll = employee.targetAll || objectSchema.target;
    employee.actualTargetAll = employee.actualTargetAll || objectSchema.actualTarget;
    employee.schemaList = employee.schemaList || [];
    employee.schemaList.push(objectSchema);

    return employee;
}

module.exports = {
    incentiveRKTK
}
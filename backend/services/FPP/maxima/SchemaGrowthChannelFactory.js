const { 
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveGrowthChannel = (
    schema, resultCalculate, growthCurrentMonthList, averagePreviousYearList, filterDataListBySchema, filterParameterListBySchema
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    
    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);
    
    return resultCalculate.map(employee => {
        const allowToContinue = filterEmployeeDataList.every(filterEmployee => 
            filterEmployee.FilterValueList[0].FlagInclude === 1 ? 
            filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]) : 
            !filterEmployee.FilterValueFilter.includes(employee[filterEmployee.FilterCode]));
        if (!allowToContinue) return employee;
                
        const objectSchema = {                    
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode
        }
        
        const growthCurrentMonth = growthCurrentMonthList.find(growth => employee.RayonCodeFilter.includes(growth.RayonCode));
        if (growthCurrentMonth && growthCurrentMonth.actual > 0) {
            objectSchema.actual = growthCurrentMonth.actual;
        } else {
            objectSchema.actual = 0;
        }

        const averagePreviousYear = averagePreviousYearList.find(previous => employee.RayonCodeFilter.includes(previous.RayonCode));        
        if (averagePreviousYear && averagePreviousYear.average > 0) {
            objectSchema.target = averagePreviousYear.average;
        } else {
            objectSchema.target = 0;
        }

        objectSchema.actualTarget = convertNaNToZero((objectSchema.actual - objectSchema.target) / objectSchema.target * 100);
    
        const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
        objectSchema.incentive = bonus;
        objectSchema.totalIncentive = bonus * 500000;

        employee.totalIncentive = (employee.totalIncentive || 0) + (bonus * 500000);
        employee.schemaList.push(objectSchema);

        return employee;
    });
}

module.exports = {
    incentiveGrowthChannel
}
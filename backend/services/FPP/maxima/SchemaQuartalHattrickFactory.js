const {
    convertArrayToNestedArray,
    clasificationGroupList,
    convertNaNToZero,
    isExistEmployee,
    calculateBonus
} = require('../../../helpers/utility');

const incentiveQuarterHattrick = (
    schema, resultCalculate, employeeDataList, actualValueList, targetValueList, 
    filterDataListBySchema, filterParameterListBySchema, bodyData
) => {
    const filterEmployeeDataList = convertArrayToNestedArray(filterDataListBySchema
    .filter(data => data.FilterCode.startsWith('Employee')), 'FilterCode', 'FilterValue');
    const filterEmployeeList = convertArrayToNestedArray(employeeDataList
    .filter(currentEmployee => filterEmployeeDataList
        .every(element => element.FilterValueList[0].FlagInclude === 1 ? 
            element.FilterValueFilter.includes(currentEmployee[element.FilterCode]) : 
            !element.FilterValueFilter.includes(currentEmployee[element.FilterCode])
        )
    ),
    'EmployeeID', 'RayonCode', 'RayonCodeSup', 'EmployeeSubLineDesc');

    const preeliminationAndList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'AND');
    const preeliminationOrList = filterParameterListBySchema.filter(data => !data.RecordNoD && data.Operator1H === 'OR');
    const clasificationList = clasificationGroupList(filterParameterListBySchema);

    for (let employee of filterEmployeeList) {
        let isEmployeeExist = true;
        let currentEmployee = isExistEmployee(employee, resultCalculate);
        if (!currentEmployee) {
            isEmployeeExist = false;
            currentEmployee = employee;
            currentEmployee.EmployeeName = employee.RayonCodeList[0].EmployeeName;
            currentEmployee.EmployeePositionCode = employee.RayonCodeList[0].EmployeePositionCode;
            currentEmployee.EmployeeName = employee.RayonCodeList[0].EmployeeName;
            currentEmployee.EmployeeAreaCode = employee.RayonCodeList[0].EmployeeAreaCode;
            currentEmployee.EmployeeVacant = employee.RayonCodeList[0].EmployeeVacant;
        }
        switch (currentEmployee.EmployeePositionCode) {
            case 'MR': {
                currentEmployee = setupEmployeePropertiesMR(
                    currentEmployee, schema, actualValueList, targetValueList, 
                    preeliminationAndList, preeliminationOrList, clasificationList, bodyData
                );
                break;
            }
            default: {
                currentEmployee = setupEmployeePropertiesNonMR(
                    currentEmployee, schema, resultCalculate, preeliminationAndList, preeliminationOrList, clasificationList
                );
                break;
            }
        }
        if (currentEmployee.actualAll > 0 || currentEmployee.targetAll > 0) {
            if (isEmployeeExist) {
                let dataEmployee = resultCalculate
                .find(data => data.RayonCodeFilter.includes(currentEmployee.RayonCodeFilter[0]));
                dataEmployee = currentEmployee;
            } else {

                if (currentEmployee.RayonCodeList.length > 0) {
                    const displayEmployeeDefault = currentEmployee.RayonCodeList.find(tempEmployee => tempEmployee.IsDefault == 1);
                    if (displayEmployeeDefault) {
                        currentEmployee.RayonCode = displayEmployeeDefault.RayonCode;
                    }
                }

                resultCalculate = resultCalculate || [];
                resultCalculate.push(currentEmployee);
            }
        }
    }

    return resultCalculate;
}

const setupEmployeePropertiesMR = (
    employee, schema, actualValueList, targetValueList, preeliminationAndList, preeliminationOrList, clasificationList, bodyData
) => {
    let tempTotalActual = 0;
    let tempTotalTarget = 0;
    let tempCountExceedTarget = 0;
    let tempObjectSchemaList = [];
    for (let monthIndex = bodyData.monthFrom; monthIndex <= bodyData.monthTo; monthIndex++) {
        const tempObjectSchema = {
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode,
            monthIndex: monthIndex,
            yearIndex: bodyData.yearFrom,
            periodDescription: '',
            actual: 0,
            target: 0,
            actualTarget: 0,
            countProduct: 0
        }
        const actualObject = actualValueList.find(actual => employee.RayonCodeFilter.includes(actual.RayonCode) && 
        actual.MonthSales === monthIndex);
        if (actualObject && actualObject.actual > 0) {
            tempObjectSchema.actual = actualObject.actual;
            tempTotalActual = tempTotalActual + actualObject.actual;
        }
        let targetObject = targetValueList.find(target => employee.RayonCodeFilter.includes(target.RayonCode) &&
        target.MonthSales === monthIndex);
        if (targetObject && targetObject.target > 0) {
            tempObjectSchema.target = targetObject.target;
            tempTotalTarget = tempTotalTarget + targetObject.target;
            tempObjectSchema.periodDescription = targetObject.MonthPeriod;
        }
        if (tempObjectSchema.target > 0) {
            tempObjectSchema.actualTarget = convertNaNToZero(tempObjectSchema.actual / tempObjectSchema.target * 100);
        }
        if (tempObjectSchema.actualTarget >= 100) {
            tempCountExceedTarget = tempCountExceedTarget + 1;
        }
        tempObjectSchemaList.push(tempObjectSchema);
    }

    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode
    }
    objectSchema.actual = tempTotalActual;
    objectSchema.target = tempTotalTarget;
    objectSchema.countProduct = tempCountExceedTarget;
    objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);

    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
    for (const tempObjectSchema of tempObjectSchemaList) {
        tempObjectSchema.countProduct = tempCountExceedTarget;
        tempObjectSchema.incentive = bonus;
        tempObjectSchema.totalIncentive = bonus;
        
        employee.schemaList = employee.schemaList || [];
        employee.schemaList.push(tempObjectSchema);
    }
    
    employee.totalIncentive = (employee.totalIncentive || 0) + bonus;
    employee.actualAll = employee.actualAll || objectSchema.actual;
    employee.targetAll = employee.targetAll || objectSchema.target;
    employee.actualTargetAll = employee.actualTargetAll || objectSchema.actualTarget;

    return employee;
}

const setupEmployeePropertiesNonMR = (
    employee, schema, resultCalculate, preeliminationAndList, preeliminationOrList, clasificationList
) => {
    let tempTotalActual = 0;
    let tempTotalTarget = 0;
    let tempCountExceedTarget = 0;
    let tempObjectSchemaList = [];
    resultCalculate.filter(existingEmployee => existingEmployee.RayonCodeList
        .some(filterRayonCode => employee.RayonCodeFilter.includes(filterRayonCode.RayonCodeSup)))
        .forEach(existingEmployee => {
            tempObjectSchemaList = tempObjectSchemaList.concat(existingEmployee.schemaList);
        });
    tempObjectSchemaList = Object.values(tempObjectSchemaList.reduce((res, item) => {
        const value = item['monthIndex']
        const existing = res[value] || {
            ['monthIndex']: value, 
            ['yearIndex']: 0, 
            ['periodDescription']: '', 
            ['actual']: 0, 
            ['target']: 0
        }
        return {
            ...res,
            [value] : {
                ...existing,
                ['yearIndex']: item['yearIndex'],
                ['periodDescription']: item['periodDescription'],
                ['actual']: existing['actual'] + item['actual'],
                ['target']: existing['target'] + item['target']
            }
        } 
    }, {}));
    tempObjectSchemaList = tempObjectSchemaList.map(tempObjectSchema => {
        tempTotalActual = tempTotalActual + tempObjectSchema.actual;
        tempTotalTarget = tempTotalTarget + tempObjectSchema.target;
        
        const objectSchema = {
            schemaId: schema.SchemaId,
            schemaLabel: schema.SchemaLabel,
            rayonCode: employee.RayonCode,
            monthIndex: tempObjectSchema.monthIndex,
            yearIndex: tempObjectSchema.yearIndex,
            periodDescription: tempObjectSchema.periodDescription,
            actual: tempObjectSchema.actual,
            target: tempObjectSchema.target,
            actualTarget: 0,
            CountExceedTarget: 0
        }
        if (objectSchema.target > 0) {
            objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);
        }
        if (objectSchema.actualTarget >= 100) {
            tempCountExceedTarget = tempCountExceedTarget + 1;
        }
        return objectSchema;
    });

    const objectSchema = {                    
        schemaId: schema.SchemaId,
        schemaLabel: schema.SchemaLabel,
        rayonCode: employee.RayonCode
    }
    objectSchema.actual = tempTotalActual;
    objectSchema.target = tempTotalTarget;
    objectSchema.countProduct = tempCountExceedTarget;
    objectSchema.actualTarget = convertNaNToZero(objectSchema.actual / objectSchema.target * 100);

    const bonus = calculateBonus(employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList);
    for (const tempObjectSchema of tempObjectSchemaList) {
        tempObjectSchema.countProduct = tempCountExceedTarget;
        tempObjectSchema.incentive = bonus;
        tempObjectSchema.totalIncentive = bonus;

        employee.schemaList = employee.schemaList || [];
        employee.schemaList.push(tempObjectSchema);
    }
    
    employee.totalIncentive = (employee.totalIncentive || 0) + bonus;
    employee.actualAll = employee.actualAll || objectSchema.actual;
    employee.targetAll = employee.targetAll || objectSchema.target;
    employee.actualTargetAll = employee.actualTargetAll || objectSchema.actualTarget;

    return employee;
}

module.exports = {
    incentiveQuarterHattrick
}
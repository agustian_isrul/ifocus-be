const queryOperation = require('./query-operations');
const { reduceFilterTarikanList } = require('../../helpers/utility');

const calculateFPP = async (bodyData) => {
    try {
        const schemaList = await queryOperation.getSchemaList(bodyData.SchemeConfigId, bodyData.SchemeType);
        const employeeDataList = await queryOperation.getEmployeeDataList(bodyData.LineName);
        const filterTarikanDataList = await queryOperation.getFilterTarikanDataList(
            bodyData.SchemeConfigId, bodyData.SchemeType);
        const parameterBonusList = await queryOperation.getParameterBonusList(
            bodyData.SchemeConfigId, bodyData.SchemeType);
        let resultCalculate = [];

        for (let schema of schemaList) {
            const filterDataListBySchema = filterTarikanDataList
            .filter(data => data.ConfigDetailId === schema.ConfigDetailId);            
            const filterParameterListBySchema = parameterBonusList
            .filter(data => data.ConfigDetailId === schema.ConfigDetailId);
            const filterAndTarikanList = reduceFilterTarikanList(filterDataListBySchema, 'AND');
            const filterOrTarikanList = reduceFilterTarikanList(filterDataListBySchema, 'OR');

            switch (schema.SchemaId) {
                case 1: {
                    const actualDataList = await queryOperation.getActualDataList(
                        bodyData, filterAndTarikanList, filterOrTarikanList);
                    const targetDataList = await queryOperation.getTargetDataList(
                        bodyData, filterAndTarikanList, filterOrTarikanList);
                    switch (bodyData.LineName) {
                        case 'MAXIMA': {
                            const { incentiveAT } = require('./maxima/SchemaATFactory');
                            resultCalculate = await incentiveAT(
                                schema, resultCalculate, employeeDataList, actualDataList, targetDataList, 
                                filterDataListBySchema, filterParameterListBySchema);
                            break;
                        }
                        case 'ONCOLOGY': {
                            const { incentiveAT } = require('./oncology/SchemaATFactory');
                            resultCalculate = await incentiveAT(
                                schema, resultCalculate, employeeDataList, actualDataList, targetDataList, 
                                filterDataListBySchema, filterParameterListBySchema);
                            break;
                        }
                        default: {
                            const { incentiveAT } = require('./optima/SchemaATFactory');
                            resultCalculate = await incentiveAT(
                                schema, resultCalculate, employeeDataList, actualDataList, targetDataList, 
                                filterDataListBySchema, filterParameterListBySchema);
                            break;
                        }
                    }
                    actualDataList.length = 0;
                    targetDataList.length = 0;
                    break;
                }
                case 2: {
                    const { incentiveProductFocus } = require('./optima/SchemaProductFocusFactory');    
                    const actualDataList = await queryOperation.getActualProductList(
                        bodyData, schema.ConfigDetailId, filterAndTarikanList, filterOrTarikanList);
                    const targetDataList = await queryOperation.getTargetProductList(bodyData.LineName);
                    resultCalculate = await incentiveProductFocus(
                        schema, resultCalculate, actualDataList, targetDataList, 
                        filterDataListBySchema, filterParameterListBySchema);
                    break;
                }
                case 3: {    
                    const actualDataList = await queryOperation.getActualProductList(
                        bodyData, schema.ConfigDetailId, filterAndTarikanList, filterOrTarikanList);
                    const targetDataList = await queryOperation.getTargetProductList(bodyData.LineName);
                    switch (bodyData.LineName) {
                        case 'MAXIMA': {
                            const { incentiveGroupProductFokus } = require('./maxima/SchemaGroupProductFactory');
                            resultCalculate = await incentiveGroupProductFokus(
                                schema, resultCalculate, actualDataList, targetDataList, 
                                filterDataListBySchema, filterParameterListBySchema);
                            break;
                        }
                        case 'ONCOLOGY': {
                            const { incentiveGroupProductFokus } = require('./oncology/SchemaGroupProductFactory');
                            resultCalculate = await incentiveGroupProductFokus(
                                schema, resultCalculate, actualDataList, targetDataList, 
                                filterDataListBySchema, filterParameterListBySchema);
                            break;
                        }
                        default:
                            break;
                    }
                    actualDataList.length = 0;
                    targetDataList.length = 0;
                    break;
                }
                case 7: {
                    const previousMonthList = filterDataListBySchema
                    .filter(data => data.FilterCode === 'MonthSales');
                    const previousYearObject = filterDataListBySchema
                    .find(data => data.FilterCode === 'YearSales');
                    let previousYear = 2021;
                    let relasiBeliList = [];
                    if (previousYearObject) {
                        const previousMonth = previousMonthList.map(data => { return data.FilterValue });
                        previousYear = previousYearObject.FilterValue;
                        relasiBeliList = await queryOperation.getRelasiBeliList(
                            bodyData.LineName, bodyData.monthFrom, bodyData.yearFrom, 
                            previousMonth, previousYear);
                    }
                    switch (bodyData.LineName) {
                        case 'MAXIMA': {
                            const { incentiveRelasiBeli } = require('./maxima/SchemaRelasiBeliFactory');
                            const outletList = await queryOperation.getTargetOutletList(bodyData.LineName);
                            resultCalculate = await incentiveRelasiBeli(
                                schema, resultCalculate, relasiBeliList, outletList, 
                                filterDataListBySchema, filterParameterListBySchema);
                            outletList.length = 0;
                            break;
                        }
                        case 'ONCOLOGY': {
                            const { incentiveRelasiBeli } = require('./oncology/SchemaRelasiBeliFactory');
                            resultCalculate = await incentiveRelasiBeli(
                                schema, resultCalculate, relasiBeliList, 
                                filterDataListBySchema, filterParameterListBySchema);
                            break;
                        }
                        default:
                            break;
                    }
                    relasiBeliList.length = 0;
                    break;
                }
                case 8: {
                    const { incentiveGrowthChannel } = require('./maxima/SchemaGrowthChannelFactory');
                    const growthCurrentMonthList = await queryOperation.getActualDataList(
                        bodyData, filterAndTarikanList, filterOrTarikanList);
                    const averagePreviousYearList = await queryOperation.getAveragePreviousYearList(bodyData.LineName);
                    resultCalculate = await incentiveGrowthChannel(
                        schema, resultCalculate, growthCurrentMonthList, averagePreviousYearList, 
                        filterDataListBySchema, filterParameterListBySchema);
                    growthCurrentMonthList.length = 0;
                    averagePreviousYearList.length = 0;
                    break;
                }
                case 14: {
                    const { incentiveQuarterHattrick } = require('./maxima/SchemaQuartalHattrickFactory');
                    const actualValueList = await queryOperation.getActualDataList( bodyData, filterAndTarikanList, filterOrTarikanList);
                    const targetValueList = await queryOperation.getTargetDataList( bodyData, filterAndTarikanList, filterOrTarikanList);
                    
                    const ratioA = await queryOperation.getActualDataList( bodyData, filterAndTarikanList, filterOrTarikanList);
                    const ratioB = await queryOperation.getActualDataList( bodyData, filterAndTarikanList, filterOrTarikanList);

                    resultCalculate = await incentiveQuarterHattrick(
                        schema, resultCalculate, employeeDataList, actualValueList, targetValueList, 
                        filterDataListBySchema, filterParameterListBySchema, bodyData);
                    actualValueList.length = 0;
                    targetValueList.length = 0;
                    break;
                }
                case 20: {
                    switch (bodyData.LineName) {
                        case 'MAXIMA': {
                            const { incentiveRKTK } = require('./maxima/SchemaRKTKFactory');
                            const actualDataList = await queryOperation.getActualDataList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            const targetDataList = await queryOperation.getTargetDataList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = await incentiveRKTK(
                                schema, resultCalculate, employeeDataList, actualDataList, targetDataList, 
                                filterDataListBySchema, filterParameterListBySchema);
                            actualDataList.length = 0;
                            targetDataList.length = 0;
                            break;
                        }
                        case 'ONCOLOGY': {
                            const { incentiveRKTK } = require('./oncology/SchemaRKTKFactory');
                            resultCalculate = await incentiveRKTK(
                                schema, resultCalculate, filterDataListBySchema, filterParameterListBySchema);
                            break;
                        }
                        default: {
                            const { incentiveRKTK } = require('./optima/SchemaRKTKFactory');
                            const actualDataList = await queryOperation.getActualDataList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            const targetDataList = await queryOperation.getTargetDataList(
                                bodyData, filterAndTarikanList, filterOrTarikanList);
                            resultCalculate = await incentiveRKTK(
                                schema, resultCalculate, employeeDataList, actualDataList, targetDataList, 
                                filterDataListBySchema, filterParameterListBySchema);
                            break;
                        }
                    }
                    break;
                }
                case 21: {
                    const { incentiveDelta } = require('./oncology/SchemaDeltaFactory');
                    resultCalculate = await incentiveDelta(
                        schema, resultCalculate, filterDataListBySchema, filterParameterListBySchema);
                    break;
                }
                default:
                    break;
            }
        }
        
        return resultCalculate;
    } catch (error) {
        throw error;
    }
}

module.exports = {
    calculateFPP
}
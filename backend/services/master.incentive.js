const knex = require('../../config/database');
const moment = require("moment-timezone");

async function findSchemaList() {
    return await knex('m01_schema');
}

async function findFilterList() {
    return await knex('m04_filter_data');
}

async function findItemValueList(CompanyID, LineName) {
    return await knex.select('FilterCode', 'FilterValue')
    .from('m05_product_data')
    .where('CompanyID', CompanyID)
    .where('LineDesc', LineName)
    .orderBy('FilterValue');
}

function findSubLineList(CompanyID, LineID, SubLineName) {
    return knex('m18_sub_line_master')
    .where('CompanyID', CompanyID)
    .where(function() {
        if (LineID) {
            this.where('LineID', '=', LineID);
        }
        if (SubLineName) {
            this.where('SubLineName', '=', SubLineName);
        }
    })
}

const findSystemParameter = (keyname) => {
    return knex.select(
        "m17.keyname", "m17.value"
    )
    .from('m17_system_parameter as m17')
    .where(function() {
        if (keyname) {
            this.where('m17.keyname', keyname);
        }
    });
}

const findArea = (CompanyID, AreaCode) => {
    return knex('m20_area_master')
    .where(function() {
        if (AreaCode) {
            this.where('AreaCode', '=', AreaCode);
        }
    })
    .where('CompanyID', CompanyID);
}

const findLine = (CompanyID, lineName) => {
    return knex('m21_line_master')
    .where(function() {
        if (lineName) {
            this.where('LineName', '=', lineName);
        }
    })
    .where('CompanyID', CompanyID);
}

function findOutletList(CompanyID, AreaID, LineID) {
    return knex.distinct(
        "m22.AreaID",
        "m22.AreaName as AreaCode",
        "m22.LineID",
        "m22.LineName",
        "m22.SiteCode as Sitecode",
        "m22.CustName as OutletName"
    )
    .from('m22_sales_branch as m22')
    .leftJoin('t02_outlet_panel as t02', function(){
        this.on('t02.AreaID', '=', 'm22.AreaID')
        .on('t02.LineID', '=', 'm22.LineID')
        .on('t02.Sitecode', '=', 'm22.SiteCode')
    })
    .leftJoin('t10_customer_ff as t10', function(){
        this.on('t10.AreaID', '=', 'm22.AreaID')
        .on('t10.LineID', '=', 'm22.LineID')
        .on('t10.Sitecode', '=', 'm22.SiteCode')
    })
    .leftJoin('t11_customer_contribution as t11', function(){
        this.on('t11.AreaID', '=', 'm22.AreaID')
        .on('t11.LineID', '=', 'm22.LineID')
        .on('t11.Sitecode', '=', 'm22.SiteCode')
    })
    .whereNull('t02.CustomerPanelID')
    .whereNull('t10.CustomerFFID')
    .whereNull('t11.CustomerFFID')
    .where(function() {
        if (CompanyID) {
            this.where('m22.CompanyID', '=', CompanyID);
        }
        if (AreaID) {
            this.where('m22.AreaID', '=', AreaID);
        }
        if (LineID) {
            this.where('m22.LineID', '=', LineID);
        }
    });
}

function findAllTableUpload() {
    return knex('m24_table_upload')
    .then(rows => {
        return rows;
    })
    .catch((error) => {
        throw error;
    });
}

function findColumnInfo(tableName, limitNumber) {
    const limitConditionally = (queryBuilder) => {
        if (limitNumber) {
            queryBuilder.limit(limitNumber);
        }
    };
    return knex(tableName)
    .modify(limitConditionally)
    .then(columns => {
        return columns;
    })
    .catch((error) => {
        throw error;
    });
}

function insertDataPendukung(tableName, LineDesc, startDate, endDate, dataPendukung) {
    try {
        return knex.transaction(async trx => {
            const existingDataList = await trx(tableName)
            .where('LineDesc', LineDesc)
            .whereRaw('(? between StartPeriod and EndPeriod)', [startDate.format('YYYY-MM-DD')])
            .limit(1);

            if (existingDataList && existingDataList.length > 0) {
                const existingStartPeriod = moment(existingDataList[0].StartPeriod);
                if (existingStartPeriod.isBefore(startDate, "date")) {
                    await trx(tableName)
                    .where('LineDesc', LineDesc)
                    .whereRaw('(? between StartPeriod and EndPeriod)', [startDate.format('YYYY-MM-DD')])
                    .update({
                        EndPeriod: startDate.subtract(1, 'd').format('YYYY-MM-DD')
                    });
                } else {
                    await trx(tableName)
                    .where({
                        LineDesc: LineDesc,
                        StartPeriod: startDate.format('YYYY-MM-DD')
                    })
                    .del();
                }
            }

            await trx.batchInsert(tableName, dataPendukung, 1000);
        });
    } catch (error) {
        throw error;
    }
}

module.exports = {
    findSchemaList,
    findFilterList,
    findItemValueList,
    findSubLineList,
    findSystemParameter,
    findArea,
    findLine,
    findOutletList,
    findAllTableUpload,
    findColumnInfo,
    insertDataPendukung
}
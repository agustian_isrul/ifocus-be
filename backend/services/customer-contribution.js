const moment = require("moment-timezone");

const knex = require('../../config/database')

function findPageData(outletPanel, pageSetting) {
    return knex.with('table_data', (qb) => {
        qb.select(
            "t11.CustomerFFID",
            "t11.CompanyID",
            "t11.PosStructureID",
            "t11.AreaID",
            "t11.AreaCode",
            "t11.AreaName",
            "t11.LineID",
            "t11.LineName",
            "t11.CustomerAreaID",
            "t11.CustomerAreaCode",
            "t11.CustomerAreaName",
            "t11.SubLine",
            "t11.Sitecode",
            "t11.OutletName",
            "t11.OutletAddress",
            "t11.RayonCode",
            "t11.EmployeeID",
            "t11.EmployeeName",
            "t11.EmployeePosition",
            "t11.EmployeeMRType",
            "t11.Partition",
            "t11.StartDate",
            "t11.EndDate",
            "t11.isActive",
            "t11.UserID",
            "t11.LastUpdate",
            "t11.ApprovalStatus",
            "t11.ApprovalDate",
            knex.raw(
                "case when CURDATE() between t11.StartDate and t11.EndDate and t11.ApprovalDate is not null then 'Active' " +
                "else 'Non Active' end as status"
            )
        )
        .from('t11_customer_contribution as t11')
        .where(function() {
            if (outletPanel) {
                if (outletPanel.CompanyID) {
                    this.where('t11.CompanyID', outletPanel.CompanyID);
                }
                if (outletPanel.PosStructureID) {
                    this.where('t11.PosStructureID', outletPanel.PosStructureID);
                }
                if (outletPanel.area) {
                    this.where('t11.AreaID', outletPanel.area.AreaID);
                }
                if (outletPanel.lini) {
                    this.where('t11.LineID', outletPanel.lini.LineID);
                }
                if (outletPanel.RayonCode) {
                    this.where('t11.RayonCode', outletPanel.RayonCode);
                }
                if (outletPanel.EmployeeID) {
                    this.where('t11.EmployeeID', outletPanel.EmployeeID);
                }
                if (outletPanel.EmployeeName) {
                    this.where('t11.EmployeeName', outletPanel.EmployeeName);
                }
                if (outletPanel.EmployeePosition) {
                    this.where('t11.EmployeePosition', outletPanel.EmployeePosition);
                }
                if (outletPanel.Sitecode) {
                    this.where('t11.Sitecode', outletPanel.Sitecode);
                }
                if (outletPanel.OutletName) {
                    this.where('t11.OutletName', outletPanel.OutletName);
                }
                if (outletPanel.StartDate) {
                    const formatStartDate = moment(outletPanel.StartDate).format('YYYY-MM-DD');
                    this.where('t11.StartDate', '>=', formatStartDate);
                }
                if (outletPanel.EndDate) {
                    const formatEndDate = moment(outletPanel.EndDate).format('YYYY-MM-DD');                    
                    this.where('t11.EndDate', '<=', formatEndDate);
                }
                if (outletPanel.status) {
                    const today = moment().format('YYYY-MM-DD');
                    this.where('t11.StartDate', '<=', today)
                    .where('t11.EndDate', '>=', today);
                    if (outletPanel.status === 'Non Active') {
                        this.whereNull('t11.ApprovalDate');
                    }
                    if (outletPanel.status === 'Active') {
                        this.whereNotNull('t11.ApprovalDate');
                    }
                }
            }
        })
        .where('t11.isActive', '<>', 3)
    })
    .select('*', knex.raw('select count(*) from table_data').wrap('(', ') totalRows'))
    .from('table_data')
    .limit(pageSetting && pageSetting.rows > 0 ? pageSetting.rows : 10)
    .offset(pageSetting && pageSetting.first > 0 ? pageSetting.first : 0)
    .orderBy(
        pageSetting && pageSetting.sortField ? pageSetting.sortField : 'CustomerFFID',
        pageSetting && pageSetting.sortOrder === 1 ? 'asc' : 'desc'
    );
}

function saveData(bodyDataList, profileUser) {
    try {
        const today = moment();
        return knex.transaction(async trx => {
            for (const tempNewContribution of bodyDataList) {
                const tempNewStartDate = moment(tempNewContribution.StartDate);
                const tempNewEndDate = moment(tempNewContribution.EndDate);
                const tempEditEndDate = Object.create(moment(tempNewContribution.StartDate)).subtract(1, 'd');

                const tempOldContributionList = await trx.select(
                    "t11.CustomerFFID",
                    "t11.StartDate",
                    "t11.EndDate",
                )
                .from('t11_customer_contribution as t11')
                .where('t11.Sitecode', tempNewContribution.Sitecode)
                .where('t11.RayonCode', tempNewContribution.RayonCode)
                .where('t11.SubLine', tempNewContribution.SubLine)
                .where('t11.CompanyID', tempNewContribution.CompanyID)
                .whereRaw('(? between t11.StartDate and t11.EndDate)', [tempNewStartDate.format('YYYY-MM-DD')]);

                if (tempOldContributionList && tempOldContributionList.length > 0) {
                    for (const tempOldContribution of tempOldContributionList) {
                        const tempOldStartDate = moment(tempOldContribution.StartDate);

                        // old period = new period => delete
                        if (tempOldStartDate.isSameOrAfter(tempNewStartDate, "date")) {
                            await trx('t11_customer_contribution')
                            .where({CustomerFFID: tempOldContribution.CustomerFFID})
                            .del();
                        }
    
                        // old period < new period => cut off
                        if (tempOldStartDate.isBefore(tempNewStartDate, "date")) {
                            await trx('t11_customer_contribution')
                            .where('CustomerFFID', tempOldContribution.CustomerFFID)
                            .update({
                                EndDate: tempEditEndDate.format('YYYY-MM-DD'),
                                LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                            });
                        }
                    }
                }

                const tempPanelList = await trx('t02_outlet_panel')
                // .where('AreaID', tempNewContribution.AreaID)
                .where('LineID', tempNewContribution.LineID)
                // .where('CustomerAreaID', tempNewContribution.CustomerAreaID)
                .where('Sitecode', tempNewContribution.Sitecode)                
                .whereRaw('(? between StartDate and EndDate)', [tempNewStartDate.format('YYYY-MM-DD')]);
                if (tempPanelList && tempPanelList.length > 0) {
                    for (const tempPanel of tempPanelList) {
                        const tempOldStartDate = moment(tempPanel.StartDate);

                        if (tempOldStartDate.isSameOrAfter(tempNewStartDate, "date")) {
                            await trx('t02_outlet_panel').where({CustomerPanelID: tempPanel.CustomerPanelID}).del();
                        }

                        if (tempOldStartDate.isBefore(tempNewStartDate, "date")) {
                            await trx('t02_outlet_panel')
                            .where('CustomerPanelID', tempPanel.CustomerPanelID)
                            .update({
                                EndDate: tempEditEndDate.format('YYYY-MM-DD'),
                                LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                            });
                        }
                    }
                }

                const tempRegulerList = await trx('t10_customer_ff')
                // .where('AreaID', tempNewContribution.AreaID)
                .where('LineID', tempNewContribution.LineID)
                .where('Sitecode', tempNewContribution.Sitecode)                
                .whereRaw('(? between StartDate and EndDate)', [tempNewStartDate.format('YYYY-MM-DD')]);
                if (tempRegulerList && tempRegulerList.length > 0) {
                    for (const tempReguler of tempRegulerList) {
                        const tempOldStartDate = moment(tempReguler.StartDate);

                        if (tempOldStartDate.isSameOrAfter(tempNewStartDate, "date")) {
                            await trx('t10_customer_ff').where({CustomerFFID: tempReguler.CustomerFFID}).del();
                        }

                        if (tempOldStartDate.isBefore(tempNewStartDate, "date")) {
                            await trx('t10_customer_ff')
                            .where('CustomerFFID', tempReguler.CustomerFFID)
                            .update({
                                EndDate: tempEditEndDate.format('YYYY-MM-DD'),
                                LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                            });
                        }
                    }
                }

                tempNewContribution.StartDate = tempNewStartDate.format('YYYY-MM-DD');
                tempNewContribution.EndDate = tempNewEndDate.format('YYYY-MM-DD');
                tempNewContribution.LastUpdate = today.format('YYYY-MM-DD hh:mm:ss');
                await trx('t11_customer_contribution').insert(tempNewContribution);    
            }
            
            const tableHistory = {
                table_name: 't11_customer_contribution',
                table_action: 2,
                table_data: JSON.stringify(bodyDataList),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        })
    } catch (error) {
        throw error;
    }
}

function editData(bodyDataList, profileUser) {
    try {
        const today = moment();
        return knex.transaction(async trx => {
            for (const tempBodyData of bodyDataList) {
                await trx('t11_customer_contribution')
                .where('CustomerFFID', tempBodyData.CustomerFFID)
                .update({
                    RayonCode: tempBodyData.RayonCode,
                    EmployeeID: tempBodyData.EmployeeID,
                    EmployeeName: tempBodyData.EmployeeName,
                    EmployeePosition: tempBodyData.EmployeePosition,
                    EmployeeMRType: tempBodyData.EmployeeMRType,
                    Partition: tempBodyData.Partition
                });
            }
            const tableHistory = {
                table_name: 't11_customer_contribution',
                table_action: 2,
                table_data: JSON.stringify(bodyData),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        });
    } catch (error) {
        return error;
    }
}

function deleteData(bodyDataList, profileUser) {
    try {
        const today = moment();
        return knex.transaction(async trx => {
            for (const tempBodyData of bodyDataList) {
                await trx('t11_customer_contribution')
                .del()
                .where({CustomerFFID: tempBodyData.CustomerFFID});
            }
            const tableHistory = {
                table_name: 't11_customer_contribution',
                table_action: 6,
                table_data: JSON.stringify(bodyDataList),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        });
    } catch (error) {
        return error;
    }
}

module.exports = {
    findPageData,
    saveData,
    editData,
    deleteData
}
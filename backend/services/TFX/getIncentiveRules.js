const sequelize = require("sequelize");
const db = require("../../../config/databaseSequalize");
const getActualTarget = require("./actual_target");
const convertRupiah = require('rupiah-format');

function getgoal(data, TargetType2) {
    // //console.log('datagetgoal',data)
    let goal = 0;
    switch (TargetType2) {
        case "actual_target":
            goal = data.AperT;
            break;
        case "actual_max":
            goal = data.AperT;
            break;
        case "actual_regular":
            goal = data.AperTReg;
            break;
        case "actual":
            goal = data.actual;
            break;
    }
    return goal
}


function schemaBonusFinal(ValueTo2, Bonus) {
    passOne = 0;
    if (ValueTo2 !== null) {
        return passOne = (ValueTo2 / 100) * Bonus;
    } else {
        return passOne = Math.ceil(Bonus)
    }
}

function getDataATRenata(dataSchema, data) {
    let schemaSORT = dataSchema.sort(function (a, b) {
        a.RecordNo - b.RecordNo;
    });
    let goal = 0;
    let passOne = 0;
    let schema = schemaSORT
    let trueXrecord = countTrueAndRecord(schema)
    if (trueXrecord == true) {
        schema = schema.filter(v => v.RecordNo2 !== null);
        if (schema.length >= 1) {
            for (x = 0; x < schema.length; x++) {
                if (schema[x].RecordNo2 !== null && schema[x].TargetType !== null) {
                    goal = getgoal(data, schema[x].TargetType)
                    //console.log('goal2', goal);
                    if (schema[x].Operator2 !== null && schema[x].Operator1 == 'AND') {
                        switch (schema[x].Operator2) {
                            case "between":
                                if (goal >= schema[x].ValueFrom && goal < schema[x].ValueTo) {
                                    if (schema[x].RecordNo2 && schema[x].TargetType2) {
                                        if (schema[x].Operator22 !== null && schema[x].ValueFrom2 !== null) {
                                            let goal2 = getgoal(data, schema[x].TargetType2)
                                            switch (schema[x].Operator22) {
                                                case ">":
                                                    if (goal2 > schema[x].ValueFrom2) {
                                                        //console.log('>', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case "<":
                                                    if (goal2 < schema[x].ValueFrom2) {
                                                        //console.log('<', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case "<=":
                                                    if (goal2 <= schema[x].ValueFrom2) {
                                                        //console.log('<=', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case ">=":
                                                    if (goal2 >= schema[x].ValueFrom2) {
                                                        //console.log('>=', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case "=":
                                                    if (goal2 == schema[x].ValueFrom2) {
                                                        //console.log('==', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                }
                                break;
                            case "=":
                                if (goal == schema[x].ValueFrom) {
                                    if (schema[x].RecordNo2 && schema[x].TargetType2) {
                                        if (schema[x].Operator22 !== null && schema[x].ValueFrom2 !== null) {
                                            let goal2 = getgoal(data, schema[x].TargetType2)
                                            switch (schema[x].Operator22) {
                                                case ">":
                                                    if (goal2 > schema[x].ValueFrom2) {
                                                        //console.log('>', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case "<":
                                                    if (goal2 < schema[x].ValueFrom2) {
                                                        //console.log('<', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case "<=":
                                                    if (goal2 <= schema[x].ValueFrom2) {
                                                        //console.log('<=', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case ">=":
                                                    if (goal2 >= schema[x].ValueFrom2) {
                                                        //console.log('>=', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case "=":
                                                    if (goal2 == schema[x].ValueFrom2) {
                                                        //console.log('==', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                }
                                break;
                            case ">=":
                                if (goal >= schema[x].ValueFrom) {
                                    if (schema[x].RecordNo2 && schema[x].TargetType2) {
                                        if (schema[x].Operator22 !== null && schema[x].ValueFrom2 !== null) {
                                            let goal2 = getgoal(data, schema[x].TargetType2)
                                            switch (schema[x].Operator22) {
                                                case ">":
                                                    if (goal2 > schema[x].ValueFrom2) {
                                                        //console.log('>', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case "<":
                                                    if (goal2 < schema[x].ValueFrom2) {
                                                        //console.log('<', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case "<=":
                                                    if (goal2 <= schema[x].ValueFrom2) {
                                                        //console.log('<=', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case ">=":
                                                    if (goal2 >= schema[x].ValueFrom2) {
                                                        //console.log('>=', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case "=":
                                                    if (goal2 == schema[x].ValueFrom2) {
                                                        //console.log('==', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                }
                                break;
                            case "<=":
                                if (goal <= schema[x].ValueFrom) {
                                    if (schema[x].RecordNo2 && schema[x].TargetType2) {
                                        if (schema[x].Operator22 !== null && schema[x].ValueFrom2 !== null) {
                                            let goal2 = getgoal(data, schema[x].TargetType2)
                                            switch (schema[x].Operator22) {
                                                case ">":
                                                    if (goal2 > schema[x].ValueFrom2) {
                                                        //console.log('>', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case "<":
                                                    if (goal2 < schema[x].ValueFrom2) {
                                                        //console.log('<', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case "<=":
                                                    if (goal2 <= schema[x].ValueFrom2) {
                                                        //console.log('<=', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case ">=":
                                                    if (goal2 >= schema[x].ValueFrom2) {
                                                        //console.log('>=', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case "=":
                                                    if (goal2 == schema[x].ValueFrom2) {
                                                        //console.log('==', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                }
                                break;
                            case ">":
                                if (goal > schema[x].ValueFrom) {
                                    if (schema[x].RecordNo2 && schema[x].TargetType2) {
                                        if (schema[x].Operator22 !== null && schema[x].ValueFrom2 !== null) {
                                            let goal2 = getgoal(data, schema[x].TargetType2)
                                            switch (schema[x].Operator22) {
                                                case ">":
                                                    if (goal2 > schema[x].ValueFrom2) {
                                                        //console.log('>', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case "<":
                                                    if (goal2 < schema[x].ValueFrom2) {
                                                        //console.log('<', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case "<=":
                                                    if (goal2 <= schema[x].ValueFrom2) {
                                                        //console.log('<=', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case ">=":
                                                    if (goal2 >= schema[x].ValueFrom2) {
                                                        //console.log('>=', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case "=":
                                                    if (goal2 == schema[x].ValueFrom2) {
                                                        //console.log('==', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                }
                                break;
                            case "<":
                                if (goal < schema[x].ValueFrom) {
                                    if (schema[x].RecordNo2 && schema[x].TargetType2) {
                                        if (schema[x].Operator22 !== null && schema[x].ValueFrom2 !== null) {
                                            let goal2 = getgoal(data, schema[x].TargetType2)
                                            switch (schema[x].Operator22) {
                                                case ">":
                                                    if (goal2 > schema[x].ValueFrom2) {
                                                        //console.log('>', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case "<":
                                                    if (goal2 < schema[x].ValueFrom2) {
                                                        //console.log('<', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case "<=":
                                                    if (goal2 <= schema[x].ValueFrom2) {
                                                        //console.log('<=', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case ">=":
                                                    if (goal2 >= schema[x].ValueFrom2) {
                                                        //console.log('>=', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                                case "=":
                                                    if (goal2 == schema[x].ValueFrom2) {
                                                        //console.log('==', schema[x].ValueFrom2);
                                                        return passOne = schemaBonusFinal(schema[x].ValueFrom2, schema[x].Bonus)
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                }
                                break;
                        }
                    }
                }
            }
        }
    }
    // //console.log('PassOne', passOne);
    return (passOne) ? passOne : 0
}


function countTrueAndRecord(schema) {
    let countRecord = Object.keys(schema).reduce(function (acc, curr) {
        if (schema[curr].RecordNo2 === null && schema[curr].TargetType !== 'growth') {
            acc += 1;
        }
        return acc;
    }, 0);

    let countTrue = Object.keys(schema).reduce(function (acc, curr) {
        if (schema[curr].RecordNo2 === null && schema[curr].Status_Rumus === true && schema[curr].TargetType !== 'growth') {
            acc += 1;
        }
        return acc;
    }, 0);

    if (countRecord == countTrue) {
        return true
    } else {
        return false
    }

}

function getTrueOrFalse(TargetType2, Operator22, ValueFrom2, ValueTo2, Bonus, actual, AT, ATReg, ATMax, productivityValue, ATTotal, netSales) {
    let goal = 0;
    switch (TargetType2) {
        case "actual_target_all":
            goal = ATTotal;
            break;
        case "actual_target":
            goal = AT;
            break;
        case "actual_max":
            goal = ATMax;
            break;
        case "actual_regular":
            goal = ATReg;
            break;
        case "actual":
            goal = actual;
            break;
        case "actual_productivitas":
            goal = actual;
            break;
        case "net_sales":
            goal = netSales;
            break;
    }
    let passTwo = '';
    if (Operator22 !== null && ValueFrom2 !== null) {
        goal = Math.round(goal)
        if (Operator22 == 'between') {
            if (goal >= ValueFrom2 && goal <= ValueTo2) {
                return passTwo = {
                    get_status: true,
                    get_bonus: Bonus
                }
            }
        } else {
            switch (Operator22) {
                case "=":
                    if (goal == ValueFrom2) {
                        return passTwo = {
                            get_status: true,
                            get_bonus: Bonus
                        }
                    }
                    break;
                case ">=":
                    if (goal >= ValueFrom2) {
                        return passTwo = {
                            get_status: true,
                            get_bonus: Bonus
                        }
                    }
                    break;
                case "<=":
                    if (goal <= ValueFrom2) {
                        return passTwo = {
                            get_status: true,
                            get_bonus: Bonus
                        }
                    }
                    break;
                case ">":
                    if (goal > ValueFrom2) {
                        return passTwo = {
                            get_status: true,
                            get_bonus: Bonus
                        }
                    }
                    break;
                case "<":
                    if (goal < ValueFrom2) {
                        return passTwo = {
                            get_status: true,
                            get_bonus: Bonus
                        }
                    }
                    break;
            }
        }
    }
    let passFalse = {
        get_status: false,
        get_bonus: 0
    }
    return (passTwo) ? passTwo : passFalse
}


async function getRayonCodeHead(EmployeeID, LineName, statusE, RayonCode, filterMoreHead) {
    let filterParent = ``;
    let filterChild = ``;
    switch (filterMoreHead) {
        case "specialist":
            filterChild = `and (EmployeeSubLineDesc = 'SPECIALIST' )`;
            break;
        case "dummypimda":
            filterParent = `and (DummyPimda = '1')`;
            break;
        case "binaanpimda":
            filterParent = `and (BinaanPimda = '1')`;
            break;
        case "withoutDP":
            filterParent = `and (DummyPimda = '0')`;
            break;
    }

    if (RayonCode != 'no') {
        filterParent = `and RayonCodeSup = '${RayonCode}'`;
    }
    let dataDM = await db.query(`select RayonCodeSup  from s03_structure_employee 
    where LineDesc ='${LineName}' and EmployeeSupID = '${EmployeeID}' group by RayonCodeSup`, {
        type: sequelize.QueryTypes.SELECT
    })
    let loopFilterDM = '';
    if (dataDM.length > 0) {
        for (dm = 0; dm < dataDM.length; dm++) {
            loopFilterDM += "'" + dataDM[dm].RayonCodeSup + "',"
        }
        if (loopFilterDM !== "") {
            loopFilterDM = loopFilterDM.slice(0, -1);
        }

    }
    // console.log('loopFilterDM', loopFilterDM);

    let data = await db.query(`select RayonCode  from s03_structure_employee 
    where LineDesc ='${LineName}' and EmployeeSupID = '${EmployeeID}' ${filterParent} 
    group by RayonCode `, {
        type: sequelize.QueryTypes.SELECT
    })

    // //console.log('dataParent',data);
    let listOneSup = [];
    let loopFilter = '';
    let filterRayon = 'no';
    let ArrayRayonCode = [];
    if (data.length > 0) {
        for (i = 0; i < data.length; i++) {
            loopFilter += "'" + data[i].RayonCode + "',"
            ArrayRayonCode.push(data[i].RayonCode);
        }
        if (loopFilter !== "") {
            loopFilter = loopFilter.slice(0, -1);
            if (data.length > 1) {
                switch (statusE) {
                    case "DM":
                        filterRayon = "RayonCodeSup" + " " + "IN" + " " + "(" + loopFilter + ")";
                        break;
                    case "SUP":
                        filterRayon = "RayonCode" + " " + "IN" + " " + "(" + loopFilter + ")";
                        break;
                }
            } else {
                switch (statusE) {
                    case "DM":
                        filterRayon = "RayonCodeSup" + " " + "=" + " " + loopFilter
                        break;
                    case "SUP":
                        filterRayon = "RayonCode" + " " + "=" + " " + loopFilter
                        break;
                }
            }
        }
    } else {
        filterRayon = 'no'
    }
    // =======================
    await Promise.all(
        data.map(async (e) => {
            let RCMR1 = await getRayonMRList(statusE, e.RayonCode, LineName);
            let dthasil = {
                rayonCodeSUP: e.RayonCode,
                rayonCodeMR: RCMR1.rayoncode,
                filterRayon: RCMR1.filterRayon

            }
            listOneSup.push(dthasil)
        }))
    // =======================

    if (filterRayon !== 'no') {
        if (statusE == 'DM') {
            let dataChild = await getdataChildRayon(LineName, filterRayon, filterChild, loopFilterDM, loopFilter);
            filterRayon = dataChild
        } else {
            filterRayon = filterRayon
        }
    }
    let detail = 'no';
    if (data.length > 0) {
        detail = {
            filterRayon: filterRayon,
            rayonPerSup: listOneSup,
            rayonCode: ArrayRayonCode,
            count: data.length,
        }
    }
    // console.log('detail',detail);
    return detail
}

const getdataChildRayon = async (LineName, filterRayon, filterChild, loopFilterDM, loopFilter) => {
    let dataChild = await db.query(`select RayonCode  from s03_structure_employee sse 
    where sse.LineDesc ='${LineName}' and (${filterRayon}) ${filterChild}`, {
        type: sequelize.QueryTypes.SELECT
    })
    // console.log('dataChild',dataChild,LineName, filterRayon, filterChild, loopFilterDM, loopFilter);
    let loopFilterChild = '';
    if (dataChild.length > 0) {
        for (i = 0; i < dataChild.length; i++) {
            loopFilterChild += "'" + dataChild[i].RayonCode + "',"
        }
        if (loopFilterChild !== "") {
            loopFilterChild = loopFilterChild.slice(0, -1);
            // console.log('ALL RAYON', loopFilterDM, loopFilter, loopFilterChild);
            // ke bentuk filter ALL RayonCode
            filterRayon = "RayonCode" + " " + "IN" + " " + "(" + loopFilterDM + "," + loopFilter + "," + loopFilterChild + ")";
            // filterRayon = "RayonCode" + " " + "IN" + " " + "(" + loopFilterChild + ")";
        }
    } else {
        filterRayon = 'no'
    }
    return filterRayon
}
const getRayonMRList = async (statusE, rayoncode, LineName) => {
    // non DM
    let filterRC = `and (RayonCode  = '${rayoncode}')`;
    if (statusE == 'DM') {
        filterRC = `and (RayonCodeSup  = '${rayoncode}')`;
    }

    let data = await db.query(`select RayonCode from s03_structure_employee sse 
    where sse.LineDesc ='${LineName}' ${filterRC} group by RayonCode`, {
        type: sequelize.QueryTypes.SELECT
    })

    let loopFilterMR = '';
    let dataQuery = '';
    if (data.length > 0) {
        for (i = 0; i < data.length; i++) {
            loopFilterMR += "'" + data[i].RayonCode + "',"
        }
        if (loopFilterMR !== "") {
            loopFilterMR = loopFilterMR.slice(0, -1);
            if (data.length > 1) {
                dataQuery = "RayonCode" + " " + "IN" + " " + "(" + loopFilterMR + ")";
            } else {
                dataQuery = "RayonCode" + " " + "=" + " " + loopFilterMR
            }
        }
    } else {
        dataQuery = 'no'
    }
    let detail = {
        rayoncode: data,
        filterRayon: dataQuery
    }
    return detail;
}



async function getActualTargetPerRayon(EmployeePositionCode, ftr, FRCode, bodyData, QueryRegPlus) {
    let listATCodeRayon = [];
    if (FRCode != 'no') {
        let dass = FRCode.rayonPerSup
        if (dass.length > 0) {
            await Promise.all(
                dass.map(async (e) => {
                    let dataAT = await getDataAT(EmployeePositionCode, e.rayonCodeSUP, ftr, e, bodyData, QueryRegPlus)
                    let hasil = {
                        rayonCodeSUP: e.rayonCodeSUP,
                        data: dataAT
                    }
                    listATCodeRayon.push(hasil)
                }))
        }
    }

    return (listATCodeRayon) ? listATCodeRayon : NULL
}

const getDataAT = async (EmployeePositionCode, rayonCodeSUP, ftr, frCode, bodyData, QueryRegPlus) => {
    let actualDD = await getActualTarget.getSalesActualValue(EmployeePositionCode, rayonCodeSUP, bodyData, frCode, ftr.filterAnd, ftr.filterOr, 'no', 'no');
    let targetDD = await getActualTarget.getSalesTargetValue(EmployeePositionCode, rayonCodeSUP, bodyData, frCode, ftr.filterAnd, ftr.filterOr, 'no');
    let actualDDReg = await getActualTarget.getSalesActualValue(EmployeePositionCode, rayonCodeSUP, bodyData, frCode, ftr.filterAnd, ftr.filterOr, 'no', QueryRegPlus);
    let targetDDReg = await getActualTarget.getSalesTargetValue(EmployeePositionCode, rayonCodeSUP, bodyData, frCode, ftr.filterAnd, ftr.filterOr, QueryRegPlus);
    let AT = (actualDD / targetDD) * 100;
    let ATReg = (actualDDReg / targetDDReg) * 100;
    let hasilat = {
        actual: actualDD,
        target: targetDD,
        actual_regular: actualDDReg,
        target_regular: targetDDReg,
        AperT: (AT) ? AT : 0,
        AperTReg: (ATReg) ? ATReg : 0,

    }

    return hasilat
}

const IncentiveTUMCatProduk = async (schema, FFRayonCode, bodyData) => {
    let getpush = [];
    await Promise.all(
        schema.map(async (e) => {
            let getInc = await getIncTUM(bodyData, FFRayonCode, e.ValueFrom, e.Operator22, e.ValueFrom2, e.ValueTo2);
            let datalist = {
                RecordNo: e.RecordNo,
                TargetType: e.TargetType,
                ValueFrom: e.ValueFrom,
                Operator22: e.Operator22,
                ValueFrom2: e.ValueFrom2,
                incentive: e.Bonus,
                SalesValue: getInc.dataValue ,
                Status: getInc.status,
            }
            getpush.push(datalist)
        }))
    // console.log('getpush', getpush);
    return getpush
}

const getIncTUM = async (bodyData, FFRayonCode, produkID, Operator, ValueFrom, ValueTo) => {
    let data = await db.query(`select sum(SalesQuantity) as salesOneMonth from s01_actual_data sad where RayonCode = '${FFRayonCode}' and LineDesc = '${bodyData.LineName}' 
    and MonthSales = ${bodyData.monthFrom} and YearSales = ${bodyData.yearFrom} and ProductID = '${produkID}'`, {
        type: sequelize.QueryTypes.SELECT
    })
    data = data[0].salesOneMonth;
    let getSalesValue = (data) ? data : 0;
    let nilai = false;
    if (Operator == 'between') {
        if (getSalesValue >= ValueFrom && getSalesValue < ValueTo) {
            nilai = true;
        }
    } else {
        switch (Operator) {
            case "=":
                if (getSalesValue = ValueFrom) {
                    nilai = true;
                }
                break;
            case ">":
                if (getSalesValue > ValueFrom) {
                    nilai = true;
                }
                break;
            case ">=":
                if (getSalesValue >= ValueFrom) {
                    nilai = true;
                }
                break;
            case "<":
                if (getSalesValue < ValueFrom) {
                    nilai = true;
                }
                break;
            case "<=":
                if (getSalesValue <= ValueFrom) {
                    nilai = true;
                }
                break;

        }
    }
    let hasil = {
        dataValue: data,
        status: (nilai) ? true : false
    }

    return hasil
}


module.exports = {
    getRayonCodeHead,
    getActualTargetPerRayon,
    getTrueOrFalse,
    countTrueAndRecord,
    getDataATRenata,
    IncentiveTUMCatProduk
}
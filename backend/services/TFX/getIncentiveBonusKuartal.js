const sequelize = require("sequelize");
const db = require("../../../config/databaseSequalize");
const getActualTarget = require("./actual_target");
const convertRupiah = require('rupiah-format');
const getIncentiveRules = require("./getIncentiveRules");
// KUARTAL LINE
async function calculateRayonKuartal(dataSchema, actual, AT, ATTotal, ATReg, ATMax, productivityValue) {
    let passOne = null;
    let truefalse = null;
    let schemaSORT = dataSchema.sort(function (a, b) {
        a.RecordNo - b.RecordNo;
    });
    let schema = schemaSORT
    let trueXrecord = getIncentiveRules.countTrueAndRecord(schema)
    // console.log('trueXrecord', trueXrecord);
    if (trueXrecord == true) {
        for (i = 0; i < schema.length; i++) {
            if (schema[i].Status_Rumus == false && schema[i].RecordNo2 !== null) {
                if (schema[i].Operator1 == 'AND') {
                    if (schema[i].ValueFrom2 !== null && schema[i].TargetType2 !== null) {
                        // get true or false schemas second step
                        truefalse = getIncentiveRules.getTrueOrFalse(schema[i].TargetType2, schema[i].Operator22, schema[i].ValueFrom2, schema[i].ValueTo2, schema[i].Bonus, actual, AT, ATReg, ATMax, productivityValue, ATTotal)
                        // console.log('calculateRayonKuartal', truefalse);
                        if (truefalse.get_status == true) {
                            passOne = Math.ceil(schema[i].Bonus)
                        }
                    }
                }
            }
        }
    }


    let hasil = {
        incentive: (passOne) ? passOne : 0,
        calculate: truefalse
    }

    // console.log(hasil);
    return hasil
}
async function calculateKuartalGrowthRegular(dataSchema, EmployeeAreaCode, bodyData, RCodeDD, actualDDReg, AProd) {
    let LineName = bodyData.LineName;
    let YearNow = parseFloat(bodyData.yearFrom);
    // jika posisi anda di Q1
    let KuartalOne = [{
        "monthFrom": 1,
        "monthTo": 3,
        "yearTo": YearNow - 1,
        "LineName": LineName,
        "Kuartal": 1,
        "status": "Q1"
    }, {
        "monthFrom": 4,
        "monthTo": 6,
        "yearTo": YearNow - 1,
        "LineName": LineName,
        "Kuartal": 2,
        "status": "Q2"
    }, {
        "monthFrom": 7,
        "monthTo": 9,
        "yearTo": YearNow - 1,
        "LineName": LineName,
        "Kuartal": 3,
        "status": "Q3"
    }, {
        "monthFrom": 10,
        "monthTo": 12,
        "yearTo": YearNow - 1,
        "LineName": LineName,
        "Kuartal": 4,
        "status": "Q4"
    }];
    // jika posisi anda di Q2
    let KuartalTwo = [{
        "monthFrom": 4,
        "monthTo": 6,
        "yearTo": YearNow - 1,
        "LineName": LineName,
        "Kuartal": 2,
        "status": "Q2"
    }, {
        "monthFrom": 7,
        "monthTo": 9,
        "yearTo": YearNow - 1,
        "LineName": LineName,
        "Kuartal": 3,
        "status": "Q3"
    }, {
        "monthFrom": 10,
        "monthTo": 12,
        "yearTo": YearNow - 1,
        "LineName": LineName,
        "Kuartal": 4,
        "status": "Q4"
    }, {
        "monthFrom": 1,
        "monthTo": 3,
        "yearTo": YearNow,
        "LineName": LineName,
        "Kuartal": 1,
        "status": "Q1"
    }, ];
    // jika posisi anda di Q3
    let KuartalThree = [{
        "monthFrom": 7,
        "monthTo": 9,
        "yearTo": YearNow - 1,
        "LineName": LineName,
        "Kuartal": 3,
        "status": "Q3"
    }, {
        "monthFrom": 10,
        "monthTo": 12,
        "yearTo": YearNow - 1,
        "LineName": LineName,
        "Kuartal": 4,
        "status": "Q4"
    }, {
        "monthFrom": 1,
        "monthTo": 3,
        "yearTo": YearNow,
        "LineName": LineName,
        "Kuartal": 1,
        "status": "Q1"
    }, {
        "monthFrom": 4,
        "monthTo": 6,
        "yearTo": YearNow,
        "LineName": LineName,
        "Kuartal": 2,
        "status": "Q2"
    }];
    // jika posisi anda di Q4
    let KuartalFour = [{
        "monthFrom": 10,
        "monthTo": 12,
        "yearTo": YearNow - 1,
        "LineName": LineName,
        "Kuartal": 4,
        "status": "Q4"
    }, {
        "monthFrom": 1,
        "monthTo": 3,
        "yearTo": YearNow,
        "LineName": LineName,
        "Kuartal": 1,
        "status": "Q1"
    }, {
        "monthFrom": 4,
        "monthTo": 6,
        "yearTo": YearNow,
        "LineName": LineName,
        "Kuartal": 2,
        "status": "Q2"
    }, {
        "monthFrom": 7,
        "monthTo": 9,
        "yearTo": YearNow,
        "LineName": LineName,
        "Kuartal": 3,
        "status": "Q3"
    }];

    let getKuartalOther = "";
    let getKuartalIncentive = "";
    let getKuartalYear = "";
    switch (bodyData.monthFrom) {
        case 1:
            getKuartalOther = KuartalOne
            getKuartalIncentive = 4
            getKuartalYear = YearNow - 1
            break;
        case 4:
            getKuartalOther = KuartalTwo
            getKuartalIncentive = 1
            getKuartalYear = YearNow
            break;
        case 7:
            getKuartalOther = KuartalThree
            getKuartalIncentive = 2
            getKuartalYear = YearNow
            break;
        case 10:
            getKuartalOther = KuartalFour
            getKuartalIncentive = 3
            getKuartalYear = YearNow
            break;
    }
    let data = await getActualTargetKuartalMax(getKuartalOther, RCodeDD, LineName);
    let dataInc = await getActualTarget.getIncMaxSales(getKuartalIncentive, getKuartalYear, RCodeDD, LineName);

    let listgoal = [];
    for (i = 0; i < data.length; i++) {
        listgoal.push(data[i].maxSales)
    }
    let basicSales = parseFloat(Math.max.apply(Math, listgoal))
    let CalculateGrowth = parseFloat((actualDDReg - basicSales) / basicSales);
    let growth = (CalculateGrowth) ? CalculateGrowth : 0;
    if (basicSales !== 0) {
        growth = growth.toFixed(2) * 100;
    } else {
        growth = 0
    }
    let passOne = 0;
    let datalog = {
        areaCode: EmployeeAreaCode,
        // dataKuartalBefore: data,
        dataBasicSales: basicSales,
        dataGrowth: growth,
        dataIncBefore: dataInc,
    }
    // console.log('datalog', datalog);
    let schemaSORT = dataSchema.sort(function (a, b) {
        a.RecordNo - b.RecordNo;
    });
    let schema = schemaSORT
    let trueXrecord = getIncentiveRules.countTrueAndRecord(schema)
    growth = 3;
    if (trueXrecord == true) {
        for (i = 0; i < schema.length; i++) {
            if (schema[i].TargetType == 'growth') {
                if (growth == schema[i].ValueFrom) {
                    passOne = true;
                } else if (growth >= schema[i].ValueFrom) {
                    passOne = true;
                } else if (growth <= schema[i].ValueFrom) {
                    passOne = true;
                } else if (growth > schema[i].ValueFrom) {
                    passOne = true;
                } else if (growth < schema[i].ValueFrom) {
                    passOne = true;
                }
            }
            if (passOne === true) {
                if (schema[i].Operator1 == 'AND' && schema[i].TargetType == null && schema[i].RecordNo2 !== null) {
                    // dataInc = 1000000;
                    if (schema[i].TargetType2 == 'previous_incentive') {
                        switch (schema[i].Operator22) {
                            case ">":
                                if (dataInc > schema[i].ValueFrom2) {
                                    passOne = schema[i].Bonus
                                }
                                break;
                            case "<":
                                if (dataInc < schema[i].ValueFrom2) {
                                    passOne = schema[i].Bonus
                                }
                                break;
                            case "<=":
                                if (dataInc <= schema[i].ValueFrom2) {
                                    passOne = schema[i].Bonus
                                }
                                break;
                            case ">=":
                                if (dataInc >= schema[i].ValueFrom2) {
                                    passOne = schema[i].Bonus
                                }
                                break;
                            case "=":
                                if (dataInc == schema[i].ValueFrom2) {
                                    passOne = schema[i].Bonus
                                }
                                break;
                        }
                    }
                }

            }
        }
    }
    console.log('Final Incentive Growth', EmployeeAreaCode, passOne);
    let hasil = {
        incentive: (passOne) ? passOne : 0,
        calculate: datalog
    }
    return hasil
}


const getActualTargetKuartalMax = async (getKuartalOther, RCodeDD, LineName) => {
    let getpush = [];
    await Promise.all(
        getKuartalOther.map(async (ebody) => {
            let dataMaxSales = await getActualTarget.getMaxSales(ebody, RCodeDD, LineName);
            let datalist = {
                status: ebody.status,
                year: ebody.yearTo,
                monthFrom: ebody.monthFrom,
                monthTo: ebody.monthTo,
                maxSales: dataMaxSales,
                maxSalesRp: convertRupiah.convert(dataMaxSales),
                lineDesc: LineName,
            }
            getpush.push(datalist)
        }))
    return getpush
}
async function calculateKuartalHattrick(dataSchema, CustomerArea, bodyData, areaCodeStr, filterAnd, filterOr, productivityValue) {
    let monthx = parseInt(bodyData.monthFrom)
    let monthkuartal = [{
        "month": monthx
    }, {
        "month": monthx + 1
    }, {
        "month": monthx + 2
    }]

    let passOne = null;
    let getpush = [];
    await Promise.all(
        monthkuartal.map(async (e) => {
            let NewbodyData = {
                monthFrom: e.month,
                monthTo: e.month,
                yearTo: bodyData.yearFrom,
                LineName: bodyData.LineName,
            }
            bodyData = NewbodyData;
            let actual = await getActualTarget.getSalesActualKuartalValue(CustomerArea, bodyData, areaCodeStr, filterAnd, filterOr, 'no');
            let target = await getActualTarget.getSalesTargetKuartalValue(CustomerArea, bodyData, areaCodeStr, filterAnd, filterOr, 'no');
            let AperT = (actual / target) * 100;
            let AperTProd = (actual / productivityValue) * 100;
            let StatusAT = (AperT >= 100) ? true : false;
            let StatusATProd = (AperT >= 100 && AperTProd >= 100) ? true : false;
            let datalist = {
                year: bodyData.yearFrom,
                month: e.month,
                actual: actual,
                target: target,
                AT: AperT.toFixed(2) + '%',
                ATProd: AperTProd.toFixed(2) + '%',
                status: StatusAT,
                statusATProd: StatusATProd
            }
            getpush.push(datalist)
        }))

    let schemaSORT = dataSchema.sort(function (a, b) {
        a.RecordNo - b.RecordNo;
    });
    let schema = schemaSORT
    let countgetpush = 0;
    let trueXrecord = getIncentiveRules.countTrueAndRecord(schema)
    if (trueXrecord == true) {
        for (i = 0; i < schema.length; i++) {
            if (schema[i].Status_Rumus == false && schema[i].RecordNo2 !== null) {
                if (schema[i].Operator1 == 'AND') {
                    if (schema[i].ValueFrom2 !== null && schema[i].OperatorType2 == 'max') {
                        // console.log('(schema[i].TargetType2)', CustomerArea, schema[i].TargetType2);
                        if (schema[i].TargetType2 == 'actual_target') {
                            countgetpush = getpush.filter(v => v.statusATProd === true).length;
                        } else if (schema[i].TargetType2 == 'actual_target_productivitas') {
                            countgetpush = getpush.filter(v => v.statusATProd === true).length;
                        } else {
                            countgetpush = getpush.filter(v => v.status === true).length;
                        }
                        switch (schema[i].Operator22) {
                            case "=":
                                if (countgetpush == schema[i].ValueFrom2) {
                                    passOne = Math.ceil(schema[i].Bonus)
                                }
                                break;
                            case ">=":
                                if (countgetpush >= schema[i].ValueFrom2) {
                                    passOne = Math.ceil(schema[i].Bonus)
                                }
                                break;
                            case "<=":
                                if (countgetpush <= schema[i].ValueFrom2) {
                                    passOne = Math.ceil(schema[i].Bonus)
                                }
                                break;
                            case ">":
                                if (countgetpush > schema[i].ValueFrom2) {
                                    passOne = Math.ceil(schema[i].Bonus)
                                }
                                break;
                            case "<":
                                if (countgetpush < schema[i].ValueFrom2) {
                                    passOne = Math.ceil(schema[i].Bonus)
                                }
                                break;
                        }
                    }
                }
            }
        }
    }

    let hasil = {
        incentive: (passOne) ? passOne : 0,
        calculate: getpush
    }

    return hasil
}

async function calculateAreaKuartal(dataSchema, CustomerArea, bodyData, areaCodeStr, filterAnd, filterOr) {
    let monthx = parseInt(bodyData.monthFrom)
    let monthkuartal = [{
        "month": monthx
    }, {
        "month": monthx + 1
    }, {
        "month": monthx + 2
    }]
    let getpush = [];
    await Promise.all(
        monthkuartal.map(async (e) => {
            let NewbodyData = {
                monthFrom: e.month,
                monthTo: e.month,
                yearTo: bodyData.yearFrom,
                LineName: bodyData.LineName,
            }
            bodyData = NewbodyData;
            let actual = await getActualTarget.getSalesActualKuartalValue(CustomerArea, bodyData, areaCodeStr, filterAnd, filterOr, 'no');
            let target = await getActualTarget.getSalesTargetKuartalValue(CustomerArea, bodyData, areaCodeStr, filterAnd, filterOr, 'no');
            let AperT = (actual / target) * 100;
            let StatusAT = (AperT >= 100) ? true : false;
            let datalist = {
                year: bodyData.yearFrom,
                month: e.month,
                actual: actual,
                target: target,
                AT: AperT.toFixed(2) + '%',
                status: StatusAT
            }
            getpush.push(datalist)
        }))
    let countgetpush = getpush.filter(v => v.status === true).length;
    let passOne = null;
    let schemaSORT = dataSchema.sort(function (a, b) {
        a.RecordNo - b.RecordNo;
    });
    let schema = schemaSORT
    let trueXrecord = getIncentiveRules.countTrueAndRecord(schema)
    if (trueXrecord == true) {
        // console.log('calculateAreaKuartal getpush', areaCodeStr, '=', countgetpush, getpush);
        for (i = 0; i < schema.length; i++) {
            if (schema[i].Status_Rumus == false && schema[i].RecordNo2 !== null) {
                if (schema[i].Operator1 == 'AND') {
                    if (schema[i].ValueFrom2 !== null && schema[i].OperatorType2 == 'max') {
                        switch (schema[i].Operator22) {
                            case "=":
                                if (countgetpush == schema[i].ValueFrom2) {
                                    passOne = Math.ceil(schema[i].Bonus)
                                }
                                break;
                            case ">=":
                                if (countgetpush >= schema[i].ValueFrom2) {
                                    passOne = Math.ceil(schema[i].Bonus)
                                }
                                break;
                            case "<=":
                                if (countgetpush <= schema[i].ValueFrom2) {
                                    passOne = Math.ceil(schema[i].Bonus)
                                }
                                break;
                            case ">":
                                if (countgetpush > schema[i].ValueFrom2) {
                                    passOne = Math.ceil(schema[i].Bonus)
                                }
                                break;
                            case "<":
                                if (countgetpush < schema[i].ValueFrom2) {
                                    passOne = Math.ceil(schema[i].Bonus)
                                }
                                break;
                        }
                    }
                }
            }
        }
    }

    let hasil = {
        incentive: (passOne) ? passOne : 0,
        calculate: getpush
    }

    return hasil
}


async function calculateATCabangKuartal(dataSchema, actualDD, targetDD, productivityValue) {
    // //console.log('cabang',dataSchema);
    let passOne = 0;
    let schemaSORT = dataSchema.sort(function (a, b) {
        a.RecordNo - b.RecordNo;
    });
    let goal = 0;
    let schema = schemaSORT
    let trueXrecord = getIncentiveRules.countTrueAndRecord(schema)
    if (trueXrecord == true) {
        for (i = 0; i < schema.length; i++) {
            if (schema[i].Status_Rumus == false && schema[i].RecordNo2 !== null) {
                if (schema[i].Operator1 == 'AND') {
                    goal = (schema[i].ValueFrom2 / 100) * (actualDD - targetDD)
                    if (goal <= 0) {
                        passOne = 0
                    } else {
                        if (goal >= schema[i].Bonus) {
                            passOne = Math.ceil(schema[i].Bonus)
                        } else {
                            passOne = Math.ceil(goal)
                        }
                    }
                }
            }
        }
    }

    let hasil = {
        incentive: (passOne) ? passOne : 0,
        calculate: goal
    }

    return hasil
}

function ATLoopTotal(hasil, data) {
    let hasilpush = [];
    let hasilX = 0;
    let hasilY = 0;
    data.map(e => {
        let totalInc = getChildATTotal(hasil, e.RayonCode)
        hasilpush.push(totalInc)
    })
    hasilpush = hasilpush.flat(1);
    hasilY = hasilpush
    // console.log('ATLoopTotal', data, hasilpush);
    hasilX = hasilpush.filter(idx => idx.ATtotal >= 100);
    // console.log('hasilX', hasilX);
    if (hasilX.length > 0) {
        hasilX = getTotalAT(hasilX)
    } else {
        hasilX = 0
    }

    if (hasilY.length > 0) {
        hasilY = getTotalATNominal(hasilY)
    } else {
        hasilY = 0
    }

    let hasilFINAL = {
        hasilATMR: (hasilX) ? hasilX : 0,
        hasilATPimda: (hasilY) ? hasilY : 0
    }
    return hasilFINAL
}

function getTotalAT(data) {
    let total = 0;
    if (data.length > 0 || data.length !== undefined) {
        total = data.map(e => e.ATtotal).reduce((acc, amount) => acc + amount);
    }
    if (total == 'undefined' || total == 'NAN' || total == null || total == undefined) {
        total = 0
    }
    total = total.toFixed(2);
    return total
}

function getTotalATNominal(data) {
    let totalActual = 0;
    let totalTarget = 0;
    if (data.length > 0 || data.length !== undefined) {
        totalActual = data.map(e => e.ActualAll).reduce((acc, amount) => acc + amount);
        totalTarget = data.map(e => e.TargetAll).reduce((acc, amount) => acc + amount);
    }

    let hasilTotal = (parseInt(totalActual) / parseInt(totalTarget)) * 100
    hasilTotal = hasilTotal.toFixed(2);
    return (hasilTotal) ? hasilTotal : 0
}

function getChildATTotal(dataParent, RayonCode) {
    let hasilpush = [];
    dataParent.map(e => {
        let datahasil = {
            RayonCode: e.RayonCode,
            EmployeeName: e.EmployeeName,
            ATtotal: e.actualTargetDefault,
            ActualAll: e.actualAll,
            TargetAll: e.targetAll
        }
        hasilpush.push(datahasil)
    })

    return hasilpush.filter(e => e.RayonCode.includes(RayonCode))
}


async function calculateKuartalPOTPimdaMR(dataSchema, RCodeDD, ATTotal, hasilparent) {
    let mTotalAT = 0;
    let passOne = 0;

    let dataAllMR = RCodeDD.dataSupMR;
    let hasilParentMR = hasilparent.filter(n => n.EmployeePositionCode.includes('MR'));
    if (dataAllMR !== null) {
        mTotalAT = ATLoopTotal(hasilParentMR, dataAllMR)
    }

    console.log('mTotalAT', mTotalAT);
    let ATMRAll = (mTotalAT.hasilATMR) ? mTotalAT.hasilATMR : 0;
    let ATPIMDAAll = (mTotalAT.hasilATPimda) ? mTotalAT.hasilATPimda : 0;
    let schemaSORT = dataSchema.sort(function (a, b) {
        a.RecordNo - b.RecordNo;
    });
    let goalPOT = 0;
    let schema = schemaSORT
    let trueXrecord = getIncentiveRules.countTrueAndRecord(schema)
    if (trueXrecord == true) {
        for (i = 0; i < schema.length; i++) {
            if (schema[i].Status_Rumus == false && schema[i].RecordNo2 !== null) {
                if (schema[i].Operator1 == 'AND' && schema[i].TargetType2 == 'POT') {
                    goalPOT = (parseInt(RCodeDD.countSupMR) * parseInt(schema[i].Bonus))
                    if (ATPIMDAAll >= 100) {
                        ATTotal = ATTotal.toFixed(2);
                        passOne = (ATTotal / ATMRAll) * goalPOT;
                    }
                }
            }
        }
    }
    let hasil = {
        incentive: (passOne) ? passOne : 0,
        calculate: "POT :" + goalPOT + " & Count :" + RCodeDD.countSupMR + " & AT Total: " + ATTotal + " & Total AT MR: " + ATMRAll + " & AT PIMDA: " + ATPIMDAAll
    }

    return hasil
}

module.exports = {
    calculateAreaKuartal,
    calculateATCabangKuartal,
    calculateRayonKuartal,
    calculateKuartalGrowthRegular,
    calculateKuartalPOTPimdaMR,
    calculateKuartalHattrick
}
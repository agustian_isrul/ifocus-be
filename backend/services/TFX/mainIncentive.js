const sequelize = require("sequelize");
const db = require("../../../config/databaseSequalize");
const moment = require("moment");
const convertRupiah = require('rupiah-format');
const getIncentiveCalculate = require("./getIncentiveSchema")
const getIncentiveBonusBulanan = require("./getIncentiveBonusBulanan")
const getIncentiveBonusKuartal = require("./getIncentiveBonusKuartal")
const getIncentiveRules = require("./getIncentiveRules")
const getActualTarget = require("./actual_target");

const dataEmployee = async (bodyData, filterAnd, filterOr, targetHJ, produktivity, ConfigDetailId, SchemeConfigId, SchemaId, SchemaLabel, hasil) => {
    
    console.log('run', SchemaLabel);
    let QueryFilterAnd = "";
    let QueryFilterOr = "";

    if (filterAnd != 'no') {
        QueryFilterAnd = "and (" + filterAnd + ")";
    } else if (filterOr != 'no') {
        QueryFilterOr = "and (" + filterOr + ")";
    }
    let data = await db.query(`select count(RayonCode) as CountRayon, RayonCode,EmployeeID,EmployeeName,EmployeePositionCode,EmployeeAreaCode,AreaBased,LineDesc,EmployeeVacant,EmployeeSubLineDesc,
    RayonCodeSup,EmployeeSupID,EmployeeMRType from s03_structure_employee sse  where LineDesc = '${bodyData.LineName}' ${QueryFilterAnd} ${QueryFilterOr}
    group by EmployeeID`, {
        type: sequelize.QueryTypes.SELECT
    });
    if (SchemaId == 9 || SchemaId == 26) {
        data = await db.query(`select count(sse.EmployeeAreaCode) as CountAreaCode , mag.EmployeeAreaCode as AreaMerge ,sse.RayonCode ,
        EmployeeID, EmployeeName,EmployeePositionCode,sse.EmployeeAreaCode,AreaBased,sse.LineDesc,EmployeeSubLineDesc,RayonCodeSup,EmployeeSupID,EmployeeMRType ,RayonCodeSup
        from s03_structure_employee sse 
        left join m15_area_group mag on mag.RayonCode = sse.RayonCode 
        where sse.LineDesc = '${bodyData.LineName}' ${QueryFilterAnd} ${QueryFilterOr}
        group by AreaMerge , EmployeeID `, {
            type: sequelize.QueryTypes.SELECT
        });
    }
    // console.log('data',data);
    await Promise.all(
        data.map(async (e) => {
            let RCodeDD = await getRayonCOdeList(e.EmployeeID, bodyData.LineName)
            let FRCode = 'no';
            if (e.EmployeePositionCode !== 'MR') {
                FRCode = await getIncentiveRules.getRayonCodeHead(e.EmployeeID, bodyData.LineName, e.EmployeePositionCode, 'no', 'no');
            }
            let filterParentAnd = 'no';
            if (bodyData.LineName == 'OPTIMA') {
                if (bodyData.SchemeType == 1) {
                    filterParentAnd = `ProductID not in ('ALEG001','HIAL001','KARY001','CRAV001','TARI001','FLUM001')`;
                }
            } else if (bodyData.lineName == 'HARMONY') {
                filterParentAnd = `ProductId not in ( 'BRIX001', 'BRIX002', 'INBU002')`;
            }
            let RayonDefault = (RCodeDD.rayonCodeDefault) ? RCodeDD.rayonCodeDefault : e.RayonCode
            let actualDD = await getActualTarget.getSalesActualValue(e.EmployeePositionCode, RayonDefault, bodyData, FRCode, filterParentAnd, 'no', 'no', 'no')
            let targetDD = await getActualTarget.getSalesTargetValue(e.EmployeePositionCode, RayonDefault, bodyData, FRCode, 'no', 'no', 'no')
            // console.log('targetDD basic',targetDD);
            let AProd = 0;
            if (targetHJ !== 0) {
                targetDD = parseFloat(targetDD * (parseFloat(targetHJ) / 100));
                // console.log('targetHJ',targetHJ, targetDD);
            }

            if (produktivity !== 0) {
                AProd = (actualDD / produktivity) * 100;
            }
            let ATMax = (actualDD / (targetDD >= produktivity ? targetDD : produktivity)) * 100;
            let AT = (actualDD / targetDD) * 100;
            if (AT == 'Infinity' || AT == Infinity) {
                AT = 0;
            }
            let incentiveCalcuate = await getFilterandCalculate(bodyData, e, RayonDefault, actualDD, targetDD, produktivity, ConfigDetailId, SchemeConfigId, SchemaId, SchemaLabel, RCodeDD, hasil)
            if (actualDD > 0 || targetDD > 0) {
                let datahasil = {
                    EmployeeID: e.EmployeeID,
                    RayonCode: RayonDefault,
                    RayonCodeSup: e.RayonCodeSup,
                    RayonCodeHub: RCodeDD.rayonCode,
                    RayonCodeFilter: RCodeDD.rayonCodeFilter,
                    RayonCodeList: RCodeDD.rayonCodeList,
                    EmployeeName: e.EmployeeName,
                    EmployeePositionCode: e.EmployeePositionCode,
                    EmployeeAreaCode: e.EmployeeAreaCode,
                    EmployeeSubLineDesc: e.EmployeeSubLineDesc,
                    EmployeeMRType: e.EmployeeMRType,
                    EmployeeVacant: e.EmployeeVacant,
                    actualAll: actualDD,
                    targetAll: targetDD,
                    targetHJSistem: targetHJ + '%',
                    productivity: produktivity,
                    actualTargetDefault: AT,
                    actualTargetAll: AT.toFixed(2) + '%',
                    actualTargetMax: ATMax.toFixed(2) + '%',
                    actualProductivity: AProd.toFixed(2) + '%',
                    // schemaList: incentiveCalcuate
                }
                const ExistEmployee = hasil.find(xx => xx.EmployeeID === e.EmployeeID && xx.RayonCode === RayonDefault);
                if (ExistEmployee) {
                    ExistEmployee.schemaList = ExistEmployee.schemaList || []
                    ExistEmployee.schemaList.push(incentiveCalcuate)
                } else {
                    datahasil.schemaList = datahasil.schemaList || []
                    datahasil.schemaList.push(incentiveCalcuate)
                    hasil.push(datahasil)
                }
            }
        }))
    return hasil
}
const getDataSupFromMR = async (EmployeeID, LineName) => {
    let data = await db.query(`select RayonCode , EmployeeName , EmployeeSupID , RayonCodeSup , SuperiorPosition  from s03_structure_employee sse where LineDesc = '${LineName}' and EmployeeID = '${EmployeeID}'`, {
        type: sequelize.QueryTypes.SELECT
    })
    let dataEmployeeIDSup = null;
    let datax = null;
    let dataxArray = [];
    let dataRayonSup = '';
    let dataxCount = 0;
    if (data.length > 0) {
        if (data[0].SuperiorPosition === 'SUP') {
            dataEmployeeIDSup = data[0].EmployeeSupID
            dataRayonSup = data[0].RayonCodeSup
            // for (i = 0; i < data.length; i++) {
            //     dataRayonSup.push(data[i].RayonCodeSup)
            // }
        }
    }

    if (dataEmployeeIDSup !== null) {
        datax = await db.query(`select RayonCode from s03_structure_employee where LineDesc = '${LineName}'  and EmployeePositionCode = 'MR' and EmployeeSupID = '${dataEmployeeIDSup}'`, {
            type: sequelize.QueryTypes.SELECT
        })
        dataxCount = await db.query(`select count(*) as jml_mr from s03_structure_employee where LineDesc = '${LineName}'  and EmployeePositionCode = 'MR' and EmployeeSupID = '${dataEmployeeIDSup}'`, {
            type: sequelize.QueryTypes.SELECT
        })
        dataxCount = dataxCount[0].jml_mr
        if (datax.length > 0) {
            for (i = 0; i < datax.length; i++) {
                dataxArray.push(datax[i].RayonCode)
            }
        }
    }
    let hasil = {
        dataSUPMR: datax,
        count: dataxCount,
        rayonCodeSup: dataRayonSup
    }

    return hasil
}

const getRayonCOdeList = async (EmployeeID, LineName) => {
    let data = await db.query(`select RayonCode,EmployeeID,EmployeeName,EmployeePositionCode,EmployeeAreaCode,EmployeeSubLineDesc,EmployeeMRType,EmployeeVacant,RayonCodeSup,LineDesc,IsDefault
    from s03_structure_employee sse where sse.EmployeeID  = '${EmployeeID}'`, {
        type: sequelize.QueryTypes.SELECT
    })
    let loopdataSupMR = null;
    if (data.length > 0) {
        loopdataSupMR = await getDataSupFromMR(EmployeeID, LineName);
    }
    let loopRayonDefault = "";
    let loopRayon = "";
    let loopRayonFStr = "";
    let loopRayonFStrDM = "";
    let loopAreaFStr = "";
    let loopCabang = "";
    let datax = "";
    let listfill = [];
    if (data.length > 0) {
        for (i = 0; i < data.length; i++) {
            if (data[i].IsDefault === 1) {
                loopRayonDefault = data[i].RayonCode
            }
            loopRayon += data[i].RayonCode + "+";
            listfill.push(data[i].RayonCode);
            loopRayonFStr += "'" + data[i].RayonCode + "',";
            loopAreaFStr += "'" + data[i].EmployeeAreaCode + "',";
        }
        if (loopRayon !== "") {
            loopRayon = loopRayon.slice(0, -1);
            loopRayonFStr = loopRayonFStr.slice(0, -1);
            loopAreaFStr = loopAreaFStr.slice(0, -1);
            loopCabang = loopAreaFStr.replace(/-/g, "");

            if (data.length > 1) {
                loopRayonFStrDM = "RayonCodeSup in" + " " + "(" + loopRayonFStr + ")";
                loopRayonFStr = "RayonCode in" + " " + "(" + loopRayonFStr + ")";
                loopAreaFStr = "CustomerArea in" + " " + "(" + loopAreaFStr + ")";
                loopCabang = "Cabang in" + " " + "(" + loopCabang + ")";

            } else {
                loopRayonFStrDM = "RayonCodeSup" + " " + "=" + " " + loopRayonFStr;
                loopRayonFStr = "RayonCode" + " " + "=" + " " + loopRayonFStr;
                loopAreaFStr = "CustomerArea" + " " + "=" + " " + loopAreaFStr;
                loopCabang = "Cabang" + " " + "=" + " " + loopCabang;
            }

            datax = {
                dataSupMR: loopdataSupMR.dataSUPMR,
                countSupMR: loopdataSupMR.count,
                rayonCodeSup: loopdataSupMR.rayonCodeSup,
                rayonCodeDefault: (loopRayonDefault) ? loopRayonDefault : null,
                rayonCode: (loopRayon) ? loopRayon : null,
                rayonCodeStrDM: (loopRayonFStrDM) ? loopRayonFStrDM : null,
                rayonCodeStr: (loopRayonFStr) ? loopRayonFStr : null,
                areaCodeStr: (loopAreaFStr) ? loopAreaFStr : null,
                cabangStr: (loopCabang) ? loopCabang : null,
                rayonCodeFilter: (listfill) ? listfill : null,
                rayonCodeList: (data) ? data : null
            }
        }
    } else {
        datax = {
            dataSupMR: null,
            countSupMR: 0,
            rayonCodeSup: null,
            rayonCodeDefault: null,
            rayonCode: null,
            rayonCodeStrDM: null,
            rayonCodeStr: null,
            areaCodeStr: null,
            cabangStr: null,
            rayonCodeFilter: null,
            rayonCodeList: null
        }
    }
    // console.log('RCodeDD', datax);
    return datax

}
const getSchemaFilterEmployee = async (bodyData) => {
    let data = [];
    let hasil = [];
    try {
        data = await db.query(`select tsc.*,ms.SchemeType  from t04_schema_config tsc 
        left join m01_schema ms on ms.SchemaId = tsc.SchemaId  where tsc.SchemeConfigId = '${bodyData.SchemeConfigId}' 
        and ms.SchemeType = '${bodyData.SchemeType}' order by tsc.SchemaLabel, tsc.ConfigDetailId  asc`, {
            type: sequelize.QueryTypes.SELECT
        })
        let dataArray = [];
        for (const element of data) {
            dataArray.push(element)
        }
        await Promise.all(
            dataArray.map( async (e) => {
                let find = await findEmployeeFilter2(e.ConfigDetailId, e.SchemeConfigId)
                let produktivity = await getActualTarget.getProduktivity(bodyData.LineName, find.filterAnd, find.filterOr)
                hasil = await dataEmployee(bodyData, find.filterAnd, find.filterOr, find.targetHJ, produktivity, e.ConfigDetailId, e.SchemeConfigId, e.SchemaId, e.SchemaLabel, hasil)
            })
        )
    } catch (error) {
        throw error
    }
    return hasil
}
async function calculate(bodyData) {
    let data = await getSchemaFilterEmployee(bodyData)
    let hasilpush = [];
    await Promise.all(
        data.map(async (e) => {
            let totalInc = await getTotalincentive(e.schemaList)
            let datahasil = {
                EmployeeID: e.EmployeeID,
                RayonCode: e.RayonCodeHub,
                RayonCodeSup: e.RayonCodeSup,
                RayonCodeFilter: e.RayonCodeFilter,
                RayonCodeList: e.RayonCodeList,
                EmployeeName: e.EmployeeName,
                EmployeePositionCode: e.EmployeePositionCode,
                EmployeeAreaCode: e.EmployeeAreaCode,
                EmployeeSubLineDesc: e.EmployeeSubLineDesc,
                EmployeeMRType: e.EmployeeMRType,
                EmployeeVacant: e.EmployeeVacant,
                totalIncentive: convertRupiah.convert(Math.round(totalInc)),
                actualAll: convertRupiah.convert(Math.round(e.actualAll)),
                targetAll: convertRupiah.convert(Math.round(e.targetAll)),
                targetHJSistem: e.targetHJSistem,
                productivity: convertRupiah.convert(Math.round(e.productivity)),
                actualTargetAll: e.actualTargetAll,
                actualTargetMaxAll: e.actualTargetMax,
                actualProductivity: e.actualProductivity,
                schemaList: e.schemaList
            }
            hasilpush.push(datahasil)
        }))
    console.log('finish', hasilpush.length, ' data');
    return hasilpush
}



const getTotalincentive = async (data) => {
    let total = '';
    if (data.length > 0 || data.length !== undefined) {
        total = data.map(e => e.incentive).reduce((acc, amount) => acc + amount);
    } else {
        total = 0
    }
    return total
}

const getCabangArea = async (filterCabang, lineName) => {
    let filterCbg = "";
    if (filterCabang !== null) {
        filterCbg = "and (" + filterCabang + ")";
    }
    let data = await db.query(`select * from m06_kriteria_cabang mkc where LineDesc = '${lineName}' ${filterCbg} group by Kriteria `, {
        type: sequelize.QueryTypes.SELECT
    })
    return (data) ? data : null
}

const getFilterandCalculate = async (bodyData, Ee, RayonDefault, ActualALL, TargetAll, productivityValue, ConfigDetailId, SchemeConfigId, SchemaId, SchemaLabel, RCodeDD, hasil) => {
    let FtrQ = await findFilter(SchemeConfigId, ConfigDetailId)
    if (FtrQ.length == 0) {
        FtrQ = {
            filterAnd: 'no',
            filterOr: 'no',
            count: 0
        }
    }

    let QueryRegPlus = 'no';
    if (bodyData.LineName == 'HARMONY') {
        QueryRegPlus = `StatusFaktur in ('I','R')`;
    }
    let FFRayonCode = RCodeDD.rayonCode
    let daysales = 'no';
    if (SchemaId == 9) {
        daysales = "DaySales between 1 and 15";
    }
    let actualDD = 0;
    let actualPAPDD = 0;
    let actualPLPDD = 0;
    let targetDD = 0;
    let actualDDReg = 0;
    let targetDDReg = 0;
    let FRCode = 'no';
    let dataATMR = '';

    if (Ee.EmployeePositionCode !== 'MR') {
        if (SchemaId == 33) { //yes penggunaan ACT Special MR from DM
            FRCode = await getIncentiveRules.getRayonCodeHead(Ee.EmployeeID, bodyData.LineName, Ee.EmployeePositionCode, 'no', 'specialist');
        } else if (SchemaId == 34 || SchemaId == 35) { // Rerata Pimda get SUP && product fokus SUP dan DM
            FRCode = await getIncentiveRules.getRayonCodeHead(Ee.EmployeeID, bodyData.LineName, Ee.EmployeePositionCode, 'no', 'withoutDP');
        } else if (SchemaId == 27) { // Area BInaan get SUP
            FRCode = await getIncentiveRules.getRayonCodeHead(Ee.EmployeeID, bodyData.LineName, Ee.EmployeePositionCode, 'no', 'binaanpimda');
        } else if (SchemaId == 9 || SchemaId == 26) { // Area calculateTarikaSalesHarian cabang
            let cabangArea = await getCabangArea(RCodeDD.cabangStr, bodyData.LineName)
            if (cabangArea.length > 1) {
                FRCode = await getIncentiveRules.getRayonCodeHead(Ee.EmployeeID, bodyData.LineName, Ee.EmployeePositionCode, RayonDefault, 'rayoncode');
                RCodeDD.areaCodeStr = 'no';
                FFRayonCode = RayonDefault
            } else {
                FRCode = await getIncentiveRules.getRayonCodeHead(Ee.EmployeeID, bodyData.LineName, Ee.EmployeePositionCode, RayonDefault, 'no');
            }
        } else {
            FRCode = await getIncentiveRules.getRayonCodeHead(Ee.EmployeeID, bodyData.LineName, Ee.EmployeePositionCode, RayonDefault, 'no');
        }
        dataATMR = await getIncentiveRules.getActualTargetPerRayon(Ee.EmployeePositionCode, FtrQ, FRCode, bodyData, QueryRegPlus)
    }

    switch (bodyData.SchemeType) {
        case "1":
            actualDD = await getActualTarget.getSalesActualValue(Ee.EmployeePositionCode, RayonDefault, bodyData, FRCode, FtrQ.filterAnd, FtrQ.filterOr, daysales, 'no');
            actualPAPDD = await getActualTarget.getSalesActualPAPValue(Ee.EmployeePositionCode, RayonDefault, bodyData, FRCode, FtrQ.filterAnd, FtrQ.filterOr, daysales, 'no');
            actualPLPDD = await getActualTarget.getSalesActualPLPValue(Ee.EmployeePositionCode, RayonDefault, bodyData, FRCode, FtrQ.filterAnd, FtrQ.filterOr, daysales, 'no');
            targetDD = await getActualTarget.getSalesTargetValue(Ee.EmployeePositionCode, RayonDefault, bodyData, FRCode, FtrQ.filterAnd, FtrQ.filterOr, 'no');
            actualDDReg = await getActualTarget.getSalesActualValue(Ee.EmployeePositionCode, RayonDefault, bodyData, FRCode, FtrQ.filterAnd, FtrQ.filterOr, daysales, QueryRegPlus);
            targetDDReg = await getActualTarget.getSalesTargetValue(Ee.EmployeePositionCode, RayonDefault, bodyData, FRCode, FtrQ.filterAnd, FtrQ.filterOr, QueryRegPlus);
            if (SchemaId == 2 || SchemaId == 35) { // product tum
                let RayonCodeTUM = null;
                if (bodyData.LineName == 'OPTIMA') {
                    RayonCodeTUM = RayonDefault
                }
                let targetTUMDD = await getTumIncentive(SchemeConfigId, ConfigDetailId, 0, RayonCodeTUM)
                if (Ee.EmployeePositionCode !== 'MR') {
                    targetTUMDD = await getTumIncentive(SchemeConfigId, ConfigDetailId, FRCode, RayonCodeTUM)
                }
                // targetDD = (targetDD >= targetTUMDD) ? targetDD : targetTUMDD
                targetDD = (targetTUMDD) ? targetTUMDD : 0;
                targetDDReg = targetDD
            } else if (SchemaId == 5) {
                actualDD = await getActualTarget.getOutletProjectActualValue(Ee.EmployeePositionCode, RayonDefault, bodyData, FRCode, FtrQ.filterAnd, FtrQ.filterOr, daysales, 'no');
                actualDDReg = await getActualTarget.getOutletProjectActualValue(Ee.EmployeePositionCode, RayonDefault, bodyData, FRCode, FtrQ.filterAnd, FtrQ.filterOr, daysales, QueryRegPlus);
                targetDD = await getOutletProjectTarget(RCodeDD, bodyData.LineName)
                targetDDReg = targetDD
            }
            break;
        case "2":
            actualDD = await getActualTarget.getSalesActualKuartalValue(Ee.EmployeeAreaCode, bodyData, RCodeDD.areaCodeStr, FtrQ.filterAnd, FtrQ.filterOr, 'no');
            targetDD = await getActualTarget.getSalesTargetKuartalValue(Ee.EmployeeAreaCode, bodyData, RCodeDD.areaCodeStr, FtrQ.filterAnd, FtrQ.filterOr, 'no');
            actualDDReg = await getActualTarget.getSalesActualKuartalValue(Ee.EmployeeAreaCode, bodyData, RCodeDD.areaCodeStr, FtrQ.filterAnd, FtrQ.filterOr, QueryRegPlus);
            targetDDReg = await getActualTarget.getSalesTargetKuartalValue(Ee.EmployeeAreaCode, bodyData, RCodeDD.areaCodeStr, FtrQ.filterAnd, FtrQ.filterOr, QueryRegPlus);
            break;
    }
    let AT = 0;
    let ATReg = 0;
    let ATMax = 0;
    let AProd = 0;
    let maxTarProduct = 0;
    let Diskon = 0;
    let netSales = 0;
    let APNetto = 0;
    let netTarget = await getActualTarget.getSalesTargetNETValue(RayonDefault, bodyData);
    let ATTotalCalculate = (ActualALL / TargetAll) * 100; // at parent
    let ATTotal = (ATTotalCalculate) ? ATTotalCalculate : 0;
    if (actualDD !== 0 && targetDD !== 0) {
        AT = (actualDD / targetDD) * 100;
        ATReg = (actualDDReg / targetDDReg) * 100;
        ATMax = (actualDD / (targetDD >= productivityValue ? targetDD : productivityValue)) * 100;
        maxTarProduct = (targetDD >= productivityValue ? targetDD : productivityValue)
        Diskon = (actualPAPDD / actualDD) * 100;
        //actual net
        netSales = actualDD - actualPAPDD;
        APNetto = (netSales / productivityValue) * 100;
        if (productivityValue !== 0) {
            AProd = (actualDD / productivityValue) * 100;
        } else {
            AProd = 0;
        }
    }
    let getSalesH1 = 0;
    let getSalesH2 = 0;
    if (bodyData.LineName == 'OGB') {
        getSalesH1 = await getActualTarget.getSalesActualMaxSales(RayonDefault, bodyData, 'H1');
        getSalesH2 = await getActualTarget.getSalesActualMaxSales(RayonDefault, bodyData, 'H2');
    }
    let nil_incetive = 0;
    let inccSchema = await getIncentiveCalculate.calculate(actualDD, targetDD, AT, ATReg, ATMax, AProd, ConfigDetailId, ActualALL, TargetAll, ATTotal, APNetto, getSalesH1, getSalesH2)
    switch (SchemaId) {
        case 1:
            nil_incetive = await getIncentiveBonusBulanan.calculateRayon(inccSchema, actualDD, AT, ATReg, ATMax, productivityValue, ATTotal, netSales)
            break;
        case 2:
            nil_incetive = await getIncentiveBonusBulanan.calculateProductFokus(inccSchema, FFRayonCode, bodyData, actualDD, AT, ATReg, ATMax, productivityValue, ATTotal, netSales)
            break;
        case 5:
            nil_incetive = await getIncentiveBonusBulanan.calculateOutletProject(inccSchema, AT, ATReg, ATMax, productivityValue)
            break;
        case 6:
            if (bodyData.LineName == 'OGB') {
                actualDD = netSales
                targetDD = netTarget
            }
            nil_incetive = await getIncentiveBonusBulanan.calculateAddTotal(inccSchema, actualDD, targetDD, productivityValue)
            break;
        case 9:
            nil_incetive = await getIncentiveBonusBulanan.calculateTarikaSalesHarian(inccSchema, actualDD, AT, ATReg, ATMax, productivityValue, bodyData.LineName, Ee.EmployeeAreaCode, ATTotal, netSales)
            break;
        case 11:
            nil_incetive = await getIncentiveBonusBulanan.calculateRerataBasic(inccSchema, dataATMR, FRCode, hasil)
            break;
        case 13:
            nil_incetive = await getIncentiveBonusKuartal.calculateRayonKuartal(inccSchema, actualDD, AT, ATTotal, ATReg, ATMax, productivityValue)
            break;
        case 14:
            nil_incetive = await getIncentiveBonusKuartal.calculateKuartalHattrick(inccSchema, Ee.EmployeeAreaCode, bodyData, RCodeDD.areaCodeStr, FtrQ.filterAnd, FtrQ.filterOr, productivityValue)
            break;
        case 16:
            nil_incetive = await getIncentiveBonusKuartal.calculateAreaKuartal(inccSchema, Ee.EmployeeAreaCode, bodyData, RCodeDD.areaCodeStr, FtrQ.filterAnd, FtrQ.filterOr)
            break;
        case 20:
            nil_incetive = await getIncentiveBonusBulanan.calculateAT_Pimda_RKTK(inccSchema, Ee, RayonDefault, FRCode, AT, ATReg, ATMax, ATTotal, hasil)
            break;
        case 22:
            nil_incetive = await getIncentiveBonusBulanan.calculateAT_DSM_RKTK(inccSchema, Ee, AT, ATReg, ATMax, ATTotal, hasil)
            break;
        case 26:
            nil_incetive = await getIncentiveBonusKuartal.calculateATCabangKuartal(inccSchema, actualDD, targetDD, productivityValue)
            break;
        case 27:
            nil_incetive = await getIncentiveBonusBulanan.calculateAreaBinaan(inccSchema, AT, ATReg, ATMax, productivityValue, ATTotal, netSales)
            break;
        case 33:
            nil_incetive = await getIncentiveBonusBulanan.calculateACTSpecialist(inccSchema, actualDD, AT, ATReg, ATMax, productivityValue, ATTotal, netSales)
            break;
        case 34:
            nil_incetive = await getIncentiveBonusBulanan.calculateRerataPimdaHarmony(inccSchema, dataATMR, FRCode)
            break;
        case 35:
            nil_incetive = await getIncentiveBonusKuartal.calculateKuartalPOTPimdaMR(inccSchema, RCodeDD, ATTotal, hasil)
            break;
        case 36:
            nil_incetive = await getIncentiveBonusBulanan.calculateAddSales(inccSchema, actualDD, targetDD, ActualALL, TargetAll, productivityValue)
            break;
        case 37:
            nil_incetive = await getIncentiveBonusKuartal.calculateKuartalGrowthRegular(inccSchema, Ee.EmployeeAreaCode, bodyData, RCodeDD, actualDDReg, AProd)
            break;
        case 38:
            nil_incetive = await getIncentiveBonusBulanan.calculateClosingSales(inccSchema, Ee.EmployeeAreaCode, bodyData)
            break;
    }
    let howtoget = nil_incetive.calculate
    if (nil_incetive.calculate == null) {
        howtoget = null;
    }
    let otherValue = null
    if (bodyData.LineName == 'OGB') {
        otherValue = {
            actualPAP: convertRupiah.convert(Math.round(actualPAPDD)),
            actualPLP: convertRupiah.convert(Math.round(actualPLPDD)),
            discount: Diskon.toFixed(2) + '%',
            APNetto: APNetto.toFixed(2) + '%',
            netSales: convertRupiah.convert(Math.round(netSales)),
            netTarget: convertRupiah.convert(Math.round(netTarget)),
            salesH1: convertRupiah.convert(Math.round(getSalesH1)),
            salesH2: convertRupiah.convert(Math.round(getSalesH2)),
        }
    }
    let datahasil2 = {
        EmployeeID: Ee.EmployeeID,
        schemaId: SchemaId,
        schemaLabel: SchemaLabel,
        RayonCode: FFRayonCode,
        actual: convertRupiah.convert(Math.round(actualDD)),
        target: convertRupiah.convert(Math.round(targetDD)),
        actualRegular: convertRupiah.convert(Math.round(actualDDReg)),
        targetRegular: convertRupiah.convert(Math.round(targetDDReg)),
        productivity: convertRupiah.convert(Math.round(productivityValue)),
        maxTargetProductivity: convertRupiah.convert(Math.round(maxTarProduct)),
        actualTarget: AT.toFixed(2) + '%',
        actualTargetMax: ATMax.toFixed(2) + '%',
        actualTargetRegular: ATReg.toFixed(2) + '%',
        actualProductivity: AProd.toFixed(2) + '%',
        incentiveRupiah: convertRupiah.convert(Math.round(nil_incetive.incentive)),
        incentive: Math.round(nil_incetive.incentive),
        totalIncentive: Math.round(nil_incetive.incentive),
        howtoget: howtoget,
        other: otherValue,
        // schema: inccSchema
        // filterConfig: FtrQ,
        // okok: FRCode
    }

    return datahasil2
}

const getOutletProjectTarget = async (RCodeDD, LineName) => {
    let QueryDD = ""
    if (RCodeDD.rayonCodeStr !== null) {
        QueryDD = "and (" + RCodeDD.rayonCodeStr + ")";
    }
    let data = await db.query(`select MaxTarget  from m10_target_outlet where LineDesc = '${LineName}' ${QueryDD} limit 1`, {
        type: sequelize.QueryTypes.SELECT
    })
    // //console.log('getOutletProjectTarget data', data);
    if (data.length > 0) {
        data = data[0].MaxTarget
    } else {
        data = 0
    }

    return (data) ? data : 0
}

const getTumIncentive = async (SchemeConfigId, ConfigDetailId, countEmployee, RayonCode) => {
    let data = await db.query(`select tftd.RecordNo,
        concat(tftd.Operator ," ",
        case when tftd.FilterCode  = 'ProductDesc' then 'LineProduct'
        when tftd.FilterCode  = 'ProductGroupDesc' then 'LineProduct' else
        tftd.FilterCode  
        end ) as query1 ,
        concat(
        case when tftd.FilterCode  = 'ProductDesc' then tftd.LineProduct
        when tftd.FilterCode  = 'ProductGroupDesc' then 'LineProduct' else
        tftd.FilterValue  
        end ) as query2 
        from t04_schema_config tsc 
        left join t05_filter_tarikan_data tftd on tftd.ConfigDetailId = tsc.ConfigDetailId 
        where tftd.FlagInclude = '1' and tsc.SchemeConfigId = '${SchemeConfigId}' and tsc.ConfigDetailId = '${ConfigDetailId}' and tftd.FilterCode in ('LineDesc','EmployeePositionCode','EmployeeMRType','EmployeeSubLineDesc','ProductDesc')
        group by tftd.LineProduct,tftd.FilterCode`, {
        type: sequelize.QueryTypes.SELECT
    })

    let loopfilter = 'no';
    for (x = 0; x < data.length; x++) {
        loopfilter += data[x].query1 + " " + "=" + " " + "'" + data[x].query2 + "'" + " ";
    }
    data = (removeFirstWord(loopfilter)) ? removeFirstWord(loopfilter) : 'no';
    let result = await getdataTumInctv(data, countEmployee, RayonCode)
    return result
}

const getdataTumInctv = async (data, countEmployee, RayonCode) => {
    // console.log('countEmployee',countEmployee);
    let queryRayon = '';
    if (RayonCode !== null) {
        queryRayon = `and (RayonCode = '${RayonCode}')`;
    }
    let queryTumFilter = "";
    if (data != 'no') {
        queryTumFilter = "and (" + data + ")";
    }
    let getdata = await db.query(`select *  from m08_tum_incentive where LineDesc is not null ${queryTumFilter} ${queryRayon} limit 1`, {
        type: sequelize.QueryTypes.SELECT
    })
    if (countEmployee != 'no') {
        if (countEmployee.count > 1) {
            getdata = await db.query(`select * from m08_tum_incentive mti where LineDesc is not null ${queryTumFilter} and ValueMr <= ${countEmployee} ${queryRayon} order by ValueMr desc limit 1`, {
                type: sequelize.QueryTypes.SELECT
            })
        }
    }

    let result = 0;
    if (getdata.length > 0) {
        result = getdata[0].TUMValue
    }
    return (result) ? result : 0
}

const getTargetHJSistem = async (ConfigDetailId) => {
    let datatargetHJ = await db.query(`select tftd.FilterValue from t05_filter_tarikan_data tftd where tftd.ConfigDetailId = '${ConfigDetailId}' and tftd.FilterCode = 'TargetHJSistem'`, {
        type: sequelize.QueryTypes.SELECT
    })

    let hsiltarget = 0;
    // console.log(datatargetHJ.length);
    if (datatargetHJ.length > 0) {
        hsiltarget = datatargetHJ[0].FilterValue;
    }
    return (hsiltarget) ? hsiltarget : 0;
}
const findEmployeeFilter2 = async (ConfigDetailId, SchemeConfigId) => {
    let data = await db.query(`select RecordNo,Operator,FilterCode,FlagInclude ,Count(FilterCode) as jml
    from t04_schema_config tsc left join t05_filter_tarikan_data tftd on tftd.ConfigDetailId = tsc.ConfigDetailId 
    where tftd.ConfigDetailId = '${ConfigDetailId}' and tsc.SchemeConfigId = '${SchemeConfigId}' and tftd.FilterCode like 'Employee%'
    group by RecordNo ,Operator ,FlagInclude`, {
        type: sequelize.QueryTypes.SELECT
    })

    let hasilpush = [];
    await Promise.all(
        data.map(async (e) => {
            let datahasil = {
                FilterCode: e.FilterCode,
                Operator: e.Operator,
                FlagInclude: e.FlagInclude,
                count: e.jml,
                getquery: await getQueryFilterCode(ConfigDetailId, e.RecordNo, e.FilterCode, e.Operator, e.FlagInclude),
                targetHJ: await getTargetHJSistem(ConfigDetailId)
            }
            hasilpush.push(datahasil)
            data = getUnionQuery(hasilpush)
            // //console.log('data',data);
        }))
    return data
}


const getQueryFilterCode = async (ConfigDetailId, RecordNo, FilterCode, Operator, FlagInclude) => {
    let data = await db.query(`select RecordNo ,Operator ,FlagInclude ,FilterCode ,LineProduct ,ProductID ,FilterValue  
    from t05_filter_tarikan_data tftd where ConfigDetailId = '${ConfigDetailId}' and RecordNo = '${RecordNo}' 
    and FilterCode = '${FilterCode}' and FlagInclude = ${FlagInclude} and Operator = '${Operator}' and tftd.FilterCode like 'Employee%'`, {
        type: sequelize.QueryTypes.SELECT
    })
    let loopFilter = "";
    if (data.length > 0) {
        for (i = 0; i < data.length; i++) {
            loopFilter += "'" + data[i].FilterValue + "',"
        }
        if (loopFilter !== "") {
            loopFilter = loopFilter.slice(0, -1);
            if (data.length > 1) {
                if (FlagInclude == 1) {
                    data = Operator + " " + FilterCode + " " + "in" + " " + "(" + loopFilter + ")";
                } else {
                    data = Operator + " " + FilterCode + " " + "not in" + " " + "(" + loopFilter + ")";
                }
            } else {
                if (FlagInclude == 1) {
                    data = Operator + " " + FilterCode + " " + "=" + " " + loopFilter
                } else {
                    data = Operator + " " + FilterCode + " " + "!=" + " " + loopFilter
                }
            }
        }
    }


    return data
}

function getUnionQuery(ftr) {
    let dataAnd = '';
    let dataOr = '';
    let countx = 0;
    let targetHJ = 0;
    for (i = 0; i < ftr.length; i++) {
        countx = ftr[i].count
        targetHJ = ftr[i].targetHJ
        switch (ftr[i].Operator) {
            case 'AND':
                dataAnd += ftr[i].getquery + " "
                break;
            case 'OR':
                dataOr += ftr[i].getquery + " "
                break;
        }
    }

    let data = {
        filterAnd: (removeFirstWord(dataAnd)) ? removeFirstWord(dataAnd) : 'no',
        filterOr: (removeFirstWord(dataOr)) ? removeFirstWord(dataOr) : 'no',
        count: (countx) ? countx : 0,
        targetHJ: (targetHJ) ? targetHJ : 0
    }

    return data
}

const getQueryFilterCodePure = async (ConfigDetailId, RecordNo, FilterCode, Operator, FlagInclude) => {
    let data = await db.query(`select RecordNo ,Operator ,FlagInclude ,FilterCode ,LineProduct ,ProductID ,FilterValue  
    from t05_filter_tarikan_data tftd where ConfigDetailId = '${ConfigDetailId}' and RecordNo = '${RecordNo}' 
    and FilterCode = '${FilterCode}' and FlagInclude = ${FlagInclude} and Operator = '${Operator}' 
    and tftd.FilterCode not like 'Employee%' and tftd.FilterCode != 'LineDesc' and tftd.FilterCode !='TargetHJSistem'`, {
        type: sequelize.QueryTypes.SELECT
    })
    let loopFilter = "";
    if (data.length > 0) {
        for (i = 0; i < data.length; i++) {
            if (data[i].ProductID !== null && data[i].FilterCode == 'ProductDesc') {
                FilterCode = "ProductID";
                loopFilter += "'" + data[i].ProductID + "',"
            } else {
                loopFilter += "'" + data[i].FilterValue + "',"
            }
        }
        if (loopFilter !== "") {
            loopFilter = loopFilter.slice(0, -1);
            if (data.length > 1) {
                if (FlagInclude == 1) {
                    data = Operator + " " + FilterCode + " " + "in" + " " + "(" + loopFilter + ")";
                } else {
                    data = Operator + " " + FilterCode + " " + "not in" + " " + "(" + loopFilter + ")";
                }
            } else {
                if (FlagInclude == 1) {
                    data = Operator + " " + FilterCode + " " + "=" + " " + loopFilter
                } else {
                    data = Operator + " " + FilterCode + " " + "!=" + " " + loopFilter
                }
            }
        }
    }


    return data
}

const findFilter = async (SchemeConfigId, ConfigDetailId) => {
    let data = await db.query(`select RecordNo,Operator,FilterCode,FlagInclude ,Count(FilterCode) as jml
        from t04_schema_config tsc left join t05_filter_tarikan_data tftd on tftd.ConfigDetailId = tsc.ConfigDetailId 
        where tftd.ConfigDetailId = '${ConfigDetailId}' and tsc.SchemeConfigId = '${SchemeConfigId}' and tftd.FilterCode not like 'Employee%' 
        and tftd.FilterCode != 'LineDesc' and tftd.FilterCode != 'TargetHJSistem'
        group by RecordNo ,Operator ,FlagInclude`, {
        type: sequelize.QueryTypes.SELECT
    })
    let hasilpush = [];
    await Promise.all(
        data.map(async (e) => {
            let datahasil = {
                FilterCode: e.FilterCode,
                Operator: e.Operator,
                FlagInclude: e.FlagInclude,
                count: e.jml,
                getquery: await getQueryFilterCodePure(ConfigDetailId, e.RecordNo, e.FilterCode, e.Operator, e.FlagInclude)
            }
            hasilpush.push(datahasil)
            data = getUnionQueryPure(hasilpush)
        }))

    return (data) ? data : 'no'
}



function getUnionQueryPure(ftr) {
    // console.log('getUnionQueryPure',ftr);
    let dataAnd = '';
    let dataOr = '';
    let countx = 0;
    for (i = 0; i < ftr.length; i++) {
        countx = ftr[i].count
        switch (ftr[i].Operator) {
            case 'AND':
                dataAnd += ftr[i].getquery + " "
                break;
            case 'OR':
                dataOr += ftr[i].getquery + " "
                break;
        }
    }
    let hasilAnd = removeFirstWord(dataAnd);
    let hasilOr = removeFirstWord(dataOr)
    let data = {
        filterAnd: (hasilAnd) ? hasilAnd : 'no',
        filterOr: (hasilOr) ? hasilOr : 'no',
        count: (countx) ? countx : 0
    }

    return data
}

function removeFirstWord(str) {
    const indexOfSpace = str.indexOf(' ');
    return str.substring(indexOfSpace + 1);
}

module.exports = {
    calculate,
    getFilterandCalculate
}
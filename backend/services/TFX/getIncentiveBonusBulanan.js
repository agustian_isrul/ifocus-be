const sequelize = require("sequelize");
const db = require("../../../config/databaseSequalize");
const getIncentiveRules = require("./getIncentiveRules");

async function calculateRerataBasic(dataSchema, dataATMR, FRCode, jsonLoad) {
    let schemaSORT = dataSchema.sort(function (a, b) {
        a.RecordNo - b.RecordNo;
    });

    let schema = schemaSORT
    let filterMaxIncentive = schema.filter(ee => ee.TargetType2 === 'max_incentive');
    // console.log('filterMaxIncentive', filterMaxIncentive[0].Bonus);
    let mTotalIncentive = 0
    const tempHasilList = jsonLoad.filter(data => data.RayonCodeList.some(subData => FRCode.rayonCode.includes(subData.RayonCode)))
    // console.log('tempHasilList',tempHasilList);
    tempHasilList.forEach(element => {
        mTotalIncentive = mTotalIncentive + element.schemaList.reduce((result, data) => {
            const nilaiFinal = data.totalIncentive
            if (nilaiFinal > filterMaxIncentive[0].Bonus) {
                nilaiFinal = filterMaxIncentive[0].Bonus
            }
            return result + nilaiFinal
        }, 0)
    })

    let trueXrecord = getIncentiveRules.countTrueAndRecord(schema)
    let passOne = 0;
    if (trueXrecord == true) {
        for (i = 0; i < schema.length; i++) {
            if (schema[i].Status_Rumus == false && schema[i].RecordNo2 !== null) {
                if (schema[i].TargetType2 == "max_incentive") {
                    if (FRCode.count > 0) {
                        passOne = mTotalIncentive / FRCode.count
                        console.log('passOne', passOne);
                    }
                }
            }
        }
    }
    let hasil = {
        incentive: (passOne) ? passOne : 0,
        calculate: "Total Inc : " + mTotalIncentive + " MR / Pimda : " + FRCode.count
    }

    return hasil
}


// BULANAN LINE
async function calculateRerataPimdaHarmony(dataSchema, dataATMR, FRCode) {
    let countSup = 0
    if (FRCode != 'no') {
        countSup = FRCode.count
    }
    let calculate = [];
    dataATMR.map(e => {
        let incentiveDataAT = getIncentiveRules.getDataATRenata(dataSchema, e.data, countSup)
        let hasil = {
            rayonCodeSUP: e.rayonCodeSUP,
            data: e.data,
            incentive: incentiveDataAT
        }
        calculate.push(hasil)
    })
    // //console.log('Rerata calculate', calculate);
    let total = 0;
    if (calculate.length > 0 || calculate.length !== undefined) {
        total = calculate.map(e => e.incentive).reduce((acc, amount) => acc + amount);
        //console.log('total Rerata', total, '/', countSup, calculate);
        total = Math.ceil((total / countSup) * 1.2);
        //console.log('total Hasil', total);
    }

    let hasil = {
        incentive: (total) ? total : 0,
        calculate: calculate
    }

    return hasil

}

async function calculateClosingSales(dataSchema, EmployeeAreaCode, bodyData) {
    let passOne = 0;
    let truefalse = null;
    let schemaSORT = dataSchema.sort(function (a, b) {
        a.RecordNo - b.RecordNo;
    });

    let schema = schemaSORT
    for (i = 0; i < schema.length; i++) {
        if (schema[i].Status_Rumus == true) {
            if (schema[i].TargetType == 'actual_target_H2') {
                passOne = (Math.ceil(schema[i].Bonus)) ? Math.ceil(schema[i].Bonus) : 0
            } else if (schema[i].TargetType == 'actual_target_H1') {
                passOne = (Math.ceil(schema[i].Bonus)) ? Math.ceil(schema[i].Bonus) : 0
            } else {
                passOne = 0;
            }
        }
    }
    let hasil = {
        incentive: (passOne) ? passOne : 0,
        calculate: truefalse
    }

    return hasil
}


function getTotalincentive(data) {
    let total = '';
    if (data.length > 0 || data.length !== undefined) {
        total = data.map(e => e.incentive).reduce((acc, amount) => acc + amount);
    } else {
        total = 0
    }
    if (total == 'undefined' || total == 'NAN' || total == null || total == undefined) {
        total = 0
    }
    return total
}

function getChildIncentiveTotal(dataParent, RayonCode) {
    let hasilpush = [];
    dataParent.map(e => {
        let totalInc = getTotalincentive(e.schemaList)
        let datahasil = {
            RayonCodeSup: e.RayonCodeSup, // MR
            EmployeeName: e.EmployeeName,
            incentive: totalInc,
        }
        hasilpush.push(datahasil)
    })
    return hasilpush.filter(e => e.RayonCodeSup.includes(RayonCode))
}

function getChildIncentiveTotalSM(dataParent, RayonCodeSM) {
    let hasilpush = [];
    dataParent.map(e => {
        let totalInc = getTotalincentive(e.schemaList)
        let datahasil = {
            RayonCode: e.RayonCode, // SUP + MR
            EmployeeName: e.EmployeeName,
            incentive: totalInc,
        }
        hasilpush.push(datahasil)
    })
    for (let z = 0; z < hasilpush.length; z++) {
        if (hasilpush[z].RayonCode === RayonCodeSM) {
            console.log('hasilpush[z]', RayonCodeSM, hasilpush[z])
        }
    }

    return hasilpush.filter(e => e.RayonCode.includes(RayonCodeSM))
}


const getSupMrData = async (EmployeeID) => {
    let filterSupMR = '';
    let data = null;
    let final = '';
    if (EmployeeID !== null) {
        filterSupMR = "and ( EmployeeSupID =" + "'" + EmployeeID + "'" + ")";

        data = await db.query(`select RayonCode,EmployeePositionCode  from s03_structure_employee where EmployeePositionCode in ('SUP','KOORD','MR') ${filterSupMR}`, {
            type: sequelize.QueryTypes.SELECT
        })
        final = {
            count: (data.length) ? data.length : 0,
            dataMr: data.filter(e => e.EmployeePositionCode.includes('MR')),
            dataSup: data.filter(e => !e.EmployeePositionCode.includes('MR')),
        }
    } else {
        final = {
            dataMr: null,
            dataSup: null,
        }
    }
    return final
}

function IncentiveLoopTotal(hasil, data) {
    let hasilpush = [];
    let hasilX = 0;
    data.map(e => {
        let totalInc = getChildIncentiveTotalSM(hasil, e.RayonCode)
        console.log('totalInc', e.RayonCode, totalInc);
        hasilpush.push(totalInc)
    })
    hasilpush = hasilpush.flat(1);
    if (hasilpush.length > 0) {
        hasilX = getTotalincentive(hasilpush)
    }

    return hasilX
}
async function calculateAT_DSM_RKTK(dataSchema, Ee, AT, ATReg, ATMax, ATTotal, hasilparent) {
    let SUPMRdata = null;
    let RayonCode = Ee.RayonCode
    let EmployeeName = Ee.EmployeeName
    if (Ee.EmployeeID !== null) {
        SUPMRdata = await getSupMrData(Ee.EmployeeID)
        // console.log('getSupMrData', SUPMRdata);
    }
    let hasilIncMR = 0;
    let dataAllMR = SUPMRdata.dataMr;
    let hasilParentMR = hasilparent.filter(n => n.EmployeePositionCode.includes('MR'));
    if (dataAllMR.length > 0) {
        hasilIncMR = IncentiveLoopTotal(hasilParentMR, dataAllMR)
    }

    let hasilIncSUP = 0;
    let dataAllSUP = SUPMRdata.dataSup;
    let hasilParentSUP = hasilparent.filter(n => !n.EmployeePositionCode.includes('MR'));
    if (dataAllSUP.length > 0) {
        hasilIncSUP = IncentiveLoopTotal(hasilParentSUP, dataAllSUP)
    }

    console.log(RayonCode, EmployeeName, 'hasilIncMR', hasilIncMR, 'hasilIncSUP', hasilIncSUP);
    let goal = SUPMRdata.count;
    let passOne = 0;
    let passTwo = 0;
    let schemaSORT = dataSchema.sort(function (a, b) {
        a.RecordNo - b.RecordNo;
    });

    let schema = schemaSORT

    let trueXrecord = getIncentiveRules.countTrueAndRecord(schema)
    // console.log('masuk test DSM');
    if (trueXrecord == true) {
        // console.log('masuk DSM');
        for (i = 0; i < schema.length; i++) {
            if (schema[i].Status_Rumus == false && schema[i].RecordNo2 !== null) {
                if (schema[i].TargetType == 'RKTK') {
                    if (schema[i].Operator2 == 'between') {
                        if (goal >= schema[i].ValueFrom && goal <= schema[i].ValueTo) {
                            passOne = true
                        }
                    } else {
                        switch (schema[i].Operator2) {
                            case "=":
                                if (goal == schema[i].ValueFrom) {
                                    passOne = true
                                }
                                break;
                            case ">=":
                                if (goal >= schema[i].ValueFrom) {
                                    passOne = true
                                }
                                break;
                            case "<=":
                                if (goal <= schema[i].ValueFrom) {
                                    passOne = true
                                }
                                break;
                            case ">":
                                if (goal > schema[i].ValueFrom) {
                                    passOne = true
                                }
                                break;
                            case "<":
                                if (goal < schema[i].ValueFrom) {
                                    passOne = true
                                }
                                break;
                            default:
                                passOne = false
                        }
                    }
                    if (passOne === true) {
                        // console.log('masuk DSM 2');
                        if (schema[i].TargetType2 !== null && schema[i].RecordNo2 !== null) {
                            switch (schema[i].TargetType2) {
                                case "actual_target":
                                    goal = AT;
                                    break;
                                case "actual_max":
                                    goal = ATMax;
                                    break;
                                case "actual_regular":
                                    goal = ATReg;
                                    break;
                                case "actual_target_all":
                                    goal = ATTotal;
                                    break;
                            }
                            if (schema[i].Operator22 == 'between') {
                                if (goal >= schema[i].ValueFrom2 && goal <= schema[i].ValueTo2) {
                                    passTwo = schema[i].Bonus
                                }
                            } else {
                                switch (schema[i].Operator22) {
                                    case ">":
                                        if (goal > schema[i].ValueFrom2) {
                                            passTwo = schema[i].Bonus
                                        }
                                        break;
                                    case "<":
                                        if (goal < schema[i].ValueFrom2) {
                                            passTwo = schema[i].Bonus
                                        }
                                        break;
                                    case "<=":
                                        if (goal <= schema[i].ValueFrom2) {
                                            passTwo = schema[i].Bonus
                                        }
                                        break;
                                    case ">=":
                                        if (goal >= schema[i].ValueFrom2) {
                                            passTwo = schema[i].Bonus
                                        }
                                        break;
                                    case "=":
                                        if (goal == schema[i].ValueFrom2) {
                                            passTwo = schema[i].Bonus
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    let hasilIncentiveIndex = ((hasilIncMR + hasilIncSUP) * passTwo);
    let hasil = {
        incentive: hasilIncentiveIndex,
        calculate: "Incentive Sub:" + hasilIncSUP + " MR Incentive :" + hasilIncMR + " & index :" + passTwo
    }
    return hasil
}
async function calculateAT_Pimda_RKTK(dataSchema, Ee, RayonDefault, FRCode, AT, ATReg, ATMax, ATTotal, hasilparent) {
    let RayonCode = (RayonDefault) ? RayonDefault : Ee.RayonCode;
    let hasilparentInc = getChildIncentiveTotal(hasilparent, RayonCode)
    let getTotalInc = 0;
    if (hasilparentInc.length > 0) {
        getTotalInc = getTotalincentive(hasilparentInc)
    }
    // console.log('hasilparentInc', RayonCode, ' and :', getTotalInc);
    let countSup = 0;
    if (FRCode != 'no') {
        countSup = FRCode.count
    }
    let goal = countSup;
    let passOne = 0;
    let passTwo = 0;
    let schemaSORT = dataSchema.sort(function (a, b) {
        a.RecordNo - b.RecordNo;
    });

    let schema = schemaSORT

    let trueXrecord = getIncentiveRules.countTrueAndRecord(schema)
    if (trueXrecord == true) {
        for (i = 0; i < schema.length; i++) {
            if (schema[i].Status_Rumus == false && schema[i].RecordNo2 !== null) {
                if (schema[i].TargetType == 'RKTK') {
                    if (schema[i].Operator2 == 'between') {
                        if (goal >= schema[i].ValueFrom && goal <= schema[i].ValueTo) {
                            passOne = true
                        }
                    } else {
                        switch (schema[i].Operator2) {
                            case "=":
                                if (goal == schema[i].ValueFrom) {
                                    passOne = true
                                }
                                break;
                            case ">=":
                                if (goal >= schema[i].ValueFrom) {
                                    passOne = true
                                }
                                break;
                            case "<=":
                                if (goal <= schema[i].ValueFrom) {
                                    passOne = true
                                }
                                break;
                            case ">":
                                if (goal > schema[i].ValueFrom) {
                                    passOne = true
                                }
                                break;
                            case "<":
                                if (goal < schema[i].ValueFrom) {
                                    passOne = true
                                }
                                break;
                            default:
                                passOne = false
                        }
                    }
                    if (passOne === true) {
                        if (schema[i].TargetType2 !== null && schema[i].RecordNo2 !== null) {
                            switch (schema[i].TargetType2) {
                                case "actual_target":
                                    goal = AT;
                                    break;
                                case "actual_max":
                                    goal = ATMax;
                                    break;
                                case "actual_regular":
                                    goal = ATReg;
                                    break;
                                case "actual_target_all":
                                    goal = ATTotal;
                                    break;
                            }
                            if (schema[i].Operator22 == 'between') {
                                if (goal >= schema[i].ValueFrom2 && goal <= schema[i].ValueTo2) {
                                    passTwo = schema[i].Bonus
                                }
                            } else {
                                switch (schema[i].Operator22) {
                                    case ">":
                                        if (goal > schema[i].ValueFrom2) {
                                            passTwo = schema[i].Bonus
                                        }
                                        break;
                                    case "<":
                                        if (goal < schema[i].ValueFrom2) {
                                            passTwo = schema[i].Bonus
                                        }
                                        break;
                                    case "<=":
                                        if (goal <= schema[i].ValueFrom2) {
                                            passTwo = schema[i].Bonus
                                        }
                                        break;
                                    case ">=":
                                        if (goal >= schema[i].ValueFrom2) {
                                            passTwo = schema[i].Bonus
                                        }
                                        break;
                                    case "=":
                                        if (goal == schema[i].ValueFrom2) {
                                            passTwo = schema[i].Bonus
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    let hasilIncentiveIndex = (getTotalInc * passTwo);
    let hasil = {
        incentive: hasilIncentiveIndex,
        calculate: "incentive :" + getTotalInc + " & index :" + passTwo + " & MR Count : " + countSup
    }
    return hasil
}

async function calculateAreaBinaan(dataSchema, actual, AT, ATReg, ATMax, productivityValue, ATTotal, netSales) {
    let passOne = 0;
    let truefalse = null;
    let schemaSORT = dataSchema.sort(function (a, b) {
        a.RecordNo - b.RecordNo;
    });

    let schema = schemaSORT
    for (i = 0; i < schema.length; i++) {
        if (schema[i].Status_Rumus == true && schema[i].TargetType2 !== null && schema[i].ValueFrom2 !== null) {
            // get true or false schemas second step
            truefalse = getIncentiveRules.getTrueOrFalse(schema[i].TargetType2, schema[i].Operator22, schema[i].ValueFrom2, schema[i].ValueTo2, schema[i].Bonus, actual, AT, ATReg, ATMax, productivityValue, ATTotal, netSales)
            //console.log('calculateAreaBinaan', truefalse);
            if (truefalse.get_status == true) {
                if (schema[i].ValueTo2) {
                    passOne = (schema[i].ValueTo2 / 100) * schema[i].Bonus;
                } else {
                    passOne = (Math.ceil(schema[i].Bonus)) ? Math.ceil(schema[i].Bonus) : 0
                }
            }
        }
    }
    let hasil = {
        incentive: (passOne) ? passOne : 0,
        calculate: truefalse
    }

    return hasil
}

async function calculateOutletProject(dataSchema, AT, ATReg, ATMax, productivityValue) {
    // //console.log('dataSchema',AT,dataSchema);
    let goal = 0;
    let passOne = null;
    let schemaSORT = dataSchema.sort(function (a, b) {
        a.RecordNo - b.RecordNo;
    });

    let schema = schemaSORT

    let trueXrecord = getIncentiveRules.countTrueAndRecord(schema)
    if (trueXrecord == true) {
        // //console.log('true');
        for (i = 0; i < schema.length; i++) {
            if (schema[i].Status_Rumus == false && schema[i].RecordNo2 !== null) {
                if (schema[i].Operator1 == 'AND' && schema[i].TargetType == null && schema[i].Operator2 == null) {
                    if (schema[i].ValueFrom2 !== null && schema[i].TargetType2 !== null && schema[i].Operator22 !== null && schema[i].ValueFrom2 !== null) {
                        switch (schema[i].TargetType2) {
                            case "actual_target":
                                goal = AT;
                                break;
                            case "actual_max":
                                goal = ATMax;
                                break;
                            case "actual_regular":
                                goal = ATReg;
                                break;
                        }
                        if (schema[i].Operator22 !== null && schema[i].Operator12 == 'AND') {
                            switch (schema[i].Operator22) {
                                case "between":
                                    if (goal >= schema[i].ValueFrom2 && goal < schema[i].ValueTo2) {
                                        passOne = (Math.ceil(schema[i].Bonus)) ? Math.ceil(schema[i].Bonus) : 0
                                    }
                                    break;
                                case "=":
                                    if (goal == schema[i].ValueFrom2) {
                                        if (schema[i].ValueTo2) {
                                            passOne = (schema[i].ValueTo2 / 100) * schema[i].Bonus;
                                        } else {
                                            passOne = (Math.ceil(schema[i].Bonus)) ? Math.ceil(schema[i].Bonus) : 0
                                        }
                                    }
                                    break;
                                case ">=":
                                    if (goal >= schema[i].ValueFrom2) {
                                        if (schema[i].ValueTo2) {
                                            passOne = (schema[i].ValueTo2 / 100) * schema[i].Bonus;
                                        } else {
                                            passOne = (Math.ceil(schema[i].Bonus)) ? Math.ceil(schema[i].Bonus) : 0
                                        }
                                    }
                                    break;
                                case "<=":
                                    if (goal <= schema[i].ValueFrom2) {
                                        if (schema[i].ValueTo2) {
                                            passOne = (schema[i].ValueTo2 / 100) * schema[i].Bonus;
                                        } else {
                                            passOne = (Math.ceil(schema[i].Bonus)) ? Math.ceil(schema[i].Bonus) : 0
                                        }
                                    }
                                    break;
                                case ">":
                                    if (goal > schema[i].ValueFrom2) {
                                        if (schema[i].ValueTo2) {
                                            passOne = (schema[i].ValueTo2 / 100) * schema[i].Bonus;
                                        } else {
                                            passOne = (Math.ceil(schema[i].Bonus)) ? Math.ceil(schema[i].Bonus) : 0
                                        }
                                    }
                                    break;
                                case "<":
                                    if (goal < schema[i].ValueFrom2) {
                                        if (schema[i].ValueTo2) {
                                            passOne = (schema[i].ValueTo2 / 100) * schema[i].Bonus;
                                        } else {
                                            passOne = (Math.ceil(schema[i].Bonus)) ? Math.ceil(schema[i].Bonus) : 0
                                        }
                                    }
                                    break;
                                default:
                                    passOne = 0
                            }
                        }

                    } else {
                        passOne = Math.ceil(schema[i].Bonus)
                    }
                }
            }
        }
    }

    let hasil = {
        incentive: (passOne) ? passOne : 0,
        calculate: trueXrecord
    }

    return hasil
}

// async function calculateATSales(dataSchema, actual, AT, ATReg, ATMax, productivityValue, ATTotal, netSales) {
//     let passOne = 0;
//     let schemaSORT = dataSchema.sort(function (a, b) {
//         a.RecordNo - b.RecordNo;
//     });

//     let schema = schemaSORT
//     let truefalse = null;

//     let trueXrecord = getIncentiveRules.countTrueAndRecord(schema)
//     if (trueXrecord == true) {
//         ////console.log('true');
//         for (i = 0; i < schema.length; i++) {
//             if (schema[i].Status_Rumus == false && schema[i].RecordNo2 !== null) {
//                 if (schema[i].Operator1 == 'AND') {
//                     truefalse = getIncentiveRules.getTrueOrFalse(schema[i].TargetType2, schema[i].Operator22, schema[i].ValueFrom2, schema[i].ValueTo2, schema[i].Bonus, actual, AT, ATReg, ATMax, productivityValue, ATTotal, netSales)
//                     if (truefalse.get_status == true) {
//                         passOne = Math.ceil(schema[i].Bonus)
//                     }
//                 }
//             }
//         }
//     }

//     let hasil = {
//         incentive: (passOne) ? passOne : 0,
//         calculate: trueXrecord
//     }

//     return hasil
// }
async function calculateRayon(dataSchema, actual, AT, ATReg, ATMax, productivityValue, ATTotal, netSales) {
    // console.log(dataSchema, actual, AT, ATReg, ATMax, productivityValue, ATTotal, netSales);
    let goal = AT;
    let passOne = 0;
    let truefalse = null;
    let schemaSORT = dataSchema.sort(function (a, b) {
        a.RecordNo - b.RecordNo;
    });
    let schema = schemaSORT
    let trueXrecord = getIncentiveRules.countTrueAndRecord(schema)
    if (trueXrecord == true) {
        ////console.log('true');
        for (i = 0; i < schema.length; i++) {
            if (schema[i].Status_Rumus == false && schema[i].RecordNo2 !== null) {
                if (schema[i].Operator1 == 'AND') {
                    truefalse = getIncentiveRules.getTrueOrFalse(schema[i].TargetType2, schema[i].Operator22, schema[i].ValueFrom2, schema[i].ValueTo2, schema[i].Bonus, actual, AT, ATReg, ATMax, productivityValue, ATTotal, netSales)
                    if (truefalse.get_status == true) {
                        passOne = Math.ceil(schema[i].Bonus)
                    }
                }
            }
        }
    } else {
        for (i = 0; i < schema.length; i++) {
            if (schema[i].Status_Rumus == true && schema[i].TargetType2 == null) {
                let listgoal = [];
                listgoal.push(schema[i].ValueFrom)
                // console.log('listgoal', listgoal)
                var pass = listgoal.reduce((prev, curr) => {
                    return (Math.abs(curr - goal) < Math.abs(prev - goal) ? curr : prev);
                });
                if (pass == schema[i].ValueFrom) {
                    passOne = Math.ceil(schema[i].Bonus)
                } else {
                    passOne = 0
                }
            } else if (schema[i].Status_Rumus == true && schema[i].TargetType2 !== null) {
                truefalse = getIncentiveRules.getTrueOrFalse(schema[i].TargetType2, schema[i].Operator22, schema[i].ValueFrom2, schema[i].ValueTo2, schema[i].Bonus, actual, AT, ATReg, ATMax, productivityValue, ATTotal, netSales)
                if (truefalse.get_status == true) {
                    passOne = Math.ceil(schema[i].Bonus)
                }

            }
        }
    }

    let hasil = {
        incentive: (passOne) ? passOne : 0,
        calculate: truefalse
    }

    return hasil
}
async function calculateAddSales(dataSchema, actual, target) {
    let passOne = 0;
    let schemaSORT = dataSchema.sort(function (a, b) {
        a.RecordNo - b.RecordNo;
    });

    let schema = schemaSORT

    let trueXrecord = getIncentiveRules.countTrueAndRecord(schema)
    if (trueXrecord == true) {
        ////console.log('true');
        for (i = 0; i < schema.length; i++) {
            if (schema[i].Status_Rumus == false && schema[i].RecordNo2 !== null) {
                if (schema[i].Operator1 == 'AND') {
                    if (schema[i].ValueFrom2 !== null) {
                        passOne = (schema[i].ValueFrom2 / 100) * (actual - target);
                        if (passOne >= schema[i].Bonus) {
                            passOne = Math.ceil(schema[i].Bonus)
                        } else {
                            if (passOne <= 0) {
                                passOne = 0;
                            } else {
                                passOne = Math.ceil(passOne)
                            }
                        }
                    } else {
                        passOne = Math.ceil(schema[i].Bonus)
                    }
                }
            }
        }
    }

    let hasil = {
        incentive: (passOne) ? passOne : 0,
        calculate: trueXrecord
    }

    return hasil
}
async function calculateAddTotal(dataSchema, actualDD, targetDD, productivityValue) {
    // rumus ex: 2.0% X ( ACT - (TGT atau PRODUKTIVITAS )) Max 2,5jt
    // console.log('has', dataSchema, actualDD, targetDD, productivityValue);
    let passOne = 0;
    let schemaSORT = dataSchema.sort(function (a, b) {
        a.RecordNo - b.RecordNo;
    });

    let schema = schemaSORT

    let tgtprod = 0;
    let trueXrecord = getIncentiveRules.countTrueAndRecord(schema)
    let nilX = 0;
    if (trueXrecord == true) {
        ////console.log('true');
        for (i = 0; i < schema.length; i++) {
            if (schema[i].Status_Rumus == false && schema[i].RecordNo2 !== null) {
                if (schema[i].Operator1 == 'AND') {
                    if (schema[i].ValueFrom2 !== null) {
                        if (targetDD > productivityValue || targetDD == productivityValue) {
                            tgtprod = targetDD
                        } else {
                            tgtprod = productivityValue
                        }
                        nilX = schema[i].ValueFrom2;
                        passOne = (nilX / 100) * (actualDD - (tgtprod));
                        if (passOne >= schema[i].Bonus) {
                            passOne = Math.ceil(schema[i].Bonus)
                        } else {
                            if (passOne <= 0) {
                                passOne = 0;
                            } else {
                                passOne = Math.ceil(passOne)
                            }
                        }
                    } else {
                        passOne = Math.ceil(schema[i].Bonus)
                    }
                }
            }
        }
    }

    let hasil = {
        incentive: (passOne) ? passOne : 0,
        calculate: nilX + '/ 100 x ' + actualDD + ' - ' + tgtprod,
    }

    return hasil
}

async function calculateProductFokus(dataSchema, FFRayonCode, bodyData, actual, AT, ATReg, ATMax, productivityValue, ATTotal, netSales) {
    // console.log('dataSchema',dataSchema);
    let passOne = 0;
    let schemaSORT = dataSchema.sort(function (a, b) {
        a.RecordNo - b.RecordNo;
    });

    let schema = schemaSORT
    let trueXrecord = getIncentiveRules.countTrueAndRecord(schema)
    let truefalse = null;
    if (trueXrecord == true) {
        console.log('true');
        for (i = 0; i < schema.length; i++) {
            if (schema[i].Status_Rumus == false && schema[i].RecordNo2 !== null) {
                if (schema[i].Operator1 == 'AND' && schema[i].TargetType == null) {
                    if (schema[i].TargetType2 == null && schema[i].OperatorType2 == null) {
                        passOne = Math.ceil(schema[i].Bonus)

                    } else {
                        truefalse = getIncentiveRules.getTrueOrFalse(schema[i].TargetType2, schema[i].Operator22, schema[i].ValueFrom2, schema[i].ValueTo2, schema[i].Bonus, actual, AT, ATReg, ATMax, productivityValue, ATTotal, netSales)
                        if (truefalse.get_status == true) {
                            passOne = Math.ceil(schema[i].Bonus)
                        }
                    }
                } else {
                    if (schema[i].TargetType == 'category_product') {
                        schema = schema.filter(e => e.TargetType.includes('category_product'))
                        schema = await getIncentiveRules.IncentiveTUMCatProduk(schema, FFRayonCode, bodyData)
                        truefalse = schema.filter(e => e.Status === true)
                        if (truefalse.length > 0 || truefalse.length !== undefined) {
                            passOne = getTotalincentive(truefalse)
                        }
                    }
                }
            }
        }
    }

    let hasil = {
        incentive: (passOne) ? passOne : 0,
        calculate: truefalse
    }

    return hasil
}
async function calculateACTSpecialist(dataSchema, actual, AT, ATReg, ATMax, productivityValue, ATTotal, netSales) {
    let passOne = 0;
    let schemaSORT = dataSchema.sort(function (a, b) {
        a.RecordNo - b.RecordNo;
    });
    let schema = schemaSORT
    let trueXrecord = getIncentiveRules.countTrueAndRecord(schema)
    if (trueXrecord == true) {
        for (i = 0; i < schema.length; i++) {
            if (schema[i].Status_Rumus == false && schema[i].RecordNo2 !== null) {
                if (schema[i].Operator1 == 'AND') {
                    if (schema[i].ValueFrom2 !== null && schema[i].TargetType2 !== null) {
                        // get true or false schemas second step
                        let truefalse = getIncentiveRules.getTrueOrFalse(schema[i].TargetType2, schema[i].Operator22, schema[i].ValueFrom2, schema[i].ValueTo2, schema[i].Bonus, actual, AT, ATReg, ATMax, productivityValue, ATTotal, netSales)
                        //console.log('calculateACTSpecialist', truefalse);
                        if (truefalse.get_status == true) {
                            passOne = Math.ceil(schema[i].Bonus)
                        }
                    }
                }
            }
        }
    }

    let hasil = {
        incentive: (passOne) ? passOne : 0,
        calculate: trueXrecord
    }

    return hasil
}

async function calculateTarikaSalesHarian(dataSchema, actual, AT, ATReg, ATMax, productivityValue, lineName, EmployeeAreaCode, ATTotal, netSales) {
    let areaCabang = EmployeeAreaCode.replace(/-/g, "");
    let passOne = 0;
    let schemaSORT = dataSchema.sort(function (a, b) {
        a.RecordNo - b.RecordNo;
    });
    let schema = schemaSORT
    let trueXrecord = getIncentiveRules.countTrueAndRecord(schema)
    // this way how to get data count status trus with count password
    let truefalse = null;
    if (trueXrecord == true) {
        for (i = 0; i < schema.length; i++) {
            if (schema[i].Status_Rumus == false && schema[i].RecordNo2 !== null) {
                if (schema[i].Operator1 == 'AND') {
                    if (schema[i].ValueFrom2 !== null && schema[i].TargetType2 !== null) {
                        // get true or false schemas second step
                        truefalse = getIncentiveRules.getTrueOrFalse(schema[i].TargetType2, schema[i].Operator22, schema[i].ValueFrom2, schema[i].ValueTo2, schema[i].Bonus, actual, AT, ATReg, ATMax, productivityValue, ATTotal, netSales)
                        //console.log('getIncentiveRules.getTrueOrFalse', truefalse);
                        if (truefalse.get_status == true) {
                            passOne = await getDataKriteriaCabang(lineName, areaCabang)
                        }
                    }
                }
            }
        }
    }

    let hasil = {
        incentive: (passOne) ? passOne : 0,
        calculate: truefalse
    }

    return hasil
}

const getDataKriteriaCabang = async (Linename, areaCabang) => {
    let data = await db.query(`select Insentif from m06_kriteria_cabang where LineDesc = '${Linename}' and Cabang = '${areaCabang}' limit 1`, {
        type: sequelize.QueryTypes.SELECT
    })
    if (data.length > 0) {
        // //console.log('data', data);
        return data[0].Insentif
    } else {
        return 0
    }
}


async function calculateProductFokusDSM(dataSchema, actual, AT, ATReg, ATMax, productivityValue) {
    let passOne = 0;
    return (passOne) ? passOne : 0
}


module.exports = {
    calculateAddTotal,
    calculateRayon,
    calculateProductFokus,
    calculateACTSpecialist,
    calculateAreaBinaan,
    calculateAddSales,
    calculateRerataPimdaHarmony,
    calculateOutletProject,
    calculateTarikaSalesHarian,
    calculateProductFokusDSM,
    calculateAT_Pimda_RKTK,
    calculateAT_DSM_RKTK,
    calculateClosingSales,
    calculateRerataBasic
    // calculateATSales,
}
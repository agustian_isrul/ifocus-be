const sequelize = require("sequelize");
const db = require("../../../config/databaseSequalize");

async function getSalesTargetNETValue(RayonCode, bodyData) {
    let data = await db.query(`select ValueNet from m10_target_net mtn where RayonCode = '${RayonCode}' and LineDesc = '${bodyData.LineName}' and MonthSales = ${bodyData.monthFrom} and YearSales = ${bodyData.yearFrom} `, {
        type: sequelize.QueryTypes.SELECT
    })
    let rupiah = 0;
    if (data.length > 0) {
        rupiah = data[0].ValueNet
    }
    // console.log(RayonCode, bodyData.monthFrom, bodyData.yearFrom, ' VALUE NET :', rupiah);
    return rupiah
}

async function getSalesActualMaxSales(RayonCode, bodyData, status) {
    let data = await db.query(`select MaxSales from m13_max_sales where RayonCode = '${RayonCode}' and LineDesc = '${bodyData.LineName}' and YearSales = ${bodyData.yearFrom} and GrowthType = '${status}'`, {
        type: sequelize.QueryTypes.SELECT
    })
    let rupiah = 0;
    if (data.length > 0) {
        rupiah = data[0].MaxSales
    }
    return rupiah
}
async function getOutletProjectActualValue(EmployeePositionCode, rayoncode, bodyData, FilterRCode, filterAnd, filterOr, DaySales, QueryRegPlus) {
    let QueryFilterAnd = "";
    let QueryFilterOr = "";

    if (filterAnd != 'no') {
        QueryFilterAnd = "and (" + filterAnd + ")";
    }
    if (filterOr != 'no') {
        QueryFilterOr = "and (" + filterOr + ")";
    }

    let rupiah = 0;
    let RayonFilter = "";
    if (EmployeePositionCode !== 'MR') {
        if (FilterRCode != 'no') {
            RayonFilter = `and (s01.DMRayonCode = '${rayoncode}')`;
        } else {
            RayonFilter = `and (s01.DMRayonCode = '${rayoncode}')`;
        }
    } else {
        RayonFilter = `and (s01.RayonCode = '${rayoncode}')`;
    }

    if (QueryRegPlus !== 'no') {
        QueryFilterOr = "and (" + QueryRegPlus + ")";
    }
    let filterDaysales = "";
    if (DaySales != 'no') {
        filterDaysales = "and (" + DaySales + ")";
    }

    let data = await db.query(`select SUM(s01.SalesValue) as jml 
            from s01_actual_data s01 
            inner join m09_outlet_project mop on mop.RayonCode = s01.DMRayonCode and mop.Site_Code = s01.SiteCode 
            where s01.LineDesc = '${bodyData.LineName}' ${RayonFilter}  
            and s01.MonthSales between ${bodyData.monthFrom} and ${bodyData.monthTo} and s01.YearSales = "${bodyData.yearFrom}" ${QueryFilterAnd} ${QueryFilterOr} ${filterDaysales}`, {
        type: sequelize.QueryTypes.SELECT
    })
    rupiah = data[0].jml
    if (rupiah == null) {
        rupiah = 0
    }
    // console.log('a',rupiah);
    return (rupiah) ? rupiah : 0
}

async function getSalesActualPAPValue(EmployeePositionCode, rayoncode, bodyData, FilterRCode, filterAnd, filterOr, DaySales, QueryRegPlus) {
    let QueryFilterAnd = "";
    let QueryFilterOr = "";

    if (filterAnd != 'no') {
        QueryFilterAnd = "and (" + filterAnd + ")";
    }
    if (filterOr != 'no') {
        QueryFilterOr = "and (" + filterOr + ")";
    }

    let rupiah = 0;
    let RayonFilter = "";
    if (EmployeePositionCode !== 'MR') {
        if (FilterRCode != 'no') {
            if (FilterRCode.filterRayon != 'no') {
                RayonFilter = "and (" + FilterRCode.filterRayon + ")";
            } else {
                RayonFilter = `and (s01.RayonCode = '${rayoncode}')`;
            }
        } else {
            RayonFilter = `and (s01.RayonCode = '${rayoncode}')`;
        }
    } else {
        RayonFilter = `and (s01.RayonCode = '${rayoncode}')`;
    }

    if (QueryRegPlus != 'no') {
        QueryFilterOr = "and (" + QueryRegPlus + ")";
    }
    let filterDaysales = "";
    if (DaySales != 'no') {
        filterDaysales = "and (" + DaySales + ")";
    }
    let data = await db.query(`select SUM(s01.PAP) as jml 
            from s01_actual_data s01 
            where s01.LineDesc = '${bodyData.LineName}' ${RayonFilter}  
            and s01.MonthSales between ${bodyData.monthFrom} and ${bodyData.monthTo} and s01.YearSales = "${bodyData.yearFrom}" ${QueryFilterAnd} ${QueryFilterOr} ${filterDaysales}`, {
        type: sequelize.QueryTypes.SELECT
    })
    rupiah = data[0].jml
    if (rupiah == null) {
        rupiah = 0
    }
    // console.log('a',rupiah);
    return (rupiah) ? rupiah : 0
}
async function getSalesActualPLPValue(EmployeePositionCode, rayoncode, bodyData, FilterRCode, filterAnd, filterOr, DaySales, QueryRegPlus) {
    let QueryFilterAnd = "";
    let QueryFilterOr = "";

    if (filterAnd != 'no') {
        QueryFilterAnd = "and (" + filterAnd + ")";
    }
    if (filterOr != 'no') {
        QueryFilterOr = "and (" + filterOr + ")";
    }

    let rupiah = 0;
    let RayonFilter = "";
    if (EmployeePositionCode !== 'MR') {
        if (FilterRCode != 'no') {
            if (FilterRCode.filterRayon != 'no') {
                RayonFilter = "and (" + FilterRCode.filterRayon + ")";
            } else {
                RayonFilter = `and (s01.RayonCode = '${rayoncode}')`;
            }
        } else {
            RayonFilter = `and (s01.RayonCode = '${rayoncode}')`;
        }
    } else {
        RayonFilter = `and (s01.RayonCode = '${rayoncode}')`;
    }

    if (QueryRegPlus != 'no') {
        QueryFilterOr = "and (" + QueryRegPlus + ")";
    }
    let filterDaysales = "";
    if (DaySales != 'no') {
        filterDaysales = "and (" + DaySales + ")";
    }
    let data = await db.query(`select SUM(s01.PLP) as jml 
            from s01_actual_data s01 
            where s01.LineDesc = '${bodyData.LineName}' ${RayonFilter}  
            and s01.MonthSales between ${bodyData.monthFrom} and ${bodyData.monthTo} and s01.YearSales = "${bodyData.yearFrom}" ${QueryFilterAnd} ${QueryFilterOr} ${filterDaysales}`, {
        type: sequelize.QueryTypes.SELECT
    })
    rupiah = data[0].jml
    if (rupiah == null) {
        rupiah = 0
    }
    // console.log('a',rupiah);
    return (rupiah) ? rupiah : 0
}

async function getSalesActualValue(EmployeePositionCode, rayoncode, bodyData, FilterRCode, filterAnd, filterOr, DaySales, QueryRegPlus) {
    // console.log('filterAnd, filterOr', filterAnd, filterOr,rayoncode,FilterRCode);
    let QueryFilterAnd = "";
    let QueryFilterOr = "";

    if (filterAnd != 'no') {
        QueryFilterAnd = "and (" + filterAnd + ")";
    }
    if (filterOr != 'no') {
        QueryFilterOr = "and (" + filterOr + ")";
    }

    let rupiah = 0;
    let RayonFilter = "";
    if (EmployeePositionCode !== 'MR') {
        if (FilterRCode != 'no') {
            if (FilterRCode.filterRayon != 'no') {
                RayonFilter = "and (" + FilterRCode.filterRayon + ")";
            } else {
                RayonFilter = `and (s01.RayonCode = '${rayoncode}')`;
            }
        } else {
            RayonFilter = `and (s01.RayonCode = '${rayoncode}')`;
        }
    } else {
        RayonFilter = `and (s01.RayonCode = '${rayoncode}')`;
    }

    if (QueryRegPlus != 'no') {
        QueryFilterOr = "and (" + QueryRegPlus + ")";
    }
    let filterDaysales = "";
    if (DaySales != 'no') {
        filterDaysales = "and (" + DaySales + ")";
    }
    let data = await db.query(`select SUM(s01.SalesValue) as jml 
            from s01_actual_data s01 
            where s01.LineDesc = '${bodyData.LineName}' ${RayonFilter}  
            and s01.MonthSales between ${bodyData.monthFrom} and ${bodyData.monthTo} and s01.YearSales = "${bodyData.yearFrom}" ${QueryFilterAnd} ${QueryFilterOr} ${filterDaysales}`, {
        type: sequelize.QueryTypes.SELECT
    })
    rupiah = data[0].jml
    if (rupiah == null) {
        rupiah = 0
    }
    // console.log('a',rupiah);
    return (rupiah) ? rupiah : 0
}



async function getSalesTargetValue(EmployeePositionCode, rayoncode, bodyData, FilterRCode, filterAnd, filterOr, QueryRegPlus) {
    // console.log('FilterRCode', rayoncode, FilterRCode.filterRayon);
    let QueryFilterAnd = "";
    let QueryFilterOr = "";

    if (filterAnd != 'no') {
        QueryFilterAnd = "and (" + filterAnd + ")";
    }
    if (filterOr != 'no') {
        QueryFilterOr = "and (" + filterOr + ")";
    }

    let rupiah = 0;
    let RayonFilter = "";
    if (EmployeePositionCode !== 'MR') {
        if (FilterRCode != 'no') {
            if (FilterRCode.filterRayon != 'no') {
                RayonFilter = "and (" + FilterRCode.filterRayon + ")";
            } else {
                RayonFilter = `and (s02.RayonCode = '${rayoncode}')`;
            }
        } else {
            RayonFilter = `and (s02.RayonCode = '${rayoncode}')`;
        }
    } else {
        RayonFilter = `and (s02.RayonCode = '${rayoncode}')`;
    }

    if (QueryRegPlus != 'no') {
        QueryFilterOr = "and (" + QueryRegPlus + ")";
    }

    let data = await db.query(`select SUM(s02.TargetValue) as jml 
                from s02_target_data s02 
                where s02.LineDesc = '${bodyData.LineName}' ${RayonFilter} 
                and s02.MonthSales between ${bodyData.monthFrom} and ${bodyData.monthTo} and s02.YearSales = "${bodyData.yearFrom}" ${QueryFilterAnd} ${QueryFilterOr}`, {
        type: sequelize.QueryTypes.SELECT
    })

    rupiah = data[0].jml
    if (rupiah == null) {
        rupiah = 0
    }
    return (rupiah) ? rupiah : 0
}


async function getSalesActualKuartalValue(area, bodyData, areaCodeStr, filterAnd, filterOr, QueryRegPlus) {
    // console.log('areaCodeStr',areaCodeStr);
    let QueryFilterAnd = "";
    let QueryFilterOr = "";

    if (filterAnd != 'no') {
        QueryFilterAnd = "and (" + filterAnd + ")";
    }
    if (filterOr != 'no') {
        QueryFilterOr = "and (" + filterOr + ")";
    }


    let AreaFilter = "";
    if (areaCodeStr != 'no') {
        AreaFilter = "and (" + areaCodeStr + ")";
    } else {
        AreaFilter = `and (s01.CustomerArea = '${area}')`;
    }

    if (QueryRegPlus !== 'no') {
        QueryFilterOr = "and (" + QueryRegPlus + ")";
    }

    let data = await db.query(`select SUM(s01.SalesValue) as jml 
                from s01_actual_data s01
                where s01.LineDesc = '${bodyData.LineName}' ${AreaFilter} 
                and s01.MonthSales between ${bodyData.monthFrom} and ${bodyData.monthTo} and s01.YearSales = "${bodyData.yearFrom}"  ${QueryFilterAnd} ${QueryFilterOr}`, {
        type: sequelize.QueryTypes.SELECT
    })
    let rupiah = data[0].jml
    if (rupiah == null) {
        rupiah = 0
    }
    return (rupiah) ? rupiah : 0
}

async function getSalesTargetKuartalValue(area, bodyData, areaCodeStr, filterAnd, filterOr, QueryRegPlus) {
    let QueryFilterAnd = "";
    let QueryFilterOr = "";

    if (filterAnd != 'no') {
        QueryFilterAnd = "and (" + filterAnd + ")";
    }
    if (filterOr != 'no') {
        QueryFilterOr = "and (" + filterOr + ")";
    }


    let AreaFilter = "";
    if (areaCodeStr != 'no') {
        AreaFilter = "and (" + areaCodeStr + ")";
    } else {
        AreaFilter = `and (s02.CustomerArea = '${area}')`;
    }

    if (QueryRegPlus != 'no') {
        QueryFilterOr = "and (" + QueryRegPlus + ")";
    }

    let data = await db.query(`select SUM(s02.TargetValue) as jml 
                from s02_target_data s02 
                where s02.LineDesc = '${bodyData.LineName}' ${AreaFilter} 
                and s02.MonthSales between ${bodyData.monthFrom} and ${bodyData.monthTo} and s02.YearSales = "${bodyData.yearFrom}" ${QueryFilterAnd} ${QueryFilterOr}`, {
        type: sequelize.QueryTypes.SELECT
    })
    let rupiah = data[0].jml
    if (rupiah == null) {
        rupiah = 0
    }
    return (rupiah) ? rupiah : 0
}
async function getProduktivity(lineName, filterAnd, filterOr) {
    let QueryFilterAnd = "";
    let QueryFilterOr = "";

    if (filterAnd != 'no') {
        QueryFilterAnd = "and (" + filterAnd + ")";
    } else if (filterOr != 'no') {
        QueryFilterOr = "and (" + filterOr + ")";
    }
    let data = await db.query(`select Productivitas, count(*) as count from m07_productivitas mp where LineDesc = '${lineName}' ${QueryFilterAnd} ${QueryFilterOr} limit 1`, {
        type: sequelize.QueryTypes.SELECT
    });
    if (data[0].Productivitas > 0) {
        data = data[0].Productivitas
    } else {
        data = 0;
    }
    //console.log('p',rupiah)
    return (data) ? data : 0

}

async function getMaxSales(ebody, RCodeDD, LineName) {
    let filterCOde = "";
    if (RCodeDD.rayonCodeStr !== null) {
        filterCOde = "and (" + RCodeDD.rayonCodeStr + ")";
    }
    let dataMax = await db.query(`select * from m13_max_sales where LineDesc = '${LineName}' ${filterCOde} 
    and QuarterN = ${ebody.Kuartal} and YearSales = '${ebody.yearFrom}' and GrowthType = 'REGULAR'`, {
        type: sequelize.QueryTypes.SELECT
    })
    let data = 0;
    if (dataMax.length > 0) {
        data = dataMax[0].MaxSales
    }
    // console.log('dataMAX',data);
    return (data) ? data : 0
}

async function getIncMaxSales(getKuartalIncentive, getKuartalYear, RCodeDD, LineName) {
    let filterCOde = "";
    if (RCodeDD.rayonCodeStr !== null) {
        filterCOde = "and (" + RCodeDD.rayonCodeStr + ")";
    }
    let dataIncentive = await db.query(`select * from m13_max_sales where LineDesc = '${LineName}' ${filterCOde} 
    and QuarterN = ${getKuartalIncentive} and YearSales = '${getKuartalYear}' and GrowthType = 'INCENTIVE'`, {
        type: sequelize.QueryTypes.SELECT
    })
    let data = 0;
    if (dataIncentive.length > 0) {
        data = dataIncentive[0].MaxSales
    }
    // console.log('dataIncentive',data);
    return (data) ? data : 0
}

module.exports = {
    getOutletProjectActualValue,
    getSalesActualValue,
    getSalesTargetValue,
    getSalesActualKuartalValue,
    getSalesTargetKuartalValue,
    getProduktivity,
    getMaxSales,
    getIncMaxSales,
    getSalesActualPAPValue,
    getSalesActualPLPValue,
    getSalesTargetNETValue,
    getSalesActualMaxSales
}
const sequelize = require("sequelize");
const db = require("../../../config/databaseSequalize");

async function calculate(actual, target, AT, ATReg, ATMax, AProd, ConfigDetailId, ActualALL, TargetAll, ATTotal, AP_Netto, getSalesH1, getSalesH2) {
    let getJsonPass = await getPassword(actual, target, AT, ATReg, ATMax, AProd, ConfigDetailId, ActualALL, TargetAll, ATTotal, AP_Netto, getSalesH1, getSalesH2)
    return getJsonPass
}

const getBonusConfig = async (ConfigDetailId) => {
    let data = await db.query(`select t06.*,t07.RecordNo as RecordNo2,t07.TargetType as TargetType2,t07.OperatorType as OperatorType2 ,t07.Operator1 
    as Operator12, t07.Operator2 as Operator22,t07.ValueFrom as ValueFrom2 , t07.ValueTo as ValueTo2,t07.Bonus 
    from t06_bonus_config t06 left join t07_bonus_detail t07 on t07.RecordNo = t06.RecordNo and t07.ConfigDetailId = t06.ConfigDetailId 
    where t06.ConfigDetailId ='${ConfigDetailId}'`, {
        type: sequelize.QueryTypes.SELECT
    })
    return data
}

const getPassword = async (actual, target, AT, ATReg, ATMax, AProd, ConfigDetailId, ActualALL, TargetAll, ATTotal, AP_Netto, getSalesH1, getSalesH2) => {
    let dataParent = await getBonusConfig(ConfigDetailId)
    let getpush = [];
    await Promise.all(
        dataParent.map(async (e) => {
            let makeRumus = await makeRumusOne(e, actual, target, AT, ATReg, ATMax, AProd, ActualALL, TargetAll, ATTotal, AP_Netto, getSalesH1, getSalesH2);
            let datalist = {
                RecordNo: e.RecordNo,
                TargetType: e.TargetType,
                OperatorType: e.OperatorType,
                Operator1: e.Operator1,
                Operator2: e.Operator2,
                ValueFrom: e.ValueFrom,
                ValueTo: e.ValueTo,
                RecordNo2: e.RecordNo2,
                TargetType2: e.TargetType2,
                OperatorType2: e.OperatorType2,
                Operator12: e.Operator12,
                Operator22: e.Operator22,
                ValueFrom2: e.ValueFrom2,
                ValueTo2: e.ValueTo2,
                Bonus: e.Bonus,
                Status_Rumus: (makeRumus) ? true : false,
            }
            getpush.push(datalist)
        }))

    return getpush
}

const makeRumusOne = async (datas, actual, target, AT, ATReg, ATMax, AProd, ActualALL, TargetAll, ATTotal, AP_Netto, getSalesH1, getSalesH2) => {
    // console.log(ATTotal,datas);
    let ATh1 = (getSalesH1 / target) * 100;
    let ATh2 = (getSalesH2 / target) * 100;

    let hasil = null;
    if (datas.TargetType != null) {
        switch (datas.TargetType) {
            case "actual":
                hasil = calculatePassword(actual, datas)
                break;
            case "actual_target_all":
                hasil = calculatePassword(ATTotal, datas)
                break;
            case "actual_target":
                hasil = calculatePassword(AT, datas)
                break;
            case "actual_productivitas":
                hasil = calculatePassword(AProd, datas)
                break;
            case "actual_tum":
                hasil = calculatePassword(AT, datas)
                break;
            case "actual_max":
                hasil = calculatePassword(ATMax, datas)
                break;
            case "actual_max_all":
                hasil = calculatePassword(ATTotal, datas)
                break;
            case "actual_all":
                hasil = calculatePassword(ActualALL, datas)
                break;
            case "target_all":
                hasil = calculatePassword(TargetAll, datas)
                break;
            case "actual_regular":
                hasil = calculatePassword(ATReg, datas)
                break;
            case "actual_target_H1":
                hasil = calculatePassword(ATh1, datas)
                break;
            case "actual_target_H2":
                hasil = calculatePassword(ATh2, datas)
                break;
            case "actual_productivitas_netto":
                hasil = calculatePassword(AP_Netto, datas)
                break;
        }
    } else {
        hasil = false
    }
    return hasil
}

const calculatePassword = (ATValue, datas) => {
    if (datas.Operator2 == 'between') {
        if (ATValue >= datas.ValueFrom && ATValue < datas.ValueTo) {
            return (datas) ? true : false
        }
    } else {
        switch (datas.Operator2) {
            case "=":
                if (ATValue = datas.ValueFrom) {
                    return (datas) ? true : false
                }
                break;
            case ">":
                if (ATValue > datas.ValueFrom) {
                    return (datas) ? true : false
                }
                break;
            case ">=":
                if (ATValue >= datas.ValueFrom) {
                    return (datas) ? true : false
                }
                break;
            case "<":
                if (ATValue < datas.ValueFrom) {
                    return (datas) ? true : false
                }
                break;
            case "<=":
                if (ATValue <= datas.ValueFrom) {
                    return (datas) ? true : false
                }
                break;

        }
    }
}

module.exports = {
    calculate
}
const knex = require('../../config/database');

async function summaryCalculate(bodyData) {
    bodyData.header.map(item => {
        item.StartDate = new Date(item.StartDate);
        item.EndDate = new Date(item.EndDate);
        return item; 
    });
    await knex('s05_incentive_header').insert(bodyData.header);
    await knex('s06_incentive_detail').insert(bodyData.detail);
}

module.exports = {
    summaryCalculate
}
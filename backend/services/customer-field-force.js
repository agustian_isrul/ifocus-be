const moment = require("moment-timezone");

const knex = require('../../config/database')

function findPageData(outletPanel, pageSetting) {
    return knex.with('table_data', (qb) => {
        qb.select(
            "t10.CustomerFFID",
            "t10.CompanyID",
            "t10.PosStructureID",
            "t10.AreaID",
            "t10.AreaCode",
            "t10.AreaName",
            "t10.LineID",
            "t10.LineName",
            "t10.SubLine",
            "t10.RayonCode",
            "t10.EmployeeID",
            "t10.EmployeeName",
            "t10.EmployeePosition",
            "t10.EmployeeMRType",
            "t10.Sitecode",
            "t10.OutletName",
            "t10.OutletAddress",
            "t10.StartDate",
            "t10.EndDate",
            "t10.isActive",
            "t10.UserID",
            "t10.LastUpdate",
            "t10.ApprovalStatus",
            "t10.ApprovalDate",
            knex.raw(
                "case when CURDATE() between t10.StartDate and t10.EndDate and t10.ApprovalDate is not null then 'Active' " +
                "else 'Non Active' end as status"
            )
        )
        .from('t10_customer_ff as t10')
        .where(function() {
            if (outletPanel) {
                if (outletPanel.CompanyID) {
                    this.where('t10.CompanyID', outletPanel.CompanyID);
                }
                if (outletPanel.PosStructureID) {
                    this.where('t10.PosStructureID', outletPanel.PosStructureID);
                }
                if (outletPanel.area) {
                    this.where('t10.AreaID', outletPanel.area.AreaID);
                }
                if (outletPanel.lini) {
                    this.where('t10.LineID', outletPanel.lini.LineID);
                }
                if (outletPanel.RayonCode) {
                    this.where('t10.RayonCode', outletPanel.RayonCode);
                }
                if (outletPanel.EmployeeID) {
                    this.where('t10.EmployeeID', outletPanel.EmployeeID);
                }
                if (outletPanel.EmployeeName) {
                    this.where('t10.EmployeeName', outletPanel.EmployeeName);
                }
                if (outletPanel.EmployeePosition) {
                    this.where('t10.EmployeePosition', outletPanel.EmployeePosition);
                }
                if (outletPanel.Sitecode) {
                    this.where('t10.Sitecode', outletPanel.Sitecode);
                }
                if (outletPanel.OutletName) {
                    this.where('t10.OutletName', outletPanel.OutletName);
                }
                if (outletPanel.StartDate) {
                    const formatStartDate = moment(outletPanel.StartDate).format('YYYY-MM-DD');
                    this.where('t10.StartDate', '>=', formatStartDate);
                }
                if (outletPanel.EndDate) {
                    const formatEndDate = moment(outletPanel.EndDate).format('YYYY-MM-DD');                    
                    this.where('t10.EndDate', '<=', formatEndDate);
                }
                if (outletPanel.status) {
                    const today = moment().format('YYYY-MM-DD');
                    this.where('t10.StartDate', '<=', today)
                    .where('t10.EndDate', '>=', today);
                    if (outletPanel.status === 'Non Active') {
                        this.whereNull('t10.ApprovalDate');
                    }
                    if (outletPanel.status === 'Active') {
                        this.whereNotNull('t10.ApprovalDate');
                    }
                }
            }
        })
        .where('t10.isActive', '<>', 3)
    })
    .select('*', knex.raw('select count(*) from table_data').wrap('(', ') totalRows'))
    .from('table_data')
    .limit(pageSetting && pageSetting.rows > 0 ? pageSetting.rows : 10)
    .offset(pageSetting && pageSetting.first > 0 ? pageSetting.first : 0)
    .orderBy(
        pageSetting && pageSetting.sortField ? pageSetting.sortField : 'CustomerFFID',
        pageSetting && pageSetting.sortOrder === 1 ? 'asc' : 'desc'
    );
}

function saveData(bodyDataList, profileUser) {
    try {
        const today = moment();
        return knex.transaction(async trx => {
            for (const tempNewReguler of bodyDataList) {
                const tempNewStartDate = moment(tempNewReguler.StartDate);
                const tempNewEndDate = moment(tempNewReguler.EndDate);
                const tempEditEndDate = Object.create(moment(tempNewReguler.StartDate)).subtract(1, 'd');

                const tempOldRegulerList = await trx('t10_customer_ff as t10')
                .where('t10.Sitecode', tempNewReguler.Sitecode)
                .where('t10.SubLine', tempNewReguler.SubLine)
                .where('t10.CompanyID', profileUser.CompanyID)
                .whereRaw('(? between t10.StartDate and t10.EndDate)', [tempNewStartDate.format('YYYY-MM-DD')]);
    
                if (tempOldRegulerList && tempOldRegulerList.length > 0) {
                    for (const tempOldReguler of tempOldRegulerList) {
                        const tempOldStartDate = moment(tempOldReguler.StartDate);
    
                        // old period = new period => delete
                        if (tempOldStartDate.isSameOrAfter(tempNewStartDate, "date")) {
                            await trx('t10_customer_ff')
                            .where({CustomerFFID: tempOldReguler.CustomerFFID})
                            .del();
                        }
    
                        // old period < new period => cut off
                        if (tempOldStartDate.isBefore(tempNewStartDate, "date")) {
                            await trx('t10_customer_ff')
                            .where('CustomerFFID', tempOldReguler.CustomerFFID)
                            .update({
                                EndDate: tempEditEndDate.format('YYYY-MM-DD'),
                                LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                            });
                        }
                    }
                }

                const tempPanelList = await trx('t02_outlet_panel')
                // .where('AreaID', tempNewReguler.AreaID)
                .where('LineID', tempNewReguler.LineID)
                .where('Sitecode', tempNewReguler.Sitecode)                
                .whereRaw('(? between StartDate and EndDate)', [tempNewStartDate.format('YYYY-MM-DD')]);
                if (tempPanelList && tempPanelList.length > 0) {
                    for (const tempPanel of tempPanelList) {
                        const tempOldStartDate = moment(tempPanel.StartDate);

                        if (tempOldStartDate.isSameOrAfter(tempNewStartDate, "date")) {
                            await trx('t02_outlet_panel').where({CustomerPanelID: tempPanel.CustomerPanelID}).del();
                        }

                        if (tempOldStartDate.isBefore(tempNewStartDate, "date")) {
                            await trx('t02_outlet_panel')
                            .where('CustomerPanelID', tempPanel.CustomerPanelID)
                            .update({
                                EndDate: tempEditEndDate.format('YYYY-MM-DD'),
                                LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                            });
                        }
                    }
                }

                const tempContributionList = await trx('t11_customer_contribution')
                // .where('AreaID', tempNewReguler.AreaID)
                .where('LineID', tempNewReguler.LineID)
                // .where('CustomerAreaID', tempNewReguler.AreaID)
                .where('Sitecode', tempNewReguler.Sitecode)                
                .whereRaw('(? between StartDate and EndDate)', [tempNewStartDate.format('YYYY-MM-DD')]);
                if (tempContributionList && tempContributionList.length > 0) {
                    for (const tempContribution of tempContributionList) {
                        const tempOldStartDate = moment(tempContribution.StartDate);

                        if (tempOldStartDate.isSameOrAfter(tempNewStartDate, "date")) {
                            await trx('t11_customer_contribution').where({CustomerFFID: tempContribution.CustomerFFID}).del();
                        }

                        if (tempOldStartDate.isBefore(tempNewStartDate, "date")) {
                            await trx('t11_customer_contribution')
                            .where('CustomerFFID', tempContribution.CustomerFFID)
                            .update({
                                EndDate: tempEditEndDate.format('YYYY-MM-DD'),
                                LastUpdate: today.format('YYYY-MM-DD hh:mm:ss')
                            });
                        }
                    }
                }

                tempNewReguler.StartDate = tempNewStartDate.format('YYYY-MM-DD');
                tempNewReguler.EndDate = tempNewEndDate.format('YYYY-MM-DD');
                tempNewReguler.LastUpdate = today.format('YYYY-MM-DD hh:mm:ss');
                await trx('t10_customer_ff').insert(tempNewReguler);
            }
            
            const tableHistory = {
                table_name: 't10_customer_ff',
                table_action: 2,
                table_data: JSON.stringify(bodyDataList),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        });
    } catch (error) {
        throw error;
    }
}

function editData(bodyData, profileUser) {
    try {
        const today = moment();
        return knex.transaction(async trx => {
            await trx('t10_customer_ff')
            .where('CustomerFFID', bodyData.CustomerFFID)
            .update({
                RayonCode: bodyData.RayonCode,
                EmployeeID: bodyData.EmployeeID,
                EmployeeName: bodyData.EmployeeName,
                EmployeePosition: bodyData.EmployeePosition,
                EmployeeMRType: bodyData.EmployeeMRType
            });
            const tableHistory = {
                table_name: 't10_customer_ff',
                table_action: 2,
                table_data: JSON.stringify(bodyData),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        });
    } catch (error) {
        return error;
    }
}

function deleteData(bodyDataList, profileUser) {
    try {
        const today = moment();
        return knex.transaction(async trx => {
            for (const tempBodyData of bodyDataList) {
                await trx('t10_customer_ff').where({CustomerFFID: tempBodyData.CustomerFFID}).del();
            }
            const tableHistory = {
                table_name: 't10_customer_ff',
                table_action: 6,
                table_data: JSON.stringify(bodyData),
                modified_by: profileUser.Username,
                modified_at: today.format('YYYY-MM-DD hh:mm:ss')
            }
            await trx('t01_table_history').insert(tableHistory);
        });
    } catch (error) {
        return error;
    }
}

module.exports = {
    findPageData,
    saveData,
    editData,
    deleteData
}
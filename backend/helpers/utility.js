const convertArrayToNestedArray = (array, key, keyGroup, extraKey1, extraKey2) => Object.values(
    array.reduce((res, item) => {
        const value = extraKey1 ? item[key] + item[extraKey1] + item[extraKey2] : item[key];
        const existing = res[value] || {
            [key]: item[key], 
            [keyGroup]:'' , 
            [keyGroup + "Filter"]: [], 
            [keyGroup + "List"]:[]
        }
        return {
            ...res,
            [value] : {
                ...existing,
                [keyGroup]: [...existing[keyGroup + "Filter"], item[keyGroup]].join(' + '),
                [keyGroup + "Filter"]: [...existing[keyGroup + "Filter"], item[keyGroup]],
                [keyGroup + "List"]: [...existing[keyGroup + "List"], item]
            }
        } 
    }, {})
)

const reduceFilterTarikanList = (array, operator) => Object.values(
    array
    .filter(data => !data.FilterCode.startsWith('Employee') && data.Operator === operator)
    .reduce((result, item) => {
        const value = item.FilterCode;
        const existing = result[value] || {
            FilterCode: value, 
            FilterValue: '',
            FilterValueFilter: [], 
            ProductIDList: [],
            FilterValueList: []
        }
        return {
            ...result,
            [value] : {
                ...existing,
                FilterValue: [...existing.FilterValueFilter, item.FilterValue].join(' + '),
                FilterValueFilter: [...existing.FilterValueFilter, item.FilterValue],
                ProductIDList: [...existing.ProductIDList, item.ProductID],
                FilterValueList: [...existing.FilterValueList, item]
            }
        } 
    }, {})
)

const createEmployeeFPPList = (employeeList, filterEmployeeDataList) => Object.values(
    employeeList
    .filter(currentEmployee => filterEmployeeDataList
        .every(element => element.FilterValueList[0].FlagInclude === 1 ? 
            element.FilterValueFilter.includes(currentEmployee[element.FilterCode]) : 
            !element.FilterValueFilter.includes(currentEmployee[element.FilterCode])
        )
    )
    .reduce((result, item) => {
        const keyGroup = 'RayonCode';
        const value = item.RayonCodeSup ? item.EmployeeID + item.RayonCodeSup : item.EmployeeID;
        const existing = result[value] || {
            EmployeeID: item.EmployeeID, 
            [keyGroup]:'' , 
            [keyGroup + "Filter"]: [],
            [keyGroup + "List"]: [], 
            EmployeeName: '',
            EmployeePositionCode: '',
            EmployeeAreaCode: '',
            EmployeeSubLineDesc: '',
            EmployeeVacant: ''
        }
        return {
            ...result,
            [value] : {
                ...existing,
                [keyGroup]: item["IsDefault"] === 1 ? item[keyGroup] : existing[keyGroup],
                [keyGroup + "Filter"]: [...existing[keyGroup + "Filter"], item[keyGroup]],
                [keyGroup + "List"]: [...existing[keyGroup + "List"], item],
                EmployeeName: item.EmployeeName,
                EmployeePositionCode: item.EmployeePositionCode,
                EmployeeAreaCode: item["IsDefault"] === 1 ? item["EmployeeAreaCode"] : existing["EmployeeAreaCode"],
                EmployeeSubLineDesc: item.EmployeeSubLineDesc,
                EmployeeVacant: item.EmployeeVacant
            }
        }
    }, {})
)

const clasificationGroupList = (dataList) => Object.values(
    dataList.filter(data => data.RecordNoD)
    .reduce((result, item) => {
        const value = item.RecordNoH;
        const existing = result[value] || {
            RecordNo: value,
            TargetTypeH: '',
            Operator1H: '',
            Operator2H: '',
            ValueFromH: '',
            ValueToH: '',
            detail: []
        };
        return {
            ...result,
            [value] : {
                ...existing,
                TargetTypeH: item.TargetTypeH,
                Operator1H: item.Operator1H,
                Operator2H: item.Operator2H,
                ValueFromH: item.ValueFromH,
                ValueToH: item.ValueToH,
                detail: [...existing.detail, item]
            }
        }
    }, {})
)

const convertNaNToZero = (inputNumber) => {
    if (!inputNumber) {
        return 0;
    }
    if (!Number.isFinite(inputNumber)) {
        return 0;
    }
    if (Number.isNaN(inputNumber)) {
      return 0;
    }
    return inputNumber;
}

const isExistEmployee = (employee, resultCalculate) => {
    if (resultCalculate && resultCalculate.length > 0) {
        return resultCalculate.find(data => data.RayonCodeFilter.includes(employee.RayonCodeFilter[0])) || null;
    }
    return null;
}

const calculateBonus = (employee, objectSchema, preeliminationAndList, preeliminationOrList, clasificationList) => {
    const isExistPassword = calculatePassword(employee, objectSchema, preeliminationAndList, preeliminationOrList);
    if (isExistPassword === 0) {
        return 0;
    }
    
    if (clasificationList && clasificationList.length > 0) {
        const objectClassification = clasificationList
        .sort((a, b) => b.ValueFromH - a.ValueFromH)
        .find(criteria => validateByTargetType(
            criteria.TargetTypeH, employee, objectSchema, criteria.Operator2H, criteria.ValueFromH, criteria.ValueToH
        ));
        if (objectClassification && objectClassification.detail && objectClassification.detail.length > 0) {
            let criteriaDetailList = objectClassification.detail.filter(criteriaDetail => !criteriaDetail.Bonus);
            if (criteriaDetailList && criteriaDetailList.length > 0) {
                for (const criteriaDetail of criteriaDetailList) {
                    if (validateByTargetType(criteriaDetail.TargetTypeD, employee, objectSchema, criteriaDetail.Operator2D, 
                        criteriaDetail.ValueFromD, criteriaDetail.ValueToD) == false) {
                        return 0;
                    }
                }
            }
            criteriaDetailList = objectClassification.detail.filter(criteriaDetail => criteriaDetail.Bonus);
            const objectIncentive = criteriaDetailList.sort((a, b) => b.ValueFromD - a.ValueFromD)
            .find(criteriaDetail => criteriaDetail.Bonus && validateByTargetType(
                criteriaDetail.TargetTypeD, employee, objectSchema, criteriaDetail.Operator2D, 
                criteriaDetail.ValueFromD, criteriaDetail.ValueToD
            ));
            if (objectIncentive) {
                return objectIncentive.Bonus;
            }
        }
    }
    return 0;
}

const calculatePassword = (employee, objectSchema, preeliminationAndList, preeliminationOrList) => {
    if (preeliminationAndList && preeliminationAndList.length > 0) {
        const isValidate = preeliminationAndList.every(parameterData => validateByTargetType(
            parameterData.TargetTypeH, employee, objectSchema, parameterData.Operator2H, 
            parameterData.ValueFromH, parameterData.ValueToH)
        );
        if (isValidate === false) {
            return 0;
        }
    }
    if (preeliminationOrList && preeliminationOrList.length > 0) {
        const isValidate = preeliminationOrList.some(parameterData => validateByTargetType(
            parameterData.TargetTypeH, employee, objectSchema, parameterData.Operator2H, 
            parameterData.ValueFromH, parameterData.ValueToH)
        );
        if (isValidate === false) {
            return 0;
        }
    }
    return 1;
}

const validateByTargetType = (TargetType, employee, objectSchema, operator, valueFrom, valueTo) => {
    let tempValueFrom = valueFrom;
    if (objectSchema && valueFrom in objectSchema) {
        tempValueFrom = objectSchema[valueFrom];
    }
    if (employee && valueFrom in employee) {
        tempValueFrom = employee[valueFrom];
    }
    if (objectSchema && TargetType in objectSchema) {
        return validatePassword(objectSchema[TargetType], operator, tempValueFrom, valueTo);
    }
    if (employee && TargetType in employee) {
        return validatePassword(employee[TargetType], operator, tempValueFrom, valueTo);
    }

    return true;
}

const validatePassword = (operand, operator, valueFrom, valueTo) => {
    switch (operator) {
        case 'between' : {
            return new Number(operand) >= new Number(valueFrom) 
                && new Number(operand) < new Number(valueTo);
        }
        case '>=' : {
            return new Number(operand) >= new Number(valueFrom);
        }
        case '<=' : {
            return new Number(operand) <= new Number(valueFrom);
        }
        case '>' : {
            return new Number(operand) > new Number(valueFrom);
        }
        case '<' : {
            return new Number(operand) < new Number(valueFrom);
        }
        case '=' : {
            return operand == valueFrom;
        }
        case '<>' : {
            return operand != valueFrom;
        }
        default:
            break;
    }
    return true;
}

const calculateincentive = (objectSchema, preeliminationList, clasificationList) => {
    for (parameterData of preeliminationList) {
        if (validateTargetType(parameterData.TargetTypeH, objectSchema, parameterData.Operator2H, 
            parameterData.ValueFromH, parameterData.ValueToH) === false) {
            return 0;
        }
    }
    let bonus = 0;
    for (criteria of clasificationList) {
      if (validateTargetType(criteria.TargetTypeH, objectSchema, criteria.Operator2H, 
          criteria.ValueFromH, criteria.ValueToH) && 
          validateTargetType(criteria.TargetTypeD, objectSchema, criteria.Operator2D, 
              criteria.ValueFromD, criteria.ValueToD)) {
          switch (objectSchema.schemaId) {
            case 6  : {
              let tempBonus = 0;
              if (criteria.ValueFromH) {
                  tempBonus = Math.round(criteria.ValueFromH / 100 * (objectSchema.actual - objectSchema.maxTargetProductivity));
              } else {
                  tempBonus = Math.round(criteria.ValueFromD / 100 * (objectSchema.actual - objectSchema.maxTargetProductivity));
              }
              bonus = (tempBonus >= criteria.Bonus) ? criteria.Bonus : tempBonus;
              break;
            }
            case 32  : {
                let tempBonus = 0;
               
                if (criteria.ValueFromH) {
                    tempBonus = Math.round(criteria.ValueFromH / 100 * objectSchema.actualRegular);
                } else {
                    tempBonus = Math.round(criteria.ValueFromD / 100 * objectSchema.actualRegular);
                }
                
                if(tempBonus >= criteria.ValueFromD && tempBonus <= criteria.ValueToD){

                    bonus = tempBonus;
                }else if(tempBonus >= criteria.Bonus){
                    bonus = criteria.Bonus;
                }

                break;
            }

            case 31  : {
                
                let bonus = 0;
                if(objectSchema.MonthOnTarget >=  Math.round(criteria.ValueFromD) ){
                    bonus = criteria.Bonus;
                }
                
                return bonus;
               
            }

            case 11 : {
                if (objectSchema.EmployeeSubLineDesc === 'VICTORY' && 
                (objectSchema.EmployeePositionCode === 'SUP' || 
                objectSchema.EmployeePositionCode === 'KOORD')) {
                    return Math.round(objectSchema.totalIncentiveAT / objectSchema.countSubordinate);
                } else if (objectSchema.EmployeeSubLineDesc === 'NAVY' && 
                (objectSchema.EmployeePositionCode === 'SUP' || 
                objectSchema.EmployeePositionCode === 'KOORD')) {
                    if (bonus < criteria.Bonus) {
                        bonus = criteria.Bonus
                    } else {
                        return bonus;
                    }
                } else if (objectSchema.EmployeeSubLineDesc === 'VICTORY' && 
                objectSchema.EmployeePositionCode === 'DM') {
                    return Math.round((objectSchema.totalIncentiveAddTotal + 
                        objectSchema.totalIncentiveAT) / objectSchema.countSubordinate);
                }
            }
            case 28 : return objectSchema.actual * criteria.Bonus;
            case 3 : {

                if (objectSchema.LineDesc === 'HARMONY' && (objectSchema.EmployeePositionCode === 'SUP' || objectSchema.EmployeePositionCode === 'KOORD')) {
                    return  Math.round((objectSchema.totalBonusOwnProduct / objectSchema.jumlahMr) * criteria.Bonus);
                } else {
                    if (bonus < criteria.Bonus) {
                        bonus = criteria.Bonus
                    } else {
                       return  bonus;
                    }
                }
               
            }

            case 34 : {
                if (objectSchema.LineDesc === 'HARMONY' && (objectSchema.EmployeePositionCode === 'SUP' || objectSchema.EmployeePositionCode === 'KOORD')) {
                    return  Math.round(objectSchema.totalBonusAt / objectSchema.jumlahMr);
                } else {
                    if (bonus < criteria.Bonus) {
                        bonus = criteria.Bonus
                    } else {
                        return bonus;
                    }
                }  
                
            }

            case 24 : {
                if (objectSchema.EmployeePositionCode == 'DM') {
                    return  Math.round(objectSchema.totalIncentivePimda / objectSchema.jumlahPimda);
                } else {
                    if (bonus < criteria.Bonus) {
                        bonus = criteria.Bonus
                    } else {
                        return bonus;
                    }
                }
               
            }
            default : {

                if (bonus < criteria.Bonus) {
                    bonus = criteria.Bonus
                } else {
                    return bonus;
                }
            }
          }
      }
    }

    return bonus > 0 ? bonus : 0;
}

const validateTargetType = (TargetTypeH, objectSchema, operator, valueFrom, valueTo) => {    
    switch (TargetTypeH) {
        case "actual<prod" : {
            return objectSchema.actual < objectSchema.productivity;
        }
        case "actual>=prod" : {
            return objectSchema.actual >= objectSchema.productivity;
        }
        case "actual" : {
            return validatePassword(objectSchema.actual, operator, valueFrom, valueTo);
        }
        case "Productivitas" : {
            return validatePassword(objectSchema.productivity, operator, valueFrom, valueTo);
        }
        case 'actual_productivitas' : {
            return validatePassword(objectSchema.actualProductivity, operator, valueFrom, valueTo);
        }
        case 'actual_target' : {
            return validatePassword(objectSchema.actualTarget, operator, valueFrom, valueTo);
        }
        case 'actual_target_all' : {
            return validatePassword(objectSchema.actualTargetAll, operator, valueFrom, valueTo);
        }
        case 'actual_regular' : {
            return validatePassword(objectSchema.actualRegular, operator, valueFrom, valueTo);
        }
        case 'actual_target_regular' : {
            return validatePassword(objectSchema.actualTargetRegular, operator, valueFrom, valueTo);
        }
        case 'previous_incentive' : {
            return validatePassword(objectSchema.previousIncentive, operator, valueFrom, valueTo);
        }
        case 'actual_max' : {
            return validatePassword(objectSchema.actualTargetMax, operator, valueFrom, valueTo);
        }
        case 'actual_tum' : {
            return validatePassword(objectSchema.actualTarget, operator, valueFrom, valueTo);
        }
        case 'actual_navy' : {
            return validatePassword(objectSchema.actualTarget, operator, valueFrom, valueTo);
        }
        case 'growth_on_target' : {
            return validatePassword(objectSchema.GrowthOnTarget, operator, valueFrom, valueTo);
        }
        case 'total_incentive_pimda' : {
            return validatePassword(objectSchema.totalIncentivePimda, operator, valueFrom, valueTo);
        }
        case 'growth' : {
            return validatePassword(objectSchema.Growth, operator, valueFrom, valueTo);
        }
        case 'EmployeeSubLineDesc' : {
            return validatePassword(objectSchema.EmployeeSubLineDesc, operator, valueFrom, valueTo);
        }
        case 'categoryProduct' : {
            return validatePassword(objectSchema.categoryProduct, operator, valueFrom, valueTo);
        }
        case 'LineProduct' : {
            return validatePassword(objectSchema.keyGroup, operator, valueFrom, valueTo);
        }
        case 'ProductGroupDesc' : {
            return validatePassword(objectSchema.keyGroup, operator, valueFrom, valueTo);
        }
        case 'ChannelGroup' : {
            return validatePassword(objectSchema.keyGroup, operator, valueFrom, valueTo);
        }
        case 'EmployeeMRType' : {
            return validatePassword(objectSchema.EmployeeMRType, operator, valueFrom, valueTo);
        }
        case 'countSubordinate' : {
            return validatePassword(objectSchema.countSubordinate, operator, valueFrom, valueTo);
        }
        case 'Kriteria' : {
            return validatePassword(objectSchema.Kriteria, operator, valueFrom, valueTo);
        }
        default:
            break;
    }
    return true;
}

const getQuarterList = (month,years,numberPreviousQuarter) => {
    const quarterlist = [];
    var yearTemp = years;
    var currentQuarter =  (Math.floor(month / 3) * 3) - 3;
    for (var a=0; a < numberPreviousQuarter; a ++){

        var quarter = currentQuarter-=1;
        if( quarter<1){
            currentQuarter =4;
            quarter = currentQuarter;
            yearTemp -=1;
        }
        quarterlist.push(
            {
                quarter :quarter,
                years:yearTemp
            }
        )
    }

    return quarterlist;
}

const toCamelCase = (value) => {
    return value.toLowerCase()
        .replace(/\s(.)/g, function($1) { return $1.toUpperCase(); })
        .replace(/\s/g, '')
        .replace(/^(.)/, function($1) { return $1.toLowerCase(); });
}

const capitalizeFirstChar = (value) => {
    return value.charAt(0).toUpperCase() + value.slice(1);
}

module.exports = {
    convertArrayToNestedArray,
    reduceFilterTarikanList,
    convertNaNToZero,
    isExistEmployee,
    calculatePassword,
    calculateBonus,
    validateByTargetType,
    clasificationGroupList,
    toCamelCase,
    capitalizeFirstChar,
    createEmployeeFPPList
}
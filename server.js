const apis = require("./config/ApiConfig");
const PORT = 3310;

apis.app.listen(process.env.PORT || PORT, function () {
    console.log("server connected to port ", PORT);
});
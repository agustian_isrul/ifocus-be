var express = require("express");
var app = express();
var cors = require("cors");

app.use(cors());

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
// app.use(express.json({limit: '50mb'}));
// app.use(express.urlencoded({extended: true, limit: '50mb'}));

require("../backend/routes/authenticator")(app);
require("../backend/routes/dexa.api")(app);
require("../backend/routes/outlet.panel")(app);
require("../backend/routes/master.incentive")(app);
require("../backend/routes/incentive")(app);
require("../backend/routes/file.report")(app);
require("../backend/routes/summary")(app);
require("../backend/routes/maxima")(app);

var apiConfig = {
  app: app
}

module.exports = apiConfig;
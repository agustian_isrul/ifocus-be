module.exports = {
    development: {
        client: 'mysql',
        connection: {
            host: "localhost",
            port: 13306,
            user: "root",
            password: "root-user",
            database: "ifocus-app",
            timezone: 'UTC',
            dateStrings: false
        },
        pool: { min: 2, max: 10 },
        acquireConnectionTimeout: 10000,
        debug: true
    },
  
    test: {
        client: 'mysql',
        connection: {
            host: "localhost",
            port: 3306,
            user: "root",
            password: "",
            database: "ifocus-app",
            timezone: 'UTC',
            dateStrings: false
        },
        pool: { min: 2, max: 10 },
        acquireConnectionTimeout: 10000,
        debug: false
    },
  
    production: {
        client: 'mysql',
        connection: {
            host: "192.168.74.212",
            port: 3306,
            database: "ifocusdb",
            user: "ifocusaccess",
            password: 'WMzjHCNtGdvX8!PqDEqPnnD3Ketbbtu',
            con_limit: 100,
            queue_limit: 10,
            timezone: 'UTC',
            dateStrings: false
        },
        pool: { min: 2, max: 10 },
        acquireConnectionTimeout: 10000,
        debug: false
    }
  
  }
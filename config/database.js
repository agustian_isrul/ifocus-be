const Knex = require("knex")
const config = require("./knexfile")

let knex = null
if (process.env.NODE_ENV === "production") {
  knex = Knex(config.test)
} else {
  knex = Knex(config.development)
}

module.exports = knex
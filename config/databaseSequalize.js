var conf = require("../app-config.json");
var Sequelize = require('sequelize');
var db = new Sequelize(conf.app_db.database, conf.app_db.user, conf.app_db.password, {
	dialect: 'mysql',
	host: conf.app_db.host,
	port: conf.app_db.port,
	logging: false,
	pool: {
		max: 100,
		min: 2,
		idle: 6000,
        acquire: 1000000,
	}
});

module.exports = db;